<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>CEmailSender</name>
    <message>
        <source>Unable to connect to host: </source>
        <translation type="vanished">Unable to connect to server!</translation>
    </message>
    <message>
        <location filename="../src/settings/email/cemailsender.cpp" line="121"/>
        <source>Unable to connect to host! </source>
        <translation>Unable to connect to server!</translation>
    </message>
    <message>
        <location filename="../src/settings/email/cemailsender.cpp" line="127"/>
        <source>Bad authentication credentials!</source>
        <translation>Bad email credentials!</translation>
    </message>
    <message>
        <location filename="../src/settings/email/cemailsender.cpp" line="134"/>
        <source>Unable to send email! </source>
        <translation>The email address is not valid!</translation>
    </message>
    <message>
        <source>Unable to send email: </source>
        <translation type="vanished">The email address is not valid.</translation>
    </message>
</context>
<context>
    <name>EmailSettings</name>
    <message>
        <location filename="../src/settings/email/emailsettings.cpp" line="205"/>
        <source>Please check your email settings.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainForm.ui</name>
    <message>
        <location filename="../qml/MainForm.ui.qml" line="17"/>
        <source>Enter some text...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.ui" line="20"/>
        <location filename="../src/ui_mainwindow.h" line="65"/>
        <source>Eyewear Demo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="222"/>
        <source>Fit me
better</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="495"/>
        <source>Application is in PAUSED state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1002"/>
        <source>&amp;Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1006"/>
        <source>&amp;Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1009"/>
        <source>&amp;Logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1012"/>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1015"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegDialog</name>
    <message>
        <location filename="../src/regdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/regdialog.ui" line="39"/>
        <source>License credentials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/regdialog.ui" line="109"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/regdialog.ui" line="144"/>
        <location filename="../src/regdialog.ui" line="167"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/regdialog.ui" line="189"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/regdialog.ui" line="212"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Status: ...&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/regdialog.cpp" line="22"/>
        <source>Connecting ..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/regdialog.cpp" line="40"/>
        <location filename="../src/regdialog.cpp" line="47"/>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/regdialog.cpp" line="42"/>
        <source>Invalid license</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/regdialog.cpp" line="49"/>
        <source>Invalid credentials [%1]</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationView</name>
    <message>
        <location filename="../qml/RegistrationView.qml" line="28"/>
        <source>Error validating your license. Make sure you have a working internet connection!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RegistrationView.qml" line="31"/>
        <source>Wrong login and/or password. Please try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RegistrationView.qml" line="34"/>
        <source>Your license has expired. Please contact the provider!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RegistrationView.qml" line="40"/>
        <source>Invalid license. Please contact the provider!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RegistrationView.qml" line="43"/>
        <source>License validation is required before using the application!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RegistrationView.qml" line="114"/>
        <source>Login:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RegistrationView.qml" line="159"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RegistrationView.qml" line="217"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RegistrationView.qml" line="250"/>
        <source>Checking credentials...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RegistrationView.qml" line="258"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RegistrationView.qml" line="298"/>
        <source>Please wait. License check in progress...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/RegistrationView.qml" line="306"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>botLayout</name>
    <message>
        <location filename="../qml/botLayout.qml" line="244"/>
        <source>Skip</source>
        <translation>Skip</translation>
    </message>
    <message>
        <location filename="../qml/botLayout.qml" line="278"/>
        <source>Please come closer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/botLayout.qml" line="279"/>
        <source>You are too close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/botLayout.qml" line="287"/>
        <source>Loading : </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>botLayout_vertical</name>
    <message>
        <location filename="../qml/botLayout_vertical.qml" line="258"/>
        <source>Skip</source>
        <translation type="unfinished">Skip</translation>
    </message>
    <message>
        <location filename="../qml/botLayout_vertical.qml" line="292"/>
        <source>Please come closer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/botLayout_vertical.qml" line="293"/>
        <source>You are too close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/botLayout_vertical.qml" line="301"/>
        <source>Loading : </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>botRightLayout_vertical</name>
    <message>
        <location filename="../qml/botRightLayout_vertical.qml" line="26"/>
        <source>Strike a pose !</source>
        <translation type="unfinished">Strike a pose !</translation>
    </message>
</context>
<context>
    <name>captureLayout</name>
    <message>
        <location filename="../qml/captureLayout.qml" line="56"/>
        <source>Continue on your mobile device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/captureLayout.qml" line="73"/>
        <location filename="../qml/captureLayout.qml" line="202"/>
        <source>Start Again</source>
        <translation>Start Again</translation>
    </message>
    <message>
        <location filename="../qml/captureLayout.qml" line="110"/>
        <source>Share</source>
        <translation>Share</translation>
    </message>
    <message>
        <location filename="../qml/captureLayout.qml" line="194"/>
        <source>The email address is not valid.</source>
        <translation>The email address is not valid.</translation>
    </message>
    <message>
        <location filename="../qml/captureLayout.qml" line="238"/>
        <source>Send It</source>
        <translation>Send It</translation>
    </message>
    <message>
        <location filename="../qml/captureLayout.qml" line="302"/>
        <source>Sent</source>
        <translation>Sent</translation>
    </message>
</context>
<context>
    <name>captureLayout_vertical</name>
    <message>
        <location filename="../qml/captureLayout_vertical.qml" line="71"/>
        <location filename="../qml/captureLayout_vertical.qml" line="202"/>
        <source>Start Again</source>
        <translation type="unfinished">Start Again</translation>
    </message>
    <message>
        <location filename="../qml/captureLayout_vertical.qml" line="106"/>
        <source>Share</source>
        <translation type="unfinished">Share</translation>
    </message>
    <message>
        <location filename="../qml/captureLayout_vertical.qml" line="194"/>
        <source>The email address is not valid.</source>
        <translation type="unfinished">The email address is not valid.</translation>
    </message>
    <message>
        <location filename="../qml/captureLayout_vertical.qml" line="237"/>
        <source>Send It</source>
        <translation type="unfinished">Send It</translation>
    </message>
    <message>
        <location filename="../qml/captureLayout_vertical.qml" line="300"/>
        <source>Sent</source>
        <translation type="unfinished">Sent</translation>
    </message>
</context>
<context>
    <name>cconfig</name>
    <message>
        <location filename="../qml/cconfig.qml" line="29"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cconfig.qml" line="41"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cconfig.qml" line="177"/>
        <source>Add new setting</source>
        <translation type="unfinished">Add new parameter</translation>
    </message>
    <message>
        <location filename="../qml/cconfig.qml" line="245"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>leftLayout</name>
    <message>
        <location filename="../qml/leftLayout.qml" line="50"/>
        <source>You are?</source>
        <translation>You are?</translation>
    </message>
    <message>
        <location filename="../qml/leftLayout.qml" line="146"/>
        <source>Eyeglasses or Sunglasses ?</source>
        <translation>Eyeglasses or Sunglasses ?</translation>
    </message>
</context>
<context>
    <name>rightLayout</name>
    <message>
        <location filename="../qml/rightLayout.qml" line="132"/>
        <source>Fit me
better</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/rightLayout.qml" line="177"/>
        <source>Strike a pose !</source>
        <translation>Strike a pose !</translation>
    </message>
</context>
<context>
    <name>rightLayout_vertical</name>
    <message>
        <location filename="../qml/rightLayout_vertical.qml" line="119"/>
        <source>You are?</source>
        <translation type="unfinished">You are?</translation>
    </message>
    <message>
        <location filename="../qml/rightLayout_vertical.qml" line="214"/>
        <source>Eyeglasses or Sunglasses ?</source>
        <translation type="unfinished">Eyeglasses or Sunglasses ?</translation>
    </message>
    <message>
        <location filename="../qml/rightLayout_vertical.qml" line="313"/>
        <source>Fit me
better</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>xml_context</name>
    <message>
        <source>D=?UTF-8?B?w6k=?=couvrez votre photo Acep.</source>
        <translation type="obsolete">Discover your Acep photo.</translation>
    </message>
    <message>
        <source>&amp;lt;html&gt;&amp;lt;body&gt;Hello,&amp;lt;br&gt; You have tested our digital virtual Try-On experience and we thank you for that.&amp;lt;br&gt;Please contact us at info@opticvideo.com we will be delighted to present our solutions and answer your questions. &amp;amp;#x3A; &amp;lt;br&gt; [split] &amp;lt;br&gt; Acep wish you an excellent fair. &amp;lt;br&gt; &amp;lt;br&gt; See you soon.&amp;lt;br&gt;The Acep team.</source>
        <translation type="obsolete">&amp;lt;html&gt;&amp;lt;body&gt;Hello,&amp;lt;br&gt; You have tested our digital virtual Try-On experience and we thank you for that.&amp;lt;br&gt;Please contact us at info@opticvideo.com we will be delighted to present our solutions and answer your questions. &amp;lt;br&gt; [split] &amp;lt;br&gt; Acep wish you an excellent fair. &amp;lt;br&gt; &amp;lt;br&gt; See you soon.&amp;lt;br&gt;The Acep team.</translation>
    </message>
    <message>
        <location filename="../Config/email_config.xml" line="14"/>
        <source>&amp;lt;html&gt;&amp;lt;body&gt;Hello,&amp;lt;br&gt; You have tested our digital virtual Try-On experience and we thank you for that.&amp;lt;br&gt;Please contact us at info@opticvideo.com we will be delighted to present our solutions and answer your questions. &amp;lt;br&gt; [split] &amp;lt;br&gt; Acep wish you an excellent fair. &amp;lt;br&gt; &amp;lt;br&gt; See you soon.&amp;lt;br&gt;The Acep team.</source>
        <translation>EEEEEEEEEEEEEEEEe&amp;lt;html&gt;&amp;lt;body&gt;Hello,&amp;lt;br&gt; You have tested our digital virtual Try-On experience and we thank you for that.&amp;lt;br&gt;Please contact us at info@opticvideo.com we will be delighted to present our solutions and answer your questions. &amp;lt;br&gt; [split] &amp;lt;br&gt; Acep wish you an excellent fair. &amp;lt;br&gt; &amp;lt;br&gt; See you soon.&amp;lt;br&gt;The Acep team.!!!!!!!!!!!!!!</translation>
    </message>
    <message>
        <location filename="../Config/email_config.xml" line="15"/>
        <source>Discover your Acep photo.</source>
        <translation>Discover your Acep photo.</translation>
    </message>
</context>
</TS>
