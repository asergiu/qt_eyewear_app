#ifndef OFFSCREENWIDGET_H
#define OFFSCREENWIDGET_H

#include <QWidget>
#include <QPaintEvent>
#include <QQuickWidget>

class QImage;

class TryLive;

class OffscreenWidget : public QWidget
{
public:
    OffscreenWidget(QWidget * parent);
    ~OffscreenWidget();

    // ALWAYS call setContextProperty before setSource
    void setContextProperty(const QString &name, QObject *value);
    void setSource(const QString &name);

    void updateSize(int width, int height);
    void render(TryLive & trylive, unsigned int width, unsigned int height);

    // set visibility of quick widget independent of trylive render window
    void setQuickwidgetVisible(bool visible);

protected:
    void paintEvent(QPaintEvent * event);

private:
    unsigned char * mBuffer;
    QImage * mImage;
    struct {unsigned int width, height;} mSize;

    QQuickWidget *m_renderQuickWidget;

    void clear();
};

#endif
