#if !defined(TRYLIVE_H)
#define TRYLIVE_H

#if defined(TRYLIVE_STATIC)
	#define TRYLIVE_API
#else
	#if defined(TRYLIVE_BUILD)
		#define TRYLIVE_API __declspec(dllexport)
	#else
		#define TRYLIVE_API __declspec(dllimport)
	#endif
#endif

#include <Windows.h>

#include <string>
#include <vector>
#include <map>
#include <functional>
//#if defined(_MSC_VER) && (_MSC_VER > 1910)
//#include <any>
//#endif

class TRYLIVE_API TryLive
{
public :
	class TRYLIVE_API ProductProperty
	{
		public :
		static std::string const & id;				/*!< Product internal identifier */
		static std::string const & type;			/*!< Product type */
		static std::string const & isAccessory;		/*!< Is product an accessory ? */
		static std::string const & externalId;		/*!< External product identifier */
		static std::string const & model;			/*!< Type of 3D model */
		static std::string const & reference;		/*!< Product reference */
		static std::string const & color;			/*!< Color */
		static std::string const & title;			/*!< Title */
		static std::string const & description;		/*!< Description */
		static std::string const & brand;			/*!< Brand */
		static std::string const & marketingName;	/*!< Marketing name */
		static std::string const & gender;			/*!< Gender */
		static std::string const & opticalSolar;	/*!< Optical / Solar */
		static std::string const & series;			/*!< Series */
		static std::string const & thumbnailURL;	/*!< Thumbnail URL */
	};

	class TRYLIVE_API Listener
	{
		public :
		static std::string const & tryliveStatusChanged;				/*!< Paremeters:\n TryLive status (\ref TryLiveStatus) */
		static std::string const & productStatusChanged;				/*!< Paremeters:\n product name,\n product status */
		static std::string const & assetStatusChanged;					/*!< Paremeters:\n asset status (\ref ProductStatus), asset name */
		static std::string const & assetSelectStatusChanged;			/*!< Paremeters:\n asset name,\n status (\ref ProductStatus) */
		static std::string const & trackingStatusChanged;				/*!< Paremeters:\n tracking status (\ref TrackingStatus)*/
		static std::string const & manualTransformationChanged;			/*!< Paremeters:\n manual transformation status */
		static std::string const & takePhotoEnded;						/*!< Paremeters:\n photo id,\n file name,\n photo information JSON formatted */
		static std::string const & takeScreenshotEnded;					/*!< Paremeters:\n file name */
		static std::string const & receivedArmOrientation;				/*!< Paremeters:\n asset name,\n arm orientation / Ox, arm orientation / Oy, arm orientation / Oz,\n errror */
		static std::string const & receivedReferenceEarPoints;			/*!< Paremeters:\n left point x, left point y, left point z,\n right point x, right point y, right point z */
		static std::string const & selectedViewportChanged;				/*!< Paremeters:\n viewport,\n x, y,\n width, height */
		static std::string const & receivedViewModes;					/*!< Paremeters:\n nb modes,\n string listing all available view modes */
		static std::string const & receivedProductOffset;				/*!< Paremeters:\n offset x, offset y, offset z,\n orientation / Ox, orientation / Oy, orientation / Oz,\n scale x, scale y, scale z */
		static std::string const & receivedFacePoints;					/*!< Paremeters:\n nb,\n string containing the points */
		static std::string const & receivedManualTransformationStatus;	/*!< Paremeters:\n string containing for each mode its status */
		static std::string const & receivedPhotoInfo;					/*!< Paremeters:\n photo id,\n filename,\n photo information JSON formatted */
		static std::string const & receivedProductFittingHeight;		/*!< Paremeters:\n fitting height */
		static std::string const & errorStatusChanged;					/*!< Paremeters:\n error string */
	};

	class TRYLIVE_API TryLiveStatus
	{
		public :
		static std::string const & commandReady;
		static std::string const & ready;
		static std::string const & paused;
		static std::string const & resumed;
	};

	class TRYLIVE_API ProductStatus
	{
		public :
		static std::string const & error;
		static std::string const & loading;
		static std::string const & loaded;
		static std::string const & downloading;
		static std::string const & downloaded;
		static std::string const & unloading;
		static std::string const & unloaded;
	};

	class TRYLIVE_API TrackingStatus
	{
		public :
		static std::string const & tracked;
		static std::string const & notTracked;
		static std::string const & wait;
		static std::string const & tooClose;
		static std::string const & tooFar;
		static std::string const & notCentered;
	};

	class TRYLIVE_API ManualTransformation
	{
		public :
		static std::string const & none;			/** No manual transformation */
		static std::string const & translateXY;		/** Manual translation in the XY plane */
		static std::string const & translateYZ;		/** Manual translation in the YZ plane */	
		static std::string const & translateXZ;		/** Manual translation in the XZ plane */	
		static std::string const & targetRotate;	/** Manual rotation using eye and ear targets */		
		static std::string const & targetScale;		/** Manual rotation using eyes targets */	
		static std::string const & viewport;		/** Manual viewport modification */		
	};

	typedef std::vector<std::string> IdList;
	typedef std::vector<std::string> AssetList;
	typedef std::map<std::string, std::string> PropertyMap;
	typedef std::function<void (std::vector<std::string> const &)> Callback;

	TryLive();
	~TryLive();

	/**
	*	@brief Connect to Saas using JSON configuration file
	*	@param config JSON formated configuration file path
	*	@param view window in which render occurs
	*	@return true if success, false otherwise
	*/
	bool initialize(std::string const & config, void * view);

	/**
	*	@brief Connect to Saas using json configuration file using offscreen rendering
	*	@param config json formated string configuration
	*	@param width width of the rendertarget
	*	@param width hright of the rendertarget
	*	@return true if success, false otherwise
	*/
	bool initialize(std::string const & config, unsigned int width, unsigned int height);

	/**
	*	@brief Close TryLive
	*/
	void shutdown();

	/**
	*	@brief Render one frame
	*/
	void render();

	/**
	*	@brief Resize render view
	*	@param width width
	*	@param height height
	*/
	void resize(unsigned int width, unsigned int height);

	/**
	*	@brief Resize render window using offscreen rendering
	*	@param width width of the rendertarget
	*	@param height height of the rendertarget
	*/
	void resizeWindow(unsigned int width, unsigned int height);

	/**
	*	@brief Play TryLive engine
	*	@brief <br>Once launched, calls the callback TryLive::Listener::tryliveStatusChanged with parameter TryLive::TryLiveStatus::resumed.
	*	@brief <br>The main usage is to resume the TryLive processing after a pause. 
	*	@brief <br>Before any calls to TryLive features, the application must wait for the callback TryLive::Listener::tryliveStatusChanged with parameter TryLive::TryLiveStatus::paused. 
	*/
	void play();

	/**
	*	@brief Pause TryLive engine
	*	@brief <br>The main usage is to stop temporarily TryLive processing to allow the application to use resources like live video capture or OpenGL rendering. 
	*	@brief <br>Before any usage of these resources, the application must wait for the callback TryLive::Listener::tryliveStatusChanged with parameter TryLive::TryLiveStatus::paused. 
	*/
	void pause();

	/**
	*	@brief Pause video and tracking
	*	@param camera camera id
	*/
	void pauseTrackingAndVideo(std::string const & camera);

	/**
	*	@brief Resume video and tracking
	*	@param camera camera id
	*/
	void resumeTrackingAndVideo(std::string const & camera);

	/**
	*	@brief Restart tracking
	*	@param camera Specifies the name of the camera associated to the tracking to restart
	*/
	void restartTracking(std::string const & camera);

	/**
	*	@brief Defines the zone where the tracking can start
	*	@param sizeX Horizontal size (full video width is 1)
	*	@param sizeY Vertical size (full video height is 1)
	*	@param centerX Horizontal position (0 is centered)
	*	@param centerY Vertical position (0 is centered)
	*	@param minZ Minimum distance (in mm)
	*	@param maxZ Maximum distance (in mm)
	*/
	void TryLive::setTrackingConstraints(float sizeX, float sizeY, float centerX, float centerY, float minZ, float maxZ);

	/**
	*	@brief Set pupillary distance
	*	@brief The pupillary distance has a strong impact on the scaling of glasses, because pupils are the only geometric reference found in the real scene that can be linked to a metric value: the pupillary distance.
	*   @param distance pupullary distance in mm
	*/
	void TryLive::setPupilDistance(float distance);

	/**
	*	@brief Loads a catalog
	*	@param data json formated string catalog
	*	@param clear if true, previous catalog is cleared before loading the new catalog
	*	@return true if success, false otherwise
	*/
	bool loadCatalog(std::string const & data, bool clear = true);

	/**
	*	@brief Retrieve the ids of products in the catalog
	*/
	IdList getProductIds() const;

	/**
	*	@brief Retrieve the properties of a product given its id
	*   @param id product id
	*   @return properties
	*/
	PropertyMap const & getProductProperties(std::string const & id) const;

	/**
	*	@brief Retrieve the assets of a product given its id
	*   @param id product id
	*   @return assets
	*/
	AssetList const & getProductAssets(std::string const & id) const;

	/**
	*	@brief Returns the id of the product which matches the default product in dataRecorderConfig
	*   @return product id or empty string
	*/
	std::string const & getDefaultProduct() const;

	/**
	*	@brief Download product
	*	@brief <br>During the processing, it calls the callback TryLive::Listener::productStatusChanged with a parameter indicating the current status in: TryLive::ProductStatus::downloading, TryLive::ProductStatus::downloaded, TryLive::ProductStatus::error.
	*	@brief <br>The goal of this method is to download the products and populate the application cache. It can typically be used at the start of the application in order to avoid the download of the product when the user wants to try it and then optimize the application latency.
	*   @param id product id
	*/
	void downloadProduct(std::string const & id) const;

	/**
	*	@brief Load product in order to vizualize it in the specified viewport or current viewport if not specified
	*	@brief <br>During the processing, it calls the callback TryLive::Listener::productStatusChanged with a parameter indicating the current status in: TryLive::ProductStatus::loading, TryLive::ProductStatus::loaded, TryLive::ProductStatus::error.
	*	@brief <br>If the product was not yet doanloaded it is downloaded.
	*   @param id product id
	*   @param viewport viewport id
	*   @return instance name of the product that will be used in the commnds dealing with loaded products
	*/
	// std::string setProduct(std::string const & id, std::string const & viewport = EMPTY);
	std::string setProduct(std::string const & id, std::string const & viewport = "");

	/**
	*	@brief Unset product
	*   @param name instance name
	*/
	void unsetProduct(std::string const & name);

	/**
	*	@brief Unset all products
	*/
	void unsetAllProducts();

	/**
	*	@brief Get available view modes
	*	@brief <br>The view modes are returned via the callback	TryLive::Listener::receivedViewModes with as parameters, the number of view modes, and a string containing all the view modes id separated by ";".
	*/
	void getViewModes() const;

	/**
	*	@brief Set the current view mode
	*   @param mode view mode id
	*/
	void setViewMode(std::string const & mode);

	/**
	*	@brief Get current view mode
	*/
	std::string const & getViewMode() const;

	/**
	*	@brief Take screenshot of the TryLive view
	*	@brief <br>It ends with the callback TryLive::Listener::takeScreenshotEnded with as parameter the path to the taken screenshot.
	*/
	void takeScreenshot() const;

	/** 
	*	@brief Take a photo (without AR scene) in order to be used in still image try ons
	*	Finishes with callback TryLive::Listener::takePhotoEnded:
	*
	*	- arg1: The photo identifier that could be used in the other photo methods
	*	- arg2: The path to the photo file
	*	- arg3: A JSON string containing the photo properties. This string is not supposed to interpreted or modified, it's only purpose is to let the application saving the photo informations for a future use
	*
	*	@param camera Name of the camera
	*	@param id Identifier of the photo in the TryLive player database
	*	@param path Path to the storage location of the photo (If empty string, TryLive choose the filename)
	*/
	void takePhoto(std::string const & camera, std::string const & id, std::string const & path) const;

	/**
	*	@brief Load photo for try on with glasses
	*
	*	The photo is supposed to have been taken with the takePhoto method.
	*
	*	 @param camera The name of the camera (the type of this camera must be "photo")
	*	 @param id Identifier of the photo
	*	 @param	path Path to the photo file
	*	 @param	json JSON string containing the photo information
	*/
	void setPhoto(std::string const & camera, std::string const & id, std::string const & path, std::string const & json) const;

	/**
	*	@brief Get information about a photo
	*
	*	The photo is supposed to be already loaded in the memory, either via takePhoto or setPhoto methods.
	*
	*	This method can be called when we want to save the photo information and some TryLive internal features could have modify the photo informations (for instance, manual interactions).
	*	The information are returned via the callback TryLive::Listener::receivedPhotoInfo with as parameters, the photo id, the photo filename and a JSON string containing various information associated with the photo. This JSON string can be stored by the application to be reused in a new session.
	*
	*	@param id Identifier of the photo
	*/
	void getPhotoInfo(std::string const & id) const;

	/**
	*	@brief Enable the interactive manual transformation of product poses
	*	@param mode The type of interaction (see TryLive::ManualTransformation)
	*	@param	status To either activate or deactivate
	*	@warning Only one mode can be activated at the same time
	*/
	void enableManualTransformation(std::string const & mode, bool status) const;

	/**
	*	@brief Get the current manual transformation of the product
	*
	*	Return the status of each manual transformation via the callback TryLive::Listener::receivedManualTransformationStatus:
	*
	*	- arg1: "mode1;true|false;mode2:true|false...."
	*/
	void getManualTransformationStatus() const;

	/**
	*	@brief Request position, orientation and scale offset of the product
	*
	*	Return values via callback TryLive::Listener::receivedProductOffset:
	*
	*	- arg1: "px;py;pz" The translation
	*	- arg2: "ox;oy;oz" The rotation
	*	- arg3: "sx;sy;sz" The scale
	*/
	void getProductOffset() const;

	/**
	*	Add an offset to the products' pose
	*	@param camera name of the camera that controls basic pose of the products 
	*	@param px x translation 
	*	@param py y translation
	*	@param pz z translation
	*	@param ox x rotation
	*	@param oy y rotation
	*	@param oz z rotation
	*	@param sx x scale
	*	@param sy y scale
	*	@param sz z scale
	*/
	void setProductOffset(std::string const& camera, float px, float py, float pz, float ox, float oy, float oz, float sx, float sy, float sz) const;

	/**
	*	@brief Return the fitting height of the product
	*
	*	@param product Product name
	*	@param camera Camera, if camera not specified, the fitting heught returned is not related to pupils but to 3D product origin
	*	Return value in mm via callback TryLive::Listener::receivedProductFittingHeight
	*
	*/
	void TryLive::getProductFittingHeight(std::string product, std::string camera = 0) const;

	/**
	*	@brief Send a log to the DataRecorder
		@param tag name of the log
		@param value value
	*/
//#if defined(_MSC_VER) && (_MSC_VER > 1910)
//	void sendLog(std::string const & tag, std::any const & value) const {}
//#else
 	void sendLog(std::string const & tag, std::string const & value) const {}
 	//void sendLog(std::string const & tag, int                 value) const;
 	//void sendLog(std::string const & tag, float               value) const;
 	//void sendLog(std::string const & tag, bool                value) const;
//#endif

	// TODO enableManualTransformation
	// TODO getManualTransformationStatus

	/**
	*	@brief Set the debug level
	*	@param level Level of debugging
	* 
	*	- 0 : Time measurement (startChrono, stopChrono)
	*	- 1+ : warning
	*	- 2+ : info (default value)
	*	- 3+ : all
	*
	*	Whatever the level, the errors are always logged.
	*/
	void setDebugLevel(int level) const;

	/**
	*	@brief Set the generic callback that will handle all the TryLive assynchronous responses
	*	@param cb Callback callable object
	*/
	void setCallback(Callback cb);

	/**
	*	@brief Execute Lua code
	*   @param cmd Lua code
	*   @param id id (optional)
	*/
	// void call(std::string const & cmd, std::string const & id = EMPTY) const;
	void call(std::string const & cmd, std::string const & id = "") const;

	/**
	*	@brief Execute Lua file
	*   @param name Lua file name
	*   @param id id (optional)
	*/
	void loadFile(std::string const & name, std::string const & id = "") const;

	static void log(const char* iLog, ...);

	bool getPixels(void * buffer);

private :

	static void scallback(std::vector<std::string> const & parameters, void * data);
	void callback(std::vector<std::string> const & parameters);

	void * mCtx;
};

#endif
