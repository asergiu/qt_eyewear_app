log_loaded_info("tiNetwork.lua")

tiNetwork = {
	uploadUrl = "",
	componentInterface = getWebInterface()
}

function tiNetwork.setUploadUrl(url)
	tiNetwork.uploadUrl = url
end

-- Deprecated.
function tiNetwork.upload(path, params, typeFile)
	local id = tiNetwork.componentInterface:startUploadFileAsMultipartFormData (tiNetwork.uploadUrl, path, params , typeFile)
	local finish = false
	repeat
		local status = tiNetwork.componentInterface:getPostDataStatus(id)
		if status==TI_DLFINISHED then
			new_url = tiNetwork.componentInterface:releasePostData(id)
			coroutine.yield()
			finish = true
		elseif status==TI_DLFAILED or status==TI_CANCELED then
			coroutine.yield()
			dispatchError(TI_UPLOAD_FAILED, "Unable to upload the photo.")
			finish = true
		else
			coroutine.yield()
		end
	until not finish
	
	return status, new_url
end

-------------------------------------------------------------------------------
-- Download a content of a directory.
-------------------------------------------------------------------------------
function tiNetwork.download (remotePath, localPath)

log_call ("tiNetwork.download remotePath: " .. remotePath .. " localPath: " .. localPath)
	local bSuccess = false
	local bFinish = false

	--Log the download (remote and local path)
	log_info("tiNetwork.download download started for remote: " .. remotePath..", local: " .. localPath)
	
	if isLocalPath(remotePath) then 
		log_debug("simulate download finish with success for local file")
		return true
	end
	
	-- See comments in resolvePath function
	remotePath = resolvePath(remotePath)

	-- Start the download and do a loop for retrieve the download status.
	-- Check this status to know the result of a download.
log_debug ("tiNetwork.download startRemoteUpdate remotePath: " .. remotePath .. " localPath: " .. localPath)
	local download = startRemoteUpdate(remotePath, localPath)	

	if not (download < 0) then
		repeat
			status = getRemoteUpdateStatus (download)
			if status == TI_DLFINISHED then 
				bFinish = true
				bSuccess = true
--				log_info("download finish with success")
					
			elseif (status == TI_DLFAILED) or (status == TI_DLCANCELED) then
				log_error ("Download Failed")
				bFinish = true
				bSuccess = false
			end
			coroutine.yield()	
		until bFinish
	
	else
		bSuccess = false
		log_error ("Download not begin, failed" .. download)
	end
	
	-- Need to explicite stop the download.
	if not stopRemoteUpdate (download) then
		log_error("Problem during start remote update")
	end
log_call_end()
	return bSuccess
end

-------------------------------------------------------------------------------
-- Same at start download but for download a text. 
-- This text is directly set in a string with not use a temporary file.
-------------------------------------------------------------------------------
function tiNetwork.downloadText(remotePath)

	local bSuccess = false
	local bFinish = false

	log_info("download  started for text for remote = " .. remotePath )
	
	remotePath = resolvePath(remotePath)
	
	local download = startTextDownload(remotePath)
	if not (download < 0) then
		repeat
			status = getTextDownloadStatus(download)
			if status==TI_DLFINISHED then 
				bFinish = true
				bSuccess = true
				log_info("download  finish with success")
			
					
			elseif status==TI_DLFAILED or status==TI_DLCANCELED then
				log_error("Download Failed")
				bFinish = true
				bSuccess = false
			end
			coroutine.yield()	
		until bFinish
	
	else
		bSuccess = false
		log_error("Download not begin, failed" .. download)
	end
	local textFile = getDownloadedText(download)

	return bSuccess, textFile
end


function isLocalPath(path)

	if string.sub(path,1,string.len("http:"))=="http:" then
		return false
	end
		
	if string.sub(path,1,string.len("https:"))=="https:" then
		return false
	end
	
	return true

end

-------------------------------------------------------------------------------
-- This function resolve the ".." and "." exist in a url.
-- A bug on some targets generates an error when a path contain ".." or "." for
-- download.
-- We need to resolve it before ask a download.
-------------------------------------------------------------------------------
function resolvePath(path)
	if ((string.find(path, "../", 1, true) == nil) and (string.find(path, "./", 1, true) == nil)) then
		return path
	end
	
	local t = {}
	local str = nil;
	local i = 1
	
	for str in string.gmatch(path, "([^/]+)") do
		if ((str == "http:") or (str == "https:")) then
			table.insert(t, str .. "//")
		elseif (str == "..") then
			table.remove(t);
		elseif (str ~= ".") then
			table.insert(t, str .. "/")
		end
	end
	
	path = ""
	for i = 1, #t do
		path = path .. t[i]
	end
	
	return path
end
