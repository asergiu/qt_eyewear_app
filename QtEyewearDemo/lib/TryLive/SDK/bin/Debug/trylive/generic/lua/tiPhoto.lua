log_loaded_info("tiPhoto.lua")

tiPhoto = {
	debugLevel = 5,
	nbFree = 0
}

tiPhoto.forceFile = false

-- En fait ce n'est pas TI_OS_IPHONEOS le probleme mais OpenGLES2
if getOSType() == TI_OS_IPHONEOS then
--	tiPhoto.forceFile = true
end

function tiPhoto:create()
    local new_inst = {}    -- the new instance
    setmetatable( new_inst, { __index = tiPhoto } ) -- all instances share the same metatable
	new_inst:init()
    return new_inst
end

function tiPhoto:init()
	log_debug("tiPhoto:init "..tostring(self))
	self.mFileName = ""
	self.mId = ""
	self.mCameraFOVy = 1.0
	self.mCameraRatio = 1.0
	self.mPoseValide = false
	self.mPosition = Vector3()
	self.mOrientation = Quaternion()
	self.mUserPD = 66.6666
	self.mIrisRightPos = Vector3()
	self.mIrisLeftPos = Vector3()
	self.mDistanceAdjustment = Vector3()
	self.mManualMovePosition = Vector3()
	self.mManualMoveOrientation = Vector3()
	self.mReferenceEarPointSet = false
	self.mPosLeftReferenceEarPoint = Vector3()
	self.mPosRightReferenceEarPoint = Vector3()
	self.mTimeCode = -1
	self.mFrameIndex = -1
	self.mImage = nil
	
	-- On pourrait aussi y mettre le scale camera et trans camera (qd ca existera) ...

end

function tiPhoto:free()
	self.mImage = nil
	tiPhoto.nbFree = tiPhoto.nbFree+1
	if tiPhoto.nbFree > 10 then
		collectgarbage()
		tiPhoto.nbFree = 0
	end
end

function tiPhoto:printLog(titre, debugLevel)
	if titre == nil then
		titre = ""
	end
	log_debug("Photo: "..titre.." "..tostring(self), debugLevel)
	log_debug("Id: " ..self.mId, debugLevel)
	log_debug("File: " ..self.mFileName, debugLevel)
	log_debug("Camera: FOVy: "..math.deg(self.mCameraFOVy).." Ratio: "..self.mCameraRatio, debugLevel)
	log_debug("Pose: "..tostring(self.mPoseValide), debugLevel)
	log_debug("Position: "..self.mPosition:getX()..", "..self.mPosition:getY()..", "..self.mPosition:getZ(), debugLevel)
	log_debug("Orientation: "..self.mOrientation:getX()..", "..self.mOrientation:getY()..", "..self.mOrientation:getZ()..", "..self.mOrientation:getW(), debugLevel)
	log_debug("PD: "..self.mUserPD, debugLevel)
	log_debug("Iris R: "..self.mIrisRightPos:getX()..", "..self.mIrisRightPos:getY()..", "..self.mIrisRightPos:getZ().." L: "..self.mIrisLeftPos:getX()..", "..self.mIrisLeftPos:getY()..", "..self.mIrisLeftPos:getZ(), debugLevel)
	log_debug("Dist adjust: "..self.mDistanceAdjustment:getX()..", "..self.mDistanceAdjustment:getY()..", "..self.mDistanceAdjustment:getZ(), debugLevel)
	log_debug("Manual Move Position: "..self.mManualMovePosition:getX()..", "..self.mManualMovePosition:getY()..", "..self.mManualMovePosition:getZ(), debugLevel)
	log_debug("Manual Move Orientation: "..self.mManualMoveOrientation:getX()..", "..self.mManualMoveOrientation:getY()..", "..self.mManualMoveOrientation:getZ(), debugLevel)
	log_debug("Reference ear points: "..tostring(self.mReferenceEarPointSet), debugLevel)
	if self.mReferenceEarPointSet then
		log_debug("Ear ref point R: "..self.mPosRightReferenceEarPoint:getX()..", "..self.mPosRightReferenceEarPoint:getY()..", "..self.mPosRightReferenceEarPoint:getZ().." L: "..self.mPosLeftReferenceEarPoint:getX()..", "..self.mPosLeftReferenceEarPoint:getY()..", "..self.mPosLeftReferenceEarPoint:getZ(), debugLevel)
	end
	log_debug("TimeCode: " ..self.mTimeCode, debugLevel)
	log_debug("FrameIndex: " ..self.mFrameIndex, debugLevel)
end

function tiPhoto:toJSON()
	local res = "{"
	log_debug("tiPhoto:toJSON")
self:printLog("tiPhoto:toJSON", tiPhoto.debugLevel)
	
	-- replace \ by /
	local tempFilename = self.mFileName
	tempFilename=string.gsub(tempFilename, "\\", "/")
			
	res = res.."\"filename\": \"" ..tempFilename ..  "\","  
	res = res.."\"id\": \""..self.mId.."\","
	res = res.."\"time\": "..self.mTimeCode..","
	res = res.."\"camera fov\": "..self.mCameraFOVy..","
	res = res.."\"camera ratio\": "..self.mCameraRatio..","
	res = res.."\"pose valide\": "..tostring(self.mPoseValide)..","
	res = res.."\"position\": ["..self.mPosition:getX()..","..self.mPosition:getY()..","..self.mPosition:getZ().."],"
	res = res.."\"orientation\": ["..self.mOrientation:getX()..","..self.mOrientation:getY()..","..self.mOrientation:getZ()..","..self.mOrientation:getW().."],"
	res = res.."\"PD\": "..self.mUserPD..","
	res = res.."\"right iris position\": ["..self.mIrisRightPos:getX()..","..self.mIrisRightPos:getY()..","..self.mIrisRightPos:getZ().."],"
	res = res.."\"left iris position\": ["..self.mIrisLeftPos:getX()..","..self.mIrisLeftPos:getY()..","..self.mIrisLeftPos:getZ().."],"
	res = res.."\"adjustment distance\": ["..self.mDistanceAdjustment:getX()..","..self.mDistanceAdjustment:getY()..","..self.mDistanceAdjustment:getZ().."],"
	res = res.."\"reference ear point\": "..tostring(self.mReferenceEarPointSet)..","
	if self.mReferenceEarPointSet then
		res = res.."\"left reference ear point\": ["..self.mPosLeftReferenceEarPoint:getX()..","..self.mPosLeftReferenceEarPoint:getY()..","..self.mPosLeftReferenceEarPoint:getZ().."],"
		res = res.."\"right reference ear point\": ["..self.mPosRightReferenceEarPoint:getX()..","..self.mPosRightReferenceEarPoint:getY()..","..self.mPosRightReferenceEarPoint:getZ().."],"
	end
	res = res.."\"manual position\": ["..self.mManualMovePosition:getX()..","..self.mManualMovePosition:getY()..","..self.mManualMovePosition:getZ().."],"
	res = res.."\"manual orientation\": ["..self.mManualMoveOrientation:getX()..","..self.mManualMoveOrientation:getY()..","..self.mManualMoveOrientation:getZ().."]"
	res = res.."}"
log_debug ("tiPhoto:toJSON "..res, tiPhoto.debugLevel)
	return res
end

function tiPhoto:fromTable (jsonLocation)
	log_debug("tiPhoto:fromTable "..tostring(jsonLocation))
	self.mFileName = jsonLocation["filename"]
	self.mId = jsonLocation["id"]
	self.mCameraFOVy = jsonLocation["camera fov"]
	self.mCameraRatio = jsonLocation["camera ratio"]
	self.mPoseValide = jsonLocation["pose valide"]
	if self.mPoseValide then
		self.mPosition = Vector3 (tonumber(jsonLocation["position"][1]), tonumber(jsonLocation["position"][2]), tonumber(jsonLocation["position"][3]))
		if #(jsonLocation["orientation"]) == 4 then
			self.mOrientation = Quaternion (jsonLocation["orientation"][1], jsonLocation["orientation"][2], jsonLocation["orientation"][3], jsonLocation["orientation"][4])
		else
			euler2Quat (jsonLocation["orientation"][1], jsonLocation["orientation"][2], jsonLocation["orientation"][3], self.mOrientation)
		end
	end
	if jsonLocation["PD"] ~= nil then
		self.mUserPD = jsonLocation["PD"]
	end
	if jsonLocation["right iris position"] ~= nil then
		self.mIrisRightPos = Vector3 (jsonLocation["right iris position"][1], jsonLocation["right iris position"][2], jsonLocation["right iris position"][3])
	end
	if jsonLocation["left iris position"] ~= nil then
		self.mIrisLeftPos = Vector3 (jsonLocation["left iris position"][1], jsonLocation["left iris position"][2], jsonLocation["left iris position"][3])
	end
	if jsonLocation["adjustment distance"] ~= nil then
		self.mDistanceAdjustment = Vector3 (jsonLocation["adjustment distance"][1], jsonLocation["adjustment distance"][2], jsonLocation["adjustment distance"][3])
	end
	if jsonLocation["manual position"] ~= nil then
		self.mManualMovePosition = Vector3 (jsonLocation["manual position"][1], jsonLocation["manual position"][2], jsonLocation["manual position"][3])
	end
	if jsonLocation["manual orientation"] ~= nil then
		self.mManualMoveOrientation = Vector3 (jsonLocation["manual orientation"][1], jsonLocation["manual orientation"][2], jsonLocation["manual orientation"][3])
	end
	self.mReferenceEarPointSet = jsonLocation["reference ear point"]
	if self.mReferenceEarPointSet then
		self.mPosLeftReferenceEarPoint = Vector3 (jsonLocation["left reference ear point"][1], jsonLocation["left reference ear point"][2], jsonLocation["left reference ear point"][3])
		self.mPosRightReferenceEarPoint = Vector3 (jsonLocation["right reference ear point"][1], jsonLocation["right reference ear point"][2], jsonLocation["right reference ear point"][3])
	end
self:printLog("tiPhoto:fromTable", tiPhoto.debugLevel)
end

function tiPhoto:fromJSON (JSONstring)
	log_debug("tiPhoto:fromJSON "..tostring(JSONstring))
	if JSONstring ~= nil then	
		local jsonLocation
		
		if (pcall (function() jsonLocation = json.decode(JSONstring) end)) then	
			self:fromTable (jsonLocation)
		else
			log_warning("tiPhoto:fromJSON impossible to decode JSON string : "..JSONstring)
		end
	else
		log_warning("tiPhoto:fromJSON impossible to decode a null JSON string")
	end
end

function tiPhoto:saveImage()
	log_call ("tiPhoto:saveImage")
	if self.mImage ~= nil then
		if self.mFileName ~= nil then
			log_debug ("tiPhoto:saveImage "..tostring(self.mFileName))
			self.mImage:save (self.mFileName)
		else
			log_warning ("tiPhoto:saveImage filename = nil")
		end
	else
		log_warning ("tiPhoto:saveImage image = nil")
	end
	log_call_end()
end

function tiPhoto:saveAndFreeImage()
	log_call ("tiPhoto:saveAndFreeImage")
	self:saveImage()
	self.mImage = nil
	log_call_end()
end

function tiPhoto:captureVideoFrame (camera, minFrameIndex)
--	log_call ("tiPhoto:captureVideoFrame minFrameIndex: "..tostring(minFrameIndex))
	self.mImage = nil
	startChrono("tiPhoto:captureVideoFrame")
	if camera ~= nil then
		self.mImage, self.mCameraFOVy, self.mCameraRatio, self.mTimeCode, self.mFrameIndex = camera:captureImage(minFrameIndex)
	else
		log_error ("tiPhoto:captureVideoFrame null camera")
	end
	stopChrono("tiPhoto:captureVideoFrame")
--	log_call_end()
	return (self.mImage ~= nil)
end

function tiPhoto:applyToCamera (camera)
	log_call ("tiPhoto:applyToCamera "..camera:getName())
--self:printLog("", 1)
	if self.mImage ~= nil then
		err = camera:setImage (self.mImage)
	else
		log_debug("tiPhoto:applyToCamera mFileName: "..tostring(self.mFileName))
		if tiPhoto.forceFile then
			err = camera:setImageFromFile (self.mFileName)
		else
			self.mImage = Image()
			self.mImage:load (self.mFileName)
			err = camera:setImage (self.mImage)
		end
	end
	if err ~= eOk then
		log_error ("tiPhoto:applyToCamera err: "..err)
	end
	camera:setModel (self.mCameraFOVy, self.mCameraRatio)
	log_call_end()
end

function tiPhoto:getImage()
	return self.mImage
end

function tiPhoto:getWeight()
	local weight = -1
	if self.mImage ~= nil then
		  weight = self.mImage:getWidth() *  self.mImage:getHeight() * self.mImage:getChannelNb() * self.mImage:getDepth() / 8
	end
	return weight
end

function tiPhoto:isLoaded()
	return (self.mImage ~= nil)
end
