--[[
This class is the base class of input processors which goal is to convert the input device outputs (touch, mouse click, etc.) to interaction parameters (translate, scale, ...)
Each derivated InputProcessor has few mandatory methos:
- enable in order to activate ror not the processors
- init in order to initialize specific data
- reset in order to reset current data
- getInputData in order to do convert the input device data into basic data, generally a point and and a move vector
- applyInputData in order to convert the inputData to interaction effect

]] --
log_loaded_info("tiInputProcessor.lua")

tiInputProcessor = {
}

tiInputProcessor.logLevel = 5

tiInputProcessor.TI_MAX_GAP_BETWEEN_EACH_MOVE_FRAME = 0.3
tiInputProcessor.TI_MAX_GAP_BETWEEN_EACH_MOVE_FRAME = 0.3

function tiInputProcessor:create (properties)
	log_debug("tiInputProcessor:create properties: "..tostring(properties))
    local new_inst = {}    -- the new instance
    setmetatable( new_inst, { __index = tiInputProcessor } ) -- all instances share the same metatable
	self:init (properties)
	new_inst:init (properties)
    return new_inst
end

function tiInputProcessor:getInputData (touches, viewport)
	log_debug("tiInputProcessor:getInputData "..tostring(self), tiInputProcessor.logLevel)
	return nil
end

function tiInputProcessor:applyInputData (inputData, viewport)
	log_debug("tiInputProcessor:applyInputData "..tostring(self), tiInputProcessor.logLevel)
end

function tiInputProcessor:process (touches, viewport)
	log_debug("tiInputProcessor:process "..tostring(self), tiInputProcessor.logLevel)
	local inputData = self:getInputData (touches, viewport)
	log_debug("tiInputProcessor:process inputData: "..tostring(inputData), tiInputProcessor.logLevel)
	if inputData ~= nil then
		self:applyInputData (inputData, viewport)
		return true
	end
	return false
end

function tiInputProcessor:enable (state)
	log_debug("tiInputProcessor:enable "..tostring(state))
	if state == nil then
		state = not self:isEnable()
	end
--			log_debug ("tiInputProcessorManager.enable mode: "..tostring(mode).." state: "..tostring(state))			
	self.mEnable = state
end

function tiInputProcessor:init (properties)
	log_debug("tiInputProcessor:init "..tostring(self))
	self.mEnable = false
	self.mFirstTouch = Vector2()
	self.mSecondTouch = Vector2()
	self.mIsFirstTouchSaved = false
	self.mIsSecondTouchSaved = false
	self.mPreviousNumberTouch = 0
	local x, y = RenderWindow (getCurrentScene():getMainView()):getMetrics()  
	self.mGapEachMoveInPixels = tiInputProcessor.TI_MAX_GAP_BETWEEN_EACH_MOVE_FRAME * ((x + y) /2 )
end		

function tiInputProcessor:resetFirstTouch()
	self.mFirstTouch = Vector2()
	self.mIsFirstTouchSaved = false
end

function tiInputProcessor:setFirstTouch(pos)
	self.mFirstTouch = Vector2(pos)
	self.mIsFirstTouchSaved = true
end

function tiInputProcessor:isFirstTouchSet()
	return self.mIsFirstTouchSaved
end

function tiInputProcessor:resetSecondTouch()
	self.mSecondTouch = Vector2()
	self.mIsSecondTouchSaved = false
end

function tiInputProcessor:setSecondTouch(pos)
	self.mSecondTouch = Vector2(pos)
	self.mIsSecondTouchSaved = true
end

function tiInputProcessor:isSecondTouchSet()
	return self.mIsSecondTouchSaved
end

function tiInputProcessor:reset()
	log_debug("tiInputProcessor:init "..tostring(self))
end		

function tiInputProcessor:printStatus()
	log_debug ("tiInputProcessor:printStatus mIsFirstTouchSaved: "..tostring(self.mIsFirstTouchSaved).." mFirstTouch: "..self.mFirstTouch:getX()..","..self.mFirstTouch:getY(), tiInputProcessor.logLevel)
	log_debug ("tiInputProcessor:printStatus mIsSecondTouchSaved: "..tostring(self.mIsSecondTouchSaved).." mPreviousNumberTouch: "..tostring(self.mPreviousNumberTouch), tiInputProcessor.logLevel)
	log_debug ("tiInputProcessor:printStatus mGapEachMoveInPixels: "..self.mGapEachMoveInPixels, tiInputProcessor.logLevel)
end

