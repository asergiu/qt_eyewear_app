log_loaded_info("tiExternalCommunication.lua")

-- This file manages all the external call to D'Fusion component (callback).

-- Set it for listen callback in lua code.
gCallbackExternalCom = nil

-- Choose the good component (web or mobile). Web is not used.
local componentInterface = getComponentInterface()

-------------------------------------------------------------------------------
-- Set it for listen callback in lua code.
-------------------------------------------------------------------------------
function setExternalComCallback(cb)
	gCallbackExternalCom = cb
end
-------------------------------------------------------------------------------
-- Do the real call for external callback.
-- And log it!
-------------------------------------------------------------------------------
function executeExternalCall(fctName, fctA1, fctA2, fctA3, fctA4, fctA5, fctA6, fctA7, fctA8, fctA9, fctA10, fctA11, fctA12)
	-- The name is mandatory
	if fctName == nil then
		log_error("Invalid call to executeExternalCall, fcName is nil")
		return
	end

	-- Generate the message to log.
	local strLog = "External Call: " .. fctName .. " "
	
	-- Check each parameters, the number of parameter is variable, so do not try to concat a nil object.
	if fctA1 ~= nil then
		strLog = strLog .. fctA1 .. " "
	else 
		fctA1 = ""
	end
	if fctA2 ~= nil then
		strLog = strLog .. fctA2 .. " "
	else 
		fctA2 = ""
	end
	if fctA3 ~= nil then
		strLog = strLog .. fctA3 .. " "
	else 
		fctA3 = ""
	end
	if fctA4 ~= nil then
		strLog = strLog .. fctA4 .. " "
	else 
		fctA4 = ""
	end
	if fctA5 ~= nil then
		strLog = strLog .. fctA5 .. " "
	else 
		fctA5 = ""
	end
	if fctA6 ~= nil then
		strLog = strLog .. fctA6 .. " "
	else 
		fctA6 = ""
	end
	if fctA7 ~= nil then
		strLog = strLog .. fctA7 .. " "
	else 
		fctA7 = ""
	end
	if fctA8 ~= nil then
		strLog = strLog .. fctA8 .. " "
	else 
		fctA8 = ""
	end
	if fctA9 ~= nil then
		strLog = strLog .. fctA9 .. " "
	else 
		fctA9 = ""
	end
	if fctA10 ~= nil then
			strLog = strLog .. fctA10 .. " "
	else 
		fctA10 = ""
	end
	if fctA11 ~= nil then
		strLog = strLog .. fctA11 .. " "
	else 
		fctA11 = ""
	end
	if fctA12 ~= nil then
		strLog = strLog .. fctA12 .. " "
	else 
		fctA12 = ""
	end


	-- Log it!
	log_info(strLog)
	
	componentInterface:executeAppFunc(fctName, fctA1, fctA2, fctA3, fctA4, fctA5, fctA6, fctA7, fctA8, fctA9, fctA10, fctA11, fctA12)	
	
	-- Call an eventual lua listener.
	if gCallbackExternalCom ~= nil then
		gCallbackExternalCom(fctName, fctA1, fctA2, fctA3, fctA4, fctA5, fctA6)
	end
end

-------------------------------------------------------------------------------
-- Helper function for external callback.
-- Calls generic callback interface
-------------------------------------------------------------------------------
function sendCallback(fctName, fctA1, fctA2, fctA3, fctA4, fctA5, fctA6, fctA7, fctA8, fctA9, fctA10, fctA11)
	executeExternalCall("receivedCallback", fctName, fctA1, fctA2, fctA3, fctA4, fctA5, fctA6, fctA7, fctA8, fctA9, fctA10, fctA11)
end

