LOG("tiLogManager.lua")
-- This file redefine some helper function for log information.

-- Define the log level: 
-- always : error
-- 0 : Time measurement (startChrono, stopChrono)
-- 1+ : warning
-- 2+ : info
-- 3+ : all

local log_level = tonumber(2)
local displayCaller = false

function set_log_level(level, caller)
	log_level = tonumber(level)
	if caller ~= nil then
		displayCaller = caller
	end
end

function getCaller()
	local caller = "nil"
	local t = debug.getinfo (5, "nfSl")
	if t ~= nil then
		if t.what == "C" then   -- is a C function?
			caller = "C function"
		else
			caller = t.source .. " (" .. t.currentline .. ")"
			if t.name ~= nil then
				caller = caller .. " " .. t.namewhat .. " " .. t.name
			end
		end	
	end
	return caller
end

function logBase(text)
	if displayCaller then
		text = text .. " called by: " .. getCaller()
	end
	text = "[TLLog] "..text
	if getOSType() == TI_OS_IPHONEOS then
		-- On iOS, the LOG function add always 2 empty lines to the logs
		-- A Tiare issue that we will have to fix in the future ...
		print(text)
	else
		LOG(text)
	end
end

logBase (os.date("Date: %Y/%m/%d %H:%M:%S"))

local log_verbose_level = 5
-- Set to false for desactivate all the logs except errors and warnings.
local bLogDebug = true

function getStrTime()
	return string.format("%8.3f",os.clock())
end

callLevel = 0
function getIndent()
	indent = ""
	for i=0,callLevel do
			indent = indent .. "...."
	end
	return indent
end

function log_call(txt, level)
	if level == nil then
		level = 1
	end	
	level = tonumber(level)
	if level <= log_level then		
		logBase(getStrTime() .. getIndent() .. "[ " .. txt .. " ............")
	end
	callLevel = callLevel+1

--	lua_Debug ar;
--  lua_getstack(L, 1, &ar);
--  lua_getinfo(L, "nSl", &ar);
--  int line = ar.currentline 

end

function log_call_end()
	if callLevel > 0 then
		callLevel = callLevel-1
	end
end

function log_debug(txt, level)
	if level == nil then
		level = 1
	end
	level = tonumber(level)
	if level <= log_level then
		logBase(getStrTime() .. getIndent() .. "## " .. tostring(txt) .. " ##")
	end
end

function log_pose (txt, position, orientation, level)
	local formpos = "%7.2f"
	local msg = txt
	if position ~= nil then
		msg = msg.." pos: "..string.format(formpos,position:getX())..","..string.format(formpos,position:getY())..","..string.format(formpos,position:getZ())
	else
		msg = msg.." pos nulle"
	end
	if orientation ~= nil then
		local formori = "%7.2f"
--		local pitch, yaw, roll = quat2Euler(orientation)
--		msg = msg.." ori: "..string.format(formori,pitch)..","..string.format(formori,yaw)..","..string.format(formori,roll)
		msg = msg.." ori: "..string.format(formori,orientation:getX())..","..string.format(formori,orientation:getY())..","..string.format(formori,orientation:getZ())
	end
	log_debug (msg, level)
end

function log_keyvaltab (titre, tab, level)
	local s = ""
	if tab ~= nil then
		for key, val in pairs(tab) do
			s = s .. key .. ": " .. val .. " "
		end
	end
	log_debug (titre..": "..s, level)
end

function log_debug_simple(txt, level)
	if level == nil then
		level = 1
	end
	if level <= log_level then
		logBase(txt)
	end
end

local tableLevel = 0

function getIndentTable()
	indent = ""
	for i=0,tableLevel do
			indent = indent .. "    "
	end
	return indent
end

function getAllData(t, prevData)
  -- if prevData == nil, start empty, otherwise start with prevData
  local data = prevData or {}

  -- copy all the attributes from t
  for k,v in pairs(t) do
    data[k] = data[data] or v
  end
  -- get t's metatable, or exit if not existing
  local mt = getmetatable(t)
  if type(mt)~='table' then return data end

  -- get the __index from mt, or exit if not table
  local index = mt.__index
  if type(index)~='table' then return data end

  -- include the data from index into data, recursively, and return
  return getAllData(index, data)
end	

function log_object(t, title)
	if title == nil then
		title = ""
	end
	if t == nil then
		log_debug (getIndentTable() .. "Table " .. title .. " nil")
	else
		if type(t) == "table" then
			log_debug (getIndentTable() .. "Table " .. title .. "(" .. tostring(t) .. ") :")
			for k, v in pairs(t) do
				if type(v) == "table" then
					log_debug (getIndentTable() .. k .. ": ")
					tableLevel = tableLevel+1
					log_object (v)
					tableLevel = tableLevel-1
				else
					log_debug (getIndentTable() .. k .. ": " .. tostring(v))
				end
			end
		else
			log_debug (title .. " " .. tostring(t) .. " :")
			log_object (getAllData (getmetatable(t), nil), title)
		end
	end
end

function xlog(log)
	if log_level >= 1 then
		logBase(getStrTime()..log)
	end
end
	
function log_error(error)
	logBase(getStrTime().."[ERROR] "..error)
end

function log_warning(warning)
	if log_verbose_level > 1 then
		logBase(getStrTime().."[WARNING] "..warning)
	end
end

function log_info(info)
	if log_verbose_level > 1 then
		xlog("[INFO] "..info)
	end
end

function log_loading_info(object)
	if log_verbose_level > 2 then
		xlog("[LOADING] "..object)
	end
end

function log_loaded_info(object)
	if log_verbose_level > 2 then
		xlog("[LOADED] "..object)
	end
end

function log_releasing_info(object)
	if log_verbose_level > 2 then
		xlog("[RELEASING] "..object)
	end
end

function log_released_info(object)
	if log_verbose_level > 2 then
		xlog("[RELEASED] "..object)
	end
end

xlog("[INFO] Log verbose level :"..log_verbose_level)
log_loaded_info("log_manager.lua")


function log_camera (camera, titre)
	local s = ""
	if titre == nil then
		titre = ""
	end
	if camera ~= nil then
		if camera:isNull() then
			s = "null cam"
		else
			s = tostring(camera)
			local isUsingCameraModel = camera:isUsingCameraModel()
			if isUsingCameraModel then	
				local cameraModel, 
						cameraSizeX, cameraSizeY, 
						cameraCenterX, cameraCenterY, 
						cameraFocalX, cameraFocalY, 
						cameraA1, cameraA2, cameraP1, cameraP2, 
						cameraDistortion = camera:getCameraModel()
				s =  	s .. " " ..
						" sx sy ".. cameraSizeX .. " "..cameraSizeY .. 			
						" cx cy ".. cameraCenterX .. " "..cameraCenterY ..  
						" fx fy ".. cameraFocalX .. " "..cameraFocalY ..
						" disto ".. cameraA1 .. " "..cameraA2 .. " "..cameraP1 .. " "..cameraP2 .. " ".. tostring(cameraDistortion)
			else
				local fovy = camera:getFOVy()
				local aspectRatio = camera:getAspectRatio()
				s = 	s .. " " ..
						" fovy " .. fovy .. " aspectRatio " .. aspectRatio
			end
		end
	else
		s = "nil cam"
	end
	log_debug(titre .. " [Camera] " .. s)
end

function log_viewport (vp, titre)
	local s = ""
	if titre == nil then
		titre = ""
	end
	if vp ~= nil then
		local x0, y0, x1, y1 = vp:getActualBackgroundVideoRect ()
		s = s .. " "..vp:getName().." enable: "..tostring(vp:getEnabled())
		s = s .. " dim:"..vp:getActualLeft()..","..vp:getActualTop()..","..vp:getActualWidth()..","..vp:getActualHeight()
		s = s .. " actual vidrec:"..x0..","..y0..","..x1..","..y1
		s = s .. " zorder: "..vp:getCurrentZOrder ().." mask: "..vp:getVisibilityMask().." scheme: "..vp:getCurrentScheme() 
	else
		s = "null viewport"
	end
	log_debug(titre .. " [Viewport] " .. s)
end

---------------------------------------------------------
-- Time measurement management

local tabChronos = {}
local chronoLevel = 0

local function getChronoKey(name, param)
	if param == nil then
		param = ""
	end
	return name..":"..param,  param
end

function startChrono(name, param)
	tabChronos[getChronoKey(name, param)] = os.clock()
	chronoLevel = chronoLevel+1
end

function stopChrono(name,param)
	local key, param = getChronoKey(name, param)
	chronoLevel = chronoLevel-1
	begin = tabChronos[key]
	if begin ~= nil then
		dt = os.clock() - begin
		if log_level >= 0 then
			logBase(getStrTime().."|Chrono|"..chronoLevel.."|"..name.."|"..param.."|"..string.format("%.6f",dt))
		end
		tabChronos[key] = nil
	end
end

function printStack(title)
	if title == nil then
		title = ""
	end
	log_debug ("///// Call Stack ///// " .. title)
	local level = 2
	local t = debug.getinfo (level, "nfSl")
	while t ~= nil do
		local info = ""
		if t.what == "C" then   -- is a C function?
			info = "C function"
		else
--			info = t.source .. " (" .. t.currentline .. ") "
			info = t.short_src .. " (" .. t.currentline .. ") "
			if t.name ~= nil then
				info = info .. " " .. t.namewhat .. " " .. t.name
			end
		end
		log_debug (level-2 .. " " .. info)
		
		level = level+1
		t = debug.getinfo (level, "nfSl")
	end
end


function printStack2(title)
	LOG(debug.traceback())
end

function printStack3 ()
      local level = 1
      while true do
        local info = debug.getinfo(level, "Sl")
        if not info then break end
        if info.what == "C" then   -- is a C function?
          LOG(level, "C function")
        else   -- a Lua function
          LOG(string.format("[%s]:%d", info.short_src, info.currentline))
        end
        level = level + 1
      end
end

--versionTryLive = "undefined"

function getVersion()
	if versionTryLive == nil then
		versionTryLive = "undefined"
	end
	return versionTryLive
end

function log_version()
	log_info ("TryLive SDK Lua files version: "..tostring(getVersion()))
end


