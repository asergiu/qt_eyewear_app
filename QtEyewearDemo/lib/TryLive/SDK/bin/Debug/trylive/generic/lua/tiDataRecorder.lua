-- tiDataRecorder.lua script
-- This class is used to send several clients logs

log_loaded_info("tiDataRecorder.lua")

tiDataRecorder = {
	mRecording = false
}
			
function tiDataRecorder.load (config)
log_debug("tiDataRecorder.load ")			
	local token = "1"
	DataRecord.start(config, token)
	mRecording = true			
end
			
function tiDataRecorder.stop()
	DataRecord.stop()
	mRecording = false
end
			
function tiDataRecorder.flush()
log_debug("tiDataRecorder.flush")				
	DataRecord.flush()			
end
			
function tiDataRecorder.log(tag, ...)
	local s = tag.." "
	for i,v in ipairs(arg) do
		s = s.." "..v
	end
log_debug("tiDataRecorder.log " .. s)
	DataRecord.log(tag, unpack(arg))			
end
