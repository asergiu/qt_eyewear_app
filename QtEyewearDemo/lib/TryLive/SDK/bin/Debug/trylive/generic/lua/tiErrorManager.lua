log_loaded_info("tiErrorManager.lua")
-- This file manages the error chain.
-- Call the function dispatchError in the lua call for raise the error with a log + a callback.


-- The differents error code.
TI_UNABLE_TO_OPEN_VIDEO_CAPTURE = "0"
TI_FILE_NOT_FOUND = "1"
TI_IMAGE_NOT_FOUND = "2"
TI_INCONSISTENT_FORMAT = "3"
TI_INCONSISTENT_COMMAND = "4"
TI_INCONSISTENT_PARAMETERS = "5"
TI_NO_ASSETS = "6"
TI_UPLOAD_FAILED = "7"
TI_INTERNAL_ERROR = "8"

-- Map error code and string of it.
TI_ERROR_CODE = {
	{ code = "0", text = "TI_UNABLE_TO_OPEN_VIDEO_CAPTURE"},
	{ code = "1", text = "TI_FILE_NOT_FOUND"},
	{ code = "2", text = "TI_IMAGE_NOT_FOUND"},
	{ code = "3", text = "TI_INCONSISTENT_FORMAT"},
	{ code = "4", text = "TI_INCONSISTENT_COMMAND"},
	{ code = "5", text = "TI_INCONSISTENT_PARAMETERS"},
	{ code = "6", text = "TI_NO_ASSETS"},
	{ code = "7", text = "TI_UPLOAD_FAILED"},
	{ code = "8", text = "TI_INTERNAL_ERROR"},
}

-- This internal function retrieves an error ID given the error name.
local function getErrorID(errorId)
	for i, errorElement in ipairs(TI_ERROR_CODE) do
		if errorElement.code == errorId then
			return errorElement.text
		end
	end
	return "Unknow Error"
end
--[[ Tiare Lua errors
enum tieBaseError
{
	/** The operation completed successfully. */
	eOk									= 0,
	/** The operation failed (Generic failure). */
	eFailed								= -1,
	/** A system call failed (Generic system failure). */
	eSysCallFailed						= -2,
	/** Method not yet implemented. */
	eNotImplemented						= -3,
	/** Not ready. When an object is valid, but not ready (waiting for other data to work). */
	eNotSupported						= -4,
	/** The operation is denied. */
	eDenied								= -5,
	/** System out of memory. */
	eOutOfMemory						= -6,
	/** Object is not initialized. */
	eNotInitialized						= -7,
	/** The value of a parameter is inconsistent. */
	eInvalidParameter					= -8,
	/** The state of the object is not valid to perform the operation. */
	eInvalidState						= -9,
	/** An exception has been catched. */
	eExceptionCatched					= -10,
	/** Element not found. */
	eNotFound							= -11,
	/** Operation cancelled. */
	eCancelled							= -12,
	/** An I/O operation failed. */
	eIOFailed							= -13,
	/** Timeout occured. */
	eTimeout							= -14,

};
--]]

-- This function dispatches the error.
-- Input : an error code defined in the list above and a specific message.
function dispatchError(errorId, message)
	local sendMessage = ""
	local errorDetected = false
	if errorId == nil then
		sendMessage = "Error : dispatchError the error code is null."
		errorDetected = true
	end
	if message == nil then
		sendMessage = "Error : dispatchError the message is null."
		errorDetected = true
	end
	
	if not errorDetected then
		sendMessage = errorId .. ":".. getErrorID(errorId) ..":" .. message
	end
	
	errorAlert(sendMessage)
	log_error(sendMessage)
end

function errorAlert(str)
	sendCallback("errorStatusChanged",str)
end


