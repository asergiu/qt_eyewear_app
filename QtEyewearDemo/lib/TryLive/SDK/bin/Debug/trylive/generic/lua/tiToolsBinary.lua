log_loaded_info("tiBinaryTools.lua")
-- This file regroups all the functions for manage the binaries operations. 
-- The lua language doesn't manage this functions by default, so recode it.
-- This functions isn't really optimized, but needed for keep a simple way to use the visibility mask.

-------------------------------------------------------------------------------
-- Convert a decimal value in binary value ( string object).
-------------------------------------------------------------------------------
function decimalToBinary(number, digits)
	local binaryTable = {}
	if number == 0 then
		return {0}
	end
	local r
	local q = number
	while q ~= 0 do
		r = q % 2
		table.insert(binaryTable, r)
		q = math.floor(q / 2)
	end
	if (digits ~= nil) then
		local size = table.getn(binaryTable)
		for i = 1, digits - size, 1 do
			table.insert(binaryTable, 0)
		end
	end
	return binaryTable
end

-------------------------------------------------------------------------------
-- Execute an OR binary between to binary value (represent as string).
-------------------------------------------------------------------------------
function orBinary(bin1, bin2)
	local result = {}
	local size =  math.max(table.getn(bin1), table.getn(bin2))
	local checkB1 = 0
	local checkB2 = 0
	for i = 1, size, 1 do
		if i <= table.getn(bin1) then
			checkB1 = bin1[i]
		else
			checkB1 = 0
		end
		if i <= table.getn(bin2) then
			checkB2 = bin2[i]
		else
			checkB2 = 0
		end
		if checkB1 == 0 and checkB2 == 0 then
			table.insert(result, 0);
		else
			table.insert(result, 1);
		end
	end
	return result
end

-------------------------------------------------------------------------------
-- Execute a binary NOT operation (bin is represented as a string).
-------------------------------------------------------------------------------
function notBinary(bin)
	local result = {}
	for i = 1, table.getn(bin), 1 do
		if (bin[i] == 1) then
			table.insert(result, 0)
		else
			table.insert(result, 1)
		end
	end
	return result
end

-------------------------------------------------------------------------------
-- Execute an AND binary between to binary value (represent as string).
-------------------------------------------------------------------------------
function andBinary(bin1, bin2)
	local result = {}
	local size =  math.max(table.getn(bin1), table.getn(bin2))

	local checkB1 = 0
	local checkB2 = 0
	for i = 1, size, 1 do
		if i <= table.getn(bin1) then
			checkB1 = bin1[i]
		else
			checkB1 = 0
		end

		if i <= table.getn(bin2) then
			checkB2 = bin2[i]
		else
			checkB2 = 0
		end
			
		if checkB1 == 1 and checkB2 == 1 then
			table.insert(result, 1);
		else
			table.insert(result, 0);
		end
	end
	return result
end

-------------------------------------------------------------------------------
-- Execute a XOR binary between to binary value (represent as string).
-------------------------------------------------------------------------------
function orExcluBinary(bin1, bin2)
	local result = {}
	local size =  math.max(table.getn(bin1), table.getn(bin2))

	local checkB1 = 0
	local checkB2 = 0
	for i = 1, size, 1 do
		if i <= table.getn(bin1) then
			checkB1 = bin1[i]
		else
			checkB1 = 0
		end

		if i <= table.getn(bin2) then
			checkB2 = bin2[i]
		else
			checkB2 = 0
		end
			
		if checkB1 == 0 and checkB2 == 0 then
			table.insert(result, 0);
		else
			if checkB1 ~= checkB2 then
				table.insert(result, 1);
			else
				table.insert(result, 0);			
			end
		end
	end
	return result
end

-------------------------------------------------------------------------------
-- Execute an OR ORDER binary between to binary value (represent as string).
-- An OR ORDER is a specific things for asset management in lua.
-- It's a classic OR with a difference, if bin1 = 1 and bin2 = 0, result = 1.
-- BUT if bin1 = 0 and bin2 = 1, result  = 0
-------------------------------------------------------------------------------
function orOrderBinary(bin1, bin2)
	local result = {}
	local size =  math.max(table.getn(bin1), table.getn(bin2))

	local checkB1 = 0
	local checkB2 = 0
	for i = 1, size, 1 do
		if i <= table.getn(bin1) then
			checkB1 = bin1[i]
		else
			checkB1 = 0
		end

		if i <= table.getn(bin2) then
			checkB2 = bin2[i]
		else
			checkB2 = 0
		end
			
		if checkB1 == 1 and checkB2 == 0 then
			table.insert(result, 1);
		else
			table.insert(result, 0);
		end
	end
	return result
end

-------------------------------------------------------------------------------
-- Converts a binary value in a decimal value.
-------------------------------------------------------------------------------
function binaryToDecimal(bin)
	local number = 0
	local size = table.getn(bin)
	for i = size, 1, -1 do
		number = number + 2 ^ (i - 1) * bin[i]
	end
	return number
end
