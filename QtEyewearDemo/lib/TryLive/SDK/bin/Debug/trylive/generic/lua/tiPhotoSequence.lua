log_loaded_info("tiPhotoSequence.lua")

tiPhotoSequence = inheritsFrom (tiListenable)

tiPhotoSequence.Status = {Sleeping = 0, Recording = 1, Saving = 2, Playing = 3}
tiPhotoSequence.saveStats = false
tiPhotoSequence.nbMaxLoadedPhoto = 30
tiPhotoSequence.maxDuration = 20
tiPhotoSequence.debugLevel = 5

function tiPhotoSequence:init (properties)
	log_debug("tiPhotoSequence:init "..tostring(self))
	
	self.mPhotoList = {}
	self.mPlayCameraName = ""
	self.mRecordCameraName = ""
	self.mLastFrame = -1
	self.mFrameRecordStep = 1
	self.mNbLoadedPhoto = 0
	
	self.mCurrentPhoto = 1
	self.mPreviousPhoto = 1
	self.mFramePlayStep = 1
	self.mLoop = false
	self.mAutoPlay = true
	self.mStatus = tiPhotoSequence.Status.Sleeping
	
	if properties ~= nil then
		if properties.name ~= nil then
			self.mName = properties.name
		end
	end
end

function tiPhotoSequence:delete()
	log_debug("tiPhotoSequence:delete")
	self:fireEvent("EventPhotoSequenceDeleted")
	for i,photo in pairs(self.mPhotoList) do
		photo:free()
	end
	self.mPhotoList = {}
end

function tiPhotoSequence:getCurrentPhotoIndex()
	return self.mCurrentPhoto
end

function tiPhotoSequence:getCurrentPhoto()
	return self.mPhotoList[self.mCurrentPhoto]
end				
			
function tiPhotoSequence:startRecord (cameraName, path, filename, stopDelay, frameStep)
log_debug("tiPhotoSequence:startRecord", tiPhotoSequence.debugLevel)
	if self.mStatus == tiPhotoSequence.Status.Sleeping then
		self.mPhotoList = {}
		if path ~= nil and path ~= "" then
			self.mPath = path
		else
			self.mPath = getUserImageDirectory()
		end
		if filename ~= nil then
			self.mFilename = filename
		else
			self.mFilename = "ImgSeq"
		end
		self.mLastFrame = -1
		self.mCurrentPhoto = 1
		self.mRecordCameraName = cameraName
		self.mStartTime = os.clock()
		if stopDelay ~= nil then
			self.mStopDelay = stopDelay
		else
			self.mStopDelay = tiPhotoSequence.maxDuration
		end
		if frameStep ~= nil then
			self.mFrameRecordStep = frameStep
		else
			self.mFrameRecordStep = 1
		end
		self.mStatus = tiPhotoSequence.Status.Recording
	else
		self.mStatus = tiPhotoSequence.Status.Sleeping
		self:photoSequenceSave()
	end
end

function tiPhotoSequence:stopRecord()
log_debug("tiPhotoSequence:stopRecord", tiPhotoSequence.debugLevel)
	self.mStatus = tiPhotoSequence.Status.Sleeping
	sendCallback("recordSequenceEnded", #(self.mPhotoList))
	self:save()
end

function tiPhotoSequence:toJSON()
log_debug("tiPhotoSequence:toJSON", tiPhotoSequence.debugLevel)
	local jsonSequence = '{"photo sequence":\n'
	jsonSequence = jsonSequence..'[\n'
	local deb = 0
	for i,photo in pairs(self.mPhotoList) do
		if i == 1 then
			deb = photo.mTimeCode
		end
		photo.mTimeCode = photo.mTimeCode - deb
		jsonSequence = jsonSequence..photo:toJSON()
		if i ~= #self.mPhotoList then
			jsonSequence = jsonSequence..',\n'
		end
		log_debug ("JSON: "..photo:toJSON())
	end
	jsonSequence = jsonSequence..']\n'
	jsonSequence = jsonSequence..'}\n'
	return jsonSequence
end

function tiPhotoSequence:toStats()
log_debug("tiPhotoSequence:toStats", tiPhotoSequence.debugLevel)
	local outString = ''
	for i,photo in pairs(self.mPhotoList) do
		local pitch, yaw, roll = quat2Euler (photo.mOrientation)
		outString = outString..math.rad(pitch-180).." "..math.rad(yaw).." "..math.rad(roll).." "..photo.mPosition:getX().." "..photo.mPosition:getY().." "..-photo.mPosition:getZ()..'\n'
	end
	return outString
end

function tiPhotoSequence:save1()
log_debug("tiPhotoSequence:save", tiPhotoSequence.debugLevel)
	self.mStatus = tiPhotoSequence.Status.Saving
	for i,photo in pairs(self.mPhotoList) do
		if photo:isLoaded() then	
			photo:saveAndFreeImage()
			sendCallback("savePhotoEnded", photo.mFileName, photo:toJSON())
			self.mNbLoadedPhoto = self.mNbLoadedPhoto-1
		end
	end
	tiFile.writeText (self.mPath.."/"..self.mFilename..".json", self:toJSON())
	if tiPhotoSequence.saveStats then
		tiFile.writeText (self.mPath.."/"..self.mFilename..".stats", self:toStats())
	end
	self.mStatus = tiPhotoSequence.Status.Sleeping
	sendCallback("saveSequenceEnded", #(self.mPhotoList), self.mPath.."/"..self.mFilename..".json")
end

function tiPhotoSequence:save()
log_debug("tiPhotoSequence:save", tiPhotoSequence.debugLevel)
	self.mStatus = tiPhotoSequence.Status.Saving
end

function tiPhotoSequence:saveStep()
--log_debug("tiPhotoSequence:saveStep", tiPhotoSequence.debugLevel)
	if self.mStatus == tiPhotoSequence.Status.Saving then
		local photoToSave = nil
		for i,photo in pairs(self.mPhotoList) do
			if photo:isLoaded() then
				photoToSave = photo
			end
		end
		if photoToSave ~= nil then
			photoToSave:saveAndFreeImage()
			sendCallback("savePhotoEnded", photoToSave.mFileName, photoToSave:toJSON())
			self.mNbLoadedPhoto = self.mNbLoadedPhoto-1
		else
			tiFile.writeText (self.mPath.."/"..self.mFilename..".json", self:toJSON())
			if tiPhotoSequence.saveStats then
				tiFile.writeText (self.mPath.."/"..self.mFilename..".stats", self:toStats())
			end
			self.mStatus = tiPhotoSequence.Status.Sleeping
			sendCallback("saveSequenceEnded", #(self.mPhotoList), self.mPath.."/"..self.mFilename..".json")
		end
	end
end

function tiPhotoSequence:load (filename)
log_debug("tiPhotoSequence:load "..tostring(filename).." NOT IMPLEMENTED")
	self.mFilename = tiFile.baseName (filename)
	self.mPath = tiFile.dirName (filename)

	local data = {}
	if (pcall (function()
					data = json.load (filename)
				end
				)) then

		-- Check that there is no cycling option
		-- A cycling option is an option that have the same product and product_option that the product
--		log_object (data)
		for i, p in pairs(data["photo sequence"]) do
--log_object (p, "photo N° "..i)
			photo = tiPhoto:create()
			photo:fromTable (p)
			photo.mId = i
			self.mPhotoList[photo.mId] = photo	
		end
	end
	
	sendCallback("loadSequenceEnded", #(self.mPhotoList), filename)
end

function tiPhotoSequence:addPhoto (path)
log_debug("tiPhotoSequence:addPhoto "..tostring(path), tiPhotoSequence.debugLevel)
	local photo = tiPhoto:create()
	photo.mId = self.mCurrentPhoto
	photo.mFileName = path
	self.mPhotoList[photo.mId] = photo
	self.mCurrentPhoto = self.mCurrentPhoto+1
	self.mLastFrame = photo.mFrameIndex
end

function tiPhotoSequence:checkRecord()
	if self.mStatus == tiPhotoSequence.Status.Recording then
		if self.mStopDelay ~= nil then
			if (os.clock()-self.mStartTime) > self.mStopDelay then
				self:stopRecord()
			end
		end
		if self.mStatus == tiPhotoSequence.Status.Recording then
			local photo = tiPhoto:create()
			photo.mId = self.mCurrentPhoto
			photo.mFileName = self.mPath.."/"..self.mFilename..string.format ("%05d.jpg", photo.mId-1)
startChrono("tiPhotoSequence:checkRecord")
			if photo:captureVideoFrame (tiCameraManager.getCurrentCamera (self.mRecordCameraName), self.mLastFrame+self.mFrameRecordStep) then
				self.mNbLoadedPhoto = self.mNbLoadedPhoto+1
				log_debug ("tiPhotoSequence:checkRecord Nb Photo: "..self.mNbLoadedPhoto.." -> "..self.mNbLoadedPhoto*photo:getWeight().." bytes")
				if (tiCVModuleManager.isTracking (self.mRecordCameraName)) then
					photo.mPoseValide = true
				end
				tiScenarioPhoto.updatePhotoInfo (photo, self.mRecordCameraName)
				self.mPhotoList[photo.mId] = photo
				self.mCurrentPhoto = self.mCurrentPhoto+1
				self.mLastFrame = photo.mFrameIndex
			end
stopChrono("tiPhotoSequence:checkRecord")
		end
	end
end

function tiPhotoSequence:play(numImage)
log_debug("tiPhotoSequence:play numImage: "..tostring(numImage), tiPhotoSequence.debugLevel)
	if tiCameraManager.getCurrentCamera (self.mPlayCameraName) ~= nil then 
		if numImage ~= nil then
			self:playImage (numImage)
		else
			if self.mStatus == tiPhotoSequence.Status.Playing then
				self.mStatus = tiPhotoSequence.Status.Sleeping
			else
				self.mStatus = tiPhotoSequence.Status.Playing	
			end
			self.mCurrentPhoto = 1
		end
	else
		log_error ("tiPhotoSequence:play camera "..tostring(self.mPlayCameraName).." not defined")
	end
end

function tiPhotoSequence:set(cameraName)
log_debug("tiPhotoSequence:set on camera "..tostring(cameraName), tiPhotoSequence.debugLevel)
	self.mCurrentPhoto = 1
	self.mPlayCameraName = cameraName
	self.mStatus = tiPhotoSequence.Status.Sleeping
	self:pause(true)
end

function tiPhotoSequence:getCurrentImage()
	local image = nil
	local photo = self.mPhotoList[self.mCurrentPhoto]
	if photo ~= nil then
		image = photo:getImage()
	end
	return image
end

function tiPhotoSequence:playImage (numImage)
	if self.mStatus == tiPhotoSequence.Status.Playing then
		if #(self.mPhotoList) > 0 and 
			numImage > 0 and
			numImage <= #(self.mPhotoList) then
--log_debug("tiPhotoSequence.photoSequencePlayStep	step: "..tiPhotoSequence.mCurrentPhoto)
			if numImage ~= self.mPreviousPhoto then
				self.mCurrentPhoto = numImage
				local photo = self.mPhotoList[self.mCurrentPhoto]
--log_debug("tiPhotoSequence.photoSequencePlayStep "..tostring(photo.mFileName))
				photo:applyToCamera (tiCameraManager.getCurrentCamera (self.mPlayCameraName))
-- MOB Le scenario doit écouter la sequence et c'est lui qui doit faire le setAssetsPose, on peut utiliser l'event EventImageChanged en rajoutant les parametres de pose
-- MOB comme le scenario ne connait pas la sequence, c'est le scenariophoto qui doit écouter la sequence
				tiScenario.setAssetsPose (self.mPlayCameraName, photo.mPosition, photo.mOrientation)
-- MOB plutot que de liberer ici, mieux vaudrait gérer une liste de photo allouées et libérée au dela d'un certain nombre
				self.mPhotoList[self.mPreviousPhoto]:free()
				self.mPreviousPhoto = self.mCurrentPhoto
				self:fireEvent("EventImageChanged")
			end
		end
	end
end

function tiPhotoSequence:playStepBase (forcePlay, inc)
log_debug("tiPhotoSequence:playStepBase forcePlay: "..tostring(forcePlay).." inc: "..tostring(inc), tiPhotoSequence.debugLevel)
	if inc ~= nil then
		self.mFramePlayStep = inc
	end
log_debug("tiPhotoSequence.photoSequencePlayStep mStatus: "..tostring(self.mStatus), tiPhotoSequence.debugLevel)
	if self.mStatus == tiPhotoSequence.Status.Playing then
		self:playImage (self.mCurrentPhoto)
		if self.mAutoPlay or forcePlay then
			self.mCurrentPhoto = self.mCurrentPhoto+self.mFramePlayStep
--log_debug("tiPhotoSequence.photoSequencePlayStep	next step: "..tiPhotoSequence.mCurrentPhoto)
-- Foireux, a revoir ...				
			if self.mCurrentPhoto > #(self.mPhotoList) then
				if self.mLoop then
					self.mCurrentPhoto = (self.mCurrentPhoto+#(self.mPhotoList)) % #(self.mPhotoList)
				else
					self.mCurrentPhoto = #(self.mPhotoList)
					self.mStatus = tiPhotoSequence.Status.Sleeping
					sendCallback("playSequenceEnded")
					self:fireEvent("EventPhotoSequenceEnded")
				end
--log_debug("tiPhotoSequence.photoSequencePlayStep	next step corrected: "..tiPhotoSequence.mCurrentPhoto)
			end
			if self.mCurrentPhoto < 1 then
				if self.mLoop then
					self.mCurrentPhoto = (self.mCurrentPhoto+#(self.mPhotoList)) % #(self.mPhotoList)
--log_debug("tiPhotoSequence.photoSequencePlayStep	next step corrected: "..tiPhotoSequence.mCurrentPhoto)
				else
					self.mCurrentPhoto = 1
					self.mStatus = tiPhotoSequence.Status.Sleeping
					sendCallback("playSequenceEnded")
				end
			end
		end
	end
end

function tiPhotoSequence:pause (state)
log_debug("tiPhotoSequence:pause state: "..tostring(state).." mAutoPlay: "..tostring(self.mAutoPlay))
	if state == nil then
		self.mAutoPlay = not self.mAutoPlay
	else
		self.mAutoPlay = state
	end
end

function tiPhotoSequence:checkSave()
--	log_call ("tiPhotoSequence:checkSave self.mStatus: "..tostring(self.mStatus))
	if self.mStatus ~= tiPhotoSequence.Status.Saving then
--		log_debug("tiPhotoSequence:checkSave self.mNbLoadedPhoto: "..tostring(self.mNbLoadedPhoto).." tiPhotoSequence.nbMaxLoadedPhoto: "..tostring(tiPhotoSequence.nbMaxLoadedPhoto))
		if self.mNbLoadedPhoto >= tiPhotoSequence.nbMaxLoadedPhoto then
			local i = 1
			local foundPhoto = nil
			if i <= #(self.mPhotoList) then
				repeat
					if self.mPhotoList[i]:isLoaded() then
						foundPhoto = self.mPhotoList[i]
					end
					i = i+1
				until foundPhoto ~= nil or i > #(self.mPhotoList)
				if foundPhoto ~= nil then
					foundPhoto:saveAndFreeImage()
					sendCallback("savePhotoEnded", foundPhoto.mFileName, foundPhoto:toJSON())
					self.mNbLoadedPhoto = self.mNbLoadedPhoto-1
				end
			end
			collectgarbage()
		end
	end
--	log_call_end()
end

function tiPhotoSequence:print()
	for i, photo in pairs(self.mPhotoList) do
		log_debug ("photo "..i.." "..tostring(photo.mFileName))
	end
end
