log_loaded_info("tiViewport.lua")

tiViewport = {
	viewportBaseName = "viewport",
	VisibilityMask = {None = 0, All = 2},
	debugLevel = 1
}	

function tiViewport:create()
	log_debug("tiViewport:create "..tostring(self), tiViewport.debugLevel)
    local new_inst = {}    -- the new instance
    setmetatable( new_inst, { __index = tiViewport } ) -- all instances share the same metatable
	self:init()
    return new_inst
end

function tiViewport:init()
	log_debug("tiViewport:init "..tostring(self), tiViewport.debugLevel)
	self.mViewport = nil
	self.mCamera = nil
	self.mDimension = nil
	self.mMask = 0
	self.mId = "The unknown viewport..."
	self.mIndex = 0
	self.mListeners = {}
	self.mBackgroundColor = {r = 0.0, g = 0.0, b = 0.0, a = 1.0}
	self.mScale = 1.0
	self.mCenter = Vector2 (0, 0)
	self.mCenterLocked = false
end

function tiViewport:initialize (index)
	log_debug("tiViewport:initialize "..tostring(self).." index: "..index, tiViewport.debugLevel)
	self.mIndex = index
--	self.mViewport = Viewport (getCurrentScene():getObjectByName(tiViewport.viewportBaseName .. self.mIndex))	
self.mViewport = createViewport (self.mIndex)
	self.mViewport:setRenderTarget (tiWindowManager.getRenderTarget()) 
	self.mMask = math.pow (2, tonumber(tonumber(self.mIndex))+1)
	self:setBackgroundColor (self.mBackgroundColor.r, self.mBackgroundColor.g, self.mBackgroundColor.b, self.mBackgroundColor.a)
log_debug("tiViewport:initialize mask: "..self.mMask, tiViewport.debugLevel)
end

function tiViewport:getName()
	local name = "undefined"
	if self.mViewport ~= nil then
		name = self.mViewport:getName()
	end
	return name
end

function tiViewport:setCenterLocked (state)
	log_debug("tiViewport:setCenterLocked "..tostring(self).." state: "..tostring(state), tiViewport.debugLevel)
	self.mCenterLocked = state
end

function tiViewport:getCenterLocked()
	return self.mCenterLocked
end

function tiViewport:setBackgroundColor (r, g, b, a)
	if a == nil then
		a = 1.0
	end
	if g == nil then
		self.mBackgroundColor = r
	else
		self.mBackgroundColor = {r = r, g = g, b = b, a = a}
	end
	log_debug ("tiViewport:setBackgroundColor "..self:getName().." "..self.mBackgroundColor.r.." "..self.mBackgroundColor.g.." "..self.mBackgroundColor.b.." "..self.mBackgroundColor.a, tiViewport.debugLevel)
	self.mViewport:setBackgroundColor (self.mBackgroundColor.r, self.mBackgroundColor.g, self.mBackgroundColor.b, self.mBackgroundColor.a)
end

function tiViewport:setCamera (camera)
	local cameraName = "nil"
	if camera ~= nil then
		cameraName = camera:getName()
--	else
--		printStack()
	end
	log_debug("tiViewport:setCamera "..tostring(self).." "..tostring(cameraName))
	self.mCamera = camera
	if self.mCamera ~= nil then
		self.mCamera:addListener (self)
	end
end

function tiViewport:getCamera()
	return self.mCamera
end

function tiViewport:processEvent (source, event)
	log_call ("tiViewport:processEvent "..source:getName()..", "..event, tiViewport.debugLevel)
	if event == "EventBackgroundChanged" then
		self:reshape()
		self:initContent()
	elseif event == "EventCameraChanged" then
		self.mCamera = nil
	end
	log_call_end()
end

function tiViewport:setDimension (dimension)
	log_debug("tiViewport:setDimension "..tostring(self).." "..dimension[1].." "..dimension[2].." "..dimension[3].." "..dimension[4], tiViewport.debugLevel)
	self.mDimension = dimension
end

function tiViewport:getViewport()
	return self.mViewport
end

function tiViewport:enable (state)	
	self.mViewport:enable (state)
	if state then
		-- Set the visibility mask. It needs to be done each time because the visibility mask is reset when the viewport is disabled
		local visibilityMaskValue = self.mMask + tiViewport.VisibilityMask.All
		self.mViewport:setVisibilityMask (visibilityMaskValue)
	end
end

function tiViewport:getPosition()
	local x, y
	x = self.mViewport:getActualLeft()
	y = self.mViewport:getActualTop()
	return x, y
end

function tiViewport:getActualSize()
	local width = self.mViewport:getActualWidth()
	local height = self.mViewport:getActualHeight()
	return width, height
end

-- A voir si ce n'est pas redondant avec getPosition .....
function tiViewport:getOrigin()
	local windowWidth, windowHeight = tiWindowManager.getRenderTargetSize()
	log_debug ("tiViewport:getOrigin window: " .. windowWidth .. "x" .. windowHeight, tiViewport.debugLevel)
	return self.mDimension[1]*windowWidth, self.mDimension[2]*windowHeight
end

function tiViewport:getSize()
	local windowWidth, windowHeight = tiWindowManager.getRenderTargetSize()
	log_debug ("tiViewport:getSize window: " .. windowWidth .. "x" .. windowHeight, tiViewport.debugLevel)
	return self.mDimension[3]*windowWidth, self.mDimension[4]*windowHeight
end

function tiViewport:getDimensionNative()
	local err, screenScale = tiSystemInfo.get("screen-scale")
	if err ~= eOk then
		screenScale = 1
	end
	screenScale = tonumber(screenScale)
	local windowWidth, windowHeight = tiWindowManager.getRenderTargetSize()
	local left = tiMath.round (self.mDimension[1]*windowWidth/screenScale)
	local top = tiMath.round (self.mDimension[2]*windowHeight/screenScale)
	local width = tiMath.round (self.mDimension[3]*windowWidth/screenScale)
	local height = tiMath.round (self.mDimension[4]*windowHeight/screenScale)
log_debug ("tiViewport:getDimensionNative screenScale: "..screenScale.. " window: " .. windowWidth .. "x" .. windowHeight.. " vp: "..left.." "..top.." "..width.." "..height, tiViewport.debugLevel)
	return left, top, width, height
end

function tiViewport:setVisibilityMask (mask)
	log_debug ("tiViewport:setVisibilityMask: " .. mask, tiViewport.debugLevel)
	self.mMask = mask
end

function tiViewport:getVisibilityMask()
	return self.mMask
end

function tiViewport:normalizedIsIn (x, y)
	local res = false
	res = (x > self.mDimension[1]) and (x < self.mDimension[1]+self.mDimension[3]) and (y > self.mDimension[2]) and (y < self.mDimension[2]+self.mDimension[4])
	return res
end

function tiViewport:isIn (x, y)
	local res = false
	local sx, sy = tiWindowManager.getRenderTargetSize()
	local xn = x / sx
	local yn = y / sy
log_debug ("tiViewport:isIn x,y: "..x..","..y.." xn,yn: "..xn..","..yn.." vp: "..self.mDimension[1].." "..self.mDimension[2].." "..self.mDimension[1]+self.mDimension[3].." "..self.mDimension[2]+self.mDimension[4], tiViewport.debugLevel)
	res = (xn > self.mDimension[1]) and (xn < self.mDimension[1]+self.mDimension[3]) and (yn > self.mDimension[2]) and (yn < self.mDimension[2]+self.mDimension[4])
	return res
end

function tiViewport:pick (x, y)
	local obj = nil
	local windowWidth, windowHeight = tiWindowManager.getRenderTargetSize()
	local x0 = self.mDimension[1] * windowWidth
	local y0 = self.mDimension[2] * windowHeight
	obj = self.mViewport:pick(x-x0, y-y0, false, false)
	return obj
end

function tiViewport:reshape (notify)
	log_call ("tiViewport:reshape notify: "..tostring(notify), tiViewport.debugLevel)
	if self.mCamera ~= nil then
		local windowWidth, windowHeight = tiWindowManager.getRenderTargetSize()
		local bufferRotation, rotationToApply, inverted, videoCaptureRatio = self.mCamera:getVideoOrientation()

		log_debug ("tiViewport:reshape mWindow: " .. windowWidth .. "x" .. windowHeight, tiViewport.debugLevel)
		log_debug("tiViewport:reshape id: " .. self.mId .. " videoCaptureRatio: " .. videoCaptureRatio ..
				" vp: " .. self.mDimension[1] .. "," .. self.mDimension[2] .. " " .. self.mDimension[3] .. "x" .. self.mDimension[4], tiViewport.debugLevel)	
		if (notify) then
			-- We send a notification telling which viewport is selected and what are its coordinates
			for key, listener in pairs(mListeners) do
				listener.onViewportSelection (self.mId, self:getDimensionNative())
			end
		end
	
		-- Place and resize the viewport
		log_debug("tiViewport:reshape id: "..self.mId.." setDimensions: " .. self.mDimension[1] .. " " .. self.mDimension[2] .. " " .. self.mDimension[3] .. " " .. self.mDimension[4], tiViewport.debugLevel)
		self.mViewport:setDimensions(self.mDimension[1], self.mDimension[2], self.mDimension[3], self.mDimension[4])
		
		-- Update the video capture. In some configurations, we maybe cut a part of the video capture (in 2 or 3 viewports for example).
		local renderWindowRatio = (self.mDimension[3] * windowWidth) / (self.mDimension[4] * windowHeight)
		local renderVideoRatio = renderWindowRatio / videoCaptureRatio
	
		log_debug ("tiViewport:reshape id: "..self.mId.." renderWindowRatio: "..renderWindowRatio.." renderVideoRatio, "..renderVideoRatio, tiViewport.debugLevel)
		local resizeX, resizeY	
		if(renderVideoRatio > 1.0) then
			-- implies that the render window is proportionally larger than the video capture
			-- we have to crop vertically
			resizeX = 1.0
			resizeY = renderVideoRatio -- resizeY is always greater than 1.0
		else
			-- we have to crop horizontally
			resizeX = 1.0 / renderVideoRatio -- resizeX is always greater than 1.0 
			resizeY = 1.0
		end
		resizeX = resizeX * self.mScale
		resizeY = resizeY * self.mScale

		if self:getCenterLocked() then
			local cameraCenter = self.mCamera:getCenterNode()
			if cameraCenter:isNull() then
				log_warning ("tiViewport:reshape, no camera center to lock on")
				self:setCenterLocked(false)
			else
				local centerPos = Vector3()
				cameraCenter:getPosition (centerPos)
				log_pose ("tiViewport:reshape centerPos: ", centerPos, nil, tiViewport.debugLevel)
				local x, y = self.mViewport:getScreenPosition (centerPos)
				log_debug ("tiViewport:reshape screenpos: current: "..self.mCenter:getX().." "..self.mCenter:getY().." new rel: "..x.." "..y, tiViewport.debugLevel)
				self.mCenter = Vector2 (self.mCenter:getX()+x, self.mCenter:getY()+y)
			end
		end
		
		local centerX = self.mCenter:getX()
		local centerY = self.mCenter:getY()
log_debug ("tiViewport:reshape scale: "..self.mScale.." center: "..centerX.." "..centerY, tiViewport.debugLevel)
		local x = (1.0 - resizeX) / 2.0 - centerX/2
		local y = (1.0 - resizeY) / 2.0 + centerY/2
		log_debug("tiViewport:reshape id: "..self.mId.." setBackgroundVideoRect: " .. x .. " " .. y .. " " .. 1 * resizeX .. " " .. 1 * resizeY, tiViewport.debugLevel)
		self.mViewport:setBackgroundVideoRect(x, y, resizeX, resizeY);
		
		local x0, y0, x1, y1 = self.mViewport:getActualBackgroundVideoRect()
		log_debug("tiViewport:reshape id: "..self.mId.." getActualBackgroundVideoRect: " .. x0 .. "," .. y0 .. " " .. x1 .. "x" .. y1, tiViewport.debugLevel)
		self.mViewport:setZOrder(self.mIndex)
	else
		log_warning ("tiViewport:reshape camera not initialized for viewport "..self.mId)
	end
	log_call_end()
end

function tiViewport:initContent()
	log_call("tiViewport:initContent "..self.mId, tiViewport.debugLevel)
	if self.mCamera ~= nil then
		local texture = self.mCamera:getVideoTexture()
		local camera = self.mCamera:getCamera()	
		local err = eOk
		err = self.mViewport:setBackgroundTexture(texture)
		log_debug("tiViewport:initContent set texture on viewport "..self.mViewport:getName().." err: "..tostring(err), tiViewport.debugLevel)
		err = self.mViewport:setCamera(camera)
		log_debug("tiViewport:initContent set camera "..self.mCamera:getName().." on viewport "..tostring(self.mId).." err: "..tostring(err), tiViewport.debugLevel)
	end
	log_call_end()
end

function tiViewport:getCamera()
	return self.mCamera
end

function createViewport (index)
	local vp = Viewport(getCurrentScene():createObject(CID_VIEWPORT))
	vp:enable(true)
	vp:setName(tiViewport.viewportBaseName .. index)				
	vp:setRenderTarget (tiWindowManager.getRenderTarget()) 
--	vp:setBackgroundColor (index/5, index/5, index/5)
	vp:setVisibilityMask(-1)
	vp:setDimensions(0.0, 0.0, 1.0, 1.0)
	vp:setBackgroundVideoRect(0.0, 0.0, 1.0, 1.0)
	vp:setZOrder(index)
	vp:setScheme("Default")
	vp:setCamera(getCurrentScene():getObjectByName("tempCam"))
log_viewport(vp, "createViewport")				
	vp:enable(false)
	return vp
end

function tiViewport:getScreenPosition (pos)
	return self.mViewport:getScreenPosition(pos)
end

-- Vielle fonction de Diana utilisee par ti_getDrillingPoint

function tiViewport:convert2ImageCoords (pos)
	-- 3d point position in normalized screen coordinates
	local x = 0
	local y = 0
		
	x, y = self.mViewport:getScreenPosition(pos)
	
	-- bounds of normalized screen coordinates
	local MIN_SCREEN = -1;
	local MAX_SCREEN = 1;

-- Le swap suivant est potentiellement foireux, il dait dépendre de l'orientation du device	
	-- swap width and height, because image resolution is cam_heightxcam_widths
	local imageWidth = self.mCamera:getHeight()-- mCamera:getWidth()
	local imageHeight = self.mCamera:getWidth()-- mCamera:getHeight()
	
	local xImage = (x - MIN_SCREEN)/(MAX_SCREEN - MIN_SCREEN) * imageWidth
	-- flip, as image (0, 0) is centered in top left 
	local yImage = imageHeight - (y - MIN_SCREEN)/(MAX_SCREEN - MIN_SCREEN) * imageHeight
	return xImage, yImage
end

-- Compute a 3D point representing the intersection between the segment [origin, ray] and the plane defined by its position ands its normal
function computePlaneSegmentIntersection(origin, ray, planePosition, planeNormal)
	local dot = planeNormal:dotProduct(planePosition)
	local numerator = dot - planeNormal:getX() * origin:getX() - planeNormal:getY() * origin:getY() - planeNormal:getZ() * origin:getZ()
	local denominator = planeNormal:dotProduct(ray)
	
	if denominator == 0 or numerator == 0 then
		return nil
	else
		local res = numerator / denominator
		local position = Vector3()
		position:set(ray:getX() * res + origin:getX(), ray:getY() * res + origin:getY(), ray:getZ() * res + origin:getZ())
		return position
	end
end

-- Projection to get a 3D point from screen coordinates and ray casting
function tiViewport:getPositionFromScreen(dx, dy, vPlanePosition, vPlaneNormal)
	local x, y, z
	local vOrigin = Vector3()
	local vRay = Vector3()		-- Target
	local vPlane = Vector3()	-- Plane
	local vNormal = Vector3()	-- Normal of the plane
	local vPosition = Vector3()	-- New position
	local top, left, width, height, xCam, yCam, zCam, near, far
	
	width = self.mViewport:getActualWidth()
	height = self.mViewport:getActualHeight()
	
	self.mCamera:getPosition(vOrigin)			
	near = self.mCamera:getNearClip()
	far = self.mCamera:getFarClip()
	
	x = 2 * dx / width - 1
	y = 1 - 2 * dy / height
	z = vOrigin:distance(vPlanePosition)

	-- Compute the 3D point corresponding to the ray target
	self.mViewport:get3DPosition(x, y, vRay, z)
	
	if vRay ~= nil then				
		-- Compute the picked point defined by the intersection of the ray and the plane
		vPosition = computePlaneSegmentIntersection(vOrigin, vRay, vPlanePosition, vPlaneNormal)
--		log_debug("tiViewport:getPositionFromScreen "..dx..","..dy.." -> "..vPosition:getX()..","..vPosition:getY()..","..vPosition:getZ(), tiViewport.debugLevel)
		return vPosition
	end
	return nil
end

-- Project screen coordinate to plane
function tiViewport:projectOn3Dplane (vec, planeObject, axis)
	local normal = Vector3(0, 0, 0)
	local pos = Vector3(0, 0, 0)
	if axis == nil or axis == 'z' or axis == 'Z' then
		planeObject:getZAxis(normal, true)
	elseif axis == 'x' or axis == 'X' then
		planeObject:getXAxis(normal, true)
	else
		planeObject:getYAxis(normal, true)
	end
	planeObject:getPosition (pos)
	local pos3D = Vector3(0, 0, 0)
	pos3D = self:getPositionFromScreen (vec:getX(), vec:getY(), pos, normal)
	return pos3D
end

function tiViewport:setScale (scale, center)
	log_debug("tiViewport:setScale id: "..self.mId.." scale:"..scale.." centered: "..tostring(center), tiViewport.debugLevel)
	if center ~= nil then
		local x, y = self.mViewport:getScreenPosition (center)
		log_debug ("tiViewport:setScale asked   center: "..x.." "..y, tiViewport.debugLevel)
		log_debug ("tiViewport:setScale current center: "..self.mCenter:getX().." "..self.mCenter:getY(), tiViewport.debugLevel)
		self.mCenter = Vector2 (self.mCenter:getX()+x, self.mCenter:getY()+y)
		log_debug ("tiViewport:setScale new     center: "..self.mCenter:getX().." "..self.mCenter:getY(), tiViewport.debugLevel)
	else
--		self.mCenter = Vector2 (0, 0)
	end
	self.mScale = scale
end

function tiViewport:getScale()
	return self.mScale
end

function tiViewport:setCenter (center)
	log_debug("tiViewport:setCenter center: "..tostring(center), tiViewport.debugLevel)
	if center ~= nil then
		local x, y = self.mViewport:getScreenPosition (center)
		log_debug ("tiViewport:setCenter asked   center: "..x.." "..y, tiViewport.debugLevel)
		log_debug ("tiViewport:setCenter current center: "..self.mCenter:getX().." "..self.mCenter:getY(), tiViewport.debugLevel)
		self.mCenter = Vector2 (self.mCenter:getX()+x, self.mCenter:getY()+y)
		log_debug ("tiViewport:setCenter new     center: "..self.mCenter:getX().." "..self.mCenter:getY(), tiViewport.debugLevel)
	else
		self.mCenter = Vector2 (0, 0)
	end
end

function tiViewport:moveCenter (move)
	log_debug("tiViewport:setCenter2D center: "..tostring(move), tiViewport.debugLevel)
	if move ~= nil then
		log_debug ("tiViewport:setCenter current center: "..self.mCenter:getX().." "..self.mCenter:getY(), tiViewport.debugLevel)
		log_debug ("tiViewport:setCenter move          : "..move:getX().." "..move:getY(), tiViewport.debugLevel)
		self.mCenter = Vector2 (self.mCenter:getX()+move:getX(), self.mCenter:getY()+move:getY())
		log_debug ("tiViewport:setCenter new     center: "..self.mCenter:getX().." "..self.mCenter:getY(), tiViewport.debugLevel)
	end
end

function tiViewport:getCenter()
	return self.mCenter
end

function tiViewport:setup (config, keepOpened)
log_debug("tiViewport:setup config.id: "..config.id, tiViewport.debugLevel)
	vp.mId = config.id
	vp:setDimension (config.rectangle)
	local cam = tiCameraManager.getCurrentCamera (config.camera)
	if cam ~= nil then
		vp:setCamera (cam)
		-- If camera not yet opened, we will have to open it
		if not cam:isOpened() then
log_debug("tiViewportManager.setupViewports camera not yet opened, so open it: "..cam:getName(), tiViewport.debugLevel) 
			cam:open(false)
		end
--			if keepOpened then
			cam:play()
--			end
		vp:initContent()
		vp:reshape()
		vp:enable(true)
		-- A faire apres le enable, autrement pas pris en compte
		if config.color ~= nil then
			vp:setBackgroundColor (config.color.r, config.color.g, config.color.b, config.color.a)
		end
	else
		log_error ("tiViewport:setup camera "..tostring(config.camera).." does not exist")
	end
log_debug("tiViewport:setup viewport: "..self:toString(), tiViewport.debugLevel)
end

function tiViewport:toString()
	local s = ""
	s = s .. "mId: "..self.mId
	s = s .. " mIndex: "..self.mIndex
	s = s .. " Tiare VP: "
	local vp = self.mViewport
	if vp ~= nil then
--printStack()
		local x0, y0, x1, y1 = vp:getActualBackgroundVideoRect()
		s = s .. " "..vp:getName().." enable: "..tostring(vp:getEnabled())
		s = s .. " dim:"..vp:getActualLeft()..","..vp:getActualTop()..","..vp:getActualWidth()..","..vp:getActualHeight()
		s = s .. " actual vidrec:"..x0..","..y0..","..x1..","..y1
		s = s .. " zorder: "..vp:getCurrentZOrder().." mask: "..vp:getVisibilityMask().." scheme: "..vp:getCurrentScheme() 
	else
		s = "null viewport"
	end
	return s
end





