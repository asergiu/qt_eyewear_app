log_loaded_info("tiListenable.lua")

tiListenable = {
	logLevel = 5
}

function tiListenable:create (properties)
	log_debug("tiListenable:create properties: "..tostring(properties))
    local new_inst = {}    -- the new instance
    setmetatable( new_inst, { __index = tiCamera } ) -- all instances share the same metatable
	self.baseClasse.init (self, properties)
	new_inst:init (properties)
    return new_inst
end

function tiListenable:init(properties)
	log_debug("tiListenable:init "..tostring(self), tiListenable.logLevel)
	self.mListener = {}
	self.mName = "no name"
	if properties ~= nil then
		if properties.name ~= nil then
			self.mName = properties.name
		end
	end
end

function tiListenable:getName()
	return self.mName
end

function tiListenable:getListenerList()
	local s = ""
	for i, listener in pairs (self.mListener) do
		s = s..","..tostring(listener)
	end
	return s
end

function tiListenable:addListener (listener)
	log_debug("tiListenable:addListener cam: "..tostring(self:getName()).."("..tostring(self)..") listener: "..tostring(listener), tiListenable.logLevel)
--printStack()
	self.mListener[listener] = listener

	log_debug("tiListenable:addListener cam: "..tostring(self:getName()).."("..tostring(self)..") listeners: "..self:getListenerList(), tiListenable.logLevel)
end

function tiListenable:removeListener (listener)
	log_debug("tiListenable:removeListener cam: "..tostring(self:getName()).."("..tostring(self)..") listener: "..tostring(listener), tiListenable.logLevel)
--printStack()
	self.mListener[listener] = nil
	log_debug("tiListenable:removeListener cam: "..tostring(self:getName()).."("..tostring(self)..") listeners: "..self:getListenerList(), tiListenable.logLevel)
end

function tiListenable:removeListeners()
	log_debug("tiListenable:removeListeners cam: "..tostring(self:getName()).."("..tostring(self)..")", tiListenable.logLevel)
	self.mListener = {}
end

function tiListenable:fireEvent (event, params)
	log_call("tiListenable:fireEvent "..event, tiListenable.logLevel)
	log_debug("tiListenable:fireEvent cam: "..tostring(self:getName()).."("..tostring(self)..") listeners: "..self:getListenerList(), tiListenable.logLevel)
	for i, listener in pairs (self.mListener) do
		if type (listener.processEvent) == "function" then
			listener:processEvent (self, event, params)
		end
	end
	log_call_end()
end
