log_loaded_info("tiCameraPhoto.lua")

tiCameraPhoto = inheritsFrom (tiCamera)

function tiCameraPhoto:init(properties)
	log_debug ("tiCameraPhoto:init")
	self.mCameraType = "photo"
end

function tiCameraPhoto:setImageFromFile (filename)
	log_debug ("tiCameraPhoto:setImageFromFile "..filename)
	if self.mVideoTexture ~= nil then
		log_debug ("tiCameraPhoto:setImageFromFile ("..filename..") delete previous texture of camera "..self:getName())
		gScene:deleteObject(self.mVideoTexture)
		self.mVideoTexture = nil
	end
	self.mVideoTexture = Texture(gScene:createObject(CID_TEXTURE))
	local err = self.mVideoTexture:setResource (filename)
	self.mVideoTexture:load()
	self:fireEvent ("EventBackgroundChanged")
	return err
end

function tiCameraPhoto:setImage (image)
    local err = eOk
-- Actuellement sous Windows, les images sont inversée haut/bas quand alors que si on passe par fichier c'est OK ...
-- Tester le comportement sur iOS ...
	log_debug ("tiCameraPhoto:setImage "..self:getName().." "..tostring(image))
	if self.mVideoTexture == nil then
		log_debug ("tiCameraPhoto:setImage create camera texture: w: "..image:getWidth().." h: "..image:getHeight())
		self.mVideoTexture = Texture(gScene:createObject(CID_TEXTURE))
		self.mVideoTexture:setWidth (image:getWidth())
		self.mVideoTexture:setHeight (image:getHeight())
		self.mVideoTexture:setDepth (1)
		self.mVideoTexture:setNumMipmaps(0)
		self.mVideoTexture:load()
	end
--	image:set({1,0,0,0.5})
	image:copyToTexture (self.mVideoTexture)
	self:fireEvent ("EventBackgroundChanged")
	return err
end

function tiCameraPhoto:getVideoOrientation()
	log_debug ("tiCameraPhoto:getVideoOrientation", tiCamera.logLevel)
	local rotationToApply = DEVICE_ROTATE_NONE--self.mVideoCapture:getRotationToApply()
	local bufferRotation = DEVICE_ROTATE_NONE--self.mVideoCapture:getBufferRotation()				
	local inverted = 0--videoConfig:getInverted()
	local ratio = self:getWidth() / self:getHeight()	
	if ( (rotationToApply == DEVICE_ROTATE_90) or (rotationToApply == DEVICE_ROTATE_270)) then 
		-- if the video buffer is rotated
		ratio = 1.0 / ratio
	end
	log_debug("tiCameraPhoto:getVideoOrientation inverted: ".. inverted .. " rotationToApply: " .. rotationToApply .. " bufferRotation: "..bufferRotation.." ratio: "..ratio, 5)
	return bufferRotation, rotationToApply, inverted, ratio
end

function tiCameraPhoto:setModel(FOVy, ratio)
	log_debug("tiCameraPhoto:setModel FOVy: "..math.deg(FOVy)..", ratio: "..ratio)
	local cameraA1 = 0
	local cameraA2 = 0
	local cameraP1 = 0
	local cameraP2 = 0
	local cameraDistortion = false
	local cameraModel = 0
	local cameraSizeX, cameraSizeY = self:getVideoSize()
	local cameraCenterX = cameraSizeX/2
	local cameraCenterY = cameraSizeY/2
	local cameraFocalY = cameraSizeY/2 / math.tan (FOVy/2)
	local cameraFocalX = cameraFocalY
	log_debug("tiCameraPhoto:setModel maxx: "..cameraSizeX..","..cameraSizeY..", focaley: "..cameraFocalY)
	self.mCamera:setCameraModel (cameraModel, cameraSizeX, cameraSizeY, cameraCenterX, cameraCenterY, cameraFocalX, cameraFocalY, cameraA1, cameraA2, cameraP1, cameraP2, cameraDistortion)
	self.mCamera:applyCameraModel(true)
--	self:fireEvent ("EventBackgroundChanged")
end

function tiCameraPhoto:applyCamera()
log_debug("tiCameraPhoto:applyCamera")
	local refCamera = Camera(getCurrentScene():getObjectByName("tempCam"))	
	self.mCamera:setNearClip(refCamera:getNearClip())
	self.mCamera:setFarClip(refCamera:getFarClip())
	log_camera(self.mCamera, "tiCameraPhoto:applyCamera camera de base")
	self.mCamera:applyCameraModel(true)
--	self:fireEvent ("EventBackgroundChanged")
end

function tiCameraPhoto:open(notify)
	log_call ("tiCameraPhoto:open notify: "..tostring(notify))
	if notify == nil then
		notify = true
	end
	
	log_debug("tiCameraPhoto:open NE FAIT RIEN POUR L'INSTANT")
	
	self.mIsOpened = true
	if notify then
		self:fireEvent ("EventBackgroundChanged")
	end
	log_debug ("tiCameraPhoto:open success: "..self:getName())
	log_call_end()	
end



