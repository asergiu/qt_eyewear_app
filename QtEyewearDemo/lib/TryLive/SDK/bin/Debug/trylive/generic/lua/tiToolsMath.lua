log_loaded_info("tiToolsMath.lua")

tiMath = {}

-------------------------------------------------------------------------------
-- Compute a new AABB from four vector.
-- Just classic min max.
-------------------------------------------------------------------------------
function tiMath.computeAABB(a, b, c, d)
	local retMin = Vector3()
	local retMax = Vector3()
	--xMin
	retMin:setX(math.min(a:getX(), b:getX(), c:getX(), d:getX()))
	--yMin
	retMin:setY(math.min(a:getY(), b:getY(), c:getY(), d:getY()))
	--zMin
	retMin:setZ(math.min(a:getZ(), b:getZ(), c:getZ(), d:getZ()))
	--xMax
	retMax:setX(math.max(a:getX(), b:getX(), c:getX(), d:getX()))
	--yMax
	retMax:setY(math.max(a:getY(), b:getY(), c:getY(), d:getY()))
	--zMax
	retMax:setZ(math.max(a:getZ(), b:getZ(), c:getZ(), d:getZ()))
	return retMin, retMax
end

function tiMath.getSignedAngle(vec1, vec2)

	if math.abs(vec1:distance(vec2)) == 0 then
		return 0
	end
		
	local angle =  vec1:getRadianAngle(vec2)*57.2957
		
	if vec1:getX()*vec2:getY()-vec1:getY()*vec2:getX() >= 0 then
		return angle*-1
	end 
	
	return angle
end

function tiMath.round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

