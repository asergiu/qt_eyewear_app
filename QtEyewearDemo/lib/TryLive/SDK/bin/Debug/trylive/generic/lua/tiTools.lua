log_loaded_info("tiTools.lua")

--[[

Inheritance mechanism

1) The base object must provide a init method to create and initialize members:

function ClasseBase:init()
	self.m1 = 1
	self.m2 = 2
end

2) The base object must provide a create method:

function ClasseBase:create(properties)
    local new_inst = {}									-- the new instance
    setmetatable( new_inst, { __index = ClasseBase } )	-- all instances share the same metatable
	self:init(properties)
	new_inst:init (properties)
    return new_inst
end

3) Définition of the derivated object

ClasseFille1 = inheritsFrom (ClasseBase)

ClasseFille1:method1()
end
...

4) If necessary the derivated object can also provide an init method and taked parameters in the properties arg

5) Instanciation

local base = ClasseBase:create()
local derivated1 = ClasseFille1:create()
local derivated2 = ClasseFille1:create()


6) To call a mother method from a derivated class:
function derivatedClasse:the_method()
	self.baseClass.the_method (self)
	-- other stuff specific to derivated class ...
end

]]--

-- Create a new class that inherits from a base class
--
function inheritsFrom (baseClass)
log_debug("inheritsFrom "..tostring(baseClass))
    -- The following lines are equivalent to the SimpleClass example:

    -- Create the table and metatable representing the class.
    local new_class = {}
    local class_mt = { __index = new_class }

    -- Note that this function uses class_mt as an upvalue, so every instance
    -- of the class will share the same metatable.
    --
	
    function new_class:create (properties)
		LOG ("new_class:create "..tostring(new_class).." properties: "..tostring(properties))
        local newinst = {}
		baseClass.init(newinst, properties)
        setmetatable (newinst, class_mt)
		newinst:init (properties)
		newinst.initialized = true -- utile pour les singleton qui ne passent pas par un "create"
		newinst.baseClass = baseClass
        return newinst
    end
	
	-- Fonction à appeller dans la cas d'un singleton en début de function si on veut s'assurer qu'il a été initialisé
	
	function new_class:checkInit()
		if new_class.initialized == nil then
			new_class:init()
			new_class.initialized = true
		end
	end
	
    -- Return the class object of the instance
    function new_class:class()
        return new_class
    end

    -- Return the super class object of the instance
    function new_class:superClass()
        return baseClass
    end

    -- Return true if the caller is an instance of theClass
    function new_class:isa (theClass)
        local b_isa = false
        local cur_class = new_class

        while (nil ~= cur_class) and (false == b_isa) do
            if cur_class == theClass then
                b_isa = true
            else
                cur_class = cur_class:superClass()
            end
        end
        return b_isa
    end

    -- The following is the key to implementing inheritance:

    -- The __index member of the new class's metatable references the
    -- base class.  This implies that all methods of the base class will
    -- be exposed to the sub-class, and that the sub-class can override
    -- any of these methods.
    --
    if baseClass then
        setmetatable( new_class, { __index = baseClass } )
    end

    return new_class
end

-------------------------------------------------------------------------------
-- This function emulate a sleep method. Sleep doesn't in lua, so implements it.
-- Warning : Do not use the os.clock functions this functions return a "different"
-- time between each devices. Use the D'Fusion timer.
-------------------------------------------------------------------------------
function wait(iSecond)
	local scene = getCurrentScene()
	local waitStartTime = scene:getTime()
    repeat
        coroutine.yield()
	until (scene:getTime() - waitStartTime) >= iSecond
end


function pause (nb)
	for i = 0, nb do
		coroutine.yield()
	end
end

function split (source, delimiters)
        local elements = {}
        local pattern = '([^'..delimiters..']+)'
        string.gsub(source, pattern, function(value) elements[#elements + 1] =     value;  end);
        return elements
end

tiSystemInfo = {
	data = {}
}

function tiSystemInfo.init()
	-- Maybe missing, so put some default values
	tiSystemInfo.data["screen-scale"] = 1.0
	tiSystemInfo.data["os-type"] = getOSType()
	tiSystemInfo.data["os-description"] = getOSDescription()
	
	local sysInfo = {}	
	local erroraz, sysInfo = getSystemInfo("all")
	log_debug ("tiSystemInfo.init: "..sysInfo)
	if erroraz == eOk then
		local vals = split (sysInfo, ";")
		for i, v in pairs (vals) do
			local vv = split (v, "=")
			tiSystemInfo.data[vv[1]] = vv[2]
log_debug(">>>>> "..tostring(vv[1]).." : "..tostring(vv[2]))			
		end
	else
		LOG ('Erreur getSystemInfo("all")' .. tostring(erroraz))
	end
end
-- exemple sous iOS:
-- device-manufacturer = Apple
-- device-model = iPad4,1
-- device-name = iPad de Acep TryLive
-- has-back-camera=true
-- has-front-camera=true
-- has-front-camera = true
-- number-of-cameras = 2
-- number-of-processor-cores=2
-- screen-scale=2.0
-- system-name=iOS
-- system-version=11.0.3

-- example sous Android:
-- ApplicationInfo.sourceDir=/data/app/com.trylive.sample.eyewear-2/base.apk
-- a-priori-excluded-egl-schemes=none
-- a-priori-exclusion-on-egl-schemes=false
-- a-priori-exclusion-on-fsaa-support=false
-- android-api-version=18
-- android.os.Build.BOARD=universal7420
-- android.os.Build.BRAND=samsung
-- android.os.Build.CPU_ABI=arm64-v8a
-- android.os.Build.DEVICE=zeroflte
-- android.os.Build.DISPLAY=NRD90M.G920FXXU5EQCS
-- android.os.Build.FINGERPRINT=samsung/zerofltexx/zeroflte:7.0/NRD90M/G920FXXU5EQCS:user/release-keys
-- android.os.Build.HOST=SWDG4514
-- android.os.Build.ID=NRD90M
-- android.os.Build.MANUFACTURER=samsung
-- android.os.Build.MODEL=SM-G920F
-- android.os.Build.PRODUCT=zerofltexx
-- android.os.Build.TAGS=release-keys
-- android.os.Build.TYPE=user
-- android.os.Build.USER=dpi
-- device-manufacturer=SAMSUNG
-- device-model=SM-G920F
-- fsaa-is-supported=true
-- fsaa-max-value-allowed=8
-- has-back-camera=true
-- has-front-camera=true
-- number-of-cameras=2
-- number-of-processor-cores=8
-- system-name=andro...


function tiSystemInfo.get (info)
	local res = eOk
	local val = tiSystemInfo.data[tostring(info)]
	if val == nil then
		res = eFailed
	end
	return res, tostring(val)
end

tiSystemInfo.init()





