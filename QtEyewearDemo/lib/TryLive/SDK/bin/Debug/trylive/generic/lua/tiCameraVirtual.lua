log_loaded_info("tiCameraVirtual.lua")

tiCameraVirtual = inheritsFrom (tiCamera)

function tiCameraVirtual:init(properties)
	log_debug ("tiCameraVirtual:init")
	self.mCameraType = "virtual"

	if properties ~= nil then
		if properties.FOVy ~= nil then
			self.mCamera:setFOVy (math.rad(properties.FOVy))
		else
			log_warning ("tiCameraVirtual:init FOVy not defined for camera "..self:getName())
		end
		if properties.ratio ~= nil then
			self.mCamera:setAspectRatio (properties.ratio)
		else
			log_warning ("tiCameraVirtual:init ratio not defined for camera "..self:getName())
		end
	else
		log_warning ("tiCameraVirtual:init missing properties")
	end
end

function tiCameraVirtual:setModel(FOVy, ratio)
	log_debug("tiCameraVirtual:setModel FOVy:"..FOVy..", ratio: "..ratio)
	self.mCamera:setFOVy (math.rad(FOVy))
	self.mCamera:setAspectRatio (ratio)
end

function tiCameraVirtual:applyCamera()
log_debug("tiCameraVirtual:applyCamera")
	local refCamera = Camera(getCurrentScene():getObjectByName("tempCam"))	
	self.mCamera:setNearClip(refCamera:getNearClip())
	self.mCamera:setFarClip(refCamera:getFarClip())
	log_camera(self.mCamera, "tiCameraVirtual:applyCamera camera de base")
	-- self.mCamera:applyCameraModel(true)
	self:fireEvent ("EventBackgroundChanged")
end

function tiCameraVirtual:open(notify)
	log_call ("tiCameraVirtual:open notify: "..tostring(notify))
	if notify == nil then
		notify = true
	end
	
	log_debug("tiCameraVirtual:open NE FAIT RIEN POUR L'INSTANT")
	self:applyCamera()
	self.mIsOpened = true
	if notify then
		self:fireEvent ("EventBackgroundChanged")
	end
	log_debug ("tiCameraVirtual:open success: "..self:getName())
	log_call_end()	
end

function tiCameraVirtual:getVideoOrientation()
	log_debug ("tiCameraVirtual:getVideoOrientation", tiCamera.logLevel)
	local rotationToApply = DEVICE_ROTATE_NONE--self.mVideoCapture:getRotationToApply()
	local bufferRotation = DEVICE_ROTATE_NONE--self.mVideoCapture:getBufferRotation()				
	local inverted = 0--videoConfig:getInverted()
	local ratio = self.mConfig.ratio
	log_debug("tiCameraVirtual:getVideoOrientation inverted: ".. inverted .. " rotationToApply: " .. rotationToApply .. " bufferRotation: "..bufferRotation.." ratio: "..ratio, 5)
	return bufferRotation, rotationToApply, inverted, ratio
end



