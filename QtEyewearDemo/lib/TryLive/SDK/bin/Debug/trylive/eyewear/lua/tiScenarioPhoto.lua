log_loaded_info("tiScenarioPhoto.lua")

-- Management of the photo mode

tiScenarioPhoto = {
	-- Reference aux instance de manager externes
	mScenario = nil,
	-- Gestion des photos
	mPhoto = {},		-- Tableau de l'ensemble des photos en memoire
	mCurrentPhoto = {},	-- Tableau de photos courante, une par camera photo (indexe par le friendly name des camera)
	
	-- Gestion de la prise de photo
	mPhotoShotRequested = false,
	mPhotoToCapture = nil,
	mPhotoShotCamera = nil,
	mNumPhoto = 1,
	mFicNumPhoto = "",
	debugLevel = 5
}

function tiScenarioPhoto.initialize()
	tiScenarioPhoto.initPhotoCount()
end

function tiScenarioPhoto.initPhotoCount()
	tiScenarioPhoto.mFicNumPhoto = getUserPhotoPath ("tlPhotoCount.txt")
	if tiFile.exists (tiScenarioPhoto.mFicNumPhoto) then
		local f = assert(io.open(tiScenarioPhoto.mFicNumPhoto, "r"))
		if f ~= nil then
			tiScenarioPhoto.mNumPhoto = tonumber(f:read("*all"))
			if tiScenarioPhoto.mNumPhoto == nil then
				tiScenarioPhoto.mNumPhoto = 1
			end
			f:close()
		else
			log_erreur ("tiScenarioPhoto.initPhotoCount Can't read file "..tiScenarioPhoto.mFicNumPhoto)
		end
	else
		tiScenarioPhoto.mNumPhoto = 1
	end
end

function tiScenarioPhoto.incPhotoCount()
	tiScenarioPhoto.mNumPhoto = tiScenarioPhoto.mNumPhoto+1
	local f = assert(io.open(tiScenarioPhoto.mFicNumPhoto, "w"))
	if f ~= nil then
		f:write(tiScenarioPhoto.mNumPhoto)
		f:close()
	else
		log_erreur ("tiScenarioPhoto.incPhotoCount Can't write file "..tiScenarioPhoto.mFicNumPhoto)
	end
end

function tiScenarioPhoto.generatePhotoFileName()		
	local fileName = getUserPhotoPath ("tlPhoto"..string.format("%06d",tiScenarioPhoto.mNumPhoto)..".jpg")
	tiScenarioPhoto.incPhotoCount() 
	return fileName
end

function tiScenarioPhoto.reset()
	for cameraName, photo in pairs (tiScenarioPhoto.mCurrentPhoto) do
		tiScenarioPhoto.updatePhotoInfo (photo, cameraName)
		tiScenarioPhoto.mCurrentPhoto[cameraName] = nil 
	end
end
						
function tiScenarioPhoto.setPupilDistance (dist)
log_debug ("tiScenarioPhoto.setPupilDistancePhoto	"..dist)
	
	for cameraName, photo in pairs (tiScenarioPhoto.mCurrentPhoto) do
		local scaleFactor = dist/photo.mUserPD
log_debug ("tiScenarioPhoto.setPupilDistancePhoto scaleFactor: "..scaleFactor)
		local camera = tiCameraManager.getCurrentCamera (cameraName)
		local leftIris, rightIris = tiScenario.getIris (camera, true)
tiSceneGraph.print("tiScenarioPhoto.setPupilDistancePhoto debut")	
log_pose("tiScenarioPhoto.setPupilDistancePhoto debut leftIris",leftIris)			
		-- On met a scale la position local des assets
		-- locale car cette position ne doit pas prendre en compte une eventuelle distance d'ajustement due à un deplacement d'iris
		local position = Vector3()
		local orientation = Quaternion()
		local scale = Vector3()
		position, orientation = tiScenario.getAssetsPose (cameraName)
		local scaledPosition = Vector3()
		scaledPosition:set(position:getX()*scaleFactor, position:getY()*scaleFactor, position:getZ()*scaleFactor)
		tiScenario.setAssetsPose (cameraName, scaledPosition, orientation)

		--La distance d'ajustement doit aussi etre scalée
		
		local distAjust = tiScenario.getDistanceAdjustment (cameraName)
		distAjust = Vector3 (distAjust:getX()*scaleFactor, distAjust:getY()*scaleFactor, distAjust:getZ()*scaleFactor)
		tiScenario.setDistanceAdjustment (cameraName, distAjust)
		
		-- Update iris in the scene graph
	
		rightIris = Vector3 (rightIris:getX()*scaleFactor, rightIris:getY()*scaleFactor, rightIris:getZ()*scaleFactor)
		leftIris = Vector3 (leftIris:getX()*scaleFactor, leftIris:getY()*scaleFactor, leftIris:getZ()*scaleFactor)
log_pose("tiScenarioPhoto.setPupilDistancePhoto fin leftIris",leftIris)
		tiScenario.setIris (camera, leftIris, rightIris, true)
	
tiSceneGraph.print("tiScenarioPhoto.setPupilDistancePhoto fin")	
	-- Update the photo info with the scene graph
	
		tiScenarioPhoto.updatePhotoInfo (photo, cameraName)
		photo.mUserPD = dist
	end
end

----------------------------------------------------------------------------------
--	Photos management
----------------------------------------------------------------------------------

function tiScenarioPhoto.deletePhoto (photoId)
	local photo = tiScenarioPhoto.mPhoto[photoId]
	if photo ~= nil then
		photo:free()
		tiScenarioPhoto.mPhoto[photoId] = nil
	end
end

function tiScenarioPhoto.tryOnPhoto (cameraName, photoId, fileName, photoInfo)

	log_call ("tiScenarioPhoto.tryOnPhoto " .. tostring(photoId) .. " : " .. tostring(fileName) .. " on camera: "..tostring(cameraName))

	local camera = tiCameraManager.getCurrentCamera (cameraName)
	if camera ~= nil then
		if camera:getType() == "photo" then
		
			local photo
			
			-- Prefer photo internal data than those specified in the parameters
			-- So if id is already known, take it as is, else create a new photo with the parameters
			-- The filename may have changed externally, so update it

			if tiScenarioPhoto.mPhoto[photoId] == nil then
				photo = tiPhoto:create()
				photo:fromJSON (photoInfo)
				photo.mId = photoId
				tiScenarioPhoto.mPhoto[photoId] = photo
			else
				photo = tiScenarioPhoto.mPhoto[photoId]
			end
			if photo.mFileName ~= fileName then
				photo.mFileName = fileName
				photo.mImage = nil
			end

			-- Before switching from the current photo on the new one, save the current parameters (iris, etc.) in the previous current photo
			
			tiScenarioPhoto.updatePhotoInfo (tiScenarioPhoto.mCurrentPhoto[cameraName], cameraName)
			local camera = tiCameraManager.getCurrentCamera (cameraName)
			photo:applyToCamera (camera)

			tiScenario.setDistanceAdjustment (cameraName, Vector3(0,0,0))

			tiScenarioPhoto.mCurrentPhoto[cameraName] = photo

			-- Mise a jour du scene graphe
			
			log_debug("tiScenarioPhoto.tryOnPhoto photo position (".. photo.mPosition:getX() .. ", ".. photo.mPosition:getY() .. ", " .. photo.mPosition:getZ() .. ")")
			tiScenario.setAssetsPose (cameraName, photo.mPosition, photo.mOrientation)
			tiScenario.setIris (camera, photo.mIrisLeftPos, photo.mIrisRightPos, false) 
			tiScenario.setAssetsManualOffset (cameraName, photo.mManualMovePosition, photo.mManualMoveOrientation, 1.0)
			tiScenario.setDistanceAdjustment (cameraName, photo.mDistanceAdjustment)
			if photo.mReferenceEarPointSet then
				tiScenario.setReferenceEarPoints (cameraName, photo.mPosLeftReferenceEarPoint, photo.mPosRightReferenceEarPoint)
			else
				tiScenario.setReferenceEarPoints (cameraName, nil, nil)
			end	
			tiSceneGraph.getTopLevelNode():forceUpdate(true)
			
			-- Update the scene graph with the current PD
			
			tiScenarioPhoto.setPupilDistance (tiScenario.getPupilDistance())
			
			tiSceneGraph.print("tiScenarioPhoto.tryOnPhoto apres update scene graph ".. photoId.. " ".. fileName)				
			tiScenarioInteraction.update (cameraName)
			--tiSceneGraph.print("Apres init tryon photo")		
		else
			log_error ("tiScenarioPhoto.tryOnPhoto Type of camera "..tostring(cameraName).." is: "..camera:getType())
		end
	else
		log_error ("tiScenarioPhoto.tryOnPhoto Can't find camera "..tostring(cameraName))
	end
	log_call_end()		
end

function tiScenarioPhoto.updatePhotoInfo (photo, cameraName)
	log_debug ("tiScenarioPhoto.updatePhotoInfo camera: "..tostring(cameraName), tiScenarioPhoto.debugLevel)
for j,t in pairs(tiScenarioPhoto.mPhoto) do
t:printLog("avant update", tiScenarioPhoto.debugLevel)
end
	if photo ~= nil then
		log_debug ("tiScenarioPhoto.updatePhotoInfo iris avant id: "..photo.mId.." previous: "..photo.mIrisRightPos:getX()..","..photo.mIrisRightPos:getY()..","..photo.mIrisRightPos:getZ().." "..photo.mIrisLeftPos:getX()..","..photo.mIrisLeftPos:getY()..","..photo.mIrisLeftPos:getY().." "..photo.mDistanceAdjustment:getX()..","..photo.mDistanceAdjustment:getY()..","..photo.mDistanceAdjustment:getZ(), tiScenarioPhoto.debugLevel)
		local scenario = tiScenario
		
		photo.mPosition = scenario.getPosition (cameraName)
		photo.mOrientation = scenario.getOrientation (cameraName)
		photo.mUserPD = scenario.getPupilDistance()
		local camera = tiCameraManager.getCurrentCamera (cameraName)
		photo.mIrisLeftPos, photo.mIrisRightPos = scenario.getIris (camera)
		photo.mDistanceAdjustment = scenario.getDistanceAdjustment (cameraName)
		
		local manualMoveScale
		photo.mManualMovePosition, photo.mManualMoveOrientation, manualMoveScale = scenario.getAssetsManualOffset (cameraName)
		
		photo.mReferenceEarPointSet = scenario.isReferenceEarPointsSet (cameraName)
		if photo.mReferenceEarPointSet then
			photo.mPosLeftReferenceEarPoint, photo.mPosRightReferenceEarPoint = scenario.getReferenceEarPoints (cameraName)
		end

		log_debug ("tiScenarioPhoto.updatePhotoInfo iris apres id: "..photo.mId.." new     : "..photo.mIrisRightPos:getX()..","..photo.mIrisRightPos:getY()..","..photo.mIrisRightPos:getZ().." "..photo.mIrisLeftPos:getX()..","..photo.mIrisLeftPos:getY()..","..photo.mIrisLeftPos:getY().." "..photo.mDistanceAdjustment:getX()..","..photo.mDistanceAdjustment:getY()..","..photo.mDistanceAdjustment:getY(), tiScenarioPhoto.debugLevel)
	end
for j,t in pairs(tiScenarioPhoto.mPhoto) do
t:printLog("apres update", tiScenarioPhoto.debugLevel)
end			
end

function tiScenarioPhoto.getPhotoInfo (photoId)
	local photo = tiScenarioPhoto.mPhoto[photoId]
	local res = "nil"

	if photo ~= nil then
		for cameraName, photo in pairs (tiScenarioPhoto.mCurrentPhoto) do
			if photo.mId == photoId then
				tiScenarioPhoto.updatePhotoInfo (photo, cameraName)
			end
		end
		sendCallback("receivedPhotoInfo", photo.mId, photo.mFileName, photo:toJSON())
		res = photo:toJSON()
	else
		sendCallback("receivedPhotoInfo", -1, "", "")
		log_warning ("tiScenario.getPhotoInfo "..photoId..", photo = nil")
	end
	return res
end
			
function tiScenarioPhoto.takePhotoWithPose (cameraName, photoId, fileName)
	if fileName == nil or fileName == "" then
		fileName = tiScenarioPhoto.generatePhotoFileName() 
	end	
	log_debug("tiScenarioPhoto.takePhotoWithPose "..fileName.." from camera: "..tostring(cameraName))
tiSceneGraph.print("tiScenarioPhoto.takePhotoWithPose ".. photoId.. " ".. fileName)
	if not tiScenarioPhoto.mPhotoShotRequested then
		tiScenarioPhoto.mPhotoShotCamera = tiCameraManager.getCurrentCamera (cameraName)
		tiScenarioPhoto.mPhotoShotRequested = true
		tiScenarioPhoto.mPhotoToCapture = tiPhoto:create()
		tiScenarioPhoto.mPhotoToCapture.mFileName = fileName
		tiScenarioPhoto.mPhotoToCapture.mId = photoId
	end
	return tiScenarioPhoto.mPhotoToCapture
end
			
function tiScenarioPhoto.checkPhotoToCapture()
	if tiScenarioPhoto.mPhotoShotRequested then
		tiScenarioPhoto.mPhotoToCapture:captureVideoFrame (tiScenarioPhoto.mPhotoShotCamera)
		tiScenarioPhoto.mPhotoToCapture:saveImage()
-- Temporaire histoire d'etre sur de recharger à partir du fichier pour l'instant					
tiScenarioPhoto.mPhotoToCapture.mImage = nil					
	
		-- Store the pose information associated to this video frame if traking was running
	
		local cameraName = tiScenarioPhoto.mPhotoShotCamera:getFriendlyName()
		if (tiCVModuleManager.isTracking (cameraName)) then				
			tiScenarioPhoto.mPhotoToCapture.mPoseValide = true
			tiScenarioPhoto.updatePhotoInfo (tiScenarioPhoto.mPhotoToCapture, cameraName)
		else
			log_debug ("tiScenario.checkPhotoToCapture " .. "pose not valide")
			tiScenarioPhoto.mPhotoToCapture.mPoseValide = false
			tiScenarioPhoto.mPhotoToCapture.mPosition = Vector3()
		end
		tiDataRecorder.log("trylive_feature_begin", "snapshot_nonAR")

		tiScenarioPhoto.mPhoto[tiScenarioPhoto.mPhotoToCapture.mId] = tiScenarioPhoto.mPhotoToCapture
		tiScenarioPhoto.mPhotoShotRequested = false
		sendCallback("takePhotoEnded", tiScenarioPhoto.mPhotoToCapture.mId, tiScenarioPhoto.mPhotoToCapture.mFileName, tiScenarioPhoto.mPhotoToCapture:toJSON())
		tiScenarioPhoto.mPhotoToCapture = nil
	end
end

function tiScenarioPhoto.loopStep()
	tiScenarioPhoto.checkPhotoToCapture()
end
			
