log_loaded_info("tiProductManager.lua")

tiProductManager = {
	-- Tableau des produits chargés
	-- Indexé par le nom d'instance du produits
	-- Pour chaque produit on a un tableau des noms d'instance des assets correspondants
	mProductList = {},
}

function tiProductManager.getProductList()
	return tiProductManager.mProductList
end

function tiProductManager.findProduct (id)
	return tiProductManager.mProductList[id]
end

function productLoadingStatusChanged (status, productName)
	log_debug ("productLoadingStatusChanged product: "..tostring(productName)..", status: "..status)
	sendCallback("productStatusChanged", tostring(productName), status)
end

function tiProductManager.initialize()	
end

function tiProductManager.updateGeometry (productName, viewport, omaFile, thickness, radius, precision, rawRadius, panto)
log_call("tiProductManager.updateGeometry " .. tostring(productName))		
	local product = tiProductManager.findProduct (productName)
	if product ~= nil then
		for i, asset in pairs (product.assets) do
			tiAssetManager.updateGeometryAsset (asset.name, viewport, omaFile, thickness, radius, precision, rawRadius, panto)
		end
	else
		log_error("tiProductManager.updateGeometry not found " .. tostring(productName))	
	end
log_call_end()				
end


function tiProductManager.getDrillingPoints(productName, viewport)
log_call("tiProductManager.getDrillingPoints " .. tostring(productName))				
	local drills = {}
log_warning ("tiProductManager.getDrillingPoints NON IMPLEMENTE")
	-- local asset = getObjectByViewportAndName(viewport, assetName)
	-- if asset ~= nil then 
		-- drills = asset:getDrillingPoints()
	-- else
		-- dispatchError(TI_INTERNAL_ERROR, "Unable to find the asset: " .. assetName .. " in viewport: " .. viewport)
	-- end
log_call_end()				
	return drills 
end

function tiProductManager.sendArmOrientation(productName)
log_call("tiProductManager.sendArmOrientation " .. tostring(productName))	
	local armOrientation1 = nil
	local err1 = eOk
	local armOrientation2 = nil
	local err2 = eOk

	local product = tiProductManager.findProduct (productName)
	if product ~= nil then
		for i, asset in pairs (product.assets) do
			if armOrientation1 == nil then
				armOrientation1, err1 = tiAssetManager.getArmOrientation (asset.name)
			else
				armOrientation2, err2 = tiAssetManager.getArmOrientation (asset.name)
			end
		end
	end

	sendCallback("receivedArmOrientation", tostring(productName), armOrientation1[1], armOrientation1[2], armOrientation1[3], err1, armOrientation2[1], armOrientation2[2], armOrientation2[3], err2)
log_call_end()			
end

function tiProductManager.getFittingHeight(productName)
	log_call("tiProductManager.getFittingHeight " .. tostring(productName))	

	local fittingHeight = -1
	local err1 = eOk
	local err2 = eOk

	local product = tiProductManager.findProduct (productName)
	if product ~= nil then
		for i, asset in pairs (product.assets) do
			local fh = tiAssetManager.sendFittingHeight (asset.name)
			if fh > 0 then	
				fittingHeight = fh
			end
		end
log_debug("tiProductManager.getFittingHeight / face pivot: "..fittingHeight)		
		if fittingHeight < 0 then
			dispatchError(TI_INTERNAL_ERROR, "Impossible to retreive fitting height of: " .. tostring(productName))
		end		
	else
		dispatchError(TI_INTERNAL_ERROR, "Unable to find the product: " .. tostring(productName))
	end

	log_call_end()
	return fittingHeight
end

function tiProductManager.sendFittingHeight(productName)
log_call("tiProductManager.sendFittingHeight " .. tostring(productName))	

	local fittingHeight = tiProductManager.getFittingHeight(productName)

--	sendCallback("receivedArmOrientation", productName, armOrientation1[1], armOrientation1[2], armOrientation1[3], err1, armOrientation2[1], armOrientation2[2], armOrientation2[3], err2)
log_call_end()			
end

function tiProductManager.setArmRelativeOrientation (productName, dx, dy, dz)
log_call("tiProductManager.setArmRelativeOrientation " .. productName)	
	local product = tiProductManager.findProduct (productName)
	if product ~= nil then
		for i, asset in pairs (product.assets) do
			tiAssetManager.setArmRelativeOrientation (asset.name, dx, dy, dz)
		end
	end
log_call_end()		
end

function tiProductManager.unloadAllProducts (viewportId)
log_call("tiProductManager.unloadAllProducts viewportId: " .. tostring(viewportId))
	for i, product in pairs (tiProductManager.mProductList) do
		local a_gicler = false
		for j, asset in pairs (product.assets) do
			local a = tiAssetManager.getAsset(asset.name)
			if ((viewportId == nil) or (a:getViewport() == viewportId)) then
				a_gicler = true
				local res = tiAssetManager.unloadAsset (asset.name)
			end
		end
		if a_gicler then
			product.assets = {}
		end
	end
log_call_end()
end

function tiProductManager.unloadProduct (productName)
log_call("tiProductManager.unloadProduct " .. tostring(productName))
	local product = tiProductManager.findProduct (productName)
	if product ~= nil then
		for i, asset in pairs (product.assets) do
			res = tiAssetManager.unloadAsset (asset.name)
		end
		product.assets = {}
	end
--	tiProductManager.mProductList[productName] = nil
log_call_end()					
end

function tiProductManager.downloadProduct (remotePath)
	log_call("tiProductManager.downloadProduct remotePath: " .. tostring(remotePath))
	local productName = "anonymous"
	local res = eOk
	startChrono("tiProductManager.downloadProduct",tostring(productName))
	productLoadingStatusChanged(TI_LOADING_ASSET_DOWNLOADING, tostring(productName))
	for i, url in pairs (remotePath) do
		local assetName = productName .. "_" .. tostring(i)
		local res1 = tiAssetManager.downloadAsset(url)
		if res1 then
		else
			res = eFailed
		end
	end
	if res == eOk then
		productLoadingStatusChanged(TI_LOADING_ASSET_DOWNLOADED, tostring(productName))
	else
		productLoadingStatusChanged(TI_LOADING_ASSET_ERROR, tostring(productName))
	end
	stopChrono("tiProductManager.downloadProduct",tostring(productName))
	log_call_end()
	return res
end

local productCount = 0

function tiProductManager.loadProduct (id, remotePath, productName, viewport, isAccessory, omaFile, thickness, radius, precision, rawRadius, panto)
	log_call("tiProductManager.loadProduct id: ".. tostring(id).." remotePath: "..tostring(remotePath).." viewport: "..tostring(viewport).." accessory: "..tostring(isAccessory).." oma: "..tostring(omaFile).." thickness: "..tostring(thickness).." radius: "..tostring(radius).." precision: "..tostring(precision).." rawRadius: "..tostring(rawRadius).." panto: "..tostring(panto))
	local res = eOk
	local product = {}
	if remotePath ~= nil then
		if isAccessory == nil then
			isAccessory = false
		end
		if viewport == nil then
			viewport = tiViewportManager.getSelectedViewportId()
		end
		productLoadingStatusChanged(TI_LOADING_ASSET_LOADING, tostring(productName))
		startChrono("tiProductManager.loadProduct",tostring(productName))
		product.instanceName = productName
		product.id = id
		product.isAccessory = isAccessory
		product.assets = {}
		productCount = productCount+1
		for i, url in pairs (remotePath) do
			local asset = {}
			asset.name = productName .. "_" .. tostring(i)
			asset.url = url
			table.insert (product.assets, asset)
		end
		tiProductManager.mProductList[id] = product
	else
		-- Cas ou les produits on été prélaodés dans le tableau de produit et que les instanceName sont égaux aux ids ...
		product = tiProductManager.findProduct (id)
	end
	if product ~= nil then
		for i, asset in pairs (product.assets) do
			local res1 = tiAssetManager.loadAsset (asset.url, asset.name, viewport, product.isAccessory, omaFile, thickness, radius, precision, rawRadius, panto)
			if res1 == eOk then
				log_debug("tiProductManager.loadProduct asset.name : " .. asset.name)
			else
				res = eFailed
			end
		end
	end

	log_debug("tiProductManager.loadProduct store productName : " .. tostring(product.instanceName))

	if res == eOk then
		-- TODO get the mode from the current viewport/camera
		local mode = "unknown"
		local vp = tiViewportManager.getViewport (viewport)
		if vp ~= nil then
			local cam = vp:getCamera()
			if cam ~= nil then
				mode = cam:getType()
			end
		end
		tiDataRecorder.log("trylive_tryon", id, tostring(isAccessory), mode)
		productLoadingStatusChanged(TI_LOADING_ASSET_LOADED, product.instanceName) -- TODO add viewport here ?
	else
		productLoadingStatusChanged(TI_LOADING_ASSET_ERROR, product.instanceName)
	end
	stopChrono("tiProductManager.loadProduct",product.instanceName)
--tiProductManager.sendFittingHeight(product.instanceName)	
	log_call_end()
	
	return res
end


function tiProductManager.addProduct (productId, assetsUrls, properties)
	log_debug ("tiProductManager.addProduct "..tostring(productId).." "..tostring(assetsUrls).." "..tostring(properties))
	local product = {}
	product.instanceName = productId
	product.id = productId
	product.isAccessory = false
	product.properties = properties
	product.assets = {}
	for i, url in pairs (assetsUrls) do
		local asset = {}
		asset.name = productId .. "_" .. tostring(i)
		asset.url = url
		table.insert (product.assets, asset)
	end
	tiProductManager.mProductList[productId] = product
	
--	log_object (tiProductManager.mProductList)
end

function tiProductManager.findProductIdByProperty (field, value)
	local id = nil
	for i, p in pairs (tiProductManager.mProductList) do
		if p.properties ~= nil then
			if p.properties[field] == value then
				id = i
				break
			end
		end
	end
--log_debug ("tiProductManager.findProductIdByProperty field: "..tostring(field).." value: "..tostring(value).." id: "..tostring(id))
	return id
end

function tiProductManager.getProductsIdByProperty (field)
	local productsId = {}
	for i, p in pairs (tiProductManager.mProductList) do
		if p.properties ~= nil then
			if p.properties[field] ~= nil then
				productsId[p.properties[field]] = i
			end
		end
	end
	return productsId
end

