log_loaded_info("tiInputTargetScale.lua")

tiInputTargetScale = inheritsFrom (tiInputTarget)

tiInputTargetScale.Target = {None = 0, LeftEye = 1, RightEye = 2}

function tiInputTargetScale:init (properties)
	log_debug("tiInputTargetScale:init "..tostring(self).." properties: "..tostring(properties))		
	local trackingPivotName = "object3d_face_main_pivot"
	self.mTargets[tiInputTargetScale.Target.LeftEye] = tiTarget:create({nodeName = "targetPupilLeft", overlay = "LeftEyeTargetOverlay", offsetX = 1/4, offsetY = 1/4, parentNode = trackingPivotName})
	self.mTargets[tiInputTargetScale.Target.RightEye] = tiTarget:create({nodeName = "targetPupilRight", overlay = "RightEyeTargetOverlay", offsetX = 1/4, offsetY = 1/4, parentNode = trackingPivotName})
end

-- Set distance of glasses when dragging eyes
-- 'other' remains static 
function tiInputTargetScale:mouseMoveTargetEye (viewport, target, otherTarget, move)
	if move:magnitude() > 0 then			
		-- Get eye targets position to get current pixel PD
		local firstEyePos = target:getPosition2D(viewport)   
		local otherEyePos = otherTarget:getPosition2D(viewport)   
		local pdPixelPrev = firstEyePos:distance(otherEyePos)
		
--log_debug("tiInputTargetScale.mouseMoveTargetEye prevpos: "..eyePrev:getX()..","..eyePrev:getY().." move: "..move:getX()..","..move:getY())
		-- move target
		target:move2D (move)

		-- Get eye targets new position to get new pixel PD
		firstEyePos = target:getPosition2D(viewport)   
		otherEyePos = otherTarget:getPosition2D(viewport)   
		local pdPixelCur = firstEyePos:distance(otherEyePos)
		log_debug ("tiInputTargetScale.mouseMoveTargetEye iris1: "..firstEyePos:getX()..","..firstEyePos:getY().." iris2: "..otherEyePos:getX()..","..otherEyePos:getY().." PDpix: "..pdPixelCur, tiInputProcessor.logLevel)					

		-- Compute the scale change based on PD change
		local scale = pdPixelPrev/pdPixelCur
		
		-- Pivot contains its own position (the pose computed by the tracking)
		-- Pivot is impacted by a higher level node scalePivot that contains the distance adjustment due to iris pixel PD adjustment
		-- NewPivot = OldPivot * newPD/oldPD
		-- NewPivot-OldPivot = OldPivot * (newPD/oldPD -1) = translation to apply to scalePivot
		
		local facePivot = tiSceneGraph.getFaceMainPivotNode(viewport)
		facePivot:forceUpdate(true)			
		local facePivotPos = Vector3()
--			facePivot:getPosition(facePivotPos, facePivot:getParent())
--			log_debug("tiInputTargetScale.mouseMoveTargetEye: facePivot: " .. facePivotPos:getX() .. " ".. facePivotPos:getY() .. " ".. facePivotPos:getZ())
		facePivot:getPosition(facePivotPos)		
--			log_debug("tiInputTargetScale.mouseMoveTargetEye: facePivot+scalePivot: " .. facePivotPos:getX() .. " ".. facePivotPos:getY() .. " ".. facePivotPos:getZ())
		local scalePivot = tiSceneGraph.getPupilPivotNode()
		scalePivot:translate(facePivotPos:getX()*(scale-1), facePivotPos:getY()*(scale-1), facePivotPos:getZ()*(scale-1)) -- TODO fix me
		scalePivot:forceUpdate(true)
		
		-- project overlay coordinates
		local eyeTarget = target:getNode()
		local curEyeVec = Vector3(viewport:projectOn3Dplane (firstEyePos, eyeTarget)) 
		eyeTarget:setPosition(curEyeVec:getX(),curEyeVec:getY(),curEyeVec:getZ())
		
		local otherEyeTarget = otherTarget:getNode()
		local curOtherEarVec = Vector3(viewport:projectOn3Dplane (otherEyePos, otherEyeTarget)) 
		otherEyeTarget:setPosition(curOtherEarVec:getX(),curOtherEarVec:getY(),curOtherEarVec:getZ())
		--log_debug("tiInputTargetScale.mouseMoveTargetEye: curOtherEarVec: " .. curOtherEarVec:getX() .. " ".. curOtherEarVec:getY() .. " ".. curOtherEarVec:getZ())
	end			
end

function tiInputTargetScale:applyInputData (inputData, viewport)
	log_debug("tiInputTargetScale:applyInputData", tiInputProcessor.logLevel)
	local otherTarget = nil
	if inputData.move:magnitude() > 0 then
		if inputData.target == self.mTargets[tiInputTargetScale.Target.RightEye] then
			otherTarget = self.mTargets[tiInputTargetScale.Target.LeftEye]
		else
			otherTarget = self.mTargets[tiInputTargetScale.Target.RightEye]
		end
		tiInputTargetScale:mouseMoveTargetEye (viewport, inputData.target, otherTarget, inputData.move)
	end		
end



		
		
		
