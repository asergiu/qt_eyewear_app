log_loaded_info("tiCustomerConfigManager.lua")
-- This class manages all the customer data.
-- This data contain :
--   - UI information : the path to the UI
--   - Features : The features available for this player.
-- The differents features are share ( for the photo), tryOnPhoto (not use on mobile),
-- tryOnVideo (not use in mobile), compare (multiviewports), customizable (customizable assets).
-- Many lua files will regulary check a feature with the function "isAvailaibleFeatures".

tiCustomerConfigManager = {
	-- The available feature enum
	TI_FEATURES_NAME = 
	{
		TI_SHARE = "share",
		TI_TRYONPHOTO = "tryOnPhoto",
		TI_TRYONVIDEO = "tryOnVideo",
		TI_COMPARE = "compare",
		TI_CUSTOMIZE = "customizable"
	},
	mInstance = nil,
	getInstance = function()
log_call("tiCustomerConfigManager.getInstance")	
		if(tiCustomerConfigManager.mInstance == nil) then
			local this = {}
			
			local mFeaturesAvailable = {share = false, tryOnPhoto = false, tryOnVideo = false, compare = false, customizable = false}			
			
			local mSaveCustomerConfigPath = ""
			local mCustomerConfigFileName = ""
			local mDataCustomer = {}
			
			local mIsInit = false
			
			-- This function read the configuration (from json file) and set true or false to the good feature.
			local function checkFeatures()
			
				if mDataCustomer["features"] then
					for fea,val in pairs(mFeaturesAvailable) do
						if mDataCustomer["features"][fea] then
							mFeaturesAvailable[fea] = mDataCustomer["features"][fea]
						end
					end
				end
			end
			
			function this.isInit()
				log_call("tiCustomerConfigManager.isInit")
				log_call_end()
				return mIsInit
			end
			
			function this.setCustomerConfigPath(customPath)
			
				if mIsInit then
					dispatchError(TI_INCONSISTENT_COMMAND, "This function is authorized only once!")
					return
				end
				log_info("setCustomerConfigPath to "..customPath)
				
				-- Split the path and the file name.
				mSaveCustomerConfigPath = tiFile.dirName(customPath)
				mCustomerConfigFileName = tiFile.baseName(customPath)
				
				-- Begin the init sequence. Get the datas ...
				this.getConfigData()
				
				coroutine.yield()
				tiScenario.initialize(this)
				mIsInit = true
				sendCallback("tryliveStatusChanged",TI_INIT_FINISHED_APPLI_READY)
			end
			
			-- This function get the json file and init the value in it.
			function this.getConfigData()
				log_call("tiCustomerConfigManager.getConfigData")				
				
				coroutine.yield()
				if mSaveCustomerConfigPath == "" or mSaveCustomerConfigPath == nil then 
					return false;
				end
				
				local result = nil
				local textFile = nil
				result, textFile = tiNetwork.downloadText (mSaveCustomerConfigPath .. mCustomerConfigFileName)
				
				if result then
					log_info("download successfully ended, customer config file " .. mSaveCustomerConfigPath .. mCustomerConfigFileName)
				else
					dispatchError(TI_INCONSISTENT_PARAMETERS, "Error during download JSON customer file, remote path : " .. mSaveCustomerConfigPath .. " .")
					return false
				end
				-- Get the data in JSON string
				if( textFile == nil ) then
					dispatchError(TI_INTERNAL_ERROR, "Error during download JSON customer file, the file text saas is empty.")
					return false
				end	
				-- A text is decode, a file is load (see bellow)
				mDataCustomer = json.decode(textFile);	
log_object (mDataCustomer, "CUSTOMER CONFIG")		
				if mDataCustomer == nil then
					return false
				end
				
				coroutine.yield()
				-- Check the content of the table and read it
				if checkFeatures() == false then
					return false
				end
				
				coroutine.yield()
				log_call_end()
				return true
			end
			
			function this.clearCustomerConfig()
			
			    mDataCustomer = {}
				mFeaturesAvailable = {}
				mFeaturesAvailable = { screenshot=false, video=false, zoom=false, tryOnPhoto=false, tryOnVideo=false,
										recalibrateButton=false, referenceInVideo=false, referenceInPicture=false,
										tryAndCompare=false, cameraZoom=false}
			end
			
			function this.isAvailaibleFeatures(features)
				if mFeaturesAvailable[features] then
					return mFeaturesAvailable[features]
				else
					return false
				end
			end
			
			function this.setAvailaibleFeatures(features, val)
				if mFeaturesAvailable[features] then
					mFeaturesAvailable[features] = val
				end
			end
			
			function this.getTrackingVersion()
				if mDataCustomer["tracking"] == nil then
					return "FT1"
				else
					return mDataCustomer["tracking"]
				end
			end
			
			function this.getMarkerSize()
				if mDataCustomer["marker_size"] == nil then
					return 0
				else
					return mDataCustomer["marker_size"]
				end
			end
			
			local function tonumberbutnevernil (s)
				local v = tonumber (s)
				if v ==  nil then
					v = 0
				end
				return v
			end
			
			function this.get3DAssetsOffset()
				local offset = {0, 0, 0}
				if mDataCustomer["offsets"] ~= nil then
					if mDataCustomer["offsets"]["3d"] ~= nil then
						local con = mDataCustomer["offsets"]["3d"]
						offset = {tonumberbutnevernil(con["x"]), tonumberbutnevernil(con["y"]), tonumberbutnevernil(con["z"])}
					end
				end
--log_object(mDataCustomer,"mDataCustomer")
--log_object(offset,"offset")

				return offset
			end
			
			function this.get3DLiteAssetsOffset()
				local offset = {0, 0, 0}
				if mDataCustomer["offsets"] ~= nil then
					if mDataCustomer["offsets"]["3dlite"] ~= nil then
						local con = mDataCustomer["offsets"]["3dlite"]
						offset = {tonumberbutnevernil(con["x"]), tonumberbutnevernil(con["y"]), tonumberbutnevernil(con["z"])}
					end
				end

				return offset
			end
			
			tiCustomerConfigManager.mInstance = this
		end
log_call_end()		
		return tiCustomerConfigManager.mInstance
	end

}
