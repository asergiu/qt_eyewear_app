log_loaded_info("tiScenario.lua")
-- The main file.
-- This class manages the all scenario and manage the global loop (input manager, performance manager, tracking ...)

-- MULTICAM
-- Un certain nombre de fonction prennent en parametre une camera
-- Elle n'est pas utilisee pour l'instant mais c'est en prevision de viewports ayant un background independant et donc une camera differente
-- Pour l'instant il c'est l'unique camera courrante qui est prise en compte à la place de ce parametre



local debugOn = false
local imageName = "image"
local imageNumber = 1
local numPhoto = 1

function saveScreenToImage(title)
	if title == nil then
		title = ""
	end
	local name
	local base = "C:\\Users\\Pascal\\AppData\\Roaming\\Total Immersion\\Statistics\\"
	local base = ""
--	name = base .. imageName .."f".. string.sub(string.format("00000%d", imageNumber),-5) .. string.gsub(title,'/','_',10) .. ".jpg"
-- Sauve aussi ce qu'il y a eventuellement qu dessus de la fenetre Tiare, par exemple une autre fenetre
--	RenderWindow(getCurrentScene():getMainView()):dump(name, true)
--	log_debug("Save screenshot : "..name)
	name = base .. imageName.."b" .. string.sub(string.format("00000%d", imageNumber),-5) .. string.gsub(title,'/','_',10) .. ".jpg"
--	RenderWindow(getCurrentScene():getMainView()):dump(name, false)
	name = base .. imageName.."n" .. string.sub(string.format("00000%d", imageNumber),-5) .. string.gsub(title,'/','_',10) .. ".jpg"
--	RenderWindow(getCurrentScene():getMainView()):dumpNextFrame(name)
--	log_debug("Save screenshot : "..name)
	imageNumber = imageNumber+1
end

local imageCountDown = 0

function saveScreen()
	imageCountDown = 2 -- 2 pour dumpNextFrame, pour dump(true) c'est 3 et dump(false) c'est 5
end

function saveScreenIfNeeded()
--	imageCountDown = imageCountDown-1
--	if imageCountDown == 0 then
		if debugOn then
			saveScreenToImage("")
		end
--	end
end



-- Tracking status
TI_TRACKING_STATUS_TRACKED = "tracked"
TI_TRACKING_STATUS_NOT_TRACKED = "not tracked"
TI_TRACKING_STATUS_WAITING = "wait"
TI_TRACKING_STATUS_TOO_CLOSE = "too close"
TI_TRACKING_STATUS_TOO_FAR = "too far"
TI_TRACKING_STATUS_NOT_CENTERED = "not centered"

local mLastStatusSend = ""

function trackingStatusChanged(trackingStatus)
	if mLastStatusSend ~= trackingStatus then
		sendCallback("trackingStatusChanged",trackingStatus)
		if trackingStatus == "tracked" then
			tiDataRecorder.log("trylive_track_begin")
		elseif trackingStatus == "not_tracked" then
			tiDataRecorder.log("trylive_track_end")
		end
	end
	mLastStatusSend = trackingStatus
end

-- Memorize the previous value send for avoid to send this callback is nothing change.
local odlNumVP = 0
local oldX = 0
local oldY = 0
local oldWidth = 0
local oldHeight = 0

function viewportSelectedChanged(viewport, x, y, width, height)
	if odlNumVP ~= viewport or oldX ~= x or oldY ~= y or oldWidth ~= width or oldHeight ~= height then
		sendCallback("selectedViewportChanged", viewport, x, y, width, height)
		odlNumVP = viewport 
		oldX = x 
		oldY = y 
		oldWidth = width 
		oldHeight = height
	else
--		log_debug("viewportSelectedChanged no change: "..viewport.." "..x.." "..y.." "..width.." "..height)
	end
end

function executeDeveloperScript1()
	-- Load d'un script spécifique au développeur, ou il peut notamment régler son propre PD
	
	local developerFile = "./trylive/eyewear/lua/Developer.lua"
	developerFile = getScriptDirectory().."Developer.lua"
	local runFile = nil
	if (not pcall (function()
					runFile = assert(loadfile(developerFile))
					log_warning("Load developer script fileName: '"..developerFile.."' => res = "..tostring(runFile))
				end
				)) then 
					log_debug("Load developer script fileName: '"..developerFile.."' => res = "..tostring(runFile))
		runFile = nil
	end
	if runFile ~= nil then
		runFile()
	end
end

function executeDeveloperScript()
	-- Load d'un script spécifique au développeur, ou il peut notamment régler son propre PD
	
	local developerFile = "Developer.lua"
	developerFilePath = getScriptDirectory()..developerFile
	developerFilePath2 = getAppDataPath (developerFile)
	
	if File.check (developerFilePath) then
		File.copy (developerFilePath, developerFilePath2)
		local runFile = nil
		if (not pcall (function()
						runFile = assert(loadfile(developerFilePath2))
						log_warning("Load developer script fileName: '"..developerFilePath2.."' => res = "..tostring(runFile))
					end
					)) then 
						log_debug("Load developer script fileName: '"..developerFilePath2.."' => res = "..tostring(runFile))
			runFile = nil
		end
		if runFile ~= nil then
			runFile()
		end
	end
end

tiScenario = {
	Mode = {None = "0", Live = "1", FaceAnalysis = "2", Visagism = "3", Photo = "4"},
	View = {Single = "Single", DualVertical = "DualVertical", TriVertical = "TriVertical", Quad = "Quad", DualHorizontal = "DualHorizontal"},

	mIsInit = false,
	mCustomerConfigManager = nil,
	mCurrentView = "",
	mPupils = {},
	mDisplayFacePoints = false,
	mPhotoCamera1 = "photo 1",
	irisLogLevel = 5,
	debugScale = false,
	printSceneGraphCmpt = 0,
	nbViewport = 16
}

-- Init the scenario (the first init commands)
function tiScenario.initialize()
	log_call("tiScenario.initialize")

	tiDataRecorder.log("trylive_begin")
	
	-- Init default camera
	tiSceneGraph.initialize (tiScenario.nbViewport)
	tiViewportManager.initialize (tiScenario.nbViewport)
	tiCameraManager.initialize() 
				
	tiScenario.mCustomerConfigManager = tiCustomerConfigManager.getInstance()
	
	tiAssetManager.initialize()
	tiAssetManager.set3DAssetsOffset (tiScenario.mCustomerConfigManager.get3DAssetsOffset())
	tiAssetManager.set3DLiteAssetsOffset (tiScenario.mCustomerConfigManager.get3DLiteAssetsOffset())
	tiAssetManager.setAssetsVisibility (true)

	tiScenarioInteraction.initialize()

	-- View manager
	tiViewportManager.registerListener(tiScenario)
	
	tiScenarioPhoto.initialize()
	
	-- Create default cameras, font and back are already created by the camera manager
	tiCameraManager.setCameraConfig ("Photo 1", tiScenario.mPhotoCamera1, tiCameraManager.CameraType.Photo)

	tiScenario.createDefaultViews()
	
	-- Create default CM module instances
	tiCVModuleManager.createModule ("Tracking front", tiFaceTracking, tiScenario)
	tiCVModuleManager.createModule ("Tracking back", tiFaceTracking, tiScenario)
	tiCVModuleManager.attachCamera ("front", "Tracking front")
	tiCVModuleManager.attachCamera ("back", "Tracking back")
	tiScenario.mIsInit = true

	if tiScenario.debugScale then
		log_debug("tiScenario.initialize DEBUG ANDROID SCALE")
		tiTarget.debug = true
		tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TargetScale].mTargets[tiInputTargetScale.Target.LeftEye]:setVisible(true)
		tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TargetScale].mTargets[tiInputTargetScale.Target.RightEye]:setVisible(true)
		local objFace = Object3D(gScene:createObject(CID_OBJECT3D))
		tiSceneGraph.getFaceMainPivotNode(1):addChild(createObjectRepere3D(100))
	end			

	executeDeveloperScript()
	
	log_version()					
	log_call_end()				
end
		
function tiScenario.isInit()
	return tiScenario.mIsInit
end
			
----------------------------------------------------------------------------------
--	Viewports management
----------------------------------------------------------------------------------
			
function tiScenario.createDefaultView (mode, camera)
	tiViewportManager.addView (mode.." "..tiScenario.View.Single, {
						{id = "1", camera = camera, rectangle = {0, 0, 1, 1}}
						})
	tiViewportManager.addView (mode.." "..tiScenario.View.DualVertical, {
						{id = "1", camera = camera, rectangle = {0, 0, 0.5, 1}}, 
						{id = "2", camera = camera, rectangle = {0.5, 0, 0.5, 1}}
						})
	tiViewportManager.addView (mode.." "..tiScenario.View.Quad, { 
						{id = "1", camera = camera, rectangle = {0, 0, 0.5, 0.5}}, 
						{id = "2", camera = camera, rectangle = {0.5, 0, 0.5, 0.5}}, 
						{id = "3", camera = camera, rectangle = {0, 0.5, 0.5, 0.5}}, 
						{id = "4", camera = camera, rectangle = {0.5, 0.5, 0.5, 0.5}}
						})
	tiViewportManager.addView (mode.." "..tiScenario.View.DualHorizontal, {
						{id = "1", camera = camera, rectangle = {0, 0, 1, 0.5}}, 
						{id = "2", camera = camera, rectangle = {0, 0.5, 1, 0.5}}
						})
	tiViewportManager.addView (mode.." "..tiScenario.View.TriVertical, {
						{id = "1", camera = camera, rectangle = {0, 0, 1, 1/3}}, 
						{id = "2", camera = camera, rectangle = {0, 1/3, 1, 1/3}}, 
						{id = "3", camera = camera, rectangle = {0, 2/3, 1, 1/3}}
						})
--	tiViewportManager.addView (mode.." "..tiScenario.View.Quad, createViewportConfig (25, camera))
	
end

function createViewportConfig (nb, camera)
	local nbX, nbY = math.floor (math.sqrt (nb)-0.01)+1
	local nbY = nbX
	local stepX = 1/nbX
	local stepY = 1/nbY
	local ind = 0
	local vpconf = {}
	for x = 1, nbX do
		for y = 1, nbY do
			ind = ind+1
			local rectangle = {(x-1)*stepX, (y-1)*stepX, stepX, stepY}
			local vp = {id = tostring(ind), camera = camera, rectangle = rectangle}
			vpconf[ind] = vp 
		end
	end
	return vpconf
end
		
function tiScenario.createDefaultViews()			
	tiScenario.createDefaultView ("Front", "front")
	tiScenario.createDefaultView ("Back", "back")
	tiScenario.createDefaultView ("Photo", tiScenario.mPhotoCamera1)
end
		
function tiScenario.setViewMode(viewportMode, selectedViewport, keepOpened)
	log_call("tiScenario.setViewMode viewportMode: " .. viewportMode .. " selectedViewport: " .. tostring(selectedViewport).." keepOpened: "..tostring(keepOpened))
	if keepOpened == nil then
		keepOpened = true
	end
	if tiViewportManager.setViewMode(viewportMode, selectedViewport, keepOpened) ==  eOk then
		tiScenario.mCurrentView = viewportMode
		tiDataRecorder.log("trylive_feature_begin", "set_view", viewportMode)	
		for camName, cam in pairs (tiCameraManager.getCurrentCameras ("photo", true)) do
			tiScenarioInteraction.update (camName)
		end
	end
	log_call_end()
end

-- only used in Trylive 5
function tiScenario.getViewModes()
	local modes = {}
	modes = tiViewportManager.getModes()

	local modesString = ""
	for mode, config in pairs(modes) do
		modesString = modesString .. config .. ";"
	end

	log_debug("tiScenario.getViewModes nb: " .. #modes .. " modesString: " .. modesString)

	sendCallback("receivedViewModes", #modes, modesString)
end
			
function tiScenario.getSelectedViewport()
	return tiViewportManager.getSelectedViewportId()
end
			
function tiScenario.onViewportSelection(selectedViewport, x, y, width, height)
--	log_call ("tiScenario.onViewportSelection " .. selectedViewport .. " " .. x .. "," .. y .. " " .. width .. "x" .. height)			
	viewportSelectedChanged(selectedViewport, x, y, width, height)
--	log_call_end()				
end
			
----------------------------------------------------------------------------------
--	Scene graph management
----------------------------------------------------------------------------------

function tiScenario.showAllAssets()
	tiAssetManager.setAssetsVisibility(true)
end
			
function tiScenario.hideAllAssets()
	tiAssetManager.setAssetsVisibility(false)
end
			
function tiScenario.getDrillingPoints (assetName, viewportIndex)
	local drillingPoints3D = tiAssetManager.getDrillingPoints(assetName, viewportIndex)
	local drillingPoints2D = {}
	local viewport = tiViewportManager.getViewport (viewportIndex)
	for index, pos3D in pairs(drillingPoints) do
		local x, y = viewport:convert2ImageCoords(pos3D)
		local pos2D = Vector2 (x, y)
		table.insert (drillingPoints2D, pos2D)
	end
	return drillingPoints2D
end
			
function tiScenario.getPosition (cameraName)		
	log_debug ("tiScenario.getPosition MULTICAM camera ignored "..tostring(cameraName))
	local val = Vector3()
	local tracker = tiCVModuleManager.getModuleFromCamera (cameraName)
	if tracker ~= nil and tracker.getPosition ~= nil then
		val = tracker:getPosition()
	else
tiSceneGraph.print("tiScenario.getPosition")				
		local faceTrackingPivot = tiSceneGraph.getFaceMainPivotNode(1)
		faceTrackingPivot:getPosition (val, faceTrackingPivot:getParent())
	end
	return val
end
			
function tiScenario.getOrientation (cameraName)		
	log_debug ("tiScenario.getOrientation MULTICAM camera ignored "..tostring(cameraName))
	local val = Quaternion()
	local tracker = tiCVModuleManager.getModuleFromCamera (cameraName)
	if tracker ~= nil and tracker.getOrientation ~= nil then
		val = tracker:getOrientation()
	else
		local faceTrackingPivot = tiSceneGraph.getFaceMainPivotNode(1)
		faceTrackingPivot:getOrientation (val, faceTrackingPivot:getParent())
	end
	return val
end

function tiScenario.setAssetsPose (cameraName, position, orientation, reference)
--	log_debug ("tiScenario.setAssetsPose MULTICAM camera ignored "..tostring(cameraName))
--log_pose ("tiScenario.setAssetsPose "..tostring(cameraName), position, orientation)			
	tiAssetManager.setAssetsPose (position, orientation, reference)
end

function tiScenario.getAssetsPose (cameraName, reference)
	log_debug ("tiScenario.getAssetsPose MULTICAM camera ignored "..tostring(cameraName))
	return tiAssetManager.getAssetsPose (reference)
end
			
function tiScenario.resetAdjustments()
	for cameraName, cam in pairs (tiCameraManager.getCurrentCameras ("photo", true)) do
		tiScenario.setDistanceAdjustment (cameraName, Vector3(0,0,0))
	end
	tiAssetManager.setAssetsManualOffset (Vector3(0,0,0), Vector3(0,0,0), 1)
	tiAssetManager.setReferenceEarPoints (nil, nil)
end

function tiScenario.setProductOffset (cameraName, movePosition, moveOrientation, moveScale)
	log_debug ("tiScenario.setProductOffset MULTICAM camera ignored "..tostring(cameraName))
	tiAssetManager.setAssetsManualOffset(movePosition, moveOrientation, moveScale) -- TODO rename to setProductOffset
	tiScenarioInteraction.update (cameraName)
end
			
-- TODO deprecated in Trylive 5, now use setProductOffset
function tiScenario.setAssetsManualOffset (cameraName, movePosition, moveOrientation, moveScale)
log_debug ("tiScenario.setAssetsManualOffset MULTICAM camera ignored "..tostring(cameraName))
	tiAssetManager.setAssetsManualOffset(movePosition, moveOrientation, moveScale)
	tiScenarioInteraction.update (cameraName)
end
			
-- TODO deprecated in Trylive 5, now use getProductOffset
function tiScenario.getAssetsManualOffset (cameraName)
log_debug ("tiScenario.getAssetsManualOffset MULTICAM camera ignored "..tostring(cameraName))
	return tiAssetManager.getAssetsManualOffset()
end

-- replaces getAssetsManualOffset
function tiScenario.getProductOffset ()
	local position, orientation, scale = tiAssetManager.getAssetsManualOffset()
	sendCallback("receivedProductOffset", position:getX(),position:getY(),position:getZ(), orientation:getX(),orientation:getY(),orientation:getZ(), scale:getX(),scale:getY(),scale:getZ())
end

function tiScenario.getProductFittingHeight (productName, cameraName)
log_debug ("tiScenario.getFittingHeight productName: "..tostring(productName).." cameraName: "..tostring(cameraName))
	local fittingHeight = tiProductManager.getFittingHeight(productName)
	if cameraName ~= nil then
		local leftIris, rightIris = tiScenario.getIris (cam, false)
		log_debug ("tiScenario.getFittingHeight iris / face pivot: "..rightIris:getZ().." "..leftIris:getZ())
	end
	sendCallback("receivedProductFittingHeight", fittingHeight)
	return fittingHeight
end


function tiScenario.getDistanceAdjustment (cameraName)
log_debug ("tiScenario.getDistanceAdjustment MULTICAM camera ignored "..tostring(cameraName))			
	local distAdjustNode = tiSceneGraph.getPupilPivotNode()
	local distAdjust = Vector3(0,0,0)
	distAdjustNode:getPosition (distAdjust, distAdjustNode:getParent())
--	log_pose ("tiScenario.getDistanceAdjustment distAdjust: ", distAdjust)
	return distAdjust
end
			
function tiScenario.setDistanceAdjustment (cameraName, distAdjust)
log_debug ("tiScenario.setDistanceAdjustment MULTICAM camera ignored "..tostring(cameraName))
	local distAdjustNode = tiSceneGraph.getPupilPivotNode()
	if not distAdjustNode:isNull() then
		local daron = distAdjustNode:getParent()
		distAdjustNode:setPosition (distAdjust, daron)
--		log_pose ("tiScenario.setDistanceAdjustment distAdjust: ", distAdjust)
	end
end

function tiScenario.setIris (camera, pupilLeft3D, pupilRight3D, absolute)
log_debug ("tiScenario.setIris MULTICAM camera ignored "..tostring(camera:getName()))
log_call("tiScenario.setIris", tiScenario.irisLogLevel)				
	if absolute == nil then
		absolute = false
	end

	local leftEyeTarget = tiSceneGraph.getPupilLeftNode()
	local rightEyeTarget = tiSceneGraph.getPupilRightNode()

	log_pose("tiScenario.setIris abs = "..tostring(absolute).." irisLeft3D:  ", pupilLeft3D, nil, tiScenario.irisLogLevel)				
	log_pose("tiScenario.setIris abs = "..tostring(absolute).." irisRight3D: ", pupilRight3D, nil, tiScenario.irisLogLevel)				

	local center = Vector3((pupilLeft3D:getX()+pupilRight3D:getX())/2, (pupilLeft3D:getY()+pupilRight3D:getY())/2, (pupilLeft3D:getZ()+pupilRight3D:getZ())/2)
	local centerNode = camera:getCenterNode()
	
	if (absolute) then
		leftEyeTarget:setPosition(pupilLeft3D)
		rightEyeTarget:setPosition(pupilRight3D)
		centerNode:setPosition (center)
	else
		local pivotFaceTracking = tiSceneGraph.getFaceMainPivotNode(1)
		leftEyeTarget:setPosition (pupilLeft3D, pivotFaceTracking)
		rightEyeTarget:setPosition (pupilRight3D, pivotFaceTracking)
		centerNode:setPosition (center, pivotFaceTracking)
	end

local pl = Vector3()
local pr = Vector3()
local c = Vector3()
leftEyeTarget:getPosition(pl)
rightEyeTarget:getPosition(pr)
centerNode:getPosition(c)
log_pose("tiScenario.setIris leftIrisTarget abs:  ", pl, nil, tiScenario.irisLogLevel)				
log_pose("tiScenario.setIris rightIrisTarget abs: ", pr, nil, tiScenario.irisLogLevel)				
log_pose("tiScenario.setIris center abs: ", c, nil, tiScenario.irisLogLevel)				
log_debug ("tiScenario.setIris abs 3D PD: "..pl:distance(pr), tiScenario.irisLogLevel)			
leftEyeTarget:getPosition(pl,leftEyeTarget:getParent())
rightEyeTarget:getPosition(pr,rightEyeTarget:getParent())
centerNode:getPosition(c,centerNode:getParent())
log_pose("tiScenario.setIris leftIrisTarget rel:  ", pl, nil, tiScenario.irisLogLevel)				
log_pose("tiScenario.setIris rightIrisTarget rel: ", pr, nil, tiScenario.irisLogLevel)	
log_pose("tiScenario.setIris center rel: ", c, nil, tiScenario.irisLogLevel)				
log_debug ("tiScenario.setIris rel 3D PD: "..pl:distance(pr), tiScenario.irisLogLevel)			

	log_call_end()				
end
			
function tiScenario.getIris (cameraName, absolute)
	log_debug ("tiScenario.getIris MULTICAM camera ignored "..tostring(cameraName))
	if absolute == nil then
		absolute = false
	end
	local pivotFaceTracking = tiSceneGraph.getFaceMainPivotNode(1)
	local leftEyeTarget = tiSceneGraph.getPupilLeftNode()
	local rightEyeTarget = tiSceneGraph.getPupilRightNode()

	local rightIris = Vector3(4,5,6)
	local leftIris = Vector3 (1,2,3)
	if (absolute) then
		leftEyeTarget:getPosition (leftIris) 
		rightEyeTarget:getPosition (rightIris)
	else
		leftEyeTarget:getPosition (leftIris, pivotFaceTracking) 
		rightEyeTarget:getPosition (rightIris, pivotFaceTracking)
	end
	log_pose ("tiScenario.getIris abs= "..tostring(absolute).." leftIris : ", leftIris, nil, tiScenario.irisLogLevel)
	log_pose ("tiScenario.getIris abs= "..tostring(absolute).." rightIris: ", rightIris, nil, tiScenario.irisLogLevel)
	log_debug ("tiScenario.getIris 3D PD: "..rightIris:distance(leftIris), tiScenario.irisLogLevel)
	return leftIris, rightIris 
end

-- function tiScenario.updateCameraCenter (camera)
	-- local cameraName = camera:getName() 
	-- local irisLeft3D, irisRight3D = tiScenario.getIris (cameraName)
	-- local center = Vector3((irisLeft3D:getX()+irisRight3D:getX())/2, (irisLeft3D:getY()+irisRight3D:getY())/2, (irisLeft3D:getZ()+irisRight3D:getZ())/2)
	-- local centerNode = camera:getCenterNode(cameraName)
	-- centerNode:setPosition (center)
-- end

			
----------------------------------------------------------------------------------
--	Face points management
----------------------------------------------------------------------------------

function tiScenario.showFacePoints (enable)

	local status = true
	if not enable then 
		status = false		
		tiScenario.hideFacePoints() -- hide face points from previous display
	end

	tiScenario.mDisplayFacePoints = status
end
-- Obsolete en 5.0
function tiScenario.getFacePoints2()
	trackerName = "Tracking front"
	log_call("tiScenario.getFacePoints2 trackerName:"..trackerName)
	local nb = 0
	local points = {}
	local tracker = tiCVModuleManager.getModule (trackerName)
	if tracker ~= nil then
		nb, points = tracker:getFacePoints()
	end
	log_call_end()
	return nb, points
end
	
function tiScenario.getFacePoints (trackerName)
	log_call("tiScenario.getFacePoints trackerName:"..tostring(trackerName))
	
	local nb = 0
	local points = {}
	local tracker = tiCVModuleManager.getModule (trackerName)
	if tracker ~= nil then
		nb, points = tracker:getFacePoints()
	end

	local pointsString = ""
	for i = 0, nb-1 do
		pointsString = pointsString .. points[i*2+1] .. ";" .. points[i*2+2] .. ";"
	end

	sendCallback("receivedFacePoints", nb, pointsString)
end

function tiScenario.getVisagismResults (trackerName)
	local res = ""
	log_call("tiScenario.getVisagismResults trackerName: "..tostring(trackerName))
	local tracker = tiCVModuleManager.getModule (trackerName)
	if tracker ~= nil then
		res = tracker:getDataJSON()
log_debug(res)						
	end	
	log_call_end()
	return res
end

function tiScenario.hideFacePoints()
	local objPoints = Object3D(gScene:getObjectByName("FacePoints"))
	if not objPoints:isNull() then
		local n = objPoints:getChildrenCount()
		for i = 0, n-1 do
			local c = objPoints:getChild(0)
			gScene:deleteObject(c)
		end
	end
end
			
function displayFacePoints (tracker)
	local nb, points = tracker:getFacePoints()
	if nb > 0 then
--log_debug("displayFacePoints nb points: "..nb)
--for i = 0, nb-1 do
--	LOG ("scenario FacePoint["..i.."] = " .. points[i*2+1] .. " " ..points[i*2+2])
--end
		local objPoints = Object3D(gScene:getObjectByName("FacePoints"))
		if objPoints:isNull() then
			objPoints = Object3D(gScene:createObject(CID_OBJECT3D))
			objPoints:setName("FacePoints")
		end
		if objPoints:getChildrenCount() ~= nb then
			local n = objPoints:getChildrenCount()
			for i = 0, n-1 do
				local c = objPoints:getChild(0)
				gScene:deleteObject(c)
			end
			for i = 0, nb-1 do
				local c = ManualEntity(Object3D(gScene:createObject(CID_MANUALENTITY)))
				local size = 0.5
				c:setupBox(size,size,size,"FacePoint")
				objPoints:addChild(c)
			end						
			objPoints:setVisible(true)
		end
		local camera = tracker:getCamera()
		for i = 0, objPoints:getChildrenCount()-1 do
			local z = -200
			local x, y = camera:projectOnFrontPlane (points[i*2+1], points[i*2+2], z)
--	log_debug ("3D scenario FacePoint["..i.."] = " .. x .. " " ..y.." "..z)
			local c = objPoints:getChild(i)
			c:setPosition(x,y,z,gScene)
--c:setPosition(-y,x,z,gScene)
		end
	end
--tiSceneGraph.print("CLM")				
end
			
----------------------------------------------------------------------------------
--	Pupil distance management
----------------------------------------------------------------------------------

function tiScenario.getPupilDistance()
	local val = tiCVModuleManager.getPupilDistance()
--log_debug ("tiScenario.getPupilDistance	PD: "..val)					
	return val
end
			
function tiScenario.setPupilDistance (dist)
log_debug ("tiScenario.setPupilDistance	"..dist)
tiSceneGraph.print("tiScenario.setPupilDistance début")
	tiDataRecorder.log("trylive_feature_begin", "trueToScale")	
	dist = tonumber(dist)
	if dist > 0 then
log_debug ("tiScenario.setPupilDistance	Old PD: "..tiCVModuleManager.getPupilDistance())
		local scaleFactor = dist/tiCVModuleManager.getPupilDistance()
		tiCVModuleManager.setPupilDistance(dist)				
log_debug ("tiScenario.setPupilDistance	New PD: "..tiScenario.getPupilDistance().." scaleFactor: "..scaleFactor)

		for cameraName, cam in pairs (tiCameraManager.getCurrentCameras ("video", true)) do
			-- En mode live, on approxime en appliquant le scale sur les coordonnées xy relatives 
			-- car c'est le tracking qui mettra a jour la position (et donc le scale) global
			local leftIris, rightIris = tiScenario.getIris (cam, false)
			rightIris = Vector3 (rightIris:getX()*scaleFactor, rightIris:getY()*scaleFactor, rightIris:getZ())
			leftIris = Vector3 (leftIris:getX()*scaleFactor, leftIris:getY()*scaleFactor, leftIris:getZ())
			tiScenario.setIris (cam, leftIris, rightIris, false)
		end	
		tiScenarioPhoto.setPupilDistance (dist)
	end
tiSceneGraph.print("tiScenario.setPupilDistance fin")
end

----------------------------------------------------------------------------------
--	Tracking management
----------------------------------------------------------------------------------
function tiScenario.restartTracking (cameraName)
	log_call ("tiScenario.restartTracking "..tostring(cameraName))
	tiScenarioInteraction.reset()
	local tracker = tiCVModuleManager.getModuleFromCamera (cameraName)
	if tracker ~= nil then
		tracker:recalibrate()
	end
	log_call_end()
end
												
function tiScenario.onTracking (tracker)
	log_call ("tiScenario.onTracking "..tostring(tracker))
	trackingStatusChanged(TI_TRACKING_STATUS_TRACKED)
	if tracker.getPose ~= nil then
		local pos, ori = tracker:getPose()
		local camera = tracker:getCamera()
		cameraName = camera:getFriendlyName()					
		tiScenario.setDistanceAdjustment(cameraName, Vector3(0,0,0))
		tiAssetManager.setAssetsPose (pos, ori, camera:getCamera())
		
		local irisLeft3D, irisRight3D = tracker:getIris()
		tiScenario.setIris (camera, irisLeft3D, irisRight3D, true)

		if tiScenario.mDisplayFacePoints then
			displayFacePoints(tracker)
		end
	end
-- TTTTTT Il faudrait peut etre traiter les autres cas comme visagisme etc.
	saveScreen()
	
	log_call_end()			
end
			
function tiScenario.onWaiting(tracker)
	trackingStatusChanged(TI_TRACKING_STATUS_WAITING)
end

function tiScenario.onTooFar(tracker)
	trackingStatusChanged(TI_TRACKING_STATUS_TOO_FAR)
end

function tiScenario.onTooClose(tracker)
	trackingStatusChanged(TI_TRACKING_STATUS_TOO_CLOSE)
end

function tiScenario.onNotCentered(tracker)
	trackingStatusChanged(TI_TRACKING_STATUS_NOT_CENTERED)
end

function tiScenario.onNotTracking(tracker)
	log_call ("tiScenario.onNotTracking "..tostring(tracker))
	trackingStatusChanged(TI_TRACKING_STATUS_NOT_TRACKED)
-- Est-ce que ce n'est pas à l'app de prendre la décision de faire qq chose ? De toute façon elle reçoit l'evt					
	if tracker.getPose ~= nil then
		tiAssetManager.reinitObjectPosition(tracker:getCamera():getCamera())
	end
	log_call_end()
end

----------------------------------------------------------------------------------
--	Scenario and video management
----------------------------------------------------------------------------------

function tiScenario.pause()
	log_call ("tiScenario.pause mCurrentView: "..tostring(tiScenario.mCurrentView))
	tiDataRecorder.log("trylive_feature_begin", "scenario_paused")
	for camName, cam in pairs (tiCameraManager.getCurrentCameras ("video", true)) do
		tiCameraManager.pauseCamera (camName)
		tiCameraManager.closeTemporaryCamera (camName)
	end
	sendCallback("tryliveStatusChanged","paused")
	log_call_end()
end
			
function tiScenario.resume()
	log_call ("tiScenario.resume mCurrentView: "..tostring(tiScenario.mCurrentView))
	tiDataRecorder.log("trylive_feature_begin", "scenario_resumed")	
	tiScenario.setViewMode (tiScenario.mCurrentView)
	sendCallback("tryliveStatusChanged","resumed")
	log_call_end()
	return res
end

function tiScenario.setReferenceEarPoints (cameraName, leftPoint, rightPoint)
	log_debug ("tiScenario.setReferenceEarPoints MULTICAM camera ignored "..tostring(cameraName)) 
	tiAssetManager.setReferenceEarPoints (leftPoint, rightPoint) 
end

function tiScenario.getReferenceEarPoints (cameraName)
	--log_debug ("tiScenario.getReferenceEarPoints MULTICAM camera ignored "..tostring(cameraName)) 
	return tiAssetManager.getReferenceEarPoints()
end
	
function tiScenario.sendReferenceEarPoints (cameraName)
	--log_debug ("tiScenario.getReferenceEarPoints MULTICAM camera ignored "..tostring(cameraName)) 
	return tiAssetManager.sendReferenceEarPoints()
end
	
function tiScenario.isReferenceEarPointsSet (cameraName)
	log_debug ("tiScenario.isReferenceEarPointsSet MULTICAM camera ignored "..tostring(cameraName))
	return tiAssetManager.isReferenceEarPointsSet()
end

function tiScenario.setReferenceEarPointsWithUpdate (cameraName, leftPoint, rightPoint)
-- MULTICAM Faudrait prendre en parametre la camera					
cameraName = tiScenario.mPhotoCamera1					
	tiScenario.setReferenceEarPoints(cameraName, leftPoint, rightPoint) 
	tiScenarioInteraction.update (cameraName)
end

----------------------------------------------------------------------------------
--	Scenario main loop step
----------------------------------------------------------------------------------
			
function tiScenario.loopStep()
	
	-- Screenshot manager
	if(tiWindowManager ~= nil) then
		tiWindowManager.loop()
	end
	tiScenarioPhoto.loopStep()
	tiScenarioPhotoSequence.loopStep()
	
	tiScenarioInteraction.loopPreStep()

	local refreshTarget = false
		-- Tracking
	for i, tracker in pairs (tiCVModuleManager.getActiveModules()) do
		tracker:loop()
--log_debug("tiScenario.loopStep tracker: "..i.." getPose: "..tostring(tracker.getPose).." isTracking: "..tostring(tracker:isTracking()).." camera: "..tostring(tracker:getCamera()))
		if tracker.getPose ~= nil then
			if tracker:isTracking() then
				tiScenario.setAssetsPose ("ZOB", tracker:getPosition(), tracker:getOrientation(), tracker:getCamera():getCamera())
				-- tiScenario.updateCameraCenter (tracker:getCamera())
				if tiScenario.debugScale then
					tiScenario.printSceneGraphCmpt = (tiScenario.printSceneGraphCmpt+1) % 30
					if tiScenario.printSceneGraphCmpt == 0 then
						tiSceneGraph.print("TRACKING")
					end
				end						
				refreshTarget = true
				if tiScenario.mDisplayFacePoints then
					displayFacePoints(tracker)
				end
			end
		else
			if tiScenario.mDisplayFacePoints then
				displayFacePoints(tracker)
			end
		end
	end
	tiViewportManager.refresh()
saveScreenIfNeeded()	
refreshTarget = true -- necessaire pour voir les targets bouger sur les sequences d'image, a cleaner plus tard ...
	tiScenarioInteraction.loopPostStep (tiViewportManager.getCurrentViewport(), refreshTarget)
	tiPerfManager.displayStats()
end


-- Initialize the command manager
tiCommandManager.init()
coroutine.yield()

repeat
--log_debug("tiScenario main loop isInit: "..tostring(tiScenario.isInit()).." tiAssetManager: "..tostring(tiAssetManager).." tiCustomerConfigManager: "..tostring(tiCustomerConfigManager).." tiCommandManager: "..tostring(tiCommandManager))
	if tiAssetManager and tiCustomerConfigManager and tiCommandManager then
		if tiScenario.isInit() then
			tiScenario.loopStep()
		end
	end
until coroutine.yield()
