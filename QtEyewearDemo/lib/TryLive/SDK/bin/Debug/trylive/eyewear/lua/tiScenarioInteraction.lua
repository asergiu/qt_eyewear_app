log_loaded_info("tiScenarioInteraction.lua")

-- Management of manual interactions

tiScenarioInteraction = {
	Mode = {
		None = "none", 
		TranslateXY = "translateXY", 
		TranslateYZ = "translateYZ", 
		TranslateXZ = "translateXZ", 
		Rotate = "totate", 
		Scale = "Scale", 
		TargetRotate = "targetRotate", 
		TargetScale = "targetScale", 
		MoveViewport = "moveViewport"
	},
	-- Reference aux instance de manager externes
	mMode = nil,
	mInputProcess = {},
	mMoveOccured = false,
}

function tiScenarioInteraction.initialize()
log_call ("tiScenarioInteraction.initialize")
	tiScenarioInteraction.mMode = tiScenarioInteraction.Mode.None
	
	local refObject = tiAssetManager.getFaceManualMovePivotObject()
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TranslateXY] = tiInputTranslate:create({refAxis = 'z', refObject = refObject})
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TranslateYZ] = tiInputTranslate:create({refAxis = 'x', refObject = refObject})
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TranslateXZ] = tiInputTranslate:create({refAxis = 'y', refObject = refObject})
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.Scale] = tiInputRotateScale:create()
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.Rotate] = tiInputRotateScale:create()
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TargetScale] = tiInputTargetScale:create()
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TargetRotate] = tiInputTargetRotate:create()
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.MoveViewport] = tiInputMoveViewport:create()

log_call_end()	
end

function tiScenarioInteraction.addInteraction (inputId, inputModule)
	tiScenarioInteraction.mInputProcess[inputId] = inputModule:create()
end

function tiScenarioInteraction.enable (mode, state)
log_call ("tiScenarioInteraction.enable mode: "..tostring(mode).." state: "..tostring(state).." mMode: "..tostring(tiScenarioInteraction.mMode))
	if state == nil then
		state = not tiScenarioInteraction.isEnable (mode)
	end
	if state or (mode == tiScenarioInteraction.mMode) then
		if tiScenarioInteraction.mMode ~= tiScenarioInteraction.Mode.None then
			tiScenarioInteraction.mInputProcess[tiScenarioInteraction.mMode]:enable(false)
		end
	end
	if mode ~= tiScenarioInteraction.Mode.None then
		tiScenarioInteraction.mInputProcess[mode]:enable(state)
	end
	if state then
		tiScenarioInteraction.mMode = mode
	else
		if mode == tiScenarioInteraction.mMode then
			tiScenarioInteraction.mMode = tiScenarioInteraction.Mode.None
		end
	end	
	log_debug ("tiScenarioInteraction.enable -> mMode: "..tostring(tiScenarioInteraction.mMode))			
	
	tiScenarioInteraction.sendManualTransformationStatus()

log_call_end()
end

function tiScenarioInteraction.sendManualTransformationStatus()
	local modesString = ""
	for key, value in pairs(tiScenarioInteraction.mInputProcess) do
		modesString = modesString .. tostring(key) .. ";" .. tostring(tiScenarioInteraction.isEnable(key)) .. ";"
	end
	sendCallback("receivedManualTransformationStatus", modesString)
end

function tiScenarioInteraction.isEnable (mode)
	return tiScenarioInteraction.mMode == mode
end

function tiScenarioInteraction.isMoving()
	return tiScenarioInteraction.mMoveOccured
end


----------------------------------------------------------------------------------
--	Targets management
----------------------------------------------------------------------------------

function tiScenarioInteraction.update (cameraName)
--	log_call("tiScenarioInteraction.update")
--	log_debug ("tiScenarioInteraction.update MULTICAM camera ignored "..tostring(cameraName))
--printStack()
			-- MULTICAM XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
--tiViewportManager.printViewports()			
	local viewport = tiViewportManager.getCurrentViewport()
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TargetRotate]:setReferenceEarPoints (tiScenario.getReferenceEarPoints (cameraName)) -- Update ear target to ear points
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TargetRotate]:updateTargets (viewport)	
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TargetScale]:updateTargets (viewport)	
--	log_call_end()
end

function tiScenarioInteraction.setTargetScale (scale)
	local viewport = tiViewportManager.getCurrentViewport()
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TargetRotate]:setScale (scale)
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TargetScale]:setScale (scale)
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TargetRotate]:updateTargets (viewport)	
	tiScenarioInteraction.mInputProcess[tiScenarioInteraction.Mode.TargetScale]:updateTargets (viewport)	
end

-- premiere chose, regarder si un nouveau viewport n'a pas ete selectionne

function tiScenarioInteraction.loopPreStep() 
	if (tiInputDevice.isClick()) then
		local x, y  = tiInputDevice.getPositionClick()
--		log_debug("tiScenarioInteraction.loopPreStep click: "..x..","..y)
		tiViewportManager.selectViewportByPos (x, y)
	end
end

-- Fonction pouvant être redéfinie en fonction des besoin user ...

function tiScenarioInteraction.loopUserStep()
end

function tiScenarioInteraction.reset()
	if tiScenarioInteraction.mInputProcess[tiScenarioInteraction.mMode] ~= nil then
		tiScenarioInteraction.mInputProcess[tiScenarioInteraction.mMode]:reset()
	end
end		

-- Aquisition input
-- Traitement des resultat de l'input manager

function tiScenarioInteraction.loopPostStep (viewport, refresh) 
--log_debug("tiScenarioInteraction.loopPostStep refresh: "..tostring(refresh))
	if refresh then
		tiScenarioInteraction.update (viewport)
	end
	
	tiScenarioInteraction.loopUserStep()
	local touches = tiInputDevice.getTouches()

	if tiScenarioInteraction.mMoveOccured and #touches == 0 then
		sendCallback("manualTransformationChanged","ended")
	end
	tiScenarioInteraction.mMoveOccured = false
	local input = tiScenarioInteraction.mInputProcess[tiScenarioInteraction.mMode]
	if input ~= nil then
--		log_debug ("tiScenarioInteraction.loopPostStep process "..tostring(tiScenarioInteraction.mMode).." found")
		tiScenarioInteraction.mMoveOccured = tiScenarioInteraction.mInputProcess[tiScenarioInteraction.mMode]:process (touches, viewport)
	end
end




						


