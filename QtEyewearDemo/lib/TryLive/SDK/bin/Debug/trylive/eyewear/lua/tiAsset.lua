log_loaded_info("tiAsset.lua")
-- This class represents an asset.
-- An asset contains : 
--   - The list of mesh
--   - The json informations (data, path...)
--   - The option list 
--   - ...

-- Internal callbacks to native part

function assetsLoadingStatusChanged(status, nameAsset)
	log_debug ("assetsLoadingStatusChanged asset: "..nameAsset..", status: "..status)
	sendCallback("assetStatusChanged", status, nameAsset)
end

function assetSelectStatusChanged(name, selected)
	if selected then
		sendCallback("assetSelectStatusChanged", name, "true")
	else
		sendCallback("assetSelectStatusChanged", name, "false")
	end
end

-------------------------------------------------------------------------------
-- This methods split an array with mesh and materials in two different arrays.
-- When we load an asset, the json file contains a unique array with mesh and material.
-- For later use, we need to split it in a data array with mesh and an other with material.
-- For do this, only check the extension of the name of the rows.
-- Each row is like this : 
--		"truc.mesh" : {...}
--			or
-- 		"machin.material" : {...}
-------------------------------------------------------------------------------
function splitDataInArrays(dataToSplit)
	local meshArray = {}
	local materialArray = {}
	for u in pairs(dataToSplit) do
		if tiFile.getExtension(u) == "mesh" then
			table.insert(meshArray, u)			
		elseif tiFile.getExtension(u) == "material" then
			table.insert(materialArray, {u, dataToSplit[u]["priority"]})
		end
	end	

	function comp(a,b)
		if a[2] > b[2] then
			return true;
		end
		return false;
	end
	table.sort(materialArray, comp)
	return meshArray, materialArray
end

--[[
Tronche d'un fichier JSON d'asset:

    "product":								asset product type 
    "product_options": "right"				asset product type val
    "versions": 1.0, 
    "production_version": "_3.25.24177", 
    "type": "3d", 
    "assets": {								3D representation
        "xxxxx.material": {
            "priority": 
            "resources":
            "versions": 					quality
        }, 
        "yyyyyy.mesh": {
            "priority": 
            "position": [], 
            "scale": [], 
            "orientation": [], 
            "path": 
        }
    }, 
    "options": {							options to be attached
        "right": {							option name
            "product": "glasses_arms", 		option product type
            "scale": [], 
            "product_options": "right", 	option product type val
            "priority": 0, 
            "position": [], 
            "orientation": []
			
			"asset":						the corresponding asset
        }
    }, 
}

Name "Option" is used for the table of options as defined in the json structure. It is a kind of placeholder for an accessory.
The name "Accessory" is used for the asset attached to another asset via an option
So at the begining the option list doesn't contain any accessory, just the placeholder to receive accessory assets
  
]]--


tiAsset = {
	defaultTempleOutSideAngle = 15,	-- angle between xz plane and temple axis in degrees
	debugEarPoint = false,
	showBoudingBox = false,
	defaultQuality = "low",
	drillingPointLogLevel = 5,
	optionLogLevel = 5,
	pathLogLevel = 5,
}

----------------------------------------------------------------------------------------------
--									Méthodes statiques
----------------------------------------------------------------------------------------------

-- Gestion du versionnage
-- Le path du fichier sauvegardé en local ne contient plus le versionnage
-- On pourrait laisser Tiare faire le check des éventuelles mise a jour tout seul mais ça impliquerait une requete serveur
-- Donc on conserve en localdans un fichier de même nom que le zip avec ".origin" apres, l'url d'origine
-- Il suffit donc de lire ce fichier pour savoir si on est sur la bonne version ou non 

function tiAsset.nameFicOrigin (localPath)
--printStack()
	local localDir = localPath.gsub(localPath, "[^/]+%.zip.*", "")
log_debug ("tiAsset.nameFicOrigin localPath: "..tostring(localPath).." localDir: "..tostring(localDir), tiAsset.pathLogLevel)
	local originFile
	
	if localDir == localPath then 
		log_warning ("No zip: localPath: "..tostring(localPath))
		originFile = localPath..".origin"
	else
		originFile = localDir..localPath:match("[^/]+%.zip")..".origin"
	end
log_debug ("tiAsset.nameFicOrigin originFile: "..tostring(originFile), tiAsset.pathLogLevel)
	return originFile
end

function tiAsset.checkOrigin (remotePath, localPath)
log_debug("tiAsset.checkOrigin remotePath: "..tostring(remotePath).." localPath: "..tostring(localPath), tiAsset.pathLogLevel)
	local origineOk = false
	local ficOrigin = tiAsset.nameFicOrigin (localPath)
	local f = io.open(ficOrigin, "r")
	if f ~= nil then
		origin = f:read("*all")
		f:close()
		if origin == remotePath then
			origineOk = true
		end
	end
	return origineOk
end

function tiAsset.updateOrigin (remotePath, localPath)
	local ficOrigin = tiAsset.nameFicOrigin (localPath)
	local f = io.open(ficOrigin, "w")
	if f ~= nil then
		f:write(remotePath)
		f:close()
	end
end

function tiAsset.clearOrigin (remotePath, localPath)
end

-- Accès aux noeuds

function tiAsset.buildNodeName (product, product_option)
	local nodeName = ""
	if product_option == nil or product_option == "" then
		nodeName = "TL_" .. product
	else
		nodeName = "TL_" .. product_option .. "_" .. product
	end
	return nodeName
end

function tiAsset.nodeNameIsOption  (nodeName, product_option)
	return string.starts (nodeName, "TL_" .. product_option)
end

-- Tiare node name of an option

function tiAsset.getOptionNodeName (option)
	return tiAsset.buildNodeName (option["product"], option["product_options"]) 
end

function tiAsset.getNodeNameStartsWith (topNode, startString)
	local node = nil
	if topNode ~= nil then
		if string.starts (topNode:getName(), startString) then
			node = topNode
		else
			local i
			for i = 0, topNode:getChildrenCount()-1 do
				local child = topNode:getChild(i)
				node = tiAsset.getNodeNameStartsWith (child, startString)
				if node ~= nil then
					local p = Vector3(0,0,0)
					local op = Object3D(node)
					op:getPosition(p, op:getParent())
					log_debug("node "..op:getName().." "..p:getX()..","..p:getY()..","..p:getZ())
					return node
				end
			end
		end
	else
		log_debug("tiAsset.getNodeNameStartsWith null node")
	end
	return node		
end

function tiAsset.getEarPoints (node)
	if node ~= nil then 
		log_debug("tiAsset.getEarPoints "..node:getName())
		return tiAsset.getNodeNameStartsWith (node, tiAsset.buildNodeName("ear_point", "left")), 
				tiAsset.getNodeNameStartsWith (node, tiAsset.buildNodeName("ear_point", "right"))
	else
		log_debug("tiAsset.getEarPoints null node")
		return nil, nil
	end
end

function tiAsset.getArms (node)
	if node ~= nil and not node:isNull() then 
		log_debug("tiAsset.getArms "..node:getName())
		return	tiAsset.getNodeNameStartsWith (node, tiAsset.buildNodeName("glasses_arms", "left")), 
				tiAsset.getNodeNameStartsWith (node, tiAsset.buildNodeName("glasses_arms", "right"))
	else
		log_debug("tiAsset.getArms null node")
		return nil, nil
	end
end

----------------------------------------------------------------------------------------------
--									Méthodes d'instance
----------------------------------------------------------------------------------------------

function tiAsset:create (properties)
	log_debug("tiAsset:create properties: "..tostring(properties))
    local new_inst = {}    -- the new instance
    setmetatable( new_inst, { __index = tiAsset } ) -- all instances share the same metatable
	if self.baseClasse ~= nil then
		self.baseClasse.init (self, properties)
	end
	new_inst:init (properties)
    return new_inst
end

function tiAsset:init(properties)
	log_debug("tiAsset:init "..tostring(self))
	self.mConfig = nil
	
	self.mMesh = {}					-- List of the meshs of the asset
	self.mMainNode = nil			-- The asset top node inserted in the scen graph
	self.mDataAsset = {}			-- The asset data structure read from json file
	self.mGhost = nil
	self.mVisibilityMask = -1
	self.mVisible = false
	self.mIsSelected = false
	self.mIsCustomizable = false	-- A priori obsolete
	
	self.mRemotePath = ""
	self.mLocalPath = nil
	self.mInstanceName = ""
log_object(properties)	
	if properties ~= nil then
		self.mConfig = properties
		if properties.remotePath ~= nil then
--			self.mRemotePath = tiFile.cleanPath (properties.remotePath)
			self.mRemotePath = properties.remotePath
		end
		if properties.instanceName ~= nil then
			self.mInstanceName = properties.instanceName
		end
	end
end

-----------------------------------------------------------------------------------		
--						3D representation management
-----------------------------------------------------------------------------------		
				
-- Add the materials for the asset
-- Parse the material list

-- A priori obsolete car non utilisé dans les ZIPs

function tiAsset:addMaterials (materialList, bDownload, bAddResource)
	log_call("tiAsset:addMaterials  bDownload: " .. tostring(bDownload) .. " bAddResource: " .. tostring(bAddResource) .. " JsonName: " .. self:getJsonFileName())	
	local assetInfos = self.mDataAsset["assets"]
	local dlSucceed = false
	local nameDirMat = ""
	
	local remotePath = self:getJsonRemoteDirPath()
	local localPath = self:getJsonLocalDirPath()
	
	-- Parse list.
	for id, nameMat in pairs(materialList) do
	
		nameDirMat = tiAsset.defaultQuality .. "/"
						
		local localPathToMat = ""
		local remotePathToMat = ""
		if assetInfos[nameMat[1]]["path"] then
			localPathToMat = localPath .. assetInfos[nameMat[1]]["path"] .. nameDirMat
			remotePathToMat = remotePath .. assetInfos[nameMat[1]]["path"] .. nameDirMat
		else
			localPathToMat = localPath .. nameDirMat
			remotePathToMat = remotePath .. nameDirMat
		end
		
		if bDownload == true then
--tiNetwork.download (self:getZipRemoteDirPath(), self:getZipLocalDirPath())		
			dlSucceed = tiNetwork.download (remotePathToMat, localPathToMat)
			if dlSucceed == true and bAddResource == true then
				addResourceLocation(localPathToMat)
			end
		end
		
		-- Add resources locations
		-- Download the resources (env map ...)
		if assetInfos[nameMat[1]]["resources"] then
			for id in pairs(assetInfos[nameMat[1]]["resources"]) do
				local bSuccess = false
				if bDownload == true then
					if tiNetwork.download (remotePath .. assetInfos[nameMat[1]]["resources"][id] .. nameDirMat, localPath .. assetInfos[nameMat[1]]["resources"][id] .. nameDirMat) == true then
						bSuccess = true
					else
						dispatchError(TI_INCONSISTENT_PARAMETERS, "Asset downloading error on request remote = ".. remotePath .. assetInfos[nameMat[1]]["resources"][id] .. nameDirMat .. " local = " .. localPath .. assetInfos[nameMat[1]]["resources"][id] .. nameDirMat .. " .")
					end
				else
					bSuccess = true
				end
			
				if bSuccess == true and bAddResource == true then
					-- Say to ogre : A new resource directory is here. We the asset is loaded, ogre get automaticly the material.
					if (not self:isZipped()) then
						addResourceLocation(localPath .. assetInfos[nameMat[1]]["resources"][id] .. nameDirMat)
					end
				end
			
				if not bSuccess then	
					dispatchError(TI_INCONSISTENT_PARAMETERS, "Asset downloading error on request remote = ".. remotePath .. assetInfos[nameMat[1]]["resources"][id] .. nameDirMat .. " local = " .. localPath .. assetInfos[nameMat[1]]["resources"][id] .. nameDirMat .. " .")
					return false
				end
			end
		end
	end
	log_call_end()
	return true
end

-- The material is defined in the mesh file. 
-- When the mesh is loaded we can change dynamically the material.
-- It is used for 3D Lite assets because they are based on a template mesh referencing template materials that is replaced at load time by the asset own materials

function tiAsset:replaceMaterials (meshObject, materialList)
	log_call("tiAsset:replaceMaterials  " .. self:getJsonFileName())	
	local assetsInfos = self.mDataAsset["assets"]
	for id, nameMat in pairs(materialList) do
		if assetsInfos[nameMat[1]]["priority"] ~= 0 then
			-- Doing replacement
			nbMaterialToChange = meshObject:getMaterialCount()
			for nbm= 0, nbMaterialToChange-1 do 
				materialToChange = meshObject:getMaterial(nbm)
				
				local strMaterialToChange = tostring(materialToChange:getName())
				local specificMaterialPart = tiFile.baseNameWithoutExtension(strMaterialToChange)
				
				newMaterial = getMaterial (tiFile.baseNameWithoutExtension(nameMat[1]) .."/"..specificMaterialPart)
				
				if not (newMaterial:isNull()) then
					meshObject:replaceMaterial(materialToChange,newMaterial)
				end
			end
		end
	end
	log_call_end()
end

-- Create mesh and load it.

function tiAsset:createMesh (mesh, materialList)
	log_call("tiAsset:createMesh JsonName: " .. self:getJsonFileName())	
	local assetsInfos = self.mDataAsset["assets"]
	local object = nil
	self.mVisible = false
	
--log_debug("tiAsset:createMesh ... " .. meshInfo["path"])
		
	meshInfo = assetsInfos[mesh]
	
	-- Les assets simples ont leurs mesh dans le répertoire de l'asset, 
	-- mais certain comme par exemple les 3D lite ont leur mesh dans un autre répertoire genre common car prevenant d'un template
	if meshInfo["path"] then
		meshPath = self:getJsonLocalDirPath() .. meshInfo["path"] .. mesh
	else
		meshPath = self:getJsonLocalDirPath() .. mesh
	end
	meshPath = tiFile.cleanPath (meshPath)
    
	object = Entity (gScene:createObject(CID_ENTITY))
	object:setVisible(false)
	
	log_debug("tiAsset:createMesh object:setResource: " .. meshPath)						
	if object:setResource(meshPath) == eOk then

	-- The main node is the root for the asset.
		self.mMainNode:addChild(object)
		object:setName(self.mInstanceName .. "/" .. mesh)	
		
		-- Place, orient and resize the mesh on json information.
		if meshInfo["position"] ~= nil then
			object:setPosition(meshInfo["position"][1],meshInfo["position"][2],meshInfo["position"][3], self.mMainNode)
		end
		if meshInfo["orientation"] ~= nil then
			object:setOrientationEuler(meshInfo["orientation"][1],meshInfo["orientation"][2],meshInfo["orientation"][3], self.mMainNode)
		end
		if meshInfo["scale"] ~= nil then
			local scale = Vector3(meshInfo["scale"][1],meshInfo["scale"][2],meshInfo["scale"][3])
			object:setScale(scale)
		end
		if meshInfo["priority"] then
			object:setRenderGroup(meshInfo["priority"] + 50)	
		else
			object:setRenderGroup(50)	
		end

		-- For 3D lite assets
		self:replaceMaterials (object, materialList)
		
		if tiAsset.showBoudingBox then
			object:showBoundingBox(true)
		end

	else
		dispatchError(TI_FILE_NOT_FOUND, "Cannot load file " .. meshPath)
		gScene:deleteObject(object)
		object = nil
	end
	log_call_end()			
	return object
end

-- Handling of dynamical orientation for glasses arms
				
function tiAsset:setArmOrientation (orientEuler)
	log_debug("tiAsset:setArmOrientation name: "..self:getName().." "..orientEuler[1]..","..orientEuler[2]..","..orientEuler[3])
	local attach = self.mMainNode:getParent()
	if attach ~= nil then
		local name = attach:getName()
		if string.starts (name, tiAsset.buildNodeName("glasses_arms","left")) or string.starts (name, tiAsset.buildNodeName("glasses_arms","right")) then
			attach:setOrientationEuler (orientEuler[1], orientEuler[2], orientEuler[3], attach:getParent())
		else
			log_warning("tiAsset:setArmOrientation, asset " .. self:getName() .. " is not a temple")  
		end
	else
		log_warning("tiAsset:setArmOrientation, asset " .. self:getName() .. " has no parent")
	end
end

function tiAsset:getArmOrientation()
	local orient = nil
	local attach = self.mMainNode:getParent()
	if attach ~= nil then
		local name = attach:getName()
		if string.starts (name, tiAsset.buildNodeName("glasses_arms","left")) or string.starts (name, tiAsset.buildNodeName("glasses_arms","right")) then
			orient = {0, 0, 0}
			orient[1], orient[2], orient[3] = attach:getOrientationEuler (attach:getParent())
		else
			log_warning("tiAssets.setArmOrientation, asset " .. self:getName() .. " is not a temple")  
		end
	else
		log_warning("tiAssets.setArmOrientation, asset " .. self:getName() .. " has no parent")
	end
	return orient
end

-- If the asset is a temple, it is supposed to have an ear point option
-- If this option is not set in the loaded asset, let's create it based on a geometric computation
		
function tiAsset:checkArmsInfo()
	log_call("tiAsset:checkArmsInfo name: "..self:getName().." type: " .. self:getProductType())
	local orientation = nil
	if self:getProductType() == "glasses_arms" then
		local side = self:getProductOption()
		local earpoint = self:findOption("ear_point", side)
		local pos
		if earpoint == nil then
			if self.mDataAsset["options"] == nil then
				self.mDataAsset["options"] = {}
			end
			
			local option = {}
			option["product"] = "ear_point"
			option["product_options"] = side
			option["priority"] = 0

			pos = self:computeEarPoint(self.mMainNode)
			
			option["position"] = {pos:getX(), pos:getY(), pos:getZ()}
			option["scale"] = {1, 1, 1}
			
			option["orientation"] = {0, 0, 0}
				
			self.mDataAsset["options"][side] = option
			log_warning ("Ear point not defined in "..self:getName().." generated: "..pos:getX()..","..pos:getY()..","..pos:getZ())
		else
			local ppos = earpoint["position"]
			pos = Vector3(ppos[1], ppos[2], ppos[3])
		end
		
		local orientY = math.deg (math.atan2(pos:getX(), pos:getZ()))
		log_debug ("tiAsset:checkArmsInfo arm orientation: " .. orientY)
		orientation = {0, orientY+180, 0}
	end
	log_call_end()
	return orientation
end

-- Create the 3D representation of the asset (meshes and materials from JSON asset config file)

function tiAsset:createObject3D (bDownload, bAddResource)
	log_call("tiAsset:createObject3D ".. self:getJsonFileName() .. " " .. tostring(this) .. " bDownload: " .. tostring(bDownload) .. " bAddResource: " .. tostring(bAddResource))	
	if self.mDataAsset["assets"] == nil then
		log_call_end()
		return false
	end
startChrono("tiAsset:createObject3D",self:getId())
				
	local meshList, materialList = splitDataInArrays(self.mDataAsset["assets"])
	startChrono("tiAsset:createObject3D add resources",self:getId())
	-- If asset is zipped, materials are already downloaded, we only need add material resource location with the archive path
	if (self:isZipped()) then
log_debug("tiAsset:createObject3D addResourceLocation: "..self:getZipLocalPath())				
		addResourceLocation (self:getZipLocalPath())
		-- Default path for TryLive materials
log_debug("tiAsset:createObject3D addResourceLocation: "..self:getJsonLocalDirPath() .. tiAsset.defaultQuality .. "/")				
		addResourceLocation(self:getJsonLocalDirPath() .. tiAsset.defaultQuality .. "/")
	else 
		self:addMaterials (materialList, bDownload, bAddResource)
	end
	if bAddResource == true then
		log_debug("tiAsset:createObject3D gScene:createObject(CID_OBJECT3D) "..self:getJsonFileName())
		self.mMainNode = Object3D(gScene:createObject(CID_OBJECT3D))
		self.mMainNode:setName(self.mInstanceName)
	end
	stopChrono("tiAsset:createObject3D add resources",self:getId())

	-- Create the meshs
	-- dans la 4.5 il y a un test 	if self.mDataAsset["assets"][mesh]["path"] avant, peut etre que des fois ce truc est nul ????		
startChrono("tiAsset:createObject3D mesh ", mesh)
	for id, mesh in pairs(meshList) do
		if self.mDataAsset["assets"][mesh]["path"] then
--			coroutine.yield()	
			if bAddResource == true then
				local dlSucceed = true
				if bDownload then
					startChrono("tiAsset:createObject3D mesh download", mesh)
					dlSucceed = tiNetwork.download(self:getJsonRemoteDirPath() .. self.mDataAsset["assets"][mesh]["path"], self:getJsonLocalDirPath() .. self.mDataAsset["assets"][mesh]["path"])
					stopChrono("tiAsset:createObject3D mesh download",mesh)
				end
				if dlSucceed then
					startChrono("tiAsset:createObject3D mesh create",mesh)
					meshObject3D = self:createMesh (mesh, materialList)
					stopChrono("tiAsset:createObject3D mesh create",mesh)							
					if meshObject3D ~= nil then
local pmin = Vector3()
local pmax = Vector3()
meshObject3D:getBoundingBox(pmin,pmax)
form = "%5.1f"	
log_debug("tiAsset:createObject3D "..mesh.." Bounding Box: min: "..string.format(form,pmin:getX())..","..string.format(form,pmin:getY())..","..string.format(form,pmin:getZ()).." max: "..string.format(form,pmax:getX())..","..string.format(form,pmax:getY())..","..string.format(form,pmax:getZ()))				
						table.insert(self.mMesh, meshObject3D)
					end
				else
					log_error ("Failed to load mesh "..tostring(mesh))
				end
			end
		end
	end
stopChrono("tiAsset:createObject3D mesh ", mesh)

	-- Au cas ou les branches ne contienne pas d'ear point, on les crée 
	-- mais ça ne peut être fait qu'une fois la geometrie de la branche chargee vu qu'on vu le deduire de cette geometrie
	
	local orientationFromAsset = self:checkArmsInfo()
	
	-- Si une orientation a été renvoyée, il s'agit d'une branche et correspond à son orientation moyenne
	-- Pour la visualisation par défaut, on la remet verticale aux lunettes
	if orientationFromAsset ~= nil then
		local outsideAngle = tiAsset.defaultTempleOutSideAngle
		if orientationFromAsset[2] > 180 then
			outsideAngle = -outsideAngle
		end
		log_debug ("Ajout options ... Orientation from asset: " .. tostring(orientationFromAsset[1]) .. " " .. tostring(orientationFromAsset[2]) .. " " .. tostring(orientationFromAsset[3]))
--				local attach = self.mMainNode:getParent()
		local attach = self.mMainNode
		if attach ~= nil then
			log_debug ("Set orientation "..attach:getName().." to "..tostring(-orientationFromAsset[2]+outsideAngle))
			attach:setOrientationEuler (-orientationFromAsset[1], -orientationFromAsset[2]+outsideAngle, -orientationFromAsset[3], attach:getParent())
--					attach:setOrientationEuler (-orientationFromAsset[1], -orientationFromAsset[2], -orientationFromAsset[3], attach)
		else
			log_warning("Can't find parent of "..self:getName().." to set arm orientation")
		end
	end

	-- Create the nodes to attach options
	
	startChrono("tiAsset:createObject3D create scene graph",self:getId())
	if self:getOptionList() ~= nil then
		for optionName, optionInfo in pairs(self:getOptionList()) do
			--log_debug ("ajout options: " .. tostring(optionName) .. " " .. tostring(optionInfo))
			obj = Object3D(gScene:createObject(CID_OBJECT3D))
			obj:setName(tiAsset.getOptionNodeName(optionInfo))
			self.mMainNode:addChild(obj)
			if optionInfo["position"] ~= nil then
				local pos = optionInfo["position"]
				--log_debug ("ajout options ... Position: " .. tostring(pos[1]) .. " " .. tostring(pos[2]) .. " " .. tostring(pos[3]))
				obj:setPosition(pos[1],pos[2],pos[3], self.mMainNode)
			end
			if optionInfo["orientation"] ~= nil then
-- PATCH ORIENT
				optionInfo["orientation"] = {0,0,0}
				local orient = optionInfo["orientation"]
				log_debug ("ajout options ... Orientation: " .. tostring(orient[1]) .. " " .. tostring(orient[2]) .. " " .. tostring(orient[3]))
				obj:setOrientationEuler(orient[1],orient[2],orient[3], self.mMainNode)
			end
			if optionInfo["scale"] ~= nil then
				local scale = optionInfo["scale"]
				--log_debug ("ajout options ... Scale: " .. tostring(scale[1]) .. " " .. tostring(scale[2]) .. " " .. tostring(scale[3]))
				obj:setScale(Vector3(scale[1],scale[2],scale[3]))
			end
			
			-- Et un apericube pour debuguer les ear points ...
			
			if optionInfo["product"] == "ear_point" then
				local obj1 = ManualEntity(Object3D(gScene:createObject(CID_MANUALENTITY)))
				obj1:setupBox(3,3,3,"zob")
				obj:addChild(obj1)
				obj:setVisible (tiAsset.debugEarPoint)
			end
			self.mMainNode:forceUpdate()
		end			
	end
	stopChrono("tiAsset:createObject3D create scene graph",self:getId())
	startChrono("tiAsset:createObject3D compute BB",self:getId())
local pmin = Vector3()
local pmax = Vector3()
form = "%5.1f"	
computeBoundingBox (self.mMainNode, pmin, pmax)
	stopChrono("tiAsset:createObject3D compute BB",self:getId())
log_debug("tiAsset:createObject3D World Bounding Box: min: "..self.mMainNode:getName().." "..string.format(form,pmin:getX())..","..string.format(form,pmin:getY())..","..string.format(form,pmin:getZ()).." max: "..string.format(form,pmax:getX())..","..string.format(form,pmax:getY())..","..string.format(form,pmax:getZ()))				
log_debug("tiAsset:createObject3D World Bounding Box: size: "..self.mMainNode:getName().." "..string.format(form,pmax:getX()-pmin:getX())..","..string.format(form,pmax:getY()-pmin:getY())..","..string.format(form,pmax:getZ()-pmin:getZ()))				
stopChrono("tiAsset:createObject3D",self:getId())
	log_call_end()			
	return true
end

function tiAsset:computeEarPoint()
	local earPoint = Vector3(0,0,0)
	for i in pairs (self.mMesh) do
		log_debug("tiAssets.computeEarPoint " .. self.mMesh[i]:getName())
		local mesh = self.mMesh[i]
		local bbmin = Vector3()
		local bbmax = Vector3()
		mesh:getBoundingBox (bbmin, bbmax)
		log_debug ("BB asset min: " .. bbmin:getX() ..","..bbmin:getY()..","..bbmin:getZ()..", max: "..bbmax:getX() ..","..bbmax:getY()..","..bbmax:getZ())
		local dist = bbmin:getZ() * 0.7 
		local delta = 10
		log_debug ("dist: " .. dist)
		local vmin, vmax = mesh:intersect (0, 0, 1, -dist, delta);
		log_debug ("BB intersect min: " .. vmin[1]..","..vmin[2]..","..vmin[3]..", max: "..vmax[1]..","..vmax[2]..","..vmax[3])
		log_debug ("Ear point: " ..(vmin[1]+vmax[1])/2 .. " " .. vmin[2] .. " " .. (vmin[3]+vmax[3])/2)
		earPoint = Vector3((vmin[1]+vmax[1])/2,vmin[2],(vmin[3]+vmax[3])/2)
	end
	return earPoint
end

function tiAsset:computeFittingHeight()
	local fittingHeight = -1
	local meshVerre = nil
	for i in pairs (self.mMesh) do
		local mesh = self.mMesh[i]
		log_debug("tiAssets.computeFittingHeight " .. mesh:getName())
		if string.ends (mesh:getName(), "lens_l.mesh") or string.ends (mesh:getName(), "lens_r.mesh") then
			meshVerre = mesh
		end
	end
	if meshVerre ~= nil then
		log_debug("tiAssets.computeFittingHeight LENS " .. meshVerre:getName())
		local bbmin = Vector3()
		local bbmax = Vector3()
		meshVerre:getBoundingBox (bbmin, bbmax)
		log_debug ("tiAssets.computeFittingHeight BB local asset min: " .. bbmin:getX() ..","..bbmin:getY()..","..bbmin:getZ()..", max: "..bbmax:getX() ..","..bbmax:getY()..","..bbmax:getZ())
		local parent = meshVerre:getParent()
		local posMesh = Vector3()
		meshVerre:getPosition (posMesh, parent)
		local obj = Object3D(gScene:createObject(CID_OBJECT3D))
		parent:addChild(obj)
		obj:setPosition (posMesh, parent)
		obj:translate (bbmin)
		local pos = Vector3()
		obj:getPosition (pos, tiSceneGraph.getFaceMainPivotNode(0))
		log_warning ("tiAsset:computeFittingHeight viewport ignored")
		log_debug ("BB global asset min: " .. pos:getX() ..","..pos:getY()..","..pos:getZ())
		fittingHeight = -pos:getY()
	end
	log_debug("tiAssets.computeFittingHeight " .. self:getName().." : "..tostring(fittingHeight))
	return fittingHeight
end

function tiAsset:getViewport()
	return tiViewportManager.getViewportIdByVisibilityMask (self:getVisibilityMask())
end

-- Non utilisé

function tiAsset:customizeMaterial(matName, img)
	local materialCustom = Material(gScene:createObject(CID_MATERIAL))
	local newText = Texture(gScene:createObject(CID_TEXTURE))
	local ret = newText:setResource(img)
	if not ret then
		dispatchError(TI_INTERNAL_ERROR, "Texture set resource failed, path not found : " .. img)
		return
	end
	
	-- Find the material 
	-- Change it.
	for i in pairs (self.mMesh) do
		local nbMaterial = self.mMesh[i]:getMaterialCount()
		for nbMat = 0, nbMaterial - 1 do
			local currentMaterial = self.mMesh[i]:getMaterial(nbMat)
			local specificMaterialPart = tiFile.baseNameWithoutExtension(currentMaterial:getName())
			if specificMaterialPart == matName then
				materialCustom:setTexture(newText)
				self.mMesh[i]:replaceMaterial(currentMaterial, materialCustom)
				log_info("Change the texture of material : " .. specificMaterialPart)
				find = true
			end
		end
	end
	if not find then
		dispatchError(TI_INTERNAL_ERROR, "Unable to find the material : " .. matName)
	end
end

function tiAsset:getMainNode()
	return self.mMainNode
end

function tiAsset:getVisibilityMask()
	return self.mVisibilityMask
end
		
function tiAsset:setVisibilityMask (mask)
	local i, o
	for i in pairs(self.mMesh) do
--log_debug ("tiAssets.setVisibilityMask " ..  tostring(i) .. " " .. tostring(mask))		
		self.mMesh[i]:setVisibilityMask (mask)
	end
	if self.mMainNode ~= nil then
		self.mMainNode:setVisibilityMask(mask)
		for i = 0, self.mMainNode:getChildrenCount()-1 do
			node = self.mMainNode:getChild(i)
			name = node:setVisibilityMask(mask)
		end
	end
	
	-- Set visibility mask to options
	for i, o in pairs(self:getOptionList()) do
--log_debug(i)
--log_object(o,"option")
		if o["asset"] ~= nil then
			o["asset"]:setVisibilityMask(mask)
		end
	end
	
	self.mVisibilityMask = mask
end

function tiAsset:setRenderGroup (numGroup)
	local i, o
	for i in pairs(self.mMesh) do
		self.mMesh[i]:setRenderGroup (numGroup)
	end
	if self.mMainNode ~= nil then
		self.mMainNode:setRenderGroup (numGroup)
	end
	
	-- Set render group to options
	for i, o in pairs(self:getOptionList()) do
		if o["asset"] ~= nil then
			o["asset"]:setRenderGroup (numGroup)
		end
	end
end
	
function tiAsset:setVisible (state)
	local i, o
--	log_call("tiAsset:setVisible " .. self:getName() .. " " .. tostring(state))		
	for i in pairs(self.mMesh) do
		self.mMesh[i]:setVisible(state)
	end
	if self.mMainNode ~= nil then
		self.mMainNode:setVisible(state)
	end
	
	-- Set visibility to options
	for i, o in pairs(self:getOptionList()) do
		if o["asset"] ~= nil then
			o["asset"]:setVisible(state)
		end
	end
	
	self.mVisible = state
--	log_call_end()			
end
		
function tiAsset:getVisible()
	return self.mVisible
end
	
function tiAsset:setPosition (x, y, z)
	if self.mMainNode ~= nil then
		local parent = self.mMainNode:getParent() 
		if parent ~= nil then
			self.mMainNode:setPosition (x, y, z, parent)
		else
			log_warning ("Asset " .. self:getName() .. " has no parent, can't set its position")
		end
	end
end
		
function tiAsset:getPosition()
	if self.mMainNode ~= nil then
		return self.mMainNode:getPositionXYZ (self.mMainNode:getParent())
	end
	return 0, 0, 0
end

function tiAsset:getOrientation()
	if self.mMainNode ~= nil then
		return self.mMainNode:getOrientationEuler (self.mMainNode:getParent())
	end
	return 0, 0, 0
end

function tiAsset:setOrientation (x, y, z)
	if self.mMainNode ~= nil then
		self.mMainNode:setOrientationEuler (x, y, z, self.mMainNode:getParent())
	end
end

function tiAsset:move (x, y, z)
	if self.mMainNode ~= nil then
		self.mMainNode:translate (x, y, z, self.mMainNode:getParent())
	end
end

function tiAsset:rotate (x, y, z)
	if self.mMainNode ~= nil then
		self.mMainNode:rotate (x, y, z, TI_OBJECT3DREF_LOCAL)
	end
end

function tiAsset:setScale (x, y, z)
	if self.mMainNode ~= nil then
		local scale = Vector3(x, y, z)
		self.mMainNode:setScale (scale)
	end
end

function tiAsset:setPriority (p)
	if self.mMainNode ~= nil then
		self.mMainNode:setRenderGroup (p + 50)
	end
end

function tiAsset:getDrillingPoints()
	local tablePts = {}
	local mesh = findMeshNode (self.mMainNode)
	if mesh ~= nil then
		local nbPoints = 0
		local drills = {}
		err_ret, nbPoints, drills = mesh:getDrillPoints()
		if err_ret == eOk and nbPoints > 0 then
			local pt = Entity(gScene:createObject(CID_OBJECT3D))
			local i
			for i = 0, nbPoints-1 do
				local pt_ref = Vector3()
				log_debug(string.format("pos(%2.2f, %2.2f, %2.2f)", drills[i*3+1], drills[i*3+2], drills[i*3+3] ), 2)
				pt:setPosition(Vector3(drills[i*3+1], drills[i*3+2], drills[i*3+3]), mesh)					
				pt:getPosition(pt_ref)
				log_debug(string.format("-> pos(%2.2f, %2.2f, %2.2f)", pt_ref:getX(), pt_ref:getY(), pt_ref:getZ()), 2)
				table.insert(tablePts, pt_ref)
			end
			gScene:deleteObject(pt)
		else
			log_warning ("No drilling points in asset "..self:getName()..", error: "..err_ret..", please check you have called updateGeometry before")
		end					
	end
	return tablePts
end

local function getAttachTransforms (drills, nbPoints, side)
	local i, j
	-- Compute new attach point for temples
	-- find the two groups of drilling points checking the x sign

	local pointPos = {}
	local pointNeg = {}
	for i = 0, 2 do
		pointPos[i] = 0
		pointNeg[i] = 0
	end
	local nbPointPos = 0
	local nbPointNeg = 0
	for i = 0, nbPoints-1 do
		if drills[i*3+1] > 0 then
			for j = 0, 2 do
				pointPos[j] = pointPos[j] + drills[i*3+(j+1)]
			end
			nbPointPos = nbPointPos+1
		else
			for j = 0, 2 do
				pointNeg[j] = pointNeg[j] + drills[i*3+(j+1)]
			end
			nbPointNeg = nbPointNeg+1
		end
	end

	for i = 0, 2 do
		pointPos[i] = pointPos[i] / nbPointPos
		pointNeg[i] = pointNeg[i] / nbPointNeg
	end
log_debug("point positif: " .. string.format("pos(%2.2f, %2.2f, %2.2f)", pointPos[0], pointPos[1], pointPos[2]), tiAsset.drillingPointLogLevel)			
log_debug("point negatif: " .. string.format("pos(%2.2f, %2.2f, %2.2f)", pointNeg[0], pointNeg[1], pointNeg[2]), tiAsset.drillingPointLogLevel)

	local pointPere
	local pointFils
	if side == "R" then
		pointPere = pointPos
		pointFils = pointNeg
	else
		pointPere = pointNeg
		pointFils = pointPos
	end

log_debug("point pere: " .. string.format("pos(%2.2f, %2.2f, %2.2f)", pointPere[0], pointPere[1], pointPere[2]), tiAsset.drillingPointLogLevel)	
log_debug("point fils: " .. string.format("pos(%2.2f, %2.2f, %2.2f)", pointFils[0], pointFils[1], pointFils[2]), tiAsset.drillingPointLogLevel)
	-- Compute attach transforms
	local transfoLocale = {}
	local transfoFils = {}
	for i = 0, 2 do
		transfoLocale[i] = -pointPere[i]
		transfoFils[i] = pointFils[i]-pointPere[i]
	end
	return transfoLocale, transfoFils
end

function tiAsset:updateGeometry (omaFile, thickness, radius, precision, rawRadius, panto)
-- http://silhouettelab.com/oma-files-individually/
	startChrono("tiAsset:updateGeometry ",omaFile)
	local res =  eFailed
	log_debug ("tiAsset:updateGeometry "..tostring(self.mInstanceName).." oma: "..tostring(omaFile).." thickness: "..tostring(thickness).." radius: "..tostring(radius).." precision: "..tostring(precision).." rawRadius: "..tostring(rawRadius).." panto: "..tostring(panto))
	-- Check that the asset is a lense
	if self:getProductType() == "glasses_lens" then
		local side = "bad"
		if string.starts (self:getProductOption(), "right") then
			side = "R"
			res =  eOk
		else
			if string.starts (self:getProductOption(), "left") then
				side = "L"
				res =  eOk
			else
				log_error ("Cannot find side option in " .. self:getName())
				res =  eFailed
			end
		end
		if res == eOk then
			log_debug ("tiAsset:updateGeometry " .. self.mInstanceName .. " side: " .. side)
			-- Find the mesh node
			local mesh = findMeshNode (self.mMainNode)

			if mesh ~= nil then
			log_debug ("tiAsset:updateGeometry update mesh " .. mesh:getName())
				-- Update mesh geometry
--printEntityNode(mesh)
				res = mesh:updateGeometry (omaFile, side, thickness, radius, precision, rawRadius, panto)
--printEntityNode(mesh)
				if res == eOk then
				-- Get new drilling points
					err_ret, nbPoints, drills = mesh:getDrillPoints()
log_debug(string.format("err_ret %d",err_ret), tiAsset.drillingPointLogLevel)
log_debug(string.format("nbPoints %d",nbPoints), tiAsset.drillingPointLogLevel)
if err_ret == eOk and nbPoints > 0 then
for i = 0, nbPoints-1 do
	log_debug(string.format("pos(%2.2f, %2.2f, %2.2f)", drills[i*3+1], drills[i*3+2], drills[i*3+3] ), tiAsset.drillingPointLogLevel)
end
end				
					-- Compute transfo from new drilling points							
					transfoMesh, transfoFils = getAttachTransforms (drills, nbPoints, side)
					transfoMesh, transfoFils = getAttachTransforms (drills, nbPoints, side)
				
					log_debug("transfo mesh: " .. string.format("pos(%2.2f, %2.2f, %2.2f)", transfoMesh[0], transfoMesh[1], transfoMesh[2]), tiAsset.drillingPointLogLevel)	
					log_debug("transfo fils: " .. string.format("pos(%2.2f, %2.2f, %2.2f)", transfoFils[0], transfoFils[1], transfoFils[2]), tiAsset.drillingPointLogLevel)
						-- Update local transform
					mesh:setPosition (transfoMesh[0], transfoMesh[1], transfoMesh[2], self.mMainNode)
					
					-- Update attach temples transform
					local i
					for i = 0, self.mMainNode:getChildrenCount()-1 do
						node = self.mMainNode:getChild(i)
						name = node:getName()
						-- Criteria: the name starts with "left" or "right", not very robust, we will have to improve that 
						if tiAsset.nodeNameIsOption (name, "left") or tiAsset.nodeNameIsOption (name, "right") then
							node:setPosition (transfoFils[0], transfoFils[1], transfoFils[2], self.mMainNode)
						end
					end
				else
					dispatchError(TI_FILE_NOT_FOUND, "Cannot load file " .. omaFile)
				end
			else
				dispatchError(TI_FILE_NOT_FOUND, "Cannot find mesh for asset " .. self:getName())
			end
		else
			dispatchError(TI_FILE_NOT_FOUND, "Cannot find parent side 'left' or 'right' for asset " .. self:getName())
		end
	else
		dispatchError(TI_INCONSISTENT_FORMAT, " tiAsset:updateGeometry can only be applied to asset with 'product' field = 'glasses_lens', asset '" .. self:getName() .. "' has a 'product' field = '" .. self:getProductType() .. "'")
	end	
	stopChrono("tiAsset:updateGeometry ",omaFile)
	return res
end

-----------------------------------------------------------------------------------		
--				Asset data management		
-----------------------------------------------------------------------------------		

function tiAsset:isCustomizable()
	return self.mIsCustomizable
end

function tiAsset:setCustomizable(val)
	self.mIsCustomizable = val
end

function tiAsset:getName()
--printStack()
	return self.mInstanceName
end

function tiAsset:setName(name)
	self.mInstanceName = name
	self.mMainNode:setName (self.mInstanceName)
end

function tiAsset:getProductType()
	return self.mDataAsset["product"]
end

function tiAsset:setProductType (val)
	self.mDataAsset["product"] = val
end

function tiAsset:setProductOption (val)
	self.mDataAsset["product_options"] = val
end

function tiAsset:getProductOption()
	local res = ""
	if self.mDataAsset["product_options"] ~= nil then
		res = self.mDataAsset["product_options"]
	end
	return res
end

function tiAsset:getModelType()
	return self.mDataAsset["type"]
end
	
function tiAsset:isSelected()
	return self.mIsSelected
end
		
function tiAsset:getSelectedAsset() 
	log_call("tiAsset:getSelectedAsset " .. tostring(this) .. " " .. self.mInstanceName)	
	local res = nil
	if self.mIsSelected then
log_debug("tiAsset:getSelectedAsset is selected")
--log_object(this,"tiAsset:getSelectedAsset " .. self.mInstanceName)
		res = this
	end
	log_call_end()				
	return res
end

function tiAsset:getObjectByName (objName)
	local asset = nil
	log_call("tiAsset:getObjectByName " .. objName .. " " .. self.mInstanceName)		
	if self.mInstanceName == objName then
log_debug ("tiAsset:getObjectByName found")			
		asset = this
	else
		local i, o
		for i, o in pairs(self:getOptionList()) do
			if o["asset"] ~= nil then
				local name = o["asset"]:getName()
log_debug("tiAsset:getObjectByName " .. objName .. " mOptionsList[" .. tostring(i) .. "]: " .. name)				
				if name == objName then
					asset = o["asset"]
				end
			end
		end
	end
	log_call_end()			
	return asset
end
	
-----------------------------------------------------------------------------------		
--							Option management
-----------------------------------------------------------------------------------
		
function tiAsset:findOption (productType, optionProduct)
	local i, o
	local res = nil
	if self:getOptionList() ~= nil then 
		for i, o in pairs(self:getOptionList()) do
			if o["product_options"] == nil then
				o["product_options"] = ""
			end
			if o["product"] == productType then
				if o["product_options"] == optionProduct then
					res = o
				end
			end
			log_debug("tiAsset:findOption in " .. self.mInstanceName .. " productType: " .. o["product"] .. " =? " .. productType .. " optionProduct: " .. o["product_options"] .. " =? " .. optionProduct, tiAsset.optionLogLevel)						
		end
	end
	log_debug("tiAsset:findOption " .. tostring (res ~= nil), tiAsset.optionLogLevel)
	return res
end

function tiAsset:getOptionList()
	local options = {}
	if self.mDataAsset ~= nil then
		if self.mDataAsset["options"] ~= nil then
			options = self.mDataAsset["options"]
		end
	end
	return options
end
		
function tiAsset:getOptionListByProduct(productType, productOption)
	log_call ("tiAsset:getOptionListByProduct " ..  productType .. " " ..  productOption .. " in " .. self:getName(), tiAsset.optionLogLevel)
	local options = {}
	local i, o
	for i, o in pairs(self:getOptionList()) do
		if o["product"] == productType then
			if o["product_options"] == nil then
				o["product_options"] = ""
			end
			if o["product_options"] == productOption then
				table.insert(options, o)
			end
		end
	end
	log_debug ("tiAsset:getOptionListByProduct found nb: " ..  tostring(table.getn(options)), tiAsset.optionLogLevel)
	log_call_end()
	return options
end

function tiAsset:isSameKind (asset)
	local res = false
	if asset ~= nil then
		if self:getProductType() == asset:getProductType() then
			if self:getProductOption() == asset:getProductOption() then
				res = true
			end
		end
	end
	log_debug("tiAsset:isSameKind " .. self.mInstanceName .. " " .. asset:getName() .. " productType: " .. self:getProductType() .. " =? " .. asset:getProductType() .. " productOption: " .. self:getProductOption() .. " =? " .. asset:getProductOption() .. " " .. tostring(res))						
	return res
end

function tiAsset:hasSameKindOfAccessory (asset)
	local res = false
	if asset ~= nil then
		-- check if the options are the same
		local optionList = self:getOptionList()
		if  table.getn(optionList) > 0 then
			res = true
			for i, o in pairs(self:getOptionList()) do
				local oo = asset:getOptionListByProduct (o["product"],  o["product_options"])
				res = res and (table.getn(oo) == 1)
			end
		end
	end
	log_debug("tiAsset:hasSameKindOfAccessory " .. self.mInstanceName .. " " .. asset:getName() .. " " .. tostring(res), tiAsset.optionLogLevel)						
	return res
end

function tiAsset:isCompatibleAccessoryOf (asset)
	local i, o
	local res = false
	log_debug("tiAsset:isCompatibleAccessoryOf " .. self:getName() .. " for " .. asset:getName(), tiAsset.optionLogLevel)						
	
	if asset ~= nil then
		for i, o in pairs(asset:getOptionList()) do
			log_debug("tiAsset:isCompatibleAccessoryOf ... product: " .. o["product"], tiAsset.optionLogLevel)				
			if o["product"] ~= nil then
				if o["product"] == self:getProductType() then
					if o["product_options"] ~= nil then
						log_debug("tiAsset:isCompatibleAccessoryOf ... product option: " .. o["product_options"], tiAsset.optionLogLevel)				
						if o["product_options"] == self:getProductOption() then
							res = true
						end
					end
				end
			end
		end
	end
	log_debug("tiAsset:isCompatibleAccessoryOf " .. tostring(res), tiAsset.optionLogLevel)				
	return res
end

function tiAsset:couldHaveAccessory (asset)
	return self:findOption (asset:getProductType(), asset:getProductOption()) ~= nil
end

function tiAsset:linkAccessory (optionToLink)
	log_call("tiAsset:linkAccessory " .. optionToLink:getName() .. " to " .. self.mInstanceName .. "( ProductType:" .. optionToLink:getProductType() .. " ProductOption: " .. optionToLink:getProductOption() .. ")")		

	local res = false
	-- Check if the accessory is compatible
	local arrayPotentialOptions = self:getOptionListByProduct (optionToLink:getProductType(), optionToLink:getProductOption())
	local k, v
	for k, v in pairs(arrayPotentialOptions) do
		log_debug ("tiAsset:linkAccessory arrayPotentialOptions for " .. self.mInstanceName .. ": ".. v["product_options"], tiAsset.optionLogLevel)			
		
		-- search the 3D node to attach the option
		
		local fatherNode = nil
		if self.mMainNode ~= nil then
			local i
			for i = 0, self.mMainNode:getChildrenCount()-1 do
				node = self.mMainNode:getChild(i)
				childName = node:getName()
				log_debug("tiAsset:linkAccessory fils " .. tostring (i) .. " name:" .. childName .. " searching for " .. v["product_options"], tiAsset.optionLogLevel)
				found = string.find (childName, tiAsset.getOptionNodeName(v))
				
				-- Criteria: the name starts with the option name
				-- In fact adding many times nodes with same name will add a number after the name, ex: left, left1, left2 ...
				-- So it's why we check only the starting of the name
				
				if found == 1 then
					log_debug("tiAsset:linkAccessory found: " .. found)
					fatherNode = node
				end
			end
			if fatherNode ~= nil then
				log_debug ("tiAsset:linkAccessory " .. self.mInstanceName .. " addChild node: " .. optionToLink:getMainNode():getName() .. " to node: " .. fatherNode:getName(), tiAsset.optionLogLevel)	
				local optionMainNode = optionToLink:getMainNode()
				if optionMainNode ~= nil then
					fatherNode:addChild (optionMainNode)
				else
					log_debug ("tiAsset:linkAccessory no main node in asset " .. optionToLink:getName(), tiAsset.optionLogLevel)
				end
				
				if (v["priority"] ~= nil) then
					optionToLink:setPriority(v["priority"])
				end
				
				optionToLink:setVisible(self:getVisible())
				optionToLink:setVisibilityMask(self:getVisibilityMask())
				
				log_debug ("tiAsset:linkAccessory Link option " .. v["product_options"] .. " " .. optionToLink:getName() .. " to " .. self.mInstanceName, tiAsset.optionLogLevel)
				self.mDataAsset["options"][v["product_options"]]["asset"] = optionToLink
				
				res = true
			else
				log_error ("tiAsset:linkAccessory Error no 3D object found named " .. v["product_options"])
			end
		else
			log_error ("tiAsset:linkAccessory Warning, mainNode null for asset " .. self.mInstanceName)
		end
	end
	log_call_end()			
	return res
end

-- 		Compatible accessory add

function tiAsset:addAccessory (asset)
--[[		
	Si il y a deja un accessoire compatible
		Décrocher l’accessoire du pere
	Raccrocher nouvel accessoire à l’asset
	Si il avait un accessoire et qu’il avait des accessoires 
		Décrocher les sous accessoire de leur pere (l’accessoire trouvé)
		Raccrocher les sous accessoire au nouvel accessoire
]]--
	log_call ("tiAsset:addAccessory " .. asset:getName() .. " to " .. self:getName())
	local oldChildAsset = nil
	local option = self:findOption (asset:getProductType(), asset:getProductOption())
	if option ~= nil then
		oldChildAsset = option["asset"]
		if asset ~= oldChildAsset then 
			-- If this new accessory is not already linked to this asset
			self:linkAccessory(asset)
			if oldChildAsset ~= nil then
				local i, o
				for i, o in pairs(oldChildAsset:getOptionList()) do
					if o["asset"] ~= nil then
						if asset:linkAccessory(o["asset"]) then
							o["asset"] = nil
						end
					end
				end
			end
		end
		log_call_end()
	end				
	if oldChildAsset ~=  nil then
		log_debug ("tiAsset:addAccessory " .. asset:getName() .. " old child to remove: "..oldChildAsset:getName(), tiAsset.optionLogLevel)
	end
	log_call_end()
	return oldChildAsset
end

function tiAsset:removeAccessory (asset)
	local res = false
	local i, o
	for i, o in pairs(self:getOptionList()) do
		if o["asset"] == asset then
			gScene:addChild (asset:getMainNode())
			asset:setVisible(false)
			o["asset"] = nil
			res = true
		end
	end
	return res
end

function tiAsset:grabAccessoryFrom (asset)
	log_call ("tiAsset:grabAccessoryFrom " .. asset:getName() .. " to " .. self:getName(), tiAsset.optionLogLevel)
	local i, o
	for i, o in pairs(asset:getOptionList()) do
		if o["asset"] ~= nil then
			if self:linkAccessory(o["asset"]) then
log_debug("tiAsset:grabAccessoryFrom " .. o["asset"]:getName() .. " " .. " detached", tiAsset.optionLogLevel)
				o["asset"] = nil
			end
		end
	end
	log_call_end()			
end

-----------------------------------------------------------------------------------		
--				Gestion des paths		
-----------------------------------------------------------------------------------	

-- The base information is the remote path of the json description file
-- It is the access key coming from the catalog
-- All the various paths used to load etc. are built based on this base information
-- To illustrate the following function, let's take this example:
-- remotePath: https://cdn.trylive.com/trylive-eyewear/silhouette/carbon_t1-mst951_17-6050/zip_3/low/carbon_t1-mst951_17-6050.zip/carbon_t1/models/mst951_17/assets/6050/3d/3d_export/carbon_t1_mst951_17_6050_3d.json 

function tiAsset:getBaseLocalPath()
	return getAppDataPath().."assets/"
end

function tiAsset:getJsonRemotePath()
log_debug ("tiAsset:getJsonRemotePath mRemotePath: "..tostring(self.mRemotePath), tiAsset.pathLogLevel)
	return self.mRemotePath
end

-- Pour renvoyer un identifiant unique utilisé dans les logs et chronos

function tiAsset:getId()
	return self:getJsonRemotePath()
end

-- returns: https://cdn.trylive.com/trylive-eyewear/silhouette/carbon_t1-mst951_17-6050/zip_3/low/carbon_t1-mst951_17-6050.zip/carbon_t1/models/mst951_17/assets/6050/3d/3d_export/

function tiAsset:getJsonRemoteDirPath()
	if self.mJsonRemoteDirPath == nil then
		self.mJsonRemoteDirPath = tiFile.dirName (self:getJsonRemotePath())
	end
log_debug ("tiAsset:getJsonRemoteDirPath mRemotePath: "..tostring(self:getJsonRemotePath()).." mJsonRemoteDirPath: "..tostring(self.mJsonRemoteDirPath), tiAsset.pathLogLevel)
	return self.mJsonRemoteDirPath
end

-- returns: https://cdn.trylive.com/trylive-eyewear/silhouette/carbon_t1-mst951_17-6050/zip_3/low/carbon_t1-mst951_17-6050.zip

function tiAsset:getZipRemotePath()
--printStack()
	if self.mZipRemotePath == nil then	
		local zippos = string.find (self:getJsonRemotePath(), "%.zip")
log_debug("tiAsset:getZipRemotePath zippos: "..tostring(zippos))
		if zippos ~= nil then
			self.mZipRemotePath = string.sub (self:getJsonRemotePath(), 1, zippos+3)
		else
			self.mZipRemotePath = ""
		end
	end
log_debug ("tiAsset:getZipRemotePath mRemotePath: "..tostring(self:getJsonRemotePath()).." mZipRemotePath: "..tostring(self.mZipRemotePath), tiAsset.pathLogLevel)
	return self.mZipRemotePath
end

-- returns: https://cdn.trylive.com/trylive-eyewear/silhouette/carbon_t1-mst951_17-6050/zip_3/low/

function tiAsset:getZipRemoteDirPath()
	if self.mZipRemoteDirPath == nil then
		self.mZipRemoteDirPath = tiFile.dirName (self:getZipRemotePath())
	end
log_debug ("tiAsset:getZipRemoteDirPath mRemotePath: "..tostring(self:getJsonRemotePath()).." mZipRemoteDirPath: "..tostring(self.mZipRemoteDirPath), tiAsset.pathLogLevel)
	return self.mZipRemoteDirPath
end

-- returns: carbon_t1_mst951_17_6050_3d.json

function tiAsset:getJsonFileName()
	if self.mJsonFileName == nil then
		self.mJsonFileName = tiFile.baseName (self:getJsonRemotePath())
	end
log_debug ("tiAsset:getJsonFileName mRemotePath: "..tostring(self:getJsonRemotePath()).." mJsonFileName: "..tostring(self.mJsonFileName), tiAsset.pathLogLevel)
	return self.mJsonFileName
end

-- returns: carbon_t1-mst951_17-6050.zip

function tiAsset:getZipFileName()
	if self.mZipFileName == nil then
		self.mZipFileName = tiFile.baseName (self:getZipRemotePath())
	end
log_debug ("tiAsset:getZipFileName mRemotePath: "..tostring(self:getJsonRemotePath()).." mZipFileName: "..tostring(self.mZipFileName), tiAsset.pathLogLevel)
	return self.mZipFileName
end

-- returns: carbon_t1/models/mst951_17/assets/6050/3d/3d_export/carbon_t1_mst951_17_6050_3d.json

function tiAsset:getJsonSubPath()
	if self.mJsonSubPath == nil then
	log_debug("tiAsset:getJsonSubPath self:getJsonRemotePath: "..tostring(self:getJsonRemotePath()), tiAsset.pathLogLevel)
		local occ = string.find(self:getJsonRemotePath(), "%.zip") -- find '.zip' in path
		if occ ~= nil then
			self.mJsonSubPath = string.sub (self:getJsonRemotePath(), occ+string.len(".zip")) -- from after '.zip' until the end
		else
			occ = string.len(self:getBaseLocalPath())
			self.mJsonSubPath = string.sub (self:getJsonRemotePath(), occ)
		end
	end
log_debug ("tiAsset:getJsonSubPath mRemotePath: "..tostring(self:getJsonRemotePath()).." mJsonSubPath: "..tostring(self.mJsonSubPath), tiAsset.pathLogLevel)
	return self.mJsonSubPath
end

-- returns: /var/mobile/Containers/Data/Application/CD5F76CD-DB44-4C25-8AA2-AE6FAF91CA81/Documents/carbon_t1-mst951_17-6050.zip/carbon_t1/models/mst951_17/assets/6050/3d/3d_export/carbon_t1_mst951_17_6050_3d.json

function tiAsset:getJsonLocalPath()
	if self.mJsonLocalPath == nil then
log_debug("tiAsset:getJsonLocalPath self:getBaseLocalPath(): "..tostring(self:getBaseLocalPath()), tiAsset.pathLogLevel)	
log_debug("tiAsset:getJsonLocalPath self:getZipFileName(): "..tostring(self:getZipFileName()), tiAsset.pathLogLevel)	
log_debug("tiAsset:getJsonLocalPath self:getJsonSubPath(): "..tostring(self:getJsonSubPath()), tiAsset.pathLogLevel)	
		self.mJsonLocalPath = tiFile.cleanPath (self:getBaseLocalPath()..self:getZipFileName().."/"..self:getJsonSubPath())
	end
log_debug ("tiAsset:getJsonLocalPath mRemotePath: "..tostring(self:getJsonRemotePath()).." mJsonLocalPath: "..tostring(self.mJsonLocalPath), tiAsset.pathLogLevel)
	return self.mJsonLocalPath
end

-- returns: /var/mobile/Containers/Data/Application/CD5F76CD-DB44-4C25-8AA2-AE6FAF91CA81/Documents/carbon_t1-mst951_17-6050.zip/carbon_t1/models/mst951_17/assets/6050/3d/3d_export/

function tiAsset:getJsonLocalDirPath()
	if self.mLocalPath == nil then
		self.mLocalPath = tiFile.dirName (self:getJsonLocalPath())
	end
log_debug ("tiAsset:getJsonLocalDirPath mRemotePath: "..tostring(self:getJsonRemotePath()).." mLocalPath: "..tostring(self.mLocalPath), tiAsset.pathLogLevel)
	return self.mLocalPath
end

-- returns: /var/mobile/Containers/Data/Application/CD5F76CD-DB44-4C25-8AA2-AE6FAF91CA81/Documents/carbon_t1-mst951_17-6050.zip

function tiAsset:getZipLocalPath()
	if self.mZipLocalPath == nil then
		local zippos = string.find (self:getJsonLocalDirPath(), "%.zip")
		if zippos ~= nil then
			self.mZipLocalPath = tiFile.cleanPath (string.sub (self:getJsonLocalDirPath(), 1, zippos+3))
		else
			self.mZipLocalPath = ""
		end
	end
log_debug ("tiAsset:getZipLocalPath mRemotePath: "..tostring(self:getJsonRemotePath()).." mZipLocalPath: "..tostring(self.mZipLocalPath), tiAsset.pathLogLevel)
	return self.mZipLocalPath
end

-- returns: /var/mobile/Containers/Data/Application/92C2409F-5690-4AC5-A384-6E47CF9B0943/Documents/

function tiAsset:getZipLocalDirPath()
	if self.mZipLocalDirPath == nil then
		self.mZipLocalDirPath = tiFile.dirName (self:getZipLocalPath())
	end
log_debug ("tiAsset:getZipLocalDirPath mRemotePath: "..tostring(self:getJsonRemotePath()).." mZipLocalDirPath: "..tostring(self.mZipLocalDirPath), tiAsset.pathLogLevel)
	return self.mZipLocalDirPath
end

function tiAsset:isZipped()
	if self.mIsZipped == nil then
		local localDir = self:getJsonLocalDirPath().gsub (self:getJsonLocalDirPath(), "[^/]+%.zip.*", "")
		self.mIsZipped = (localDir ~= self:getJsonLocalDirPath())
	end
log_debug ("tiAsset:isZipped mRemotePath: "..tostring(self:getJsonRemotePath()).." mIsZipped: "..tostring(self.mIsZipped), tiAsset.pathLogLevel)
	return self.mIsZipped
end	
	
------------------------------------------------------------------------------		
--					load and download
------------------------------------------------------------------------------		

function tiAsset:download()
	local bSuccess = false
	log_call("tiAsset:download mRemotePath: " .. tostring(self:getJsonRemotePath()))
	if (self:getJsonRemotePath() ~= nil) and (self:getJsonRemotePath() ~= "") then
	
		log_debug("tiAsset:download JsonRemoteDirPath: " .. self:getJsonRemoteDirPath() .. " JsonLocalDirPath: " .. self:getJsonLocalDirPath())	
	
		local zipExists = tiFile.exists (self:getZipLocalPath())
		local originOk = tiAsset.checkOrigin (self:getZipRemoteDirPath(), self:getZipLocalPath())
		
		local chronoName
		if zipExists and originOk then
			chronoName = "tiAsset:download from disk cache"
		else
			chronoName = "tiAsset:download from server"
		end
		startChrono (chronoName, self:getId())	
		if zipExists and originOk then
	--		log_warning ("Retreive ".. zipFile.." from cache")
			bSuccess = true
		elseif tiNetwork.download (self:getZipRemoteDirPath(), self:getZipLocalDirPath()) then
			tiAsset.updateOrigin (self:getZipRemoteDirPath(), self:getZipLocalPath())
			bSuccess = true					
		else
			bSuccess = false
		end
		stopChrono (chronoName, self:getId())
	else
		log_error("tiAsset:download undefined remotePath")
	end
	log_call_end()				
	return bSuccess
end

-- Download the json file, parse it and init all the informations.

function tiAsset:loadInfo()
	local bSuccess = false
	
	log_call("tiAsset:loadInfo jsonFileName: " .. self:getJsonFileName() .. " instanceName: " .. tostring(self.mInstanceName))	
	log_debug("tiAsset:loadInfo: mRemotePath: "..self:getJsonRemotePath().." mLocalPath: "..tostring(self:getJsonLocalDirPath()).." JsonLocalPath: "..self:getJsonLocalPath())
	startChrono("tiAsset:loadInfo json", self:getId())

	if (pcall (function()
					self.mDataAsset = json.load (self:getJsonLocalPath())
				end
				)) then

		-- Check that there is no cycling option
		-- A cycling option is an option that have the same product and product_option that the product
		local Ok = true
		for i, o in pairs(self:getOptionList()) do
			if o["product"] == self.mDataAsset["product"] then
				if o["product_options"] == self.mDataAsset["product_options"] then
					log_error ("Asset "..self:getJsonFileName().." ignored because of cycling option: "..tostring(o["product"])..":"..tostring(o["product_options"]))
					Ok = false
				end
			end
		end
		if not Ok then
			self.mDataAsset = nil
		end
	else
		self.mDataAsset = nil
	end
	
--log_object(self.mDataAsset,"self.mDataAsset")		

	-- Check the data
	if self.mDataAsset == nil then
		dispatchError (TI_INTERNAL_ERROR, "Error during the parsing of the "..self:getJsonFileName().." asset config file.")
		assetsLoadingStatusChanged (TI_LOADING_ASSET_ERROR, self.mInstanceName)
	elseif self.mDataAsset["product"] == nil then 
		dispatchError (TI_INTERNAL_ERROR, "The asset didn't contain product type. The product is mandatory")
		assetsLoadingStatusChanged (TI_LOADING_ASSET_ERROR, self.mInstanceName)
	else
--log_object(self.mDataAsset,"self.mDataAsset")
	
		bSuccess = true
		if self.mDataAsset["customizable"] ~= nil then
			self.mIsCustomizable = self.mDataAsset["customizable"]
		end
		-- Do we use a special ghost ? (ex: specific wrist for watches)
		if self.mDataAsset["ghost"] ~= nil then 
			log_info ("self.mDataAsset[ghost] = " .. self.mDataAsset["ghost"])
			self.mGhost = self.mDataAsset["ghost"]
		end
	end			
		
	stopChrono("tiAsset:loadInfo json", self:getId())
--			self:printAssetInfo()
	log_call_end()				
	return bSuccess
end

function tiAsset:load()
	local res = false
		
	log_call("tiAsset:load remotePath: "..tostring(self:getJsonRemotePath()).." instanceName: "..tostring(self.mInstanceName))
	startChrono("tiAsset:load",tostring(self:getId()))

	if self:download() then
		if self.mInstanceName == nil then
			self.mInstanceName = "no instance"
		end

		if self:loadInfo() then			
			res = self:createObject3D (not self:isZipped(), true)
		end
	else
		assetsLoadingStatusChanged (TI_LOADING_ASSET_ERROR, tostring(self.mInstanceName))
		dispatchError (TI_INCONSISTENT_PARAMETERS, "Unable to download asset config file " .. tostring(self:getJsonRemotePath()) .. " .")					
	end
	stopChrono("tiAsset:load",tostring(self:getId()))	
	log_call_end()
	return res
end

function tiAsset:unlinkChild()
	local orphans = {}
	local i, o
log_call("tiAsset:unlinkChild " .. tostring (this) .. " " .. self.mInstanceName .. " node: " .. tostring(self.mMainNode))
	-- Release options
	for i, o in pairs(self:getOptionList()) do
		if o["asset"] ~= nil then
log_debug("tiAsset:unlinkChild release accessory " .. o["asset"].getName())	
			table.insert (orphans, o["asset"])
			gScene:addChild (o["asset"]:getMainNode())
		end
	end

log_call_end()		
	return orphans
end

function tiAsset:delete()
	local orphans = {}
	local i, o
	log_call("tiAsset:delete " .. tostring (self) .. " " .. self.mInstanceName .. " node: " .. tostring(self.mMainNode))
	-- Release options
	for i, o in pairs(self:getOptionList()) do
		if o["asset"] ~= nil then
log_debug("tiAsset:delete release accessory " .. o["asset"]:getName())	
			table.insert (orphans, o["asset"])
			gScene:addChild (o["asset"]:getMainNode())
		end
	end

	for i in pairs(self.mMesh) do
		if self.mMesh[i] ~= nil then
			gScene:deleteObject(self.mMesh[i])
		end
	end
	deleteObject (self.mMainNode)			
	self.mMainNode = nil
	self.mMesh = {}
	self.mDataAsset = {}
	self = {}
	log_call_end()		
	return orphans
end

local function copyTable (t)
	local res = {}
	for i, o in pairs(t) do
		if type(o) == "table" then
			res[i] = copyTable(o)
		else
			res[i] = o
		end
	end
	return res
end

function tiAsset:duplicate (instanceName)
--	self:print ("tiAsset:duplicate  ORIGINAL")
--			local duplicata = clone(this)
	local duplicata = tiAsset:create({remotePath = self:getJsonRemotePath(), instanceName = instanceName})
	duplicata.mDataAsset = copyTable(self.mDataAsset)
	duplicata.mLocalPath = self:getJsonLocalDirPath()
	duplicata:createObject3D (false, true)
				
--	duplicata:print ("tiAsset:duplicate DUPLICATA")
	
	return duplicata
end

function tiAsset:duplicate1()
	self:print ("ORIGINAL")
--			local duplicata = clone(this)
	local duplicata = deepCopy(this)
	duplicata:createObject3D (false, true)
				
	duplicata:print ("COPIE")
	
	return duplicata
end

function tiAsset:select(b)
--	log_call("tiAsset:select " .. tostring(self) .. " " .. self.mInstanceName .. " " .. tostring(b))			
	self.mIsSelected = b
	assetSelectStatusChanged(self.mInstanceName, b)
--	log_call_end()			
end	

function tiAsset:printAssetInfo() 
	log_debug ("---------- Asset info " .. tostring(self) .. " ------------")
	log_debug ("product: " .. self:getProductType())
	log_debug ("product_options: " .. self:getProductOption())
	if self.mDataAsset["production_version"] ~= nil then
		log_debug ("production_version: " .. self.mDataAsset["production_version"])
	end
	if self:getOptionList() ~= nil then
		local i, o
		for i, o in pairs(self:getOptionList()) do
			log_debug (" LinkedProduct " .. tostring(i) .. " product " .. o["product"] .. " option " .. o["product_options"])
		end
	end
	log_debug ("-----------------------------------------------------")
end

function tiAsset:print(title)
--printStack()
	log_debug("------------------------------------------------------------------------------------------")
	if title ~= nil then
		log_debug(title)
	end
	log_debug ("self.mInstanceName: " .. tostring(self.mInstanceName))
	log_debug ("self.mRemotePath: " .. tostring(self:getJsonRemotePath()))
	log_debug ("self.mLocalPath: " .. tostring(self:getJsonLocalDirPath()))
	log_debug ("self.mIsZipped: " ..  tostring(self:isZipped()))
	self:printAssetInfo() 
end

function printEntityNode(node)
	local entity = Entity(node)
	log_debug("Entity: "..entity:getName())
	log_debug("  Materials: "..entity:getName())
	local nbmat = entity:getMaterialCount ()
	if nbmat ~= nil then
		local i
		for i = 0,nbmat-1 do
			local mat = entity:getMaterial(i)
			if mat ~= nil then
				log_debug ("    "..i..": "..mat:getName())
			else
				log_debug("    "..i..": nil")
			end
		end
	else
		log_debug("  nb mat = nil")
	end
end
		
function deepcopy(orig)
	local orig_type = type(orig)
	local copy
	if orig_type == 'table' then
		copy = {}
		for orig_key, orig_value in next, orig, nil do
			copy[deepcopy(orig_key)] = deepcopy(orig_value)
		end
		setmetatable(copy, deepcopy(getmetatable(orig)))
	elseif orig_type ==  'string' then
		copy = orig.." "
	else -- number, string, boolean, etc
		copy = orig
	end
	return copy
end

function deepCopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return setmetatable(new_table, getmetatable(object))
    end
    return _copy(object)
end

function clone (t) -- deep-copy a table
    if type(t) ~= "table" then return t end
    local meta = getmetatable(t)
    local target = {}
    for k, v in pairs(t) do
        if type(v) == "table" then
            target[k] = clone(v)
        else
            target[k] = v
        end
    end
    setmetatable(target, meta)
    return target
end

