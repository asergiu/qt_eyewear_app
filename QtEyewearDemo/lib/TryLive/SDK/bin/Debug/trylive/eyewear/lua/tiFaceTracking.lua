-- tiFaceTracking.lua script
log_loaded_info("tiFaceTracking.lua")
-----------------------------
-- This class manages the face tracking and the MLT.

-- This function creates a new instance of SimpleClass
--

tiFaceTracking = inheritsFrom (tiFaceCV)

tiFaceTracking.logLevel = 5

-- distance min and max to the camera in mm
tiFaceTracking.MinZ = 100
tiFaceTracking.MaxZ = 1200
-- Max X and Y distance value to the center of the screen
-- The screen coordinate system is -0.5 -> 0.5
tiFaceTracking.MaxX = 0.5
tiFaceTracking.MaxY = 0.5
-- To change the center
tiFaceTracking.CenterX = 0
tiFaceTracking.CenterY = 0

function tiFaceTracking.setConstraints (sizeX, sizeY, centerX, centerY, minZ, maxZ)
	log_debug("tiFaceTracking:setConstraints sizeX: "..tostring(sizeX).. " sizeY: "..tostring(sizeY).. " centerX: "..tostring(centerX).. " centerY: "..tostring(centerY).. " minZ: "..tostring(minZ).. " maxZ: "..tostring(maxZ))
	tiFaceTracking.MaxX = sizeX/2
	tiFaceTracking.MaxY = sizeY/2
	tiFaceTracking.CenterX = centerX
	tiFaceTracking.CenterY = centerY
	tiFaceTracking.MinZ = minZ
	tiFaceTracking.MaxZ = maxZ	
end

function tiFaceTracking.setConstraints1 (properties)
	if properties ~= nil then
		if properties.minZ ~= nil then
			tiFaceTracking.MinZ = properties.minZ
		end
		if properties.maxZ ~= nil then
			tiFaceTracking.MaxZ = properties.maxZ
		end
		if properties.centerX ~= nil then
			tiFaceTracking.CenterX = properties.centerX
		end
		if properties.centerY ~= nil then
			tiFaceTracking.CenterY = properties.centerY
		end
		if properties.sizeX ~= nil then
			tiFaceTracking.CenterX = properties.sizeX/2
		end
		if properties.sizeY ~= nil then
			tiFaceTracking.CenterY = properties.sizeY/2
		end
	end
end

function tiFaceTracking:init (properties)
	log_debug("tiFaceTracking:init properties: "..tostring(properties))

	if self.mTrackingFile == nil then
--		self.mTrackingFile = self.baseInternalTrackingFile.."trackerNFT2.xml"
		self.mTrackingFile = self.baseInternalTrackingFile.."tracker.json"
	end

	self.mPosition = Vector3()
	self.mOrientation = Quaternion()
	self.mTiareInternalPD = 62
	self.mIrisLeft3D = Vector3()
	self.mIrisRight3D = Vector3()
end	

function tiFaceTracking:resetReferencePD()
	log_debug("tiFaceTracking:resetReferencePD before : "..self.mTiareInternalPD)
------- AAAAAAAAAAAAAAAAAAAA  bizarre .....
	self.mTiareInternalPD = self:getPupilDistance()
	log_debug("tiFaceTracking:resetReferencePD after : "..self.mTiareInternalPD)
end
					
-- return pixel coordinate (centered) of the 3D position 
function tiFaceTracking:getCoordZlocked(iPos)
	local X = iPos:getX()
	local Y = iPos:getY()
	local Z = iPos:getZ()
	local u = X * self.mFocalX / Z
	local v = Y * self.mFocalY / Z
	return u,v
end
		
function tiFaceTracking:isCentered()
	local err, x, y = self.mMLTPlugin:get2DCenter (self.mTrackingIndex, self.mTargetIndex)
	log_debug("center x "..x.." y "..y)
	x = math.abs (x - 0.5 - tiFaceTracking.CenterX)
	y = math.abs (y - 0.5 - tiFaceTracking.CenterY)
	log_debug("center norm x "..x.." y center "..y)
	
	return (x < tiFaceTracking.MaxX) and (y < tiFaceTracking.MaxY)
end

function tiFaceTracking:isTooClose()
	return (math.abs(self.mPosition:getZ()) < tiFaceTracking.MinZ)
end
			

function tiFaceTracking:isTooFar()
	return (math.abs(self.mPosition:getZ()) > tiFaceTracking.MaxZ)
end

-- Returns
-- 1. A boolean indicating if a face is tracked or not 
-- 2. the current position state
function tiFaceTracking:getPositionState()
log_debug("tiFaceTracking:getPositionState "..self.mPosition:getX().. " "..self.mPosition:getY().." "..self.mPosition:getZ(), tiFaceTracking.logLevel)
	local state = tiFaceCV.TI_POSITION_STATE_DEF.undefined
	if (self.mIsMltTracking) then
--		self.mMLTPlugin:getTargetPos(self.mTrackingIndex, self.mTargetIndex, self.mPosition, self.mOrientation)
		if not self:isCentered() then
			state = tiFaceCV.TI_POSITION_STATE_DEF.notCentered
		elseif self:isTooClose() then
			state = tiFaceCV.TI_POSITION_STATE_DEF.tooClose
		elseif self:isTooFar() then
			state = tiFaceCV.TI_POSITION_STATE_DEF.tooFar
		else
			state = tiFaceCV.TI_POSITION_STATE_DEF.wellPositionned
		end
	end
log_debug ("tiFaceTracking:getPositionState "..tiFaceCV.positionStateToString(state), tiFaceTracking.logLevel)
	return state
end

function tiFaceTracking:isTracking()
	log_debug("tiFaceTracking:isTracking mTrackingState: "..tiFaceCV.trackingStateToString(self.mTrackingState), tiFaceCV.logLevelHigh)
	
	if (self.mPaused) then
		return false
	end
	
	if (self.mTrackingState == tiFaceCV.TI_STEPSTATE_DEF.tracking) then 	

		if (self:getPositionState() == tiFaceCV.TI_POSITION_STATE_DEF.wellPositionned) then 
			return true	
		else
			self:gotoResettingTracking()
			return false
		end
	end
	
	return false
end
		

function tiFaceTracking:gotoResettingTracking()
	log_call ("tiFaceTracking:gotoResettingTracking mTrackingState: "..tiFaceCV.trackingStateToString(self.mTrackingState), tiFaceTracking.logLevel)
	local notifyNotTracking = ((self.mTrackingState ~= tiFaceCV.TI_STEPSTATE_DEF.waitingForReset) or (self.mTrackingState ~= tiFaceCV.TI_STEPSTATE_DEF.resettingTracking))
	self:setTrackingState (tiFaceCV.TI_STEPSTATE_DEF.resettingTracking)
	self.mMLTPlugin:resetTarget (self.mTrackingIndex, self.mTargetIndex)
	self.mResetTrackingTimer:restart()
	self.mPosition = Vector3()
	self.mOrientation = Quaternion()
	self:resetReferencePD()
	if notifyNotTracking then
		for key, listener in pairs(self.mListeners) do
			listener.onNotTracking(self)
		end
	end
	log_call_end()
end

function tiFaceTracking:gotoTracking()
	log_call ("tiFaceTracking:gotoTracking", tiFaceTracking.logLevel)	
	local err = 0
	local nb = 0
	
	-- err, self.mNbPointsCLM, self.mPointsCLM = self.mMLTPlugin:getCLMFaceTrackingListOfPoints (self.mTrackingIndex, self.mTargetIndex)
	-- if err ~= eOk then
		-- self.mNbPointsCLM = 0
		-- self.mPointsCLM = {}
	-- end
	
	err = self.mMLTPlugin:getPupils(self.mTrackingIndex, self.mTargetIndex, self.mIrisLeft3D, self.mIrisRight3D)
--	err, nb, pupils = MLTPluginEmulator:getPupils(self.mTrackingIndex, self.mTargetIndex)
	if err == eOk then
		log_debug("tiFaceTracking.gotoTracking iris: " ..self.mIrisLeft3D:getX().. " "..self.mIrisLeft3D:getY().. " "..self.mIrisLeft3D:getZ().. " "..self.mIrisRight3D:getX().. " "..self.mIrisRight3D:getY().. " "..self.mIrisRight3D:getZ())
		-- self.mIrisLeft3D = Vector3(tonumber(pupils[1]), tonumber(pupils[2]), tonumber(pupils[3]))
		-- self.mIrisRight3D = Vector3(tonumber(pupils[4]), tonumber(pupils[5]), tonumber(pupils[6]))
	end
	
	-- tracking started get reference PD
	-- set the reference pd
	err, self.mTiareInternalPD = self.mMLTPlugin:getPupilDistanceOfUser(self.mTrackingIndex, self.mTargetIndex)
--	err, self.mTiareInternalPD = MLTPluginEmulator:getPupilDistanceOfUser(self.mTrackingIndex, self.mTargetIndex)
log_debug("tiFaceTracking.gotoTracking mCurrentPD: "..tiFaceCV.getPupilDistance().." mTiareInternalPD: "..self.mTiareInternalPD)	
	self:setTrackingState (tiFaceCV.TI_STEPSTATE_DEF.tracking)
	
	for key, listener in pairs(self.mListeners) do
		listener.onTracking(self)
	end
	log_call_end()
end

function tiFaceTracking:update()
	local err, targetstatus = self.mMLTPlugin:getTargetStatus(self.mTrackingIndex, self.mTargetIndex)
--	local err, targetstatus = MLTPluginEmulator:getTargetStatus(self.mTrackingIndex, self.mTargetIndex)
	log_debug ("tiFaceTracking:update mTrackingIndex: "..self.mTrackingIndex.." mTargetIndex: "..self.mTargetIndex.." err: "..tostring(err)..", targetstatus: "..tostring(targetstatus)..", res: "..tostring(targetstatus > 0), tiFaceTracking.logLevel)			
	self.mIsMltTracking = (targetstatus > 0)
	log_debug ("tiFaceTracking:update mIsMltTracking: "..tostring(self.mIsMltTracking), tiFaceTracking.logLevel)
	if self.mIsMltTracking then
		self.mMLTPlugin:getTargetPos(self.mTrackingIndex, self.mTargetIndex, self.mPosition, self.mOrientation)
--		MLTPluginEmulator:getTargetPos(self.mTrackingIndex, self.mTargetIndex, self.mPosition, self.mOrientation)
	else
		self.mPosition = Vector3(0,0,0)
		self.mOrientation = Quaternion()
	end
end

function tiFaceTracking:setPupilDistance(dist)
--	log_debug("tiFaceTracking.lua: set pd: " .. dist)
	tiFaceCV.setPupilDistance(dist)
log_debug("tiFaceTracking.setPupilDistance dist: "..dist.." mTrackingIndex: "..self.mTrackingIndex.." mTargetIndex: "..self.mTargetIndex.." mCurrentPD: "..tiFaceCV.getPupilDistance().." mTiareInternalPD: "..self.mTiareInternalPD)	
	self.mMLTPlugin:setPupilDistanceOfUser(self.mTrackingIndex, self.mTargetIndex, tonumber(dist))
--	MLTPluginEmulator:setPupilDistanceOfUser(self.mTrackingIndex, self.mTargetIndex, tonumber(dist))
end
	  
function tiFaceTracking:getPupilDistance()
	return tiFaceCV.getPupilDistance()
end

function tiFaceTracking:getIris()
	-- scale the camera distance according to the pupillary distance ratio

	local scaleFactor = 1.0
	scaleFactor = self:getPupilDistance()/self.mTiareInternalPD
log_debug("tiFaceTracking:getIris Tiare    PD: "..self.mTiareInternalPD.." iris: "..self.mIrisLeft3D:getX().. " "..self.mIrisLeft3D:getY().. " "..self.mIrisLeft3D:getZ().. " "..self.mIrisRight3D:getX().. " "..self.mIrisRight3D:getY().. " "..self.mIrisRight3D:getZ())
log_debug("tiFaceTracking:getIris Tiare PD 3D: "..self.mIrisLeft3D:distance (self.mIrisRight3D))	
	local irisLeft3D = Vector3(self.mIrisLeft3D:getX()/scaleFactor, self.mIrisLeft3D:getY()/scaleFactor, self.mIrisLeft3D:getZ()/scaleFactor)
	local irisRight3D = Vector3(self.mIrisRight3D:getX()/scaleFactor, self.mIrisRight3D:getY()/scaleFactor, self.mIrisRight3D:getZ()/scaleFactor)
log_debug("tiFaceTracking:getIris TryLive    PD: "..self:getPupilDistance().." iris: "..irisLeft3D:getX().. " "..irisLeft3D:getY().. " "..irisLeft3D:getZ().. " "..irisRight3D:getX().. " "..irisRight3D:getY().. " "..irisRight3D:getZ())
log_debug("tiFaceTracking:getIris TryLive PD 3D: "..irisLeft3D:distance (irisRight3D))	

	return irisLeft3D, irisRight3D
end

function tiFaceTracking:getPosition()
	local result = Vector3()
	
	-- scale the camera distance according to the pupillary distance ratio

	local scaleFactor = 1.0

	scaleFactor = self:getPupilDistance()/self.mTiareInternalPD

	local tiarePD = self:getPupilDistance()
	local xScaled = self.mPosition:getX()*scaleFactor;
	local yScaled = self.mPosition:getY()*scaleFactor;
	local zScaled = self.mPosition:getZ()*scaleFactor;
	log_debug(string.format ("tiFaceTracking:getPosition Pos: %5.2f  %5.2f %5.2f, corrected: %5.2f %5.2f %5.2f", self.mPosition:getX(), self.mPosition:getY(), self.mPosition:getZ(), xScaled, yScaled, zScaled), tiFaceTracking.logLevel)
	
	result:set (xScaled, yScaled, zScaled)
	
	return result
end
			
function tiFaceTracking:getOrientation()
	local result = Quaternion()
	local eulerOrientX, eulerOrientY, eulerOrientZ = quat2Euler (self.mOrientation)
	euler2Quat (eulerOrientX, eulerOrientY, eulerOrientZ, result)
	return result
end
			
function tiFaceTracking:getPose()
	return self:getPosition(), self:getOrientation()
end
	   
function tiFaceTracking:getScale()
	return 1.0
end

