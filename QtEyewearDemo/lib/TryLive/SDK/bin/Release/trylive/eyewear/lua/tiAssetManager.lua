log_loaded_info("tiAssetManager.lua")
-- This class manages all the assets. 
-- All the resquest for assets call this class (set, unset, download, customize).
-- This class owns : 
--    - A list of current assets used (actually display in all the viewport)

-- The 3D arborescence for asset management : 
--   - object3d_face_main_pivot								<== This node hosts the main transform, for instance the one from the tracking (position and orientation)
--   ------ object3d_manual_move_pivot						<== This node hosts an aditionnal transform, for instance some manual relative move
--   ------------ object3d_ear_constraint_pivot
--   ------------------ object3d_viewport_1					<== This object is used for show or hide all the assets. It is also used as the top node of the assests hiding the various transforms like the two previous ones 
--   ------------------------ MainNodeAsset1				<== The main node for an asset set by a user.
--   ------------------------------ Mesh 1					<== All the assets sub nodes like mesh etc.
--   ------------------------------ Accessory1				<== Accessory node from the Asset1 json file
--   ----------------------------------- Accessory1MainNode	<== Accessory asset
--   ----------------------------------- ......
--   ------------------------ MainNodeAsset2 .............................

-- Loading status authorized
TI_LOADING_ASSET_ERROR = "error"
TI_LOADING_ASSET_LOADING = "loading"
TI_LOADING_ASSET_LOADED = "loaded"
TI_LOADING_ASSET_DOWNLOADING = "downloading"
TI_LOADING_ASSET_DOWNLOADED = "downloaded"
TI_LOADING_ASSET_UNLOADING = "unloading"
TI_LOADING_ASSET_UNLOADED = "unloaded"

tiAssetManager = {						
	mAssetsList = {},	-- This list just contains the list of all instance of tiAsset actually in use.
			
	-- Default position of the node witch contain all the assets when the appli doesn't track.
	mAssetsReinitPositionY = 0,
	mAssetsReinitPositionZ = -300,
	-- Default Asset Orientation (in degrees, nil if it doesn't need to be used)
	mAssetsDefaultOrientationX = nil,
	mAssetsDefaultOrientationY = nil,
	mAssetsDefaultOrientationZ = nil,
	-- Default Asset position (nil if it doesn't need to be used)
	mAssetsDefaultPositionX = 0,
	mAssetsDefaultPositionY = 0,
	mAssetsDefaultPositionZ = 0,
	-- Default Asset offset
	-- These offset depends on 3D model type and are applied to the asset itself
	m3DAssetsOffsetX = 0, 
	m3DAssetsOffsetY = 0, 
	m3DAssetsOffsetZ = 0, 
	m3DLiteAssetsOffsetX = 0,
	m3DLiteAssetsOffsetY = 0,
	m3DLiteAssetsOffsetZ = 0,
			
	-- external vertical offset 
	-- These asset are global for all kind of 3D models and are applied globally to the pivot
	mAssetsOffsetX = 0,
	mAssetsOffsetY = 0,
	mAssetsOffsetZ = 0,
	  
	mObject3dFaceTrackingPivot = tiSceneGraph.getFaceMainPivotNode(1),
	mObject3dFaceManualMove = Object3D(gScene:getObjectByName("object3d_manual_move_pivot")),
	mObject3dFaceVisibility = Object3D(gScene:getObjectByName("object3d_visibility")),
			
	mSizeCacheMax = 100,
	mSizeCache = 0,
	mAssetCache = {},	-- This list just contains the list of all instance of tiAsset in the cache.
	mCacheOrder = 0, -- Cache access counter
	mCacheNode = Object3D(gScene:createObject(CID_OBJECT3D)),
	printCache = false,
	mCacheMode = 0,
	CacheMode = {None = "none", Simple = "simple", Duplicate = "duplicate"},

	loadProtectedMode = false,
	lastLoadTime = -1,
	minDelayBetweenLoad = 1,
	nextLoadCall = nil,
	nextLoadInstanceName = nil,
	loadInProgress = nil,
	
	mNameLeftEarPointRef = "earLeftPoint",
	mNameRightEarPointRef = "earRightPoint",

	accessoryLogLevel = 5,
}

tiAssetManager.mCacheMode = tiAssetManager.CacheMode.None

function tiAssetManager.initialize()	
	tiAssetManager.mObject3dFaceTrackingPivot:setPosition(Vector3(0, tiAssetManager.mAssetsReinitPositionY, tiAssetManager.mAssetsReinitPositionZ), tiSceneGraph.getTopLevelNode())
	tiAssetManager.mObject3dFaceTrackingPivot:forceUpdate(true)
	tiAssetManager.mCacheNode:setName("Cache")
	tiAssetManager.mCacheNode:setPosition(-300, 0, -1000)
	tiAssetManager.mCacheNode:setVisible(false)
end
			
-- Some helper function for get something from other thing.
-- For exemple get an object by his name.
-- Get an object in a viewport and with a specific type....
			
local function findObjectByName(name)
	local res = nil
log_call("tiAssetManager.findObjectByName " .. name)
	for i, a in pairs(tiAssetManager.mAssetsList) do 
		if a:getName() == name then					
			res = a
		end
	end
log_call_end()					
	return res
end
			
local function getObjectByViewportAndName(viewport, name)
	local asset = nil
	if viewport == nil then
		asset = findObjectByName(name)
log_debug("tiAssetManager.getObjectByViewportAndName vp: " .. "nil" .. " name:" .. name .. " res=" .. tostring(asset))
	else
		for i, a in pairs(tiAssetManager.mAssetsList) do
log_debug("tiAssetManager.getObjectByViewportAndName search for " .. name .. " in vp: " .. tostring(viewport) .. " vm: " .. tiViewportManager.getVisibilityMask(viewport) .. " ... seeing " .. tiAssetManager.mAssetsList[i]:getName() .. " vm: " .. tiAssetManager.mAssetsList[i]:getVisibilityMask())									
			if a:getVisibilityMask() == tiViewportManager.getVisibilityMask(viewport) and a:getName() == name then					
				asset = a
			end
		end
	end
if asset ~= nil then
log_debug("tiAssetManager.getObjectByViewportAndName found: " .. tostring(asset))
else
log_debug("tiAssetManager.getObjectByViewportAndName not found")
end	
	return asset
end

local function findSameAssetKind (asset, viewport)
	local assetFound = nil
log_debug("tiAssetManager.findSameAssetKind " .. asset:getName() .. " vp: " .. tostring(viewport))						
	for i, a in pairs(tiAssetManager.mAssetsList) do 
		if a:getVisibilityMask() == tiViewportManager.getVisibilityMask(viewport) 
			and (a:isSameKind(asset) or a:hasSameKindOfAccessory(asset)) then
			assetFound = a
log_debug("tiAssetManager.findSameAssetKind found: " .. a:getName() .. " " .. tostring(a))						
		end
	end
log_debug("tiAssetManager.findSameAssetKind found = " .. tostring (assetFound ~= nil))						
	return assetFound
end

local function findCompatibleFather (asset, viewport)
	local father = nil
log_debug("tiAssetManager.findCompatibleFather for " .. asset:getName() .. " vp: " .. tostring(viewport), tiAssetManager.accessoryLogLevel)						
	for i, a in pairs(tiAssetManager.mAssetsList) do 
		if a:getVisibilityMask() == tiViewportManager.getVisibilityMask(viewport) 
			and a:couldHaveAccessory(asset) then
			father = a
log_debug("tiAssetManager.findCompatibleFather found: " .. a:getName() .. " " .. tostring(a), tiAssetManager.accessoryLogLevel)						
		end
	end
	return father
end

local function findCompatibleAccessoryOf (asset, viewport)
	local children = {}
log_debug("tiAssetManager.findCompatibleAccessoryOf " .. asset:getName() .. " vp: " .. tostring(viewport), tiAssetManager.accessoryLogLevel)						
	for i, a in pairs(tiAssetManager.mAssetsList) do 
		if a:getVisibilityMask() == tiViewportManager.getVisibilityMask(viewport) 
			and a:isCompatibleAccessoryOf(asset) then
			table.insert(children, a)
log_debug("tiAssetManager.findCompatibleAccessoryOf found: " .. a:getName() .. " " .. tostring(a), tiAssetManager.accessoryLogLevel)						
		end
	end
	return children
end
									
function tiAssetManager.getFacePivotObject()
	return tiAssetManager.mObject3dFaceTrackingPivot
end

function tiAssetManager.getFaceManualMovePivotObject()
	return tiAssetManager.mObject3dFaceManualMove
end
	
-- Add the object to the main node.
function tiAssetManager.addChild (object, viewport)
--log_call("tiAssetManager.addChild before")
--tiSceneGraph.printNode(object)
	local father = tiViewportManager.getViewportNode(viewport)
	if father ~= nil then
		father:addChild(object)
	else
		log_debug("tiAssetManager.addChild no viewport node " .. viewport)
	end
--log_debug("tiAssetManager.addChild apres " .. object:getName() .. " à " .. tiAssetManager.mObject3dFaceVisibility:getName())		
--tiSceneGraph.printNode(object)
--tiAssetManager.mObject3dFaceTrackingPivot:forceUpdate(true)
--log_call_end()			
end
			
-- effectively changes the visibility for all the assets
function tiAssetManager.setAssetsVisibility(visible)
	log_call("tiAssetManager.setAssetsVisibility " .. tostring(visible))
	tiAssetManager.mObject3dFaceVisibility:setVisible(visible)
--tiSceneGraph.print("tiAssetManager.setAssetsVisibility")				
	log_call_end()			
end

-- returns the first level of visible asset (no options included)
function tiAssetManager.getVisibleAssets()
	local res = {}
	for i, asset in pairs(tiAssetManager.mAssetsList) do 
		if asset.getVisible() then
			table.insert(res, asset)
		end
	end
	return res
end

-- Return the asset currently selected. (return the instance of the asset)
function tiAssetManager.getSelectedAsset()
log_call("tiAssetManager.getSelectedAsset")
tiAssetManager.dumpAssetList()
	local selectedAsset = nil
--log_object(tiAssetManager.mAssetsList,"tiAssetManager.getSelectedAsset tiAssetManager.mAssetsList")	
	for i, asset in pairs(tiAssetManager.mAssetsList) do
tiAssetManager.dumpAssetList()
log_debug("tiAssetManager.getSelectedAsset check Asset: " .. tostring(asset) .. " " .. asset:getName())			
		local res = asset:getSelectedAsset()
log_debug("tiAssetManager.getSelectedAsset found Selected: " .. tostring(res))						
		if res ~= nil then		
			selectedAsset = res
		end
	end
log_debug("tiAssetManager.getSelectedAsset, result: " .. tostring(selectedAsset))
log_call_end()		
	return selectedAsset
end

function tiAssetManager.unselect()
log_call("tiAssetManager.unselect")
	local selectedAsset = tiAssetManager.getSelectedAsset()
	if selectedAsset ~= nil then
log_object(selectedAsset,"selectedAsset")						
		selectedAsset.select(false)
	end
log_call_end()			
end

function tiAssetManager.select(assetName, viewport)
end

function tiAssetManager.customizeAsset(assetName, matName, urlImage, viewport)
	-- For allow customization on a asset, we need to check two things : 
	--   - The feature TI_CUSTOMIZE is allow (see customer config)
	if tiCustomerConfigManager.getInstance().isAvailaibleFeatures(tiCustomerConfigManager.TI_FEATURES_NAME.TI_CUSTOMIZE) then
		local asset = getObjectByViewportAndName (viewport, assetName)
		if asset ~= nil then 
			if asset:isCustomizable() then
				asset:customizeMaterial(matName, urlImage)
			end
		else
			dispatchError(TI_INTERNAL_ERROR, "Unable to find the asset: " .. assetNam .. " in viewport: " .. viewporte)
		end
	else
		dispatchError(TI_INCONSISTENT_COMMAND, "The ".. tiCustomerConfigManager.TI_FEATURES_NAME.TI_CUSTOMIZE .. " is not allow.")
	end
end

function tiAssetManager.updateGeometryAsset(instanceName, viewport, omaFile, thickness, radius, precision, rawRadius, panto)
log_call("tiAssetManager.updateGeometryAsset instanceName: "..instanceName.." viewport: "..tostring(viewport).." omaFile: "..omaFile)	
--	viewport = tostring(viewport) -- dans le script de test le viewport est un int
	local asset = findObjectByName(instanceName) --getObjectByViewportAndName(viewport, instanceName)
	if asset ~= nil then 
		asset:updateGeometry (omaFile, thickness, radius, precision, rawRadius, panto)
	else
		dispatchError(TI_INTERNAL_ERROR, "Unable to find the asset: " .. instanceName .. " in viewport: " .. viewport)
	end
--tiSceneGraph.print()				
log_call_end()				
end

function tiAssetManager.getDrillingPoints(assetName, viewport)
log_call("tiAssetManager.getDrillingPoints " .. assetName)				
	local drills = {}
	local asset = getObjectByViewportAndName(viewport, assetName)
	if asset ~= nil then 
		drills = asset:getDrillingPoints()
	else
		dispatchError(TI_INTERNAL_ERROR, "Unable to find the asset: " .. assetName .. " in viewport: " .. viewport)
	end
log_call_end()				
	return drills 
end

-- TODO Trylive 5 not used anymore, now uses setArmRelativeOrientation
function tiAssetManager.setArmOrientation (instanceName, orientEuler)
	log_call ("tiAssetManager.setArmOrientation name: "..instanceName.." vp: "..viewport)
	local asset = findObjectByName(instanceName)
	if (asset ~= nil) then
		asset:setArmOrientation (orientEuler)
		tiAssetManager.mObject3dFaceManualMove:forceUpdate(true)
	else
		log_warning ("tiAssetManager.setArmOrientation can't find "..instanceName)
	end
	tiSceneGraph.print("tiAssetManager.setArmOrientation")
	log_call_end()
end

function tiAssetManager.sendArmOrientation(assetName)

	local armOrientation = Vector3(0,0,0)
	local err = eOk

	local armOrientation, err =  tiAssetManager.getArmOrientation (assetName)

	sendCallback("receivedArmOrientation", assetName, armOrientation[1], armOrientation[2], armOrientation[3], err)
end

function tiAssetManager.getArmOrientation (instanceName)
	local err = eOk
	local orient = Vector3(0,0,0)
	local asset = findObjectByName(instanceName)
	if (asset ~= nil) then
		orient = asset:getArmOrientation()
	else
		log_warning ("tiAssetManager.getArmOrientation can't find "..instanceName)
		err = -1
	end
	return orient, err
end

function tiAssetManager.setArmRelativeOrientation (instanceName, dx, dy, dz)
	log_call ("tiAssetManager.setArmRelativeOrientation name: "..instanceName)

	local asset = findObjectByName(instanceName)
	if (asset ~= nil) then
		local orient = asset:getArmOrientation()
		
		if (orient ~= nil) then

		--	log_info("setArmRelativeOrientation orient1 " .. tostring(orient))

			log_info("setArmRelativeOrientation orient2 " .. tostring(orient[1]) .. " " .. tostring(orient[2]) .. " " .. tostring(orient[3]))

			-- TODO Vector3 from string orientEuler

			orient[1] = orient[1]+tonumber(dx)
			orient[2] = orient[2]+tonumber(dy)
			orient[3] = orient[3]+tonumber(dz)

			log_info("setArmRelativeOrientation orient new " .. tostring(orient[1]) .. " " ..tostring(orient[2]) .." " ..tostring(orient[3]))

			asset:setArmOrientation (orient)
			tiAssetManager.mObject3dFaceManualMove:forceUpdate(true)
		else
			log_warning ("tiAssetManager.setArmRelativeOrientation failed to get arm orientation for ".. instanceName)
		end
	else
		log_warning ("tiAssetManager.setArmRelativeOrientation can't find "..instanceName)
	end
	tiSceneGraph.print("tiAssetManager.setArmRelativeOrientation")
	log_call_end()
end

function tiAssetManager.initAssetOffset (asset)
	log_call("tiAssetManager.initAssetOffset " .. asset:getName() .. " type: " .. asset:getModelType())
	if (asset:getModelType() == "3d") then
		asset:setPosition (tiAssetManager.mAssetsDefaultPositionX+tiAssetManager.m3DAssetsOffsetX, tiAssetManager.mAssetsDefaultPositionY+tiAssetManager.m3DAssetsOffsetY, tiAssetManager.mAssetsDefaultPositionZ+tiAssetManager.m3DAssetsOffsetZ)
	elseif (asset:getModelType() == "3dlite") then
		asset:setPosition (tiAssetManager.mAssetsDefaultPositionX+tiAssetManager.m3DLiteAssetsOffsetX, tiAssetManager.mAssetsDefaultPositionY+tiAssetManager.m3DLiteAssetsOffsetY, tiAssetManager.mAssetsDefaultPositionZ+tiAssetManager.m3DLiteAssetsOffsetZ)
	end
	-- Little workaround to avoid lack of conventions for assets 3d modelization 
	if ((tiAssetManager.mAssetsDefaultOrientationX ~= nil) and (tiAssetManager.mAssetsDefaultOrientationY ~= nil) and (tiAssetManager.mAssetsDefaultOrientationZ ~= nil)) then
		asset:setOrientation (tiAssetManager.mAssetsDefaultOrientationX, tiAssetManager.mAssetsDefaultOrientationY, tiAssetManager.mAssetsDefaultOrientationZ)
	end
	log_call_end()	
end

function tiAssetManager.getAssetsManualOffset()

	local position = Vector3()
	tiAssetManager.mObject3dFaceManualMove:getPosition(position, tiAssetManager.mObject3dFaceTrackingPivot)
	local ox, oy, oz = tiAssetManager.mObject3dFaceManualMove:getOrientationEuler(tiAssetManager.mObject3dFaceTrackingPivot)
	local orientation = Vector3(ox, oy, oz)
	local scale = Vector3()
	tiAssetManager.mObject3dFaceManualMove:getScale(scale)
--log_pose ("tiAssetManager.getAssetsManualOffset", position, orientation)
	return position, orientation, scale
end

function tiAssetManager.setAssetsManualOffset(position, orientation, scale)
log_pose ("tiAssetManager.setAssetsManualOffset", position, orientation)
	tiAssetManager.mObject3dFaceManualMove:setPosition(position, tiAssetManager.mObject3dFaceTrackingPivot)
	tiAssetManager.mObject3dFaceManualMove:setOrientationEuler(orientation:getX(), orientation:getY(), orientation:getZ(), tiAssetManager.mObject3dFaceTrackingPivot)
	tiAssetManager.mObject3dFaceManualMove:setScale(scale)

	tiAssetManager.mObject3dFaceManualMove:forceUpdate(true)
	
	position, orientation, scale = tiAssetManager.getAssetsManualOffset()
log_pose ("tiAssetManager.setAssetsManualOffset Manual offset: ", position, orientation)
end

function tiAssetManager.setAssetsManualOffsetTranslation(trans)
log_debug ("tiAssetManager.setAssetsManualOffsetTranslation " .. trans:getX() .. " " .. trans:getY() .. " " .. trans:getZ())
	tiAssetManager.mObject3dFaceManualMove:translate(trans, tiAssetManager.mObject3dFaceTrackingPivot)
	
	tiAssetManager.mObject3dFaceManualMove:forceUpdate(true)
	
	position, orientation, scale = tiAssetManager.getAssetsManualOffset()
log_pose ("tiAssetManager.setAssetsManualOffsetTranslation Manual offset: ", position, orientation)
	position, orientation = tiAssetManager.getAssetsPose()
log_pose ("tiAssetManager.setAssetsManualOffsetTranslation Asset: ", position, orientation)
end

function tiAssetManager.getAssetsPose (reference)
	if reference == nil then
		reference = tiAssetManager.mObject3dFaceTrackingPivot:getParent()
	end
	local position = Vector3()
	local orientation = Quaternion()
	tiAssetManager.mObject3dFaceTrackingPivot:getPosition (position, reference)
	position = Vector3 (position:getX()-tiAssetManager.mAssetsOffsetX, position:getY()-tiAssetManager.mAssetsOffsetY, position:getZ()-tiAssetManager.mAssetsOffsetZ) 				
	tiAssetManager.mObject3dFaceTrackingPivot:getOrientation (orientation, reference)
	return position, orientation
end

function tiAssetManager.setAssetsPose (position, orientation, reference)
	if reference == nil then
		reference = tiAssetManager.mObject3dFaceTrackingPivot:getParent()
	end
	local s = "" 

	tiAssetManager.mObject3dFaceTrackingPivot:setPosition( 
		Vector3 (position:getX()+tiAssetManager.mAssetsOffsetX, position:getY()+tiAssetManager.mAssetsOffsetY, position:getZ()+tiAssetManager.mAssetsOffsetZ), reference)				

	if orientation ~= nil then
		tiAssetManager.mObject3dFaceTrackingPivot:setOrientation(orientation, reference)
		s = " " .. orientation:getX() .. " " .. orientation:getY() .. " " .. orientation:getZ() .. " " .. orientation:getW()
	end
	log_debug ("tiAssetManager.setAssetsPose " .. position:getX() .. " " .. position:getY() .. " " .. position:getZ() .. s .. " ref: "..reference:getName(), 5)
					
	tiAssetManager.mObject3dFaceTrackingPivot:setScale(Vector3(1,1,1))
	tiAssetManager.mObject3dFaceTrackingPivot:forceUpdate(true)
--tiSceneGraph.print("after tiAssetManager.setAssetsPose")  
end

function tiAssetManager.setAssetsDefaultOrientation(x, y, z)
log_debug ("tiAssetManager.setAssetsDefaultOrientation " .. tostring(x) .. " " .. tostring(y) .. " " .. tostring(z))
	tiAssetManager.mAssetsDefaultOrientationX = x
	tiAssetManager.mAssetsDefaultOrientationY = y
	tiAssetManager.mAssetsDefaultOrientationZ = z
end

function tiAssetManager.set3DAssetsOffset(offset)
log_debug ("tiAssetManager.set3DAssetsOffset " .. tostring(offset[1]) .. " " .. tostring(offset[2]) .. " " .. tostring(offset[3]))
	local correctionVertex = 0
	tiAssetManager.m3DAssetsOffsetX = offset[1]
	tiAssetManager.m3DAssetsOffsetY = offset[2]
	tiAssetManager.m3DAssetsOffsetZ = offset[3]+correctionVertex
log_info ("tiAssetManager.set3DAssetsOffset Correction Vertex: "..tostring(correctionVertex).."mm")				
end

function tiAssetManager.set3DLiteAssetsOffset(offset)
log_debug ("tiAssetManager.set3DLiteAssetsOffset " .. tostring(offset[1]) .. " " .. tostring(offset[2]) .. " " .. tostring(offset[3]))
	tiAssetManager.m3DLiteAssetsOffsetX = offset[1]
	tiAssetManager.m3DLiteAssetsOffsetY = offset[2]
	tiAssetManager.m3DLiteAssetsOffsetZ = offset[3]
end

function tiAssetManager.setAssetsOffset(x, y, z)
log_debug ("tiAssetManager.setAssetsOffset " .. tostring(x) .. " " .. tostring(y) .. " " .. tostring(z))
	tiAssetManager.mAssetsOffsetX = x
	tiAssetManager.mAssetsOffsetY = y
	tiAssetManager.mAssetsOffsetY = z
end			
  
function tiAssetManager.reinitObjectPosition(camera)
	log_call ("tiAssetManager.reinitObjectPosition")
	tiAssetManager.mObject3dFaceTrackingPivot:setPosition(Vector3(0, tiAssetManager.mAssetsReinitPositionY, tiAssetManager.mAssetsReinitPositionZ), camera)
	tiAssetManager.mObject3dFaceTrackingPivot:setOrientation(Quaternion(0, 0, 0, 1), camera)
	tiAssetManager.mObject3dFaceTrackingPivot:setScale(Vector3(1,1,1))
	tiAssetManager.mObject3dFaceTrackingPivot:forceUpdate(true)
	log_call_end()
end

function tiAssetManager.updateWithConstraint() 
									
	-- Store ears in node below object3d_manual_move_pivot for future use			
	local nodeLeftEarPointRef = Object3D(gScene:getObjectByName(tiAssetManager.mNameLeftEarPointRef))
	local nodeRightEarPointRef = Object3D(gScene:getObjectByName(tiAssetManager.mNameRightEarPointRef))
	
	if not nodeLeftEarPointRef:isNull() and not nodeRightEarPointRef:isNull() then 
	
		for numViewport, nodeViewport in pairs(tiSceneGraph.getViewportNodeList()) do
					
			local earConstraintOrigin = Object3D(nodeViewport) -- for coordinate calculations (ear point), is child of object3d_ear_constraint_pivot
			
			local posLeftEarPointRef = Vector3()
			local posRightEarPointRef = Vector3()
			nodeLeftEarPointRef:getPosition(posLeftEarPointRef,tiAssetManager.mObject3dFaceManualMove)
			nodeRightEarPointRef:getPosition(posRightEarPointRef,tiAssetManager.mObject3dFaceManualMove)
			
--Les 6 lignes suivantes sont a deplacer plus loin au niveau de la gestyion des branches mais pour l'instant ça fait merder les tests .......					
			local _nodeLeftArm, _nodeRightArm = tiAsset.getArms(earConstraintOrigin) 
			if _nodeLeftArm ~= nil and _nodeRightArm ~= nil then
				local nodeLeftArm = Object3D(_nodeLeftArm)
				local nodeRightArm = Object3D(_nodeRightArm)
				-- Reset arm orientation
				nodeLeftArm:setOrientationEuler(0,0,0,nodeLeftArm:getParent())
				nodeRightArm:setOrientationEuler(0,0,0,nodeRightArm:getParent())
				tiAssetManager.mObject3dFaceManualMove:forceUpdate(true)
				
				-- ear point is child of arm, so we need to update because arguments are relative to tiAssetManager.mObject3dFaceManualMove					
				-- Get position of ears points before modifing arms or any other node in scenegraph
				local posLeftEarPoint = Vector3(0,0,0)
				local posRightEarPoint = Vector3(0,0,0)
				local _nodeLeftEarPoint, _nodeRightEarPoint = tiAsset.getEarPoints(earConstraintOrigin) 
				if _nodeLeftEarPoint ~= nil and _nodeRightEarPoint ~= nil then
					local nodeLeftEarPoint = Object3D(_nodeLeftEarPoint)
					local nodeRightEarPoint = Object3D(_nodeRightEarPoint)
					nodeLeftEarPoint:getPosition(posLeftEarPoint, tiAssetManager.mObject3dFaceManualMove)
					nodeRightEarPoint:getPosition(posRightEarPoint, tiAssetManager.mObject3dFaceManualMove)
										
					local pitch = true
					if pitch then 
						local ox, oy, oz = earConstraintOrigin:getOrientationEuler();
						-- Choose pitchRight or pitchLeft depending on visibility, so according to the global yaw
						-- Set pitch and other angles to zero as roll and yaw will not be modified in this node
						if oy < 0 then
							local vec1 = Vector2(posLeftEarPoint:getZ(), posLeftEarPoint:getY())
							local vec2 = Vector2(posLeftEarPointRef:getZ(), posLeftEarPointRef:getY())						
							local pitchLeft = -1*tiMath.getSignedAngle(vec1, vec2)
							earConstraintOrigin:setOrientationEuler(pitchLeft, 0, 0, earConstraintOrigin)
							log_debug ("tiAssetManager.updateWithConstraint Global orientation: ".. ox..","..oy..","..oz.. " pitchLeft: " .. pitchLeft)
						else
							local vec1 = Vector2(posRightEarPoint:getZ(), posRightEarPoint:getY())
							local vec2 = Vector2(posRightEarPointRef:getZ(), posRightEarPointRef:getY())
							local pitchRight = -1*tiMath.getSignedAngle(vec1, vec2)
							log_debug ("tiAssetManager.updateWithConstraint Global orientation: ".. ox..","..oy..","..oz.. " pitchRight:"..pitchRight)
							earConstraintOrigin:setOrientationEuler(pitchRight, 0, 0, earConstraintOrigin)
						end
						earConstraintOrigin:forceUpdate(true)
					end
										
					-- If the model has arms, update arm rotation
					
					if (not nodeLeftArm:isNull() and not nodeRightArm:isNull()) then

						local posLeftArm = Vector3(0,0,0)
						nodeLeftArm:getPosition(posLeftArm, tiAssetManager.mObject3dFaceManualMove) 				
						local vec1 = Vector2(posLeftEarPoint:getX()-posLeftArm:getX(), posLeftEarPoint:getZ()-posLeftArm:getZ())
						local vec2 = Vector2(posLeftEarPointRef:getX()-posLeftArm:getX(), posLeftEarPointRef:getZ()-posLeftArm:getZ())
						local lyaw = tiMath.getSignedAngle(vec1, vec2)
						
						local postRightArm = Vector3(0,0,0)
						nodeRightArm:getPosition(postRightArm, tiAssetManager.mObject3dFaceManualMove)
						local vec1 = Vector2(posRightEarPoint:getX()-postRightArm:getX(), posRightEarPoint:getZ()-postRightArm:getZ())
						local vec2 = Vector2(posRightEarPointRef:getX()-postRightArm:getX(), posRightEarPointRef:getZ()-postRightArm:getZ())
						local ryaw = tiMath.getSignedAngle(vec1, vec2)
						
						local yaw = true
						
						if yaw then 
						
							nodeLeftArm:setOrientationEuler(0, -1*lyaw, 0, nodeLeftArm:getParent())
							nodeRightArm:setOrientationEuler(0, -1*ryaw, 0, nodeRightArm:getParent())
							
							tiAssetManager.mObject3dFaceManualMove:forceUpdate(true)							
						end
					end
				end
			end
		end
	end
end
			
function tiAssetManager.isReferenceEarPointsSet()
	local nodeLeftEarPointRef = Object3D(gScene:getObjectByName(tiAssetManager.mNameLeftEarPointRef))
	local nodeRightEarPointRef = Object3D(gScene:getObjectByName(tiAssetManager.mNameRightEarPointRef))
	return not nodeLeftEarPointRef:isNull() and not nodeRightEarPointRef:isNull()
end

-- Return position of ear points with origin in object3d_manual_move_pivot
function tiAssetManager.getReferenceEarPoints() 

	local nodeLeftEarPointRef = Object3D(gScene:getObjectByName(tiAssetManager.mNameLeftEarPointRef))
	local nodeRightEarPointRef = Object3D(gScene:getObjectByName(tiAssetManager.mNameRightEarPointRef))

	local posLeftEarPoint = Vector3(0,0,0)
	local posRightEarPoint = Vector3(0,0,0)
	
	if nodeLeftEarPointRef:isNull() then 			
		return nil, nil
	else
		nodeLeftEarPointRef:getPosition(posLeftEarPoint, tiAssetManager.mObject3dFaceManualMove)
		nodeRightEarPointRef:getPosition(posRightEarPoint, tiAssetManager.mObject3dFaceManualMove)
		return posLeftEarPoint, posRightEarPoint
	end 
end
			
-- set vector for left and right ear from pivot manual to corresponding ear
-- coordinates are given in tiAssetManager.mObject3dFaceManualMove coordinate system !
-- If given ear position position are null, we unset the ear point constraint

function tiAssetManager.setReferenceEarPoints(posLeftEarPoint, posRightEarPoint) 
	log_debug("tiAssetManager.setReferenceEarPoints")
	-- Store ears in node below object3d_manual_move_pivot for future use			
	local nodeLeftEarPointRef = Object3D(gScene:getObjectByName(tiAssetManager.mNameLeftEarPointRef))
	local nodeRightEarPointRef = Object3D(gScene:getObjectByName(tiAssetManager.mNameRightEarPointRef))
	
	local bActivate = true
	
	if (posLeftEarPoint ~= nil and posRightEarPoint ~= nil) then
		log_debug("tiAssetManager.setReferenceEarPoints Left:".. posLeftEarPoint:getX() .. "," .. posLeftEarPoint:getY() .. "," .. posLeftEarPoint:getZ() .. " right:".. posRightEarPoint:getX() .. "," .. posRightEarPoint:getY() .. "," .. posRightEarPoint:getZ())

		if nodeLeftEarPointRef:isNull() then 
			nodeLeftEarPointRef = Object3D(gScene:createObject(CID_OBJECT3D))
			nodeLeftEarPointRef:setName(tiAssetManager.mNameLeftEarPointRef)
			tiAssetManager.mObject3dFaceManualMove:addChild(nodeLeftEarPointRef)
		end
		
		nodeLeftEarPointRef:setPosition(posLeftEarPoint, tiAssetManager.mObject3dFaceManualMove)
		
		if nodeRightEarPointRef:isNull() then 
			nodeRightEarPointRef = Object3D(gScene:createObject(CID_OBJECT3D))
			nodeRightEarPointRef:setName(tiAssetManager.mNameRightEarPointRef)
			tiAssetManager.mObject3dFaceManualMove:addChild(nodeRightEarPointRef)
		end
		
		nodeRightEarPointRef:setPosition(posRightEarPoint, tiAssetManager.mObject3dFaceManualMove)
		
		tiAssetManager.mObject3dFaceManualMove:forceUpdate(true)
		
		tiAssetManager.updateWithConstraint()
		
	else
		log_debug("tiAssetManager.setReferenceEarPoints null points, desactivate constraint ...") 
		
		-- Delete ear nodes
		if not nodeLeftEarPointRef:isNull() then 
			gScene:deleteObject(nodeLeftEarPointRef)
		end
		
		if not nodeRightEarPointRef:isNull() then 
			gScene:deleteObject(nodeRightEarPointRef)
		end					
	end
end

function tiAssetManager.getCurrentEarPoints(viewport)
	if viewport == nil then
		viewport = "1"
		log_warning("tiAssetManager.getCurrentEarPoints no viewport specified, so take: "..viewport)
	end
	viewport = tostring(viewport) -- dans le script de test le viewport est un int
	local posLeftEarPoint = Vector3(0,0,0)
	local posRightEarPoint = Vector3(0,0,0)
	local nodeViewport = tiViewportManager.getViewportNode(viewport)
	if nodeViewport ~= nil then
		local _nodeLeftEarPoint, _nodeRightEarPoint = tiAsset.getEarPoints(nodeViewport) 
		if _nodeLeftEarPoint ~= nil and _nodeRightEarPoint ~= nil then
			local nodeLeftEarPoint = Object3D(_nodeLeftEarPoint)
			local nodeRightEarPoint = Object3D(_nodeRightEarPoint)
			nodeLeftEarPoint:getPosition(posLeftEarPoint,  tiAssetManager.mObject3dFaceManualMove)
			nodeRightEarPoint:getPosition(posRightEarPoint, tiAssetManager.mObject3dFaceManualMove)
			log_debug("tiAssetManager.getCurrentEarPoints "..nodeLeftEarPoint:getName().." "..nodeRightEarPoint:getName())
			log_debug("tiAssetManager.getCurrentEarPoints nodeRightEarPoint: "..posRightEarPoint:getX()..","..posRightEarPoint:getY()..","..posRightEarPoint:getZ())
			log_debug("tiAssetManager.getCurrentEarPoints nodeLeftEarPoint: "..posLeftEarPoint:getX()..","..posLeftEarPoint:getY()..","..posLeftEarPoint:getZ())
		else
			log_warning("tiAssetManager.getCurrentEarPoints no ear point defined in viewport: "..viewport)
		end
else
		log_warning("tiAssetManager.getCurrentEarPoints no viewport: "..viewport)
	end
	return posLeftEarPoint, posRightEarPoint
end

-- replaces command ti_getCurrentEarPoint in Trylive 5
function tiAssetManager.sendReferenceEarPoints()

	local leftPoint = nil
	local rightPoint = nil
	leftPoint, rightPoint = tiAssetManager.getCurrentEarPoints()

	sendCallback("receivedReferenceEarPoints",leftPoint:getX(),leftPoint:getY(),leftPoint:getZ(),rightPoint:getX(),rightPoint:getY(),rightPoint:getZ())
end

function tiAssetManager.sendFittingHeight(instanceName)
log_call("tiAssetManager.sendFittingHeight " .. instanceName)
	local fittingHeight = -1
	local asset = findObjectByName (instanceName)
	if asset ~= nil then 
		local fh = asset:computeFittingHeight()
		if fh > 0 then
			fittingHeight = fh
		end
	else
		dispatchError(TI_INTERNAL_ERROR, "Impossible to find asset: " .. instanceName)
	end
	log_debug("tiAssetManager.sendFittingHeight "..tostring(instanceName).." : "..tostring(fittingHeight))
log_call_end()	
	return fittingHeight
end

-- Debug functions

function tiAssetManager.dumpAssetList(title)
	if title == nil then
		title = ""
	end
	log_debug("---------------------------------------------------")
	log_debug("-----------------Dump Loaded Assets----------------- " .. title)
	for i, asset in pairs(tiAssetManager.mAssetsList) do
		log_debug_simple("[" .. tostring(i) .. "] name: '" .. asset:getName() .. "' type: " .. asset:getProductType() .. " option: " .. asset:getProductOption())
		log_debug_simple("	json: " .. asset:getJsonFileName())
		log_debug_simple("	VisMask: " .. asset:getVisibilityMask() .. " Visible: " .. tostring(asset:getVisible()) .. " Selected: " .. tostring(asset:isSelected()))
		options = asset:getOptionList()
		if options ~= nil then 
			log_debug_simple("	Options:")
			for j, option in pairs(options) do
				a = option["asset"]
				if a == nil then
					log_debug_simple ("	[" .. j .. "] " .. option["product"] .. " " .. option["product_options"] .. " : nil")
				else
					log_debug_simple("	[" .. j .. "] " .. option["product"] .. " " .. option["product_options"] .. " : '" .. a:getName() .. "' " .. a:getProductType() .. " " .. a:getProductOption()) 
				end
			end
		end
	end
	log_debug("---------------------------------------------------")
end

-----------------------------------------------------
-- Cache management
-- Le cache est un tableau d'assets chargés mais non utilisés, ce tableau est indexé par l'url du json de l'asset (définie dans le catalogue)
-- Il y a plusieurs modes possibles, mais la gestion du cache proprement dite (ajout, suppresion, recherche) est indépendante de son utilisation
-- Au niveau de l'utilisation les modes disponibles sont (tiAssetManager.CacheMode) :
-- None: 
--		Pas de cache, les assets sont loadés ou supprimés en fonction des demandes de l'application
-- Simple: 
--		Quand on veux loader un asset, on regarde d'abord s'il n'est pas dans le cache
--			Si c'est le cas, on prend celui du cache et on l'enlève du cache
--		Quand on supprime un asset, on le met dans le cache pour une potentielle future utilisation
-- Duplicate:
--		Quand on veux loader un asset, on regarde d'abbord s'il n'est pas dans le cache
--			Si ce n'est pas le cas, on load l'asset et on le met dans le cache
--		Ensuite dans tous les cas on duplique l'asset du cache pour l'utiliser
--		Quand on veux supprimer un asset, on le supprime tout simplement
--
-- Les différents modes de caches dépendent du besoin
-- La taille du cache doit être en relation avec la taille du catalogue
-- L'utilisation du cache est senséee améliorer le temps de réponse mais en contre partie celà consomme des ressources
-- Pour être efficace, il faut que le cache soit suffisament gros, sinon, on se prend l'overhead de gestion de cache sans en tirer les bénéfices 

function tiAssetManager.dumpCache(title)
	if tiAssetManager.printCache then
		if title == nil then
			title = ""
		end
		log_debug("-------------- Cache -------------- "..title)
		for i, a in pairs(tiAssetManager.mAssetCache) do
			log_debug ("In Cache: "..i..", asset: "..tostring (a.asset).." order: "..tostring(a.order))
			log_debug ("                "..a.asset:getName())
--					a.asset:print()
		end
		log_debug("----------------------------------- ")
	end
end

function tiAssetManager.setCacheSize (size)
	log_debug ("tiAssetManager.setCacheSize size: "..tostring(size))
	tiAssetManager.mSizeCacheMax = size
	if size == 0 then
		tiAssetManager.mCacheMode = tiAssetManager.CacheMode.None
	end
end

function tiAssetManager.findInCache(filename)
	log_call ("tiAssetManager.findInCache filename: " ..filename)
	local asset = nil
	local assetInCache = tiAssetManager.mAssetCache[filename]
	if assetInCache ~= nil then
		asset = assetInCache.asset
		assetInCache.order = tiAssetManager.mCacheOrder
		tiAssetManager.mCacheOrder = tiAssetManager.mCacheOrder+1
		log_debug ("tiAssetManager.findInCache filename: " ..filename.." -> asset: "..tostring(asset))
	end
	log_call_end()
	return  asset
end

local NbAddInCache = 0
local NbLoad = 0
local NbRemove = 0
local NbAssetLoad = 0
local NbDuplicate = 0

function tiAssetManager.removefromCache (filename)
	tiAssetManager.mAssetCache[filename] = nil
	tiAssetManager.mSizeCache = tiAssetManager.mSizeCache-1
end

function tiAssetManager.addInCache (filename, asset)
	log_call ("tiAssetManager.addInCache filename: " ..filename.." asset: "..tostring(asset))
--asset:print("tiAssetManager.addInCache")	
	--  Si l'asset existe deja dans le cache on libere car le nouveau va remplacer l'ancien
	local assetAlreadyInCache = tiAssetManager.mAssetCache[filename]
	if assetAlreadyInCache ~= nil then
NbRemove = NbRemove+1
		tiAssetManager.removefromCache (filename)
		assetAlreadyInCache.asset:delete()
	end

	-- If cache already full, remove the older accessed asset from cache
	if tiAssetManager.mSizeCache >= tiAssetManager.mSizeCacheMax then
		local older = tiAssetManager.mCacheOrder+1
		local olderKey = 0
		for i, a in pairs(tiAssetManager.mAssetCache) do
			if a.order < older then
				older = a.order
				olderKey = i
			end
		end
		log_debug ("tiAssetManager.addInCache Cache full ("..tiAssetManager.mSizeCache.."), delete older one: filename: "..olderKey.." asset: "..tostring(tiAssetManager.mAssetCache[olderKey].asset))
		tiAssetManager.mAssetCache[olderKey].asset:delete()
		tiAssetManager.removefromCache (olderKey)
NbRemove = NbRemove+1		
	end
	-- Add the new asset in the cache
	asset:setName ("Cache_"..asset:getJsonFileName())			
	tiAssetManager.mCacheOrder = tiAssetManager.mCacheOrder+1
	tiAssetManager.mAssetCache[filename] = {asset = asset, order = tiAssetManager.mCacheOrder}
	tiAssetManager.mSizeCache = tiAssetManager.mSizeCache+1
	tiAssetManager.mCacheNode:addChild (asset:getMainNode())
	log_debug ("tiAssetManager.addInCache " ..filename.." -> asset: "..tostring(asset))
NbAddInCache = NbAddInCache+1	
	log_call_end()
end
			
function tiAssetManager.createAsset (remotePath, instanceName)
	log_call ("tiAssetManager.createAsset remotePath: " ..remotePath.." instanceName: "..instanceName)
	local asset = nil
	if tiAssetManager.mCacheMode == tiAssetManager.CacheMode.None then
		asset = tiAsset:create({remotePath = remotePath, instanceName = instanceName})
NbAssetLoad = NbAssetLoad+1
		if not asset:load() then
			asset:delete()
			asset = nil
		end
	else
		tiAssetManager.dumpCache("tiAssetManager.createAsset begin "..tostring(tiAssetManager.mSizeCache))
		-- Try to found the asset in the cache
		local assetInCache = tiAssetManager.findInCache (remotePath)
		tiAssetManager.dumpCache("tiAssetManager.createAsset apres find "..tostring(tiAssetManager.mSizeCache))
		if assetInCache == nil then
			-- Asset not in the cache, so load it
			log_debug ("tiAssetManager.createAsset Asset " .. remotePath .. " not in cache, so load it ...")
			assetInCache = tiAsset:create ({remotePath = remotePath, instanceName = instanceName})	
			if assetInCache:load() then
NbAssetLoad = NbAssetLoad+1 
				if tiAssetManager.mCacheMode == tiAssetManager.CacheMode.Duplicate then
					-- and put in in the cache
					tiAssetManager.addInCache (remotePath, assetInCache)
				end
			else
				log_warning ("tiAssetManager.createAsset error loading asset " .. instanceName .. " " .. remotePath)
				assetInCache:delete()
				assetInCache = nil
			end
		else
			if tiAssetManager.mCacheMode == tiAssetManager.CacheMode.Simple then
				tiAssetManager.removefromCache (remotePath)
			end  
--			assetInCache:print ("tiAssetManager.createAsset Asset " .. remotePath .. " found in cache")
		end
		
		if assetInCache ~= nil then
			if tiAssetManager.mCacheMode == tiAssetManager.CacheMode.Duplicate then
				-- Increment access order at update asset access order that will be used when removing asset from cache
				tiAssetManager.mCacheOrder = tiAssetManager.mCacheOrder+1
-- a priori la ligne suvante est une connerie order n'est pas un champ de asset mais de asset en cache				
				assetInCache.order = tiAssetManager.mCacheOrder
				
				-- Duplicate the asset to use it
				asset = assetInCache:duplicate (instanceName)
NbDuplicate = NbDuplicate+1
			else
				asset = assetInCache
				asset:setName (instanceName)
			end
		end
		tiAssetManager.dumpCache("tiAssetManager.createAsset end")
--log_debug("Noeud en cache")
--tiSceneGraph.printNode (assetInCache:getMainNode())
--log_debug("Noeud dupliqué")
--tiSceneGraph.printNode (asset:getMainNode())
	end

	log_call_end()
	return asset
end

function tiAssetManager.getAsset(instanceName)
log_debug("tiAssetManager.getAsset " .. tostring(instanceName))
	return findObjectByName (instanceName)
end

-- TODO should be named unsetAsset like in trylive sdk ?
function tiAssetManager.unloadAsset(instanceName)
log_call("tiAssetManager.unloadAsset " .. instanceName)
	viewport = tostring(viewport) -- dans le script de test le viewport est un int
	-- Call on a ti_unsetAsset
	assetsLoadingStatusChanged(TI_LOADING_ASSET_UNLOADING, instanceName)
	local asset = findObjectByName (instanceName)
	if asset ~= nil then 
		tiAssetManager.removeAsset (asset)
	else
		dispatchError(TI_INCONSISTENT_PARAMETERS, "Invalid Asset.")
		assetsLoadingStatusChanged(TI_LOADING_ASSET_ERROR, instanceName)
	end
log_call_end()				
end

function tiAssetManager.downloadAsset (remotePath)
	log_call("tiAssetManager.downloadAsset remotePath: " .. tostring(remotePath))
	local instanceName = "anonymous"
	local result = false
	if remotePath == "" or remotePath == nil then
		dispatchError(TI_INCONSISTENT_PARAMETERS, "Path is invalid")
		assetsLoadingStatusChanged(TI_LOADING_ASSET_ERROR, instanceName)
	else
		tiDataRecorder.log("trylive_asset_download_begin")
		assetsLoadingStatusChanged(TI_LOADING_ASSET_LOADING, instanceName)	
		startChrono("tiAssetManager.downloadAsset",remotePath)

		local tempAsset = tiAsset:create({remotePath = remotePath})
		if tempAsset:download() then
			assetsLoadingStatusChanged(TI_LOADING_ASSET_LOADED, instanceName)
			result = true
		else
			dispatchError(TI_INTERNAL_ERROR, "Download new asset failed")				
			assetsLoadingStatusChanged(TI_LOADING_ASSET_ERROR, instanceName)
		end

		stopChrono("tiAssetManager.downloadAsset",remotePath)
		tiDataRecorder.log("trylive_asset_download_end")
	end
	log_call_end()
	return result
end

function tiAssetManager.removeAsset (asset)
	local i, a
	log_call("tiAssetManager.removeAsset " .. asset:getName())
	tiAssetManager.dumpAssetList("tiAssetManager.removeAsset begin "  .. asset:getName())
	tiSceneGraph.print("tiAssetManager.removeAsset begin")
	if asset ~= nil then
		-- Find potential parent					
		for i, a in pairs(tiAssetManager.mAssetsList) do
			-- Remove option if found
			a:removeAccessory(asset)
		end
		local orphan = {}
		for i, a in pairs(tiAssetManager.mAssetsList) do
			if a == asset then
				log_debug("tiAssetManager.removeAsset remove from table " .. asset:getName() .. " " .. tostring(i))
				table.remove(tiAssetManager.mAssetsList, i)
				log_debug("tiAssetManager.removeAsset delete " .. asset:getName())
				if tiAssetManager.mCacheMode == tiAssetManager.CacheMode.Simple then
					orphan = a:unlinkChild()
				else
					orphan = a:delete()
NbRemove = NbRemove+1				
				end
			end
		end
		if tiAssetManager.mCacheMode == tiAssetManager.CacheMode.Simple then
			tiAssetManager.addInCache (asset:getJsonRemotePath(), asset)
		end
		for i, a in pairs(orphan) do
			a:setVisible(false)
		end
		tiAssetManager.dumpAssetList("tiAssetManager.removeAsset after remove asset ")
		tiSceneGraph.print("tiAssetManager.removeAsset end")
	end
	log_call_end()				
end

function tiAssetManager.loadAssetBase (remotePath, instanceName, viewport, isAccessory, omaFile, thickness, radius, precision, rawRadius, panto)
	log_call("tiAssetManager.loadAssetBase remotePath: ".. tostring(remotePath).." instanceName: "..tostring(instanceName).." viewport: "..tostring(viewport).." accessory: "..tostring(isAccessory).." oma: "..tostring(omaFile).." thickness: "..tostring(thickness).." radius: "..tostring(radius).." precision: "..tostring(precision).." rawRadius: "..tostring(rawRadius).." panto: "..tostring(panto))
	tiAssetManager.dumpAssetList("tiAssetManager.loadAssetBase begin " .. instanceName)
	tiSceneGraph.print("tiAssetManager.loadAssetBase begin")
	startChrono("tiAssetManager.loadAssetBase",remotePath)
	local res = eFailed
	if isAccessory == nil then
		isAccessory = false
	end
	if viewport == nil then
		viewport = tiViewportManager.getSelectedViewportId()
	end
NbLoad = NbLoad+1
	if remotePath ~= nil and remotePath ~= "" then
		if tiViewportManager.getViewportNode (viewport) ~= nil then
			tiDataRecorder.log("trylive_asset_load_begin")
			
			-- Create the asset
			
			local asset = tiAssetManager.createAsset (remotePath, instanceName) 

			-- Load the asset in the scene graph
			
			if asset ~= nil then	

				if omaFile ~= nil then
					res = asset:updateGeometry (omaFile, thickness, radius, precision, rawRadius, panto)
				else
					res = eOk
				end
					
				-- Try to find a compatible father which could host this new asset

				local father = findCompatibleFather (asset, viewport)
				
				if father ~= nil then -- So, since we found potential father, it is not a top level asset
				
					isAccessory = true

					-- If a father asset has been found
					--	We add the new asset as accessory of this father asset
					--	If there was a previous accessory, it is replaced, and returned
					
					local oldChild = father:addAccessory (asset)

					if oldChild ~= nil then
					
						-- The previous accessory is now useless, we remove it
						
						tiAssetManager.removeAsset (oldChild)
					end
					
					-- No previous accessory, maybe there are some assets that could be accessory of the new one
					
					for i, a in pairs(findCompatibleAccessoryOf (asset, viewport)) do
					
						-- Try to add the asset as accessory of the new one if possible (else do nothing)
						
						asset:addAccessory (a)
					end
				else
					-- We didn't found a father for this new asset
					-- So we add it at the top of the scene graph
					
					tiAssetManager.addChild (asset:getMainNode(), viewport)

					-- Maybe there is already an equivalent asset, if it is the case, the new asset will replace it

					local oldOne = findSameAssetKind (asset, viewport)
					if oldOne == nil then
					
						-- Maybe there is some compatible accessory for this new asset
						
						for i, a in pairs(findCompatibleAccessoryOf (asset, viewport)) do
							asset:addAccessory (a)
						end
					else
						-- We replace the asset that was found
						-- First, we take its accessories, and add them to the new asset
						
						asset:grabAccessoryFrom (oldOne)
						
						-- Then we remove it
						
						tiAssetManager.removeAsset (oldOne)
						
						-- In case other sub accessories was released by the previous remove
						-- It can only happen when we mix assets/accessories structure
						
						for i, a in pairs(findCompatibleAccessoryOf (asset, viewport)) do
							asset:addAccessory (a)
						end
					end
					if not isAccessory then
						tiAssetManager.initAssetOffset (asset)			
					end
				end
				
				-- If constraint mode, update the asset angles according to the constraint
					
				tiAssetManager.updateWithConstraint()

				-- If it is an accessory we hide it because it must not be visible
				
				asset:setVisible (father ~= nil or not isAccessory)
				asset:select (not isAccessory)
				asset:setVisibilityMask (tiViewportManager.getVisibilityMask (viewport))
				
				-- Add the new asset in the assets set
				
				table.insert(tiAssetManager.mAssetsList, asset)
				tiAssetManager.dumpAssetList("tiAssetManager.loadAssetBase end " .. instanceName)
				tiSceneGraph.print("tiAssetManager.loadAssetBase end ")
			else
				log_warning ("tiAssetManager.loadAssetBase error create asset " .. instanceName .. " " .. remotePath .. " " .. tostring(viewport) .. " " .. tostring(isAccessory))
			end
			tiDataRecorder.log("trylive_asset_load_end")	
		else
			log_warning ("tiAssetManager.loadAssetBase error create asset no viewport node for ".. tostring(viewport))
		end
	else
		dispatchError(TI_INCONSISTENT_PARAMETERS, "Path is invalid")
	end
if NbLoad == 1 then
--log_debug("ZZZZZZZZZ NbLoad NbAddInCache NbAssetLoad NbDuplicate NbRemove cacheUsed cacheSize")	
end
--log_debug("ZZZZZZZZZ "..NbLoad.." "..NbAddInCache.." "..NbAssetLoad.. " "..NbDuplicate.." "..NbRemove.." "..tiAssetManager.mSizeCache.." "..tiAssetManager.mSizeCacheMax)	
	stopChrono("tiAssetManager.loadAssetBase",remotePath)
	log_call_end()
	
	return res
end

function tiAssetManager.loadAssetProtected (remotePath, instanceName, viewport, isAccessory, omaFile, thickness, radius, precision, rawRadius, panto)
--	log_debug("tiAssetManager.loadAssetProtected last request: "..tostring(tiAssetManager.nextLoadCall))
	if tiAssetManager.nextLoadCall ~= nil then
		log_warning("tiAssetManager.loadAssetProtected Load under min delay ("..tiAssetManager.minDelayBetweenLoad.."s), command ignored: "..tiAssetManager.nextLoadCall)
	end
	
	-- Construction de la string contenant cette commande de load
	
	tiAssetManager.nextLoadCall = "tiAssetManager.loadAssetBase('"..tostring(remotePath).."','"..tostring(instanceName).."','"..tostring(viewport).."'"
	if isAccessory ~= nil then
		tiAssetManager.nextLoadCall = tiAssetManager.nextLoadCall..","..tostring(isAccessory)
		if omaFile ~= nil then
			tiAssetManager.nextLoadCall = tiAssetManager.nextLoadCall.."','"..tostring(omaFile).."','"..tostring(thickness).."','"..tostring(radius).."','"..tostring(precision).."','"..tostring(rawRadius).."','"..tostring(panto).."'"
		end
	end
	tiAssetManager.nextLoadCall = tiAssetManager.nextLoadCall..")"
	
	tiAssetManager.nextLoadInstanceName = tostring(instanceName)
	log_debug("tiAssetManager.loadAssetProtected new request: "..tiAssetManager.nextLoadCall)
	
	return eOk
end

function tiAssetManager.loadAsset (remotePath, instanceName, viewport, isAccessory, omaFile, thickness, radius, precision, rawRadius, panto)
	local res
	assetsLoadingStatusChanged(TI_LOADING_ASSET_LOADING, instanceName)
	if tiAssetManager.loadProtectedMode then
		res = tiAssetManager.loadAssetProtected (remotePath, instanceName, viewport, isAccessory, omaFile, thickness, radius, precision, rawRadius, panto)
	else
		res = tiAssetManager.loadAssetBase (remotePath, instanceName, viewport, isAccessory, omaFile, thickness, radius, precision, rawRadius, panto)
	end
	if res == eOk then
		assetsLoadingStatusChanged(TI_LOADING_ASSET_LOADED, instanceName) -- TODO add viewport here ?
	else
		assetsLoadingStatusChanged(TI_LOADING_ASSET_ERROR, instanceName)
	end
	return res
end

repeat
--	log_debug("tiAssetManager.loadAssetProtected Loop current delay: "..os.clock()-tiAssetManager.lastLoadTime.." nextLoad: "..tostring(tiAssetManager.nextLoadCall))
	if tiAssetManager.nextLoadCall ~= nil and tiAssetManager.loadInProgress == nil then
		local currentTime = os.clock()
		if currentTime-tiAssetManager.lastLoadTime >= tiAssetManager.minDelayBetweenLoad then
			tiAssetManager.lastLoadTime = currentTime
			local callFunction = assert(loadstring(tiAssetManager.nextLoadCall)) -- assert gives more information that just 'nil' return in case of fail
			log_debug("tiAssetManager.loadAssetProtected loop launch: "..tiAssetManager.nextLoadCall)
			tiAssetManager.loadInProgress = tiAssetManager.nextLoadCall
			tiAssetManager.nextLoadCall = nil
			tiAssetManager.nextLoadInstanceName = nil
			res = callFunction()
			tiAssetManager.loadInProgress = nil
		end
	end
until coroutine.yield()
