-- tiFaceAnalyzer.lua script
log_loaded_info("tiFaceAnalyzer.lua")
-----------------------------
-- This class manages the face analysis

tiFaceAnalyzer = inheritsFrom (tiFaceCV)

tiFaceAnalyzer.logLevel = 5

function tiFaceAnalyzer:init(properties)
	log_debug("tiFaceAnalyzer:init properties: "..tostring(properties))

	if self.mTrackingFile == nil then 
		self.mTrackingFile = self.baseInternalTrackingFile.."FaceAnalyzer.xml"
	end
end

function tiFaceAnalyzer:refreshPoints()
	local err = 0
	err, self.mNbPointsCLM, self.mPointsCLM = self.mMLTPlugin:getCLMFaceTrackingListOfPoints (self.mTrackingIndex, self.mTargetIndex)
	log_debug("tiFaceAnalyzer:refreshPoints getCLMFaceTrackingListOfPoints err: "..err.." mNbPointsCLM: "..self.mNbPointsCLM.." eOk: "..eOk, tiFaceAnalyzer.logLevel) 
	if err ~= eOk then
		self.mNbPointsCLM = 0
		self.mPointsCLM = {}
	end
end

function tiFaceAnalyzer:gotoTracking()
	log_call ("tiFaceAnalyzer:gotoTracking", tiFaceAnalyzer.logLevel)

	self:refreshPoints()
	
	self.mTrackingState = tiFaceCV.TI_STEPSTATE_DEF.tracking
	
	for key, listener in pairs(self.mListeners) do
		listener.onTracking(self)
	end
	log_call_end()
end			

function tiFaceAnalyzer:update()
	local err, targetstatus = self.mMLTPlugin:getTargetStatus(self.mTrackingIndex, self.mTargetIndex)
	log_debug ("tiFaceAnalyzer:update mTrackingIndex: "..self.mTrackingIndex.." mTargetIndex: "..self.mTargetIndex.." err: "..err..", targetstatus: "..targetstatus..", res: "..tostring(targetstatus > 0), tiFaceAnalyzer.logLevel)			
	self:refreshPoints()
	self.mIsMltTracking = (targetstatus > 0)
end

local indexPointCLM = {
	LEFT_TEMPE						= 0,
	LEFT_CHEEKBONE0					= 1,
	LEFT_CHEEKBONE1					= 2,
	LEFT_JAW0						= 3,
	LEFT_JAW1						= 4,
	LEFT_CHIN0						= 5,
	LEFT_CHIN1						= 6,
	CHIN							= 7,
	RIGHT_CHIN1						= 8,
	RIGHT_CHIN0						= 9,
	RIGHT_JAW1						= 10,
	RIGHT_JAW0						= 11,
	RIGHT_CHEEKBONE1				= 12,
	RIGHT_CHEEKBONE0				= 13,
	RIGHT_TEMPE						= 14,
	RIGHT_EYEBROW_EXTERNAL_CORNER	= 15,
	RIGHT_EYEBROW_UPRIGHT			= 16,
	RIGHT_EYEBROW_UPLEFT			= 17,
	RIGHT_EYEBROW_INTERNAL_CORNER	= 18,
	RIGHT_EYEBROW_DOWNLEFT			= 19,
	RIGHT_EYEBROW_DOWNRIGHT			= 20,
	LEFT_EYEBROW_EXTERNAL_CORNER	= 21,
	LEFT_EYEBROW_UPLEFT				= 22,
	LEFT_EYEBROW_UPRIGHT			= 23,
	LEFT_EYEBROW_INTERNAL_CORNER	= 24,
	LEFT_EYEBROW_DOWNRIGHT			= 25,
	LEFT_EYEBROW_DOWNLEFT			= 26,
	LEFT_EYE_EXTERNAL_CORNER		= 27,
	LEFT_EYE_UP						= 28,
	LEFT_EYE_INTERNAL_CORNER		= 29,
	LEFT_EYE_DOWN					= 30,
	LEFT_EYE_PUPIL					= 31,
	RIGHT_EYE_EXTERNAL_CORNER		= 32,
	RIGHT_EYE_UP					= 33,
	RIGHT_EYE_INTERNAL_CORNER		= 34,
	RIGHT_EYE_DOWN					= 35,
	RIGHT_EYE_PUPIL					= 36,
	LEFT_TOP_NOSE					= 37,
	LEFT_MID_NOSE					= 38,
	LEFT_NOSE						= 39,
	LEFT_NOSTRIL0					= 40,
	BASE_NOSE						= 41,
	RIGHT_NOSTRIL0					= 42,
	RIGHT_NOSE						= 43,	
	RIGHT_MID_NOSE					= 44,
	RIGHT_TOP_NOSE					= 45,
	LEFT_NOSTRIL1					= 46,
	RIGHT_NOSTRIL1					= 47,	
	LEFT_MOUTH_CORNER				= 48,
	LEFT_UP_EXTERNAL_MOUTH0			= 49,
	LEFT_UP_EXTERNAL_MOUTH1			= 50,	
	UP_EXTERNAL_MOUTH				= 51,
	RIGHT_UP_EXTERNAL_MOUTH1		= 52,
	RIGHT_UP_EXTERNAL_MOUTH0		= 53,
	RIGHT_MOUTH_CORNER				= 54,
	RIGHT_DOWN_EXTERNAL_MOUTH0		= 55,
	RIGHT_DOWN_EXTERNAL_MOUTH1		= 56,
	DOWN_EXTERNAL_MOUTH				= 57,
	LEFT_DOWN_EXTERNAL_MOUTH1		= 58,
	LEFT_DOWN_EXTERNAL_MOUTH0		= 59,
	LEFT_DOWN_INTERNAL_MOUTH		= 60,
	DOWN_INTERNAL_MOUTH				= 61,
	RIGHT_DOWN_INTERNAL_MOUTH		= 62,		
	RIGHT_UP_INTERNAL_MOUTH			= 63,
	UP_INTERNAL_MOUTH				= 64,
	LEFT_UP_INTERNAL_MOUTH			= 65,
	INTERNAL_MOUTH					= 66,
	MIDDLE_NOSE						= 67,
	LEFT_EYE_UPLEFT					= 68,
	LEFT_EYE_UPRIGHT				= 69,
	LEFT_EYE_DOWNRIGHT				= 70,
	LEFT_EYE_DOWNLEFT				= 71,
	RIGHT_EYE_UPRIGHT				= 72,
	RIGHT_EYE_UPLEFT				= 73,
	RIGHT_EYE_DOWNLEFT				= 74,
	RIGHT_EYE_DOWNRIGHT				= 75,		
	LEFT_FOREHEAD0					= 76,
	LEFT_FOREHEAD1					= 77,
	LEFT_FOREHEAD2					= 78,
	LEFT_FOREHEAD3					= 79,
	FOREHEAD						= 80,
	RIGHT_FOREHEAD3					= 81,
	RIGHT_FOREHEAD2					= 82,
	RIGHT_FOREHEAD1					= 83,
	RIGHT_FOREHEAD0					= 84,
	NB_MAX_POINTS					= 85
}

local clm = nil



function displayCLMPoints(trackingindex, target)
	log_debug("displayCLMPoints")
	if clm == nil then
		local scene = getCurrentScene()
		clm = BillboardSet(scene:createObject(CID_BILLBOARDSET))
		clm:setDefaultWidth(0.010)
		clm:setDefaultHeight(0.012)
		clm:setBillboardType(TI_BS_TYPE_PERPENDICULAR_COMMON)
		clm:setMaterialByName("billboardSet/billboard")
		clm:setUseIdentityView(true)
		clm:setUseIdentityProjection(true)
		clm:setBoundingBox(Vector3(-10000, -10000, -10000), Vector3(10000, 10000, 10000))
		Object3D(scene:getObjectByName("object3d_visibility")) :addChild(clm)
	end
	local MLTPlugin = getMLTPluginManager()
	local mltProcessingWidth =  320.0
	local mltProcessingHeight = 240.0
	local points3d = {}
	local nbPoints = 0
	local err = eOk
	err, nbPoints, points2d = MLTPlugin:getCLMFaceTrackingListOfPoints(trackingindex, target)
	if err == eOk and nbPoints > 0 then
		for i = 0, nbPoints-1 do
		LOG ("CLM["..i.."] = " .. points2d[i*2+1] .. " " ..points2d[i*2+2])
			if getOSType() == TI_OS_ANDROID then
				points3d[i*3+1] = (points2d[i*2+1] *  mltProcessingWidth)  * -2.0 + 1.0
			else
				points3d[i*3+1] = (points2d[i*2+1] *  mltProcessingWidth)  * 2.0 - 1.0
			end
			points3d[i*3+2] = (points2d[i*2+2] *  mltProcessingHeight) * -2.0 + 1.0		
			points3d[i*3+3] = 300	
		LOG ("                                                                       ".. " " .. points3d[i*3+1] .. " " ..points3d[i*3+2] .. " " ..points3d[i*3+3])
		end
		clm:setPoints(points3d)	
	else
--		clm:clear()
	end
end


