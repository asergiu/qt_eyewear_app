log_loaded_info("tiScenarioPhotoSequence.lua")

-- Management of the photo mode

tiScenarioPhotoSequence = {
	-- Reference aux instance de manager externes
	mScenario = nil,
	-- Gestion des photos
	mPhotoSequence = {},		-- Tableau de l'ensemble des photos sequences en memoire
	debugLevel = 5
}

function tiScenarioPhotoSequence.initialize()
end
		
-- TEST photo sequence
-- TODO rajouter un "name" en param, voir si cette fonction est utile, sans doute plus un "add" et rajouter la fonction "delete"
function tiScenarioPhotoSequence.addPhotoSequence (name, photoSequence)
	tiScenarioPhotoSequence.mPhotoSequence[name] = photoSequence
end

function tiScenarioPhotoSequence.photoSequenceDelete (name)
	if tiScenarioPhotoSequence.mPhotoSequence[name] ~= nil then
		tiScenarioPhotoSequence.mPhotoSequence[name]:delete()
		tiScenarioPhotoSequence.mPhotoSequence[name] = nil
	else
		log_error ("tiScenarioPhotoSequence.photoSequenceDelete sequence "..tostring(name).." not found")
	end
end

-- TODO rajouter un "name" en param, mais path et filename devrait plutot etre mis dans la creation de la sequence
function tiScenarioPhotoSequence.photoSequenceStartRecord (name, cameraName, path, filename, stopDelay, frameStep)
	local photoSequence = tiPhotoSequence:create()
	tiScenarioPhotoSequence.addPhotoSequence (name, photoSequence)
	photoSequence:startRecord (cameraName, path, filename, stopDelay, frameStep)
end

function tiScenarioPhotoSequence.photoSequenceSave (name)
	if tiScenarioPhotoSequence.mPhotoSequence[name] ~= nil then
		tiScenarioPhotoSequence.mPhotoSequence[name]:save()
	else
		log_error ("tiScenarioPhotoSequence.photoSequenceSave sequence "..tostring(name).." not found")
	end
end

function tiScenarioPhotoSequence.photoSequenceLoad (name, filename)
	local photoSequence = tiPhotoSequence:create()
	photoSequence:load (filename)
	tiScenarioPhotoSequence.addPhotoSequence (name, photoSequence)
end


function tiScenarioPhotoSequence.photoSequenceStopRecord (name)
	if tiScenarioPhotoSequence.mPhotoSequence[name] ~= nil then
		tiScenarioPhotoSequence.mPhotoSequence[name]:stopRecord()
	else
		log_error ("tiScenarioPhotoSequence.photoSequenceStopRecord sequence "..tostring(name).." not found")
	end
end

function tiScenarioPhotoSequence.photoSequencePlay (name, numImage)
log_debug("tiScenarioPhotoSequence.photoSequencePlay name: "..tostring(name).. " numImage:"..tostring(numImage), tiScenarioPhotoSequence.debugLevel)
	if tiScenarioPhotoSequence.mPhotoSequence[name] ~= nil then
		tiScenarioPhotoSequence.mPhotoSequence[name]:play (numImage)
	else
		log_error ("tiScenarioPhotoSequence.photoSequencePlay sequence "..tostring(name).." not found")
	end
end

function tiScenarioPhotoSequence.photoSequenceSet (name, cameraName)
log_debug("tiScenarioPhotoSequence.photoSequenceSet name: "..tostring(name).. " cameraName:"..tostring(cameraName), tiScenarioPhotoSequence.debugLevel)
	if tiScenarioPhotoSequence.mPhotoSequence[name] ~= nil then
		tiScenarioPhotoSequence.mPhotoSequence[name]:set (cameraName)
	else
		log_error ("tiScenarioPhotoSequence.photoSequenceSet sequence "..tostring(name).." not found")
	end
end

function tiScenarioPhotoSequence.photoSequencePlayStepBase (forcePlay, inc, name)
log_debug("tiScenarioPhotoSequence.photoSequencePlayStepBase forcePlay: "..tostring(forcePlay).." inc: "..tostring(inc).." name: "..tostring(name), tiScenarioPhotoSequence.debugLevel)
	if name ~= nil then
		if tiScenarioPhotoSequence.mPhotoSequence[name] ~= nil then
			tiScenarioPhotoSequence.mPhotoSequence[name]:playStepBase (forcePlay, inc)
		else
			log_error ("tiScenarioPhotoSequence.photoSequencePlayStepBase sequence "..tostring(name).." not found")
		end
	else
		for i, seq in pairs (tiScenarioPhotoSequence.mPhotoSequence) do
			seq:playStepBase (forcePlay, inc)
		end	
	end
end

function tiScenarioPhotoSequence.photoSequencePlayStep (inc, name)
log_debug("tiScenarioPhotoSequence.photoSequencePlayStep inc: "..tostring(inc).." name: "..tostring(name), tiScenarioPhotoSequence.debugLevel)
	if name ~= nil then
		if tiScenarioPhotoSequence.mPhotoSequence[name] ~= nil then
			tiScenarioPhotoSequence.mPhotoSequence[name]:playStepBase (true, inc)
		else
			log_error ("tiScenarioPhotoSequence.photoSequencePlayStep sequence "..tostring(name).." not found")
		end
	else
		for i, seq in pairs (tiScenarioPhotoSequence.mPhotoSequence) do
			seq:playStepBase (true, inc) 
		end	
	end
end

function tiScenarioPhotoSequence.photoSequencePlayAuto (inc, name)
log_debug("tiScenarioPhotoSequence.photoSequencePlayAuto inc: "..tostring(inc).." name: "..tostring(name), tiScenarioPhotoSequence.debugLevel)
	if name ~= nil then
		if tiScenarioPhotoSequence.mPhotoSequence[name] ~= nil then
			tiScenarioPhotoSequence.mPhotoSequence[name]:playStepBase (false, inc)
		else
			log_error ("tiScenarioPhotoSequence.photoSequencePlayAuto sequence "..tostring(name).." not found")
		end
	else
		for i, seq in pairs (tiScenarioPhotoSequence.mPhotoSequence) do
			seq:playStepBase (false, inc)
		end	
	end
end


function tiScenarioPhotoSequence.photoSequencePause (name)
log_debug("tiScenarioPhotoSequence.photoSequencePause name: "..tostring(name), tiScenarioPhotoSequence.debugLevel)
	if name ~= nil then
		if tiScenarioPhotoSequence.mPhotoSequence[name] ~= nil then
			tiScenarioPhotoSequence.mPhotoSequence[name]:pause()
		else
			log_error ("tiScenarioPhotoSequence.photoSequencePause sequence "..tostring(name).." not found")
		end
	else
		for i, seq in pairs (tiScenarioPhotoSequence.mPhotoSequence) do
log_debug("tiScenarioPhotoSequence.photoSequencePause .. "..tostring(i))
			seq:pause()
		end	
	end
end

function tiScenarioPhotoSequence.loopStep()
	for i, seq in pairs (tiScenarioPhotoSequence.mPhotoSequence) do
		seq:playStepBase (false)
		seq:checkRecord()
		seq:saveStep()
	end
end

