log_loaded_info("tiInputTargetRotate.lua")

tiInputTargetRotate = inheritsFrom (tiInputTarget)

tiInputTargetRotate.Target = {None = 0, Ear = 3, Lens = 4}

tiInputTargetRotate.pitchLimit = 30
tiInputTargetRotate.yawLimit = 30
tiInputTargetRotate.rollLimit = 30

function tiInputTargetRotate:init (properties)
	log_debug("tiInputTargetRotate:init "..tostring(self).." properties: "..tostring(properties))
	local manualMovePivotName = "object3d_manual_move_pivot" 
	self.mTargets[tiInputTargetRotate.Target.Ear] = tiTarget:create({nodeName = "EarTarget", overlay = "EarTargetOverlay", offsetX = 1/4, offsetY = 1/4, parentNode = manualMovePivotName})
	self.mTargets[tiInputTargetRotate.Target.Lens] = tiTarget:create({nodeName = "LensTarget", overlay = "LensTargetOverlay", offsetX = 1/4, offsetY = 1/4, parentNode = manualMovePivotName})
end

function tiInputTargetRotate:setReferenceEarPoints (posLeftEarPoint, posRightEarPoint)
	log_call("tiInputTargetRotate.setReferenceEarPoints", tiInputProcessor.logLevel)
	if posRightEarPoint ~= nil then
		
		log_debug("tiInputTargetRotate.setReferenceEarPoints " .. posRightEarPoint:getX() .. " " .. posRightEarPoint:getY() .. " " .. posRightEarPoint:getZ(), tiInputProcessor.logLevel)
		
		local target = self.mTargets[tiInputTargetRotate.Target.Ear]
		local targetNode = target:getNode() 
		
		local pivot = target:getParentNode()   
		targetNode:setPosition(posRightEarPoint, pivot)
		pivot:forceUpdate(true)			
	else
		log_debug("tiInputTargetRotate.setReferenceEarPoints undefined", tiInputProcessor.logLevel)
	end
	log_call_end()
end

function tiInputTargetRotate:mouseMoveTargetEar (viewport, target, otherTarget, move)
	if move:magnitude() > 0 then			
		-- move target
		target:move2D (move)
		
		-- pivot
		local pivot = target:getParentNode()
		local rx, ry, rz = pivot:getOrientationEuler() -- pivot orientation
		
		-- ear coordinates
		local vecEar = target:getPosition2D(viewport) -- project new ear screen coordinates to 3D on ear z-plane
		local earTarget = target:getNode()
			
		local prevEarVec = target:getPosition3D()		
		local curEarVec = Vector3 (viewport:projectOn3Dplane (vecEar, earTarget)) 
		
		-- pitch
		local pose1 = Vector2(prevEarVec:getZ(), prevEarVec:getY())
		local pose2 = Vector2(curEarVec:getZ(), curEarVec:getY()) -- sign of y in projectOn3Dplane is inversed
		local pitchAngle = tiMath.getSignedAngle(pose1, pose2)
		
		if math.abs(rx+pitchAngle) < tiInputTargetRotate.pitchLimit then 
			pivot:pitch(pitchAngle)
			pivot:forceUpdate(true)
		end					

		-- yaw
		local pose1 = Vector2 (curEarVec:getX(), prevEarVec:getZ())
		local pose2 = Vector2 (prevEarVec:getX(), prevEarVec:getZ())
		local yawAngle = -1*tiMath.getSignedAngle(pose1, pose2)

		if math.abs(ry+yawAngle) < tiInputTargetRotate.pitchLimit then 
			pivot:yaw (yawAngle)
			pivot:forceUpdate(true)
		end
		
		otherTarget:updateOverlayFrom3DTarget(viewport)
	end
end

function tiInputTargetRotate:mouseMoveTargetLens (viewport, target, otherTarget, move)
	if move:magnitude() > 0 then			
		-- move target

		target:move2D (move)

		local pivot = target:getParentNode()
		local rx, ry, rz = pivot:getOrientationEuler() -- pivot orientation
		
		-- lens coordinates
		local vec = target:getPosition2D(viewport) -- project new ear screen coordinates to 3D on ear z-plane
		local targetNode = target:getNode()
			
		local prevVec = target:getPosition3D()
		local curVec = Vector3 (viewport:projectOn3Dplane (vec, targetNode)) 
		
		-- roll
		local pose1 = Vector2 (prevVec:getX(), curVec:getY()) 
		local pose2 = Vector2 (prevVec:getX(), prevVec:getY()) 
		
		local angle = tiMath.getSignedAngle (pose1, pose2)
		if math.abs(rz+angle) < tiInputTargetRotate.rollLimit then 
			pivot:roll(angle)
			pivot:forceUpdate(true)
		end			
		
		otherTarget:updateOverlayFrom3DTarget(viewport)
	end
end

function tiInputTargetRotate:applyInputData (inputData, viewport)
	log_debug("tiInputTargetRotate:applyInputData", tiInputProcessor.logLevel)
	local otherTarget = nil
	if inputData.move:magnitude() > 0 then
	log_debug("tiInputTargetRotate:applyInputData target: "..tostring(inputData.target).." Target.Ear: "..tostring(self.mTargets[tiInputTargetRotate.Target.Ear]).." Target.Lens: "..tostring(self.mTargets[tiInputTargetRotate.Target.Lens]), tiInputProcessor.logLevel)
		if inputData.target == self.mTargets[tiInputTargetRotate.Target.Lens] then
			tiInputTargetRotate:mouseMoveTargetLens (viewport, inputData.target, self.mTargets[tiInputTargetRotate.Target.Ear], inputData.move)			
		else
			tiInputTargetRotate:mouseMoveTargetEar (viewport, inputData.target, self.mTargets[tiInputTargetRotate.Target.Lens], inputData.move)
		end
	end
end



	

