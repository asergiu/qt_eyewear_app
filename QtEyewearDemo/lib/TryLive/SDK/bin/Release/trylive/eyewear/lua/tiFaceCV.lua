-- tiFaceCV.lua script
log_loaded_info("tiFaceCV.lua")
-----------------------------
-- This class manages the face tracking and the MLT.

MLTPluginEmulator = {
	count = 0
}

function MLTPluginEmulator:startTracking (TrackingFile, VideoCaptureId, Camera)
log_debug("MLTPluginEmulator:startTracking")
	local result = eOk
	local TrackingIndex = 0
	return result, TrackingIndex
end

function MLTPluginEmulator:setPupilDistanceOfUser (TrackingIndex, TargetIndex, dist)
log_debug("MLTPluginEmulator:setPupilDistanceOfUser")
end
	
function MLTPluginEmulator:resetTarget (TrackingIndex, mTargetIndex)
log_debug("MLTPluginEmulator:resetTarget")
end


function MLTPluginEmulator:getTargetStatus(smTrackingIndex, TargetIndex)
	local periode = 200
	local err = eOk
	local targetstatus = math.floor (self.count / (periode/2)) % 2
log_debug("MLTPluginEmulator:getTargetStatus "..targetstatus)
	self.count = self.count+1
	return err, targetstatus
end

function MLTPluginEmulator:getTargetPos(TrackingIndex, TargetIndex, Position, Orientation)
	local rayon = 200
	local periode = 100
	Position:setX(math.sin (self.count/periode*math.pi*2)*rayon)
	Position:setY(math.cos (self.count/periode*4*math.pi*2)*rayon)
	Position:setZ(-1000)
end

function MLTPluginEmulator:getTrackingRate (TrackingIndex)
	local err = eOk
	local trackingRate = 30
	return err, trackingRate
end

function MLTPluginEmulator:getCLMFaceTrackingListOfPoints (TrackingIndex, TargetIndex)
	local err = eOK
	local NbPointsCLM = 0
	local PointsCLM = {}
	return err, NbPointsCLM, PointsCLM
end

function MLTPluginEmulator:getPupils (TrackingIndex, TargetIndex)
	local err = eOk
	local nb = 2
	local pupils = {}
	pupils[1] = 0
	pupils[2] = 0
	pupils[3] = 0
	pupils[4] = 0
	pupils[5] = 0
	pupils[6] = 0
	return err, nb, pupils
end

function MLTPluginEmulator:getPupilDistanceOfUser(sTrackingIndex, TargetIndex)
	local err = eOk
	local TiareInternalPD = 62
	return err, TiareInternalPD
end

tiFaceCV = {
	----------------------------------------------------------------------------------
	--	Constants parameter configuration
	----------------------------------------------------------------------------------
	
	TI_WAITINGFORRESET_TIME_OUT = 0.2,	
	TI_ANALYSEPOSE_TIME_OUT = 2,
		
	-- The different steps during the tracking process
	TI_STEPSTATE_DEF = {resettingTracking = 0, analysingPose = 1, waitingForTracking = 2, tracking = 3, waitingForReset = 4},
	TI_POSITION_STATE_DEF = {undefined = 0, notCentered = 1, tooFar = 2, tooClose = 3, wellPositionned = 4},
	
--	recordInputImage = false,
--	recordInputImageBufferSize = 10,
--	recordInputImagePath = "c:/tmp/testsave"
}	

tiFaceCV.logLevelHigh = 5
tiFaceCV.logLevelLow = 5
tiFaceCV.currentPD = 62

function tiFaceCV.trackingStateToString (state)
	local s = "undefined"
	if		state == tiFaceCV.TI_STEPSTATE_DEF.resettingTracking then s = "resettingTracking"
	elseif	state == tiFaceCV.TI_STEPSTATE_DEF.analysingPose then s = "analysingPose"
	elseif	state == tiFaceCV.TI_STEPSTATE_DEF.waitingForTracking then s = "waitingForTracking"
	elseif	state == tiFaceCV.TI_STEPSTATE_DEF.tracking then s = "tracking"
	elseif	state == tiFaceCV.TI_STEPSTATE_DEF.waitingForReset then s = "waitingForReset"
	end
	return s
end

function tiFaceCV.positionStateToString (state)
	local s = "undefined"
	if		state == tiFaceCV.TI_POSITION_STATE_DEF.notCentered then s = "notCentered"
	elseif	state == tiFaceCV.TI_POSITION_STATE_DEF.tooFar then s = "tooFar"
	elseif	state == tiFaceCV.TI_POSITION_STATE_DEF.tooClose then s = "tooClose"
	elseif	state == tiFaceCV.TI_POSITION_STATE_DEF.wellPositionned then s = "wellPositionned"
	end
	return s
end

function tiFaceCV.getPupilDistance()
	return tiFaceCV.currentPD 
end

function tiFaceCV.setPupilDistance(val)
	if val == nil then
		val = 62
	end
	log_debug("tiFaceCV.setPupilDistance "..tostring(val))
	tiFaceCV.currentPD = val 
end

function tiFaceCV:create (properties)
    local new_inst = {}    -- the new instance
    setmetatable( new_inst, { __index = tiFaceCV } ) -- all instances share the same metatable
--	self:init (properties)
	new_inst:init (properties)
	log_debug("tiFaceCV:create "..tostring(self))
    return new_inst
end

function tiFaceCV.createPlugin (properties)
--	local plugin = getMLTPluginManager()
	local plugin = getTiareFaceTrackerPluginManager()
	-- local plugin = MLTPluginEmulator
	return plugin
end

function tiFaceCV:init (properties)
	log_debug("tiFaceCV:init "..tostring(self).." properties: "..tostring(properties))
	self.mMLTPlugin = tiFaceCV.createPlugin (properties)
	self.mTrackingState = tiFaceCV.TI_STEPSTATE_DEF.resettingTracking
	self.mAnalysePoseSubstate = tiFaceCV.TI_POSITION_STATE_DEF.undefined
	self.mPaused = true
			
	self.mFocalX = 0
	self.mFocalY = 0
	-- Tracking indexes
	self.mTrackingIndex = -1
	self.mTargetIndex = 0 -- we only track one face
	self.mIsTracking = false
	
	-- Timer management
	self.mAnalysePoseTimer = nil
	self.mResetTrackingTimer = nil
	self.mWaitForTrackingTimer = nil
						
	self.mListeners = {}
      			
	self.mNbPointsCLM = 0
	self.mPointsCLM = {}
	
	self.mTrackingFile = nil
	
	self.mCamera = nil
	
	if properties ~= nil then
		self.mTrackingFile = properties.configCV
	end
	
	self.baseInternalTrackingFile = "../../eyewear/resources/cv/"
	
	self.mTargetIndex = 0 -- we only track one face
	-- Init the timers
	self.mResetTrackingTimer = Timer()
	self.mResetTrackingTimer:stop()
	self.mAnalysePoseTimer = Timer()
	self.mAnalysePoseTimer:stop()
	self.mWaitForTrackingTimer = Timer()
	self.mWaitForTrackingTimer:stop()
	
	self.mProcessingSizeX = 320
	self.mProcessingSizeY = 240
end


function tiFaceCV:updateProcessingSizeXML (ratio)
	log_debug ("tiFaceCV:updateProcessingSizeXML "..tostring(ratio))
	local trackingFilefullPath = tiFile.cleanPath (getScriptDirectory().."/"..self.mTrackingFile)
	local updateRatio = false
	-- Recuperation de la processing size du fichier de config CV
	local originalSize = {}
	originalSize["PROCESSINGWIDTH"] = ""
	originalSize["PROCESSINGHEIGHT"] = ""
	originalSize["PROCESSINGIMAGEWIDTH"] = ""
	originalSize["PROCESSINGIMAGEHEIGHT"] = ""
	if getXMLtag (trackingFilefullPath, originalSize) == eOK then
		self.mProcessingSizeX = originalSize["PROCESSINGWIDTH"]
		self.mProcessingSizeY = originalSize["PROCESSINGHEIGHT"]
		
		-- On verifie si le ratio video et egal a celui de la processing size
		-- Si ce n'est pas le cas, on calcule la nouvelle processing size et il faudra mettre a jour le fichier de config CV
		
		-- En plus visiblement les resolution impaires foirent avec CV
		if ratio > self.mProcessingSizeX/self.mProcessingSizeY then
			self.mProcessingSizeX = round(self.mProcessingSizeY * ratio / 2) * 2
			updateRatio = true
		elseif ratio < self.mProcessingSizeX/self.mProcessingSizeY then
			self.mProcessingSizeY = round(self.mProcessingSizeX / ratio / 2) * 2
			updateRatio = true
		end
		
		if updateRatio then
		
			-- Mise a jour du fichier de config CV
			
			local destDirectory = getAppConfigPath()
			local updatedTrackingFile = tiFile.cleanPath (destDirectory..tiFile.baseNameWithoutExtension(self.mTrackingFile).."_"..tostring(ratio)..".xml")
			
			log_debug("tiFaceCV:updateProcessingSize ratio: "..ratio..", create new CV configuration file: "..updatedTrackingFile)

			-- Get the list of referenced file to copy
			
			local filesToCopy = {}
			filesToCopy["HEADCASCADECLASSIFIER"] = ""
			filesToCopy["CLM_MODEL_BIN"] = ""
			filesToCopy["MODEL"] = ""
			getXMLtag (trackingFilefullPath, filesToCopy)
					
			-- Copy these files
			
			local baseDirectory = tiFile.dirName (trackingFilefullPath)
			for tag, fileName in pairs (filesToCopy) do
				local srcFile = baseDirectory..tiFile.baseName (fileName)
				local dstFile = destDirectory..tiFile.baseName (fileName)
				tiFile.copy (srcFile, dstFile)
			end
		
			-- Generate new CV configuration with updated tags
			
			local tagsToUpdate = {}
			tagsToUpdate["PROCESSINGWIDTH"] = self.mProcessingSizeX
			tagsToUpdate["PROCESSINGHEIGHT"] = self.mProcessingSizeY
			tagsToUpdate["PROCESSINGIMAGEWIDTH"] = self.mProcessingSizeX
			tagsToUpdate["PROCESSINGIMAGEHEIGHT"] = self.mProcessingSizeY
			setXMLtag (trackingFilefullPath, updatedTrackingFile, tagsToUpdate)
			self.mTrackingFile = updatedTrackingFile
			trackingFilefullPath = updatedTrackingFile
		else
			log_debug("tiFaceCV:updateProcessingSize ratio: "..ratio..", already compatible with CV configuration file: "..self.mTrackingFile)	
		end
	else
		log_warning("tiFaceCV:updateProcessingSize can't read CV config file "..trackingFilefullPath.." to check ratio")	
	end

	return updateRatio
end

function tiFaceCV:updateProcessingSizeJSON (ratio)
	local trackingFilefullPath = tiFile.cleanPath (getScriptDirectory().."/"..self.mTrackingFile)
	log_debug ("tiFaceCV:updateProcessingSizeJSON ratio: "..tostring(ratio).." file: "..trackingFilefullPath)
	local configJSON = json.load (trackingFilefullPath)
--	log_object (configJSON, "Config CV")
	local updateRatio = false
	if configJSON ~= nil then
		local preprocessing = configJSON["preprocessing"]
		if preprocessing ~= nil then
			-- Recuperation de la processing size du fichier de config CV
			local originalSize = {}
			self.mProcessingSizeX = configJSON["preprocessing"]["size_x"]
			self.mProcessingSizeY = configJSON["preprocessing"]["size_y"]
			
			-- On verifie si le ratio video et egal a celui de la processing size
			-- Si ce n'est pas le cas, on calcule la nouvelle processing size et il faudra mettre a jour le fichier de config CV
			
			-- En plus visiblement les resolution impaires foirent avec CV
			if ratio > self.mProcessingSizeX/self.mProcessingSizeY then
				self.mProcessingSizeX = round(self.mProcessingSizeY * ratio / 2) * 2
				updateRatio = true
			elseif ratio < self.mProcessingSizeX/self.mProcessingSizeY then
				self.mProcessingSizeY = round(self.mProcessingSizeX / ratio / 2) * 2
				updateRatio = true
			end
--log_debug ("tiFaceCV:updateProcessingSizeJSON ratio: "..ratio.." new one: "..self.mProcessingSizeX/self.mProcessingSizeY.." update: "..tostring(updateRatio))
			if updateRatio then
			
				-- Mise a jour du fichier de config CV
				
				local destDirectory = getAppConfigPath()
				local updatedTrackingFile = tiFile.cleanPath (destDirectory..tiFile.baseNameWithoutExtension(self.mTrackingFile).."_"..tostring(ratio)..".json")
				
				log_debug("tiFaceCV:updateProcessingSizeJSON ratio: "..ratio..", create new CV configuration file: "..updatedTrackingFile)

				-- Get the list of referenced file to copy
				
				local filesToCopy = {}
				filesToCopy["cascade_classifier_path"] = configJSON["tracking"]["viola_jones"]["cascade_classifier_path"]
				filesToCopy["learning_path"] = configJSON["tracking"]["clm"]["learning_path"]
				filesToCopy["mask"] = configJSON["tracking"]["mask"]["model_path"]
						
				-- Copy these files
				
				local baseDirectory = tiFile.dirName (trackingFilefullPath)
				for tag, fileName in pairs (filesToCopy) do
					local srcFile = baseDirectory..tiFile.baseName (fileName)
					local dstFile = destDirectory..tiFile.baseName (fileName)
					tiFile.copy (srcFile, dstFile)
				end
			
				-- Generate new CV configuration with updated tags
				
				local tagsToUpdate = {}
				configJSON["preprocessing"]["size_x"] = self.mProcessingSizeX
				configJSON["preprocessing"]["size_y"] = self.mProcessingSizeY
				
				local strJSON = json.encode (configJSON)
				log_debug (strJSON)
				
				if File.write (updatedTrackingFile, strJSON) ~= eOk then
					log_error ("tiFaceCV:updateProcessingSizeJSON impossible to write file "..tostring(updatedTrackingFile))
				else
					self.mTrackingFile = updatedTrackingFile
				end

			else
				log_debug("tiFaceCV:updateProcessingSizeJSON ratio: "..ratio..", already compatible with CV configuration file: "..self.mTrackingFile)	
			end
		else
			log_warning("tiFaceCV:updateProcessingSizeJSON no preprocessing section in CV config file "..trackingFilefullPath.." to check ratio")	
		end
	else
		log_warning("tiFaceCV:updateProcessingSizeJSON can't read CV config file "..trackingFilefullPath.." to check ratio")	
	end

	return updateRatio
end

function tiFaceCV:updateProcessingSize (ratio)
	local updateRatio
	if tiFile.getExtension (self.mTrackingFile) == "xml" then
		updateRatio = self:updateProcessingSizeXML (ratio)
	else
		updateRatio = self:updateProcessingSizeJSON (ratio)
	end
	return updateRatio
end

function tiFaceCV:attachCamera (camera)
log_debug("tiFaceCV:attachCamera camera: "..tostring(camera))
--printStack()
	if camera ~= nil then
		camera:addListener (self)
	end
	self:updateProcessingSize(camera:getVideoRatio())
	self.mCamera = camera
end

function tiFaceCV:detachCamera()
log_debug("tiFaceCV:detachCamera")
--printStack()
	if self.mCamera ~= nil then
		self.mCamera:removeListener (self)
	end
	self.mCamera = nil
end

function tiFaceCV:getCamera()
	return self.mCamera
end

function tiFaceCV:processEvent (source, event)
	log_debug ("tiFaceCV:processEvent source: "..tostring(source).." event: "..tostring(event))
	if		event == "EventVideoStart" then
		self:runAutoTracking()
	elseif	event == "EventVideoClose" then
		-- PATCH : crash when switching from view mode 'Front Single' -> 'Photo Single' -> 'Front Single'
		--self:stopAutoTracking()
		self:pauseAutoTracking()
	elseif	event == "EventVideoPlay" then
		self:runAutoTracking()
	elseif	event == "EventVideoPause" then
		self:pauseAutoTracking()
	end
end
		
function tiFaceCV:setTrackingState (state)
	log_debug("tiFaceCV:setTrackingState old: "..tostring(self.mTrackingState).." new: "..tostring(state), tiFaceCV.logLevelHigh)
	self.mTrackingState = state
end

-- returns true if a face can be tracked, false otherwise
function tiFaceCV:isMltTracking()
log_debug("tiFaceCV:isMltTracking "..tostring(self.mIsMltTracking), tiFaceCV.logLevelHigh)
	return self.mIsMltTracking
end

function tiFaceCV:getPositionState()
log_debug ("tiFaceCV:getPositionState", tiFaceCV.logLevelHigh)
	local state = tiFaceCV.TI_POSITION_STATE_DEF.undefined
	return state
end

-- Given a dfusion timer, this function returns 
-- 1. true if the condition wasn't broken and if we can retrieve the tracking rate, false otherwise
-- 2. the number of frames since the last timer restart.
function tiFaceCV:getFramesCount (timer, breakCondition)
	local nbFrames = 0
	if not breakCondition then
		local trackingRate = 0.0
		local err = eOk
		err, trackingRate = self.mMLTPlugin:getTrackingRate (self.mTrackingIndex)
		if(err ~= eOk) then
			-- We cannot retrieve the tracking rate
			return false, 0
		end
		nbFrames = timer:getTime() * trackingRate 
	else
		-- break condition, nbFrames remains zero
		timer:restart()
	end
	return (not breakCondition), nbFrames
end

----------------------------------------------------------------------------------
--	State machine functions
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
--	Transitions
----------------------------------------------------------------------------------
	
function tiFaceCV:gotoResettingTracking()
	log_call ("tiFaceCV:gotoResettingTracking mTrackingState: "..self.mTrackingState, tiFaceCV.logLevelHigh)
	local notifyNotTracking = ((self.mTrackingState ~= tiFaceCV.TI_STEPSTATE_DEF.waitingForReset)
							or (self.mTrackingState ~= tiFaceCV.TI_STEPSTATE_DEF.resettingTracking))
	self:setTrackingState (tiFaceCV.TI_STEPSTATE_DEF.resettingTracking)
	self.mMLTPlugin:resetTarget (self.mTrackingIndex, self.mTargetIndex)
	self.mResetTrackingTimer:restart()
	if notifyNotTracking then
		for key, listener in pairs(self.mListeners) do
			listener.onNotTracking(self)
		end
	end
	log_call_end()
end
						
function tiFaceCV:gotoAnalysingPose()
	log_debug ("tiFaceCV:gotoAnalysingPose", tiFaceCV.logLevelHigh)
	self:setTrackingState (tiFaceCV.TI_STEPSTATE_DEF.analysingPose)
	self.mAnalysePoseTimer:restart()
end
			
function tiFaceCV:gotoWaitingForTracking()
	log_call ("tiFaceCV:gotoWaitingForTracking", tiFaceCV.logLevelHigh)
	for key, listener in pairs(self.mListeners) do
		listener.onWaiting(self)
	end
	self:setTrackingState (tiFaceCV.TI_STEPSTATE_DEF.waitingForTracking)
	self.mWaitForTrackingTimer:restart()
	log_call_end()
end
			
function tiFaceCV:gotoTracking()
	log_call ("tiFaceCV:gotoTracking", tiFaceCV.logLevelHigh)		

	self:setTrackingState (tiFaceCV.TI_STEPSTATE_DEF.tracking)
	
	for key, listener in pairs(self.mListeners) do
		listener.onTracking(self)
	end
	log_call_end()
end
			
function tiFaceCV:gotoWaitingForReset()
	log_call ("tiFaceCV:gotoWaitingForReset mTrackingState: "..self.mTrackingState, tiFaceCV.logLevelHigh)
	local notifyNotTracking = ((self.mTrackingState ~= tiFaceCV.TI_STEPSTATE_DEF.waitingForReset) 
							or (self.mTrackingState ~= tiFaceCV.TI_STEPSTATE_DEF.resettingTracking))
	self:setTrackingState (tiFaceCV.TI_STEPSTATE_DEF.waitingForReset)
	self.mResetTrackingTimer:restart()
	if notifyNotTracking then
		self:resetReferencePD()
		for key, listener in pairs(self.mListeners) do
			listener.onNotTracking(self)
		end
	end
	log_call_end()
end
			
----------------------------------------------------------------------------------
--	States
----------------------------------------------------------------------------------
function tiFaceCV:gotoAnalysingPose1()
	log_call ("tiFaceCV:gotoAnalysingPose1", tiFaceCV.logLevelHigh)
	local positionState = self:getPositionState()
	if tiFaceCV.TI_POSITION_STATE_DEF.undefined then
		if positionState == tiFaceCV.TI_POSITION_STATE_DEF.wellPositionned then
			-- the face is well positionned, so we move to the waitingForTracking state
			self.mAnalysePoseTimer:stop()
			self.mAnalysePoseSubstate = tiFaceCV.TI_POSITION_STATE_DEF.undefined
	--		self:gotoWaitingForTracking()
			self:gotoTracking()
		else
			-- we remain in the same state but we need to update the position state
			if self.mAnalysePoseSubstate ~= positionState then
				self.mAnalysePoseSubstate = positionState
				if self.mAnalysePoseSubstate == tiFaceCV.TI_POSITION_STATE_DEF.notCentered then 
					--log_info("TI_TRACKING_STATUS_NOT_CENTERED")
					for key, listener in pairs(self.mListeners) do
						listener.onNotCentered(self)
					end
				elseif self.mAnalysePoseSubstate == tiFaceCV.TI_POSITION_STATE_DEF.tooFar then 
					--log_info("TI_TRACKING_STATUS_TOO_FAR")
					for key, listener in pairs(self.mListeners) do
						listener.onTooFar(self)
					end
				elseif self.mAnalysePoseSubstate == tiFaceCV.TI_POSITION_STATE_DEF.tooClose then
					--log_info("TI_TRACKING_STATUS_TOO_CLOSE")
					for key, listener in pairs(self.mListeners) do
						listener.onTooClose(self)
					end
				end
			end
			self:setTrackingState (tiFaceCV.TI_STEPSTATE_DEF.resettingTracking)
			self.mMLTPlugin:resetTarget (self.mTrackingIndex, self.mTargetIndex)
		end
	end
	log_call_end()
end

function tiFaceCV:updateFromWaitingForReset()
	log_call ("tiFaceCV:_updateFromWaitingForReset", tiFaceCV.logLevelHigh)
	if(self.mResetTrackingTimer:getTime() > tiFaceCV.TI_WAITINGFORRESET_TIME_OUT) then
		-- Time out implies -> reset tracking
		self.mResetTrackingTimer:stop()
		self:gotoResettingTracking()
	end
	log_call_end()
end
			
function tiFaceCV:updateFromResettingTracking()
	log_call ("tiFaceCV:updateFromResettingTracking", tiFaceCV.logLevelHigh)
	local nbMaxPosesOK = 2
				
	local tracking, nbPoses = self:getFramesCount (self.mResetTrackingTimer, not self:isMltTracking())
	if(tracking and (nbPoses > nbMaxPosesOK)) then
		self.mResetTrackingTimer:stop()
--		self:gotoTracking()
		self:gotoAnalysingPose1()
--		self:gotoResettingTracking()
	end
	log_call_end()
end

function tiFaceCV:updateFromAnalysingPose()
	log_call ("tiFaceCV:updateFromAnalysingPose", tiFaceCV.logLevelHigh)
	if(self.mAnalysePoseTimer:getTime() > tiFaceCV.TI_ANALYSEPOSE_TIME_OUT) then
		-- Time out implies -> reset tracking
		self.mAnalysePoseTimer:stop()
		self:gotoResettingTracking()
		return
	end
	-- General case
	local positionState = self:getPositionState()
	if (not self.mIsMltTracking) then
		-- we are no longer tracking -> we move the the resettingTracking state
		self.mAnalysePoseTimer:stop()
		self.mAnalysePoseSubstate = tiFaceCV.TI_POSITION_STATE_DEF.undefined
		self:gotoResettingTracking()
	elseif positionState == tiFaceCV.TI_POSITION_STATE_DEF.wellPositionned then
		-- the face is well positionned, so we move to the waitingForTracking state
		self.mAnalysePoseTimer:stop()
		self.mAnalysePoseSubstate = tiFaceCV.TI_POSITION_STATE_DEF.undefined
--		self:gotoWaitingForTracking()
		self:gotoTracking()
	else
		-- we remain in the same state but we need to update the position state
		if self.mAnalysePoseSubstate ~= positionState then
			self.mAnalysePoseSubstate = positionState
			if self.mAnalysePoseSubstate == tiFaceCV.TI_POSITION_STATE_DEF.notCentered then 
				--log_info("TI_TRACKING_STATUS_NOT_CENTERED")
				for key, listener in pairs(self.mListeners) do
					listener.onNotCentered(self)
				end
			elseif self.mAnalysePoseSubstate == tiFaceCV.TI_POSITION_STATE_DEF.tooFar then 
				--log_info("TI_TRACKING_STATUS_TOO_FAR")
				for key, listener in pairs(self.mListeners) do
					listener.onTooFar(self)
				end
			elseif self.mAnalysePoseSubstate == tiFaceCV.TI_POSITION_STATE_DEF.tooClose then
				--log_info("TI_TRACKING_STATUS_TOO_CLOSE")
				for key, listener in pairs(self.mListeners) do
					listener.onTooClose(self)
				end
			end
		end
		self.mAnalysePoseTimer:stop()
		self.mAnalysePoseSubstate = tiFaceCV.TI_POSITION_STATE_DEF.undefined
		self:gotoResettingTracking()
	end
	log_call_end()
end

function tiFaceCV:updateFromWaitingForTracking()
	log_call ("tiFaceCV:updateFromWaitingForTracking", tiFaceCV.logLevelHigh)
	if not self:isMltTracking() then
		-- if we are no longer tracking we go to the reset state
		self.mWaitForTrackingTimer:stop()
		self:gotoResettingTracking()
	end
	-- we must remain well positionned during nbMaxFramesOK at least in order to go to the tracking state
	local nbMaxFramesOK = 0
	local wellPositionned, nbFrames = self:getFramesCount (self.mWaitForTrackingTimer, (self:getPositionState() == tiFaceCV.TI_POSITION_STATE_DEF.wellPositionned))
	if(wellPositionned and (nbFrames > nbMaxFramesOK)) then
		self.mWaitForTrackingTimer:stop()
		self:gotoTracking()
	elseif not wellPositionned then
		self.mWaitForTrackingTimer:stop()
		self:gotoAnalysingPose()
	end
	log_call_end()
end
			
function tiFaceCV:update()
	local err, targetstatus = self.mMLTPlugin:getTargetStatus(self.mTrackingIndex, self.mTargetIndex)
--	local err, targetstatus = MLTPluginEmulator:getTargetStatus(self.mTrackingIndex, self.mTargetIndex)
	log_debug ("tiFaceCV:update mTrackingIndex: "..self.mTrackingIndex.." mTargetIndex: "..self.mTargetIndex.." err: "..err..", targetstatus: "..targetstatus..", res: "..tostring(targetstatus > 0), tiFaceCV.logLevelLow)			
	self.mIsMltTracking = (targetstatus > 0)
end
			
function tiFaceCV:updateFromTracking()
	log_call ("tiFaceCV:updateFromTracking", tiFaceCV.logLevelHigh)
	if (not self:isMltTracking()) then
		self:gotoResettingTracking()	
	end
--				mMLTPlugin:getTargetPos(mTrackingIndex, mTargetIndex, mPosition, mOrientation)
	log_call_end()
end
			
----------------------------------------------------------------------------------
--	Public functions
----------------------------------------------------------------------------------

function tiFaceCV:registerListener(listener)
	table.insert(self.mListeners, listener)
end
			
function tiFaceCV:getFacePoints()
	log_debug ("tiFaceCV:getFacePoints: "..self.mNbPointsCLM.." points", tiFaceCV.logLevelLow)
	return self.mNbPointsCLM, self.mPointsCLM
end
						
function tiFaceCV:isActive() 
	return (self.mTrackingIndex >= 0) and not self.mPaused
end
	
function tiFaceCV:isTracking()
log_debug("tiFaceCV:isTracking mTrackingState: "..tiFaceCV.trackingStateToString(self.mTrackingState), tiFaceCV.logLevelHigh)
	return (not self.mPaused and (self.mTrackingState == tiFaceCV.TI_STEPSTATE_DEF.tracking))
--return true
end
					
function tiFaceCV:pauseAutoTracking()
	log_call ("tiFaceCV:pauseAutoTracking " .. self.mTrackingIndex)
	local result = eOk
	if self.mTrackingIndex  ~= -1 then
		result = self.mMLTPlugin:pauseTracking (self.mTrackingIndex)
		self.mPaused = true
		log_debug("tiFaceCV:pauseAutoTracking pause autotracking paused tracking index " .. self.mTrackingIndex)
	end
	log_call_end()
	return result
end
			
function tiFaceCV:stopAutoTracking()
	log_call ("tiFaceCV:stopAutoTracking " .. self.mTrackingIndex)
	local result = eOk
	if self.mTrackingIndex  ~= -1 then
		log_debug ("tiFaceCV:stopAutoTracking index: "..self.mTrackingIndex)
		result = self.mMLTPlugin:stopTracking(self.mTrackingIndex)
		self.mTrackingIndex = -1
	end
	log_call_end()
	return result
end
			
function tiFaceCV:runAutoTracking()
	log_call ("tiFaceCV:runAutoTracking " .. self.mTrackingIndex)
	local result = eFailed
	if self.mCamera ~= nil then
		-- Init camera model -> we retrieve the X and Y focal values
		local maxx, maxy
		maxx, maxy, self.mFocalX, self.mFocalY = self.mCamera:getCameraModel()
		log_debug ("tiFaceCV.runAutoTracking cam: size: " .. maxx .. "x" .. maxy .. " focal: " .. self.mFocalX .. "x" .. self.mFocalY)
		log_debug("tiFaceCV:runAutoTracking mTrackingIndex: " .. self.mTrackingIndex)
		if self.mTrackingIndex  ~= -1 then
			log_debug("tiFaceCV:runAutoTracking resume tracking with camera: "..tostring(self.mCamera:getName()).. " videocap id: "..tostring(self.mCamera:getVideoCaptureId()))
			result = self.mMLTPlugin:updateVidCapID (self.mTrackingIndex, self.mCamera:getVideoCaptureId())
			result = self.mMLTPlugin:runTracking (self.mTrackingIndex)
		else
			log_debug("tiFaceCV:runAutoTracking start tracking with config: " .. tostring(self.mTrackingFile).. " camera: "..tostring(self.mCamera:getName()).. " videocap id: "..tostring(self.mCamera:getVideoCaptureId()))
			result, self.mTrackingIndex = self.mMLTPlugin:startTracking (self.mTrackingFile, self.mCamera:getVideoCaptureId(), self.mCamera:getCamera())
log_debug("tiFaceCV:runAutoTracking	tiFaceCV.recordInputImage: "..tostring(tiFaceCV.recordInputImage).." tiFaceCV.recordInputImageBufferSize: "..tostring(tiFaceCV.recordInputImageBufferSize).." tiFaceCV.recordInputImagePath: "..tostring(tiFaceCV.recordInputImagePath))
			if tiFaceCV.recordInputImage ~= nil then
				self.mMLTPlugin:setRecordInputImage (self.mTrackingIndex, tiFaceCV.recordInputImage)
			end
			if tiFaceCV.recordInputImageBufferSize ~= nil then
				self.mMLTPlugin:setRecordInputImageBufferSize (self.mTrackingIndex, tiFaceCV.recordInputImageBufferSize)
			end
			if tiFaceCV.recordInputImagePath ~= nil then
				self.mMLTPlugin:setRecordInputImagePath (self.mTrackingIndex, tiFaceCV.recordInputImagePath)
			end
			if self.setPupilDistance ~= nil then 
				self:setPupilDistance(tiFaceCV.getPupilDistance())
			end
			log_debug("tiFaceCV:runAutoTracking start tracking, new index is " .. self.mTrackingIndex)
		end
		log_debug("tiFaceCV:runAutoTracking result: "..tostring(result))
		self.mPaused = false
		-- Stop the timers
		self.mResetTrackingTimer:stop()
		self.mAnalysePoseTimer:stop()
		self.mWaitForTrackingTimer:stop()
		-- reset the tracking
		self:gotoResettingTracking()
	else
		log_warning ("tiFaceCV:runAutoTracking camera is null")
	end
	log_call_end()
	return result
end
			
function tiFaceCV:recalibrate()
	log_call ("tiFaceCV:recalibrate", tiFaceCV.logLevelLow)
	self.mMLTPlugin:resetTarget (self.mTrackingIndex, self.mTargetIndex)
	log_call_end()
end
			
function tiFaceCV:waitAndResetTracking()
	log_call ("tiFaceCV:waitAndResetTracking", tiFaceCV.logLevelLow)
	self.mPaused = false
	-- Stop the timers
	self.mResetTrackingTimer:stop()
	self.mWaitForTrackingTimer:stop()
	-- reset the tracking
	self:gotoWaitingForReset()
	log_call_end()
end
			
function tiFaceCV:initialize()	
end
			
function tiFaceCV:loop()
	log_debug ("tiFaceCV:loop mPaused: "..tostring(self.mPaused), tiFaceCV.logLevelHigh)
	if self:isActive() then
		self:update()
		log_debug ("tiFaceCV:loop mTrackingState: "..tiFaceCV.trackingStateToString(self.mTrackingState), tiFaceCV.logLevelHigh)
		if self.mTrackingState == tiFaceCV.TI_STEPSTATE_DEF.waitingForReset then
			self:updateFromWaitingForReset()
		elseif self.mTrackingState == tiFaceCV.TI_STEPSTATE_DEF.resettingTracking then
			self:updateFromResettingTracking()
		elseif self.mTrackingState == tiFaceCV.TI_STEPSTATE_DEF.analysingPose then
			self:updateFromAnalysingPose()
		elseif self.mTrackingState == tiFaceCV.TI_STEPSTATE_DEF.waitingForTracking then
			self:updateFromWaitingForTracking()
		elseif self.mTrackingState == tiFaceCV.TI_STEPSTATE_DEF.tracking then
			self:updateFromTracking()
		end				
	end
end

function tiFaceCV:getProcessingStats1()
	local err, rate = self.mMLTPlugin:getTrackingRate (self.mTrackingIndex)
	return rate
end		

function tiFaceCV:getProcessingStats()
	local err, preprocessing, processing, postprocessing = self.mMLTPlugin:getTrackingAverageTimes (self.mTrackingIndex)
	-- local err, preprocessing1, processing1, postprocessing1 = self.mMLTPlugin:getTrackingMaxTimes (self.mTrackingIndex)
-- log_debug("Tracking max: "..preprocessing1.." "..processing1.." "..postprocessing1)
	return preprocessing, processing, postprocessing
end		

function tiFaceCV:setRecordInputImage (state)
	self.mMLTPlugin:setRecordInputImage (self.mTrackingIndex, state)
end		


