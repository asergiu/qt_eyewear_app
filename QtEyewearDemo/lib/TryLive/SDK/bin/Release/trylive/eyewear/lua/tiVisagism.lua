-- tiVisagism.lua script
log_loaded_info("tiVisagism.lua")
-----------------------------
-- This class manages the face analysis
		
tiVisagism = inheritsFrom (tiFaceCV)

tiVisagism.logLevel = 5

function tiVisagism:init(properties)
	log_debug("tiVisagism:init properties: "..tostring(properties))

	if self.mTrackingFile == nil then 
		self.mTrackingFile = self.baseInternalTrackingFile.."Visagism.xml"
	end
end

function tiVisagism:refreshPoints()
	local err = 0
	err, self.mNbPointsCLM, self.mPointsCLM = self.mMLTPlugin:getCLMFaceTrackingListOfPoints (self.mTrackingIndex, self.mTargetIndex)
	log_debug("tiVisagism:refreshPoints getCLMFaceTrackingListOfPoints err: "..err.." mNbPointsCLM: "..self.mNbPointsCLM.." eOk: "..eOk, tiVisagism.logLevel) 
	if err ~= eOk then
		self.mNbPointsCLM = 0
		self.mPointsCLM = {}
	end
end

function tiVisagism:gotoTracking()
	log_call ("tiVisagism:gotoTracking", tiVisagism.logLevel)

	self:refreshPoints()
	
	self.mTrackingState = tiFaceCV.TI_STEPSTATE_DEF.tracking
	
	for key, listener in pairs(self.mListeners) do
		listener.onTracking(self)
	end
	log_call_end()
end			

function tiVisagism:update()
	local err, targetstatus = self.mMLTPlugin:getTargetStatus(self.mTrackingIndex, self.mTargetIndex)
	log_debug ("tiVisagism:update mTrackingIndex: "..self.mTrackingIndex.." mTargetIndex: "..self.mTargetIndex.." err: "..err..", targetstatus: "..targetstatus..", res: "..tostring(targetstatus > 0), tiVisagism.logLevel)			
	self:refreshPoints()
	self.mIsMltTracking = (targetstatus > 0)
end

function tiVisagism:getData()
	local scoresHairlines = {}
	local scoresJawlines = {}
	local scoresShapes = {}
	local scoresSkinColor = {}
	local scoresEyesColor = {}
	local scoresHairColor = {}
	local measures = {}
	local err
	err, scoresHairlines, scoresJawlines, scoresShapes = self.mMLTPlugin:getVisagismScores (self.mTrackingIndex, self.mTargetIndex)
	log_debug ("getVisagismScores err: "..err)
	err, measures = self.mMLTPlugin:getVisagismMeasures (self.mTrackingIndex, self.mTargetIndex)
	log_debug ("getVisagismMeasures err: "..err)
--	imageRGB:getFromCapture(vidcap)
--	err, scoresSkinColor, scoresEyesColor, scoresHairColor = self.mMLTPlugin:doFaceColorAnalysis (self.mTrackingIndex, self.mTargetIndex, imageRGB)
--	log_debug ("doFaceColorAnalysis err: "..err)
	return scoresHairlines, scoresJawlines, scoresShapes, measures, scoresSkinColor, scoresEyesColor, scoresHairColor
end

function toJSON (titre, tab)
	local s = '"'..titre..'":  {'
	for key, val in pairs(tab) do
		s = s .. '"'..key .. '": ' .. val .. ", "
	end
	s = s .. '}'
	return s
end

function tiVisagism:getDataJSON()
	local scoresHairlines = {}
	local scoresJawlines = {}
	local scoresShapes = {}
	local scoresSkinColor = {}
	local scoresEyesColor = {}
	local scoresHairColor = {}
	local measures = {}
	
	scoresHairlines, scoresJawlines, scoresShapes, measures, scoresSkinColor, scoresEyesColor, scoresHairColor = self:getData()

	log_keyvaltab ("scoresHairlines", scoresHairlines)
	log_keyvaltab ("scoresJawlines", scoresJawlines)
	log_keyvaltab ("scoresShapes", scoresShapes)
	local s = ""
	for i = 1, #measures do
		s = s .. " " .. measures[i]
	end	
	
	local res = '{"visagism":{'
	res = res .. toJSON ("scoresHairlines", scoresHairlines)..','
	res = res .. toJSON ("scoresJawlines", scoresJawlines)..','
	res = res .. toJSON ("scoresShapes", scoresShapes)..','						
	res = res .. toJSON ("measures", measures)..','	
	res = res .. toJSON ("scoresSkinColor", scoresSkinColor)..','						
	res = res .. toJSON ("scoresEyesColor", scoresEyesColor)..','						
	res = res .. toJSON ("scoresHairColor", scoresHairColor)						
	res = res .. '}}'

	return res
end




