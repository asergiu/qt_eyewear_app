log_loaded_info("tiCameraManager.lua")

			
-- The following management must evolve to handle test configuration:
-- Currently test configuration try to find config relativelety to it in place searching in the sdk

tiCameraManager = {
	configFileName = "CameraConfig.json",
	configFilePath = getScriptDirectory().."../resources/",
	updatedConfigFilePath = getAppConfigPath(),
	dateConfigFilePath = getAppConfigPath().."CameraConfigUpdate.txt",
	configFileRemoteUrl = "https://cdn.trylive.com/static_files/",
	configUpdateDelay = 7*24*60*60, -- durée avant nouvel eupdate online des configs de caméra (en secondes)
	CameraFriendlyName = {Back = "back", Front = "front"},
	CameraType = {Video = "video", Photo = "photo", Virtual = "virtual"},
	Target = {Studio = "Studio", iOS = "ios", Android = "android", Windows = "windows"},
	-- inverted:
	-- MIRROR_NONE = 0, MIRROR_HORIZONTAL = 1, MIRROR_VERTICAL = 2, MIRROR_HV = 3
	defaultCameraConfig = 
	{
		-- Default
	    -- Android
		{ name = "Android generic front",	device = "android",		friendlyName = "front",	focalY = 1.06,		capture = {x = 1280, y =  720},	use = {x =  1280, y = 720},	driver = 0, pixel = "NV12",		inverted = MIRROR_VERTICAL,	cameraType = "video"},
		{ name = "Android generic back",	device = "android",		friendlyName = "back",	focalY = 1.16,		capture = {x = 1280, y =  720},	use = {x =  1280, y = 720},	driver = 1, pixel = "NV12",		inverted = MIRROR_NONE,		cameraType = "video"},
		-- iOS
		{ name = "iOS generic back",		device = "ios",			friendlyName = "back",	focalY = 1.1000,	capture = {x = 1280, y =  720},	use = {x =  960, y = 720},	driver = 0, pixel = "BGR32_1",	inverted = MIRROR_NONE,		cameraType = "video"},
		{ name = "iOS generic front",		device = "ios",			friendlyName = "front",	focalY = 1.2200,	capture = {x = 1280, y =  720},	use = {x = 1280, y = 720},	driver = 1, pixel = "BGR32_1",	inverted = MIRROR_VERTICAL,	cameraType = "video"},
		-- Windows
		{ name = "Windows Studio front",	device = "Studio",	friendlyName = "front",	focalY = 1.3125,	capture = {x =  640, y =  480},	use = {x =  640, y = 480},	driver = 0, pixel = "YUY2",		inverted = MIRROR_VERTICAL,		cameraType = "video"},
		{ name = "Windows Studio back",		device = "Studio",	friendlyName = "back",	focalY = 1.3125,	capture = {x =  640, y =  480},	use = {x =  640, y = 480},	driver = 0, pixel = "YUY2",		inverted = MIRROR_NONE,			cameraType = "video"},
		{ name = "Windows webcam front",	device = "windows",	friendlyName = "front",	focalY = 1.3125,	capture = {x =  640, y =  480},	use = {x =  640, y = 480},	driver = 0, pixel = "YUY2",		inverted = MIRROR_VERTICAL,		cameraType = "video"},
		{ name = "Windows webcam back",		device = "windows",	friendlyName = "back",	focalY = 1.3125,	capture = {x =  640, y =  480},	use = {x =  640, y = 480},	driver = 0, pixel = "YUY2",		inverted = MIRROR_NONE,			cameraType = "video"},
	},
	specificCameraConfig = 
	{
	},
	TI_PATH_LOCAL_CONFIG_FILES = "../resources/ConfigCamera/",
	
	mCurrentCamera = {}
	-- Tableau de cameras avec pour chaque camera:
	--	.config = la config camera à utiliser telle que définie dans defaultCameraConfig
	--	.camera = la tiCamera correspondante, nil si pas ouverte
}

function tiCameraManager.findConfig1 (listConfigs, device, friendlyName)
	log_debug ("tiCameraManager.findConfig search device: "..tostring(device).." friendlyName: "..tostring(friendlyName))
	for i,v in pairs(listConfigs) do
		log_debug ("tiCameraManager.findConfig see device: "..tostring(v["device"]).." friendlyName: "..tostring(v["friendlyName"]))
		if v["device"] ~= nil and v["friendlyName"] ~= nil then
			if string.find (string.lower(device), string.escape (string.lower(v["device"]))) == 1 and v["friendlyName"] == friendlyName then
				log_info("Target: " .. device .. " " .. friendlyName .. ", found: " .. v["device"].." "..v["friendlyName"].." in specific configuration: camera= "..tostring(v["cameraCalib"])..", videoConfig= "..tostring(v["videoConfig"]))
				return v
			end
		end
	end
	log_warning("tiCameraManager.findConfig Unknown target: " .. device .. " " .. friendlyName)
	return nil
end

function tiCameraManager.findConfig (listConfigs, device, friendlyName)
	log_debug ("tiCameraManager.findConfig search device: "..tostring(device).." friendlyName: "..tostring(friendlyName))
--	log_object(listConfigs, "tiCameraManager.findConfig")
	local res = listConfigs[string.lower(device.." "..friendlyName)]
	return res
end
						
-- Find the best camera

function tiCameraManager.findDefaultCameraConfig (friendlyName)
	log_call ("tiCameraManager.findDefaultCameraConfig friendlyName: " .. friendlyName)
	if friendlyName ~= tiCameraManager.CameraFriendlyName.Back and friendlyName ~= tiCameraManager.CameraFriendlyName.Front then
		dispatchError(TI_INTERNAL_ERROR, "Parameter friendlyName is invalid : " .. friendlyName)
		return
	end
	
	local finalConfig = {}
	
	-- Find the default configuration for this platform and camera
	
	local os = "zob"
	if getOSType() == TI_OS_ANDROID then		os = tiCameraManager.Target.Android
	elseif getOSType() == TI_OS_IPHONEOS then	os = tiCameraManager.Target.iOS
	elseif getOSType() >= TI_OS_WINSEVEN then	os = tiCameraManager.Target.Windows
	end
	
	local defaultPlatformConfig = tiCameraManager.findConfig (tiCameraManager.specificCameraConfig, os, friendlyName)
	
--	log_object (defaultPlatformConfig, "Default platform config")

	for field, val in pairs(defaultPlatformConfig) do
		finalConfig[field] = val
	end
--	log_object (finalConfig, "Final config apres copie config defaut")
	
	-- Try to found a configuration for this device
	
	local err, device = tiSystemInfo.get("device-model")
	if err ~= eOk then
		log_warning ("Erreur getSystemInfo('device-model')" .. tostring(err))
	end	
	
	local cameraConfig = tiCameraManager.findConfig (tiCameraManager.specificCameraConfig, device, friendlyName)
--	log_object (cameraConfig, "Specific config")
	
	-- If found, update the default configuration
	
	if cameraConfig ~= nil then
		for field,val in pairs(cameraConfig) do
			finalConfig[field] = val
		end
--		log_object (finalConfig, "Final config apres copie config specifique")
	else
		log_warning ("No specific config found for device: "..tostring(device).." friendlyName: "..tostring(friendlyName))
	end
	
	-- Currently video live pause doesn't work on Windows (but it works on iOS)
	-- So there is a Lua implementation, but currently device/video orientation is not correctly handled on iOS
	-- So on Windows, use false, and on iOS use true ...
	if os == tiCameraManager.Target.Windows then
		tiCamera.useVideoCapturePause = false
	end
	log_call_end()
	return friendlyName, finalConfig 				
end
			
function tiCameraManager.closeCamera (cameraFriendlyName)
	log_debug ("tiCameraManager.closeCamera "..cameraFriendlyName)
tiCameraManager.printCameras()	
	if cameraFriendlyName ~= nil then
		local cam = tiCameraManager.getCurrentCamera (cameraFriendlyName)
		if cam ~= nil then
			cam:close()
		else
			log_warning ("tiCameraManager.closeCamera, camera "..cameraFriendlyName.." not created")
		end
	end
tiCameraManager.printCameras()	
end

function tiCameraManager.closeTemporaryCamera (cameraFriendlyName)
	log_debug ("tiCameraManager.closeTemporaryCamera "..cameraFriendlyName)
tiCameraManager.printCameras()	
	if cameraFriendlyName ~= nil then
		local cam = tiCameraManager.getCurrentCamera (cameraFriendlyName)
		if cam ~= nil then
			cam:closeTemporary()
		else
			log_warning ("tiCameraManager.closeTemporaryCamera, camera "..cameraFriendlyName.." not created")
		end
	end
tiCameraManager.printCameras()	
end
			
function tiCameraManager.openCamera (cameraFriendlyName)
	log_call ("tiCameraManager.openCamera "..cameraFriendlyName)
	local res = false
	if cameraFriendlyName ~= nil then
		local cam = tiCameraManager.getCurrentCamera (cameraFriendlyName)
		-- In case video capture is already open, close it before in order to open a new one
		if cam ~= nil then
			cam:open()
			res = true
		else
			log_warning ("tiCameraManager.openCamera, camera "..cameraFriendlyName.." not created")
		end
	end
	log_call_end()
	return res
end
						
function tiCameraManager.playCamera (cameraFriendlyName)
	log_debug ("tiCameraManager.playCamera "..cameraFriendlyName)
	if cameraFriendlyName ~= nil then
		local cam = tiCameraManager.getCurrentCamera (cameraFriendlyName)
		if cam ~= nil then
			tiDataRecorder.log("trylive_feature_begin", "video_resumed", cameraFriendlyName)	
			cam:play()
		else
			log_warning ("tiCameraManager.playCamera, camera "..cameraFriendlyName.." not created")
		end
	end
end
			
function tiCameraManager.pauseCamera (cameraFriendlyName)
	log_debug ("tiCameraManager.pauseCamera "..cameraFriendlyName)
	if cameraFriendlyName ~= nil then
		local cam = tiCameraManager.getCurrentCamera (cameraFriendlyName)
		if cam ~= nil then
			tiDataRecorder.log("trylive_feature_begin", "video_paused", cameraFriendlyName)
			cam:pause()
		else
			log_warning ("tiCameraManager.pauseCamera, camera "..cameraFriendlyName.." not created")
		end
	end
end
			
function tiCameraManager.updateVideoBackground (cameraFriendlyName)
	log_call ("tiCameraManager.updateVideoBackground")			
	
	tiCameraManager.closeCamera (cameraFriendlyName)
	local ret = tiCameraManager.openCamera (cameraFriendlyName)
	
	log_call_end()	
	return ret		
end
						
function tiCameraManager.getVideoOrientation (cameraFriendlyName)
	if cameraFriendlyName ~= nil then
		local cam = tiCameraManager.getCurrentCamera (cameraFriendlyName)
		if cam ~= nil then
			return cam:getVideoOrientation()
		else
			log_warning ("tiCameraManager.getVideoOrientation, camera "..cameraFriendlyName.." not found")
		end
	end
	return -1, -1, -1, -1
end

function tiCameraManager.getCurrentCameras (cameraType, isOpened)
--log_debug ("tiCameraManager.getCurrentCameras isOpened: "..tostring(isOpened).." cameraType: "..tostring(cameraType))
--tiCameraManager.printCameras()
	local cameraList = {}
	for cameraName, cam in pairs(tiCameraManager.mCurrentCamera) do
		if cam.camera ~= nil then
--log_debug("tiCameraManager.getCurrentCameras add cam "..cam.config.name)
			if cameraType == nil or cameraType == cam.config.cameraType then
				if isOpened == nil or cam.camera:isOpened() then
					cameraList[cameraName] = cam.camera
				end
			end
		end
	end
	return cameraList
end

function tiCameraManager.getCurrentCamera (cameraFriendlyName)
	local res = nil
--tiCameraManager.printCameras()
--printStack()
--log_debug ("tiCameraManager.getCurrentCamera "..cameraFriendlyName)
	if cameraFriendlyName ~= nil then
--log_object (tiCameraManager.mCurrentCamera[cameraFriendlyName], "tiCameraManager.getCurrentCamera cameras courantes")
		local cam = tiCameraManager.mCurrentCamera[cameraFriendlyName]
		if cam ~= nil then
			if cam.camera == nil then
				if cam.config.cameraType == "video" then
					log_debug ("tiCameraManager.getCurrentCamera create camera video "..cameraFriendlyName)
					cam.camera = tiCameraVideo:create(cam.config)
				elseif cam.config.cameraType == "photo" then
					log_debug ("tiCameraManager.getCurrentCamera create camera photo "..cameraFriendlyName)
					cam.camera = tiCameraPhoto:create(cam.config)
				elseif cam.config.cameraType == "virtual" then
					log_debug ("tiCameraManager.getCurrentCamera create camera virtual "..cameraFriendlyName)
					cam.camera = tiCameraVirtual:create(cam.config)
				else
					log_error ("tiCameraManager.getCurrentCamera can't create "..tostring(cam.config.cameraType).." camera")
				end
			end
			res = cam.camera
		else
			log_warning ("tiCameraManager.getCurrentCamera, camera "..cameraFriendlyName.." not found")
		end	
	end
	return res
end
				
function tiCameraManager.removeAllListeners()
	for i, cam in pairs(tiCameraManager.mCurrentCamera) do
		if cam.camera ~= nil then
			cam.camera:removeListeners()
		end
	end
end

function tiCameraManager.removeListener(listener)
	for i, cam in pairs(tiCameraManager.mCurrentCamera) do
		if cam.camera ~= nil then
			cam.camera:removeListener(listener)
		end
	end
end
	
function tiCameraManager.setCurrentCamera (cameraFriendlyName, camera)
	if cameraFriendlyName ~= nil then
		local cam = tiCameraManager.mCurrentCamera[cameraFriendlyName]
		if cam ~= nil then
			cam.camera = camera
		else
			log_warning ("tiCameraManager.setCurrentCamera, camera "..cameraFriendlyName.." not found")
		end	
--	log_debug ("tiCameraManager.setCurrentCamera "..camera:getName())
	end
end
									
function tiCameraManager.setCameraConfig (name, friendlyName, cameraType, cameraCalib, videoConfig)
	log_call("tiCameraManager.setCameraConfig "..tostring(name)..","..tostring(friendlyName)..","..tostring(cameraType)..","..tostring(cameraCalib)..","..tostring(videoConfig))
	local res = true
	local cameraConfig = {}
	if friendlyName == nil and cameraType == nil and cameraCalib == nil and videoConfig == nil then
		cameraConfig = name
		friendlyName = cameraConfig.friendlyName
	else
		cameraConfig = {name = name, device = "user", friendlyName = friendlyName, cameraType = cameraType, cameraCalib = cameraCalib, videoConfig = videoConfig}
	end
	table.insert (tiCameraManager.specificCameraConfig, cameraConfig)
	local oldCam = tiCameraManager.mCurrentCamera[friendlyName]
	if oldCam == nil then
		-- Nouveau type de camera
		tiCameraManager.mCurrentCamera[friendlyName] = {}
	else
		-- On remplace une camera existante
		if oldCam.camera ~= nil then
			if oldCam.camera:isOpened() then
				oldCam.camera:close()
			end
			oldCam.camera:fireEvent("EventCameraChanged")
		end
	end
	tiCameraManager.mCurrentCamera[friendlyName].config = cameraConfig
	tiCameraManager.mCurrentCamera[friendlyName].camera = nil
	
	tiCameraManager.printCameras()
	log_call_end()
	return res
end

function tiCameraManager.loadConfigs (fileName)
	log_debug ("tiCameraManager.loadConfigs "..tostring(fileName))
	local tabConfigs = {}
	if tiFile.exists (fileName) then
		local bufferIn = File.read(fileName)
		if (not pcall (function()
							tabConfigs = json.decode (bufferIn)
						end
						)) then 
			log_warning("Error parsing Json file: '"..fileName)
			
			tabConfigs = {}
		end
		
		-- Conversion des clés d'accès en minuscule pour éviter les soucis ensuite
		
		local tabConfigs2 = {}
		for i, val in pairs (tabConfigs) do
			local ii = string.lower(i)
			tabConfigs2[ii] = val
		end 	
		tabConfigs = tabConfigs2
	else
		log_error ("Impossible to read file "..tostring(fileName))
	end
--log_object (tabConfigs, "XXXXXXXXXXXXXXXXXX configs camera loadée "..tostring(fileName).." XXXXXXXXXXXXXXXXXX")
	return tabConfigs
end


function tiCameraManager.updateCameraConfigsDate()
	local f = io.open(tiCameraManager.dateConfigFilePath, "w")
	if f ~= nil then
		f:write(os.time())
		f:close()
	end
end

function tiCameraManager.cameraConfigsOutOfDate()
	local outOfDate = true
	
	local f = io.open(tiCameraManager.dateConfigFilePath, "r")
	if f ~= nil then
		lastUpadateTime = f:read("*all")
		f:close()
		if os.time() < lastUpadateTime+tiCameraManager.configUpdateDelay then
			outOfDate = false
		end
	end

	return outOfDate
end

function tiCameraManager.initCameraConfigs()
	log_debug ("tiCameraManager.initCameraConfigs")
	
	-- Chargement des configs prédéfinies intégrées au SDK
	
	tiCameraManager.specificCameraConfig = tiCameraManager.loadConfigs (tiCameraManager.configFilePath..tiCameraManager.configFileName)
--log_object (tiCameraManager.specificCameraConfig, "tiCameraManager.initCameraConfigs 1")
	-- Download éventuel de configs updatées depuis S3
	
	if tiCameraManager.cameraConfigsOutOfDate() then
		local remotePath = tiCameraManager.configFileRemoteUrl..tiCameraManager.configFileName
		log_info ("Download camera configs from: "..remotePath)
		local res, text = tiNetwork.downloadText (remotePath)
		if res and text ~= nil then
			local localPath = tiCameraManager.updatedConfigFilePath..tiCameraManager.configFileName
			log_info ("Update camera configs: "..localPath)
			if File.write (localPath, text) == eOk then
				local new = tiCameraManager.loadConfigs (localPath)
--log_object (new, "tiCameraManager.initCameraConfigs 2")
			
				-- Update des configs camera
				
				for key, val in pairs (new) do
					tiCameraManager.specificCameraConfig[key] = val
				end
--log_object (tiCameraManager.specificCameraConfig, "tiCameraManager.initCameraConfigs 3")
				tiCameraManager.updateCameraConfigsDate()
			else
				log_warning ("Impossible to write "..tostring(localPath))
			end
		else
			log_warning ("Impossible to download "..tostring(remotePath))
		end
	else
		log_info ("Camera configs "..tiCameraManager.configFilePath..tiCameraManager.configFileName.." up to date")
	end
end

function tiCameraManager.initialize()
	log_call("tiCameraManager.initialize")

	-- Load camera configs
	
	tiCameraManager.initCameraConfigs()
	
	-- Repérage des configs courantes
	
	tiCameraManager.mCurrentCamera["front"] = {}
	tiCameraManager.mCurrentCamera["back"] = {}
	local res
	res, tiCameraManager.mCurrentCamera["front"].config = tiCameraManager.findDefaultCameraConfig ("front")
	res, tiCameraManager.mCurrentCamera["back"].config = tiCameraManager.findDefaultCameraConfig ("back")
	log_object (tiCameraManager.mCurrentCamera["back"].config,"Back camera") 
	log_object (tiCameraManager.mCurrentCamera["front"].config,"Front camera") 

	tiCameraManager.printCameras()
	log_call_end()				
end

function tiCameraManager.printCameras()
log_debug("------ tiCameraManager.mCurrentCamera --------")
	for i, c in pairs (tiCameraManager.mCurrentCamera) do
		local config_name = ""
		local camera_name = ""
		local status = ""		
		if c.config == nil then
			config_name = "nil"
		else
			config_name = c.config.name
		end
		if c.camera == nil then
			camera_name = "nil"
		else
			camera_name = c.camera:getName()
			if c.camera:isOpened() then
				status = "opened"
				if c.camera:isPaused() then
					status = status.." paused"
				end
			else
				status = "closed"
			end
		end
		log_debug(i.." config: "..config_name.." camera: "..camera_name.." "..status)
	end
log_debug("----------------------------------------------")
end

function tiCameraManager.getVideoFrameRate()
	local res = {}
	local cameras = tiCameraManager.getCurrentCameras (tiCameraManager.CameraType.Video, true)
	for i, c in pairs (cameras) do
		res[c:getFriendlyName()] = c:getFrameRate()
	end
	return res
end

