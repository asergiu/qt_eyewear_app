log_loaded_info("tiCommandManager.lua")
-- The command Manager manages the common commands and specific commands (for eyewear or home).
-- This manager owns : 
--	 - Common Command
--   - Specific command : Set at the init of the manager.

-- Internal callbacks

-- init status authorized
TI_INIT_FINISHED_COMMAND_READY = "command ready"
TI_INIT_FINISHED_APPLI_READY = "ready"

function initalizationFinished(status)
	sendCallback("tryliveStatusChanged",status)
end

-- Utility functions to format data for the callbacks

function typeof(var)
    local _type = type(var);
    if(_type ~= "table" and _type ~= "userdata") then
        return _type;
    end
	
    if(_type == "userdata") then
--        return Object (var):getClassID()
		local _meta = getmetatable(var);
		if _meta["getZ"] ~= nil then
			_type = "Vector3"
		elseif _meta["getY"] ~= nil then
			_type = "Vector2"
		end
		-- for i, n in pairs(_meta) do
			-- log_debug("i: "..tostring(i).." n: "..tostring(n))
		-- end
		return _type;
    end
	 
    local _meta = getmetatable(var);
    if(_meta ~= nil and _meta._NAME ~= nil) then
        return _meta._NAME;
    else
        return _type;
    end
end

function toCallbackData (data)
	local s = ""
	if typeof (data) == "Vector3" then
		s = data:getX()..";"..data:getY()..";"..data:getZ()
	elseif typeof (data) == "Vector2" then
		s = data:getX()..";"..data:getY()
	end
	return s
end

-- Command manager

tiCommandManager = {
	mComponentCommand = nil,
	mCommonCommand = nil,
	mSpecificCommand = nil,
	mIsInit = false,
	mIsWebComponent = false,
	mCommandArray = {}
}

-- Log all the commands received.
function tiCommandManager.logCommand(cmd)
	local str = "tiCommandManager Command received: " .. cmd["CommandName"]
	local i=0
	while cmd["arg"..i] do
		str = str .. " " .. cmd["arg"..i]
		i = i+1
	end
	log_debug(str)
end
			
function tiCommandManager.init()
	-- Get the interface
	tiCommandManager.mComponentCommand = getComponentInterface()
	if tiCommandManager.mComponentCommand == nil then
		log_error ("Can't find component interface")
	end

	-- Create the common command and the specific command.
	if tiCommandCommon ~= nil then
		tiCommandManager.mCommonCommand = tiCommandCommon.new()
	end
	log_debug("tiCommandManager.init tiCommandEyewear: "..tostring(tiCommandEyewear))
	if tiCommandEyewear ~= nil then
		tiCommandManager.mSpecificCommand = tiCommandEyewear.new()
	end
	
	tiCommandManager.mIsInit = true
	-- The command manager is ready, send the callback COMMAND_READY
	sendCallback("tryliveStatusChanged",TI_INIT_FINISHED_COMMAND_READY)
end
			
function tiCommandManager.isInit()
	return tiCommandManager.mIsInit
end

function tiCommandManager.getCommand()
	local command = nil
	if tiCommandManager.mComponentCommand ~= nil then
		local isCommand = false
		isCommand, command = tiCommandManager.mComponentCommand:pullCommand()
		-- If not command is received from the interface, check the array for command send by lua.
		if not isCommand then
			if table.getn(tiCommandManager.mCommandArray) > 0 then
				command = tiCommandManager.mCommandArray[1]
				table.remove (tiCommandManager.mCommandArray, 1)
			end
		end
	end
	return command
end

--Use it for push command directly from lua. (Just for tests)
function tiCommandManager.pushCommand(command)
	table.insert(tiCommandManager.mCommandArray, command)
end

local function pairsByKeys(t, f)
	local a = {}
	for n in pairs(t) do table.insert(a, n) end
	table.sort(a, f)
	local i = 0      -- iterator variable
	local iter = function ()   -- iterator function
		i = i + 1
		if a[i] == nil then return nil
		else return a[i], t[a[i]]
		end
	end
	return iter
end
	
function tiCommandManager.checkCommand(command)
	local knowCmd = false
	
	if not knowCmd and command["CommandName"] == "ti_call" then 
		if command["arg0"] ~= nil and command["arg1"] ~= nil then
			local stringCallFunction = tiFile.cleanPath2(command["arg0"])
			local stringTimerName = command["arg1"]
			local callFunction = nil
			-- if (not pcall (function()
							-- callFunction = assert(loadstring(stringCallFunction)) -- assert gives more information that just 'nil' return in case of fail
						-- end
						-- )) then 
				-- callFunction = nil
				-- log_error("tiCommandManager.checkCommand bad command: "..tostring(stringCallFunction))
			-- end
			
			--callFunction = assert(loadstring(stringCallFunction)) -- assert gives more information that just 'nil' return in case of fail

			--callFunction = loadstring(stringCallFunction)

			callFunction = loadstring(stringCallFunction)

			log_debug("tiCommandManager.checkCommand loadstring: function: '"..tostring(stringCallFunction).."' => res = "..tostring(callFunction) .. " with timer " .. tostring(stringTimerName))
			if callFunction ~= nil then
				res1, res2, res3, res3, res4, res5, res6, res7, res8 = callFunction()
			else
				print(err)
				log_error("tiCommandManager.checkCommand bad command: "..tostring(stringCallFunction))
			end
			sendCallback("callEnded", stringTimerName, res1, res2, res3, res3, res4, res5, res6, res7, res8)
		else
			dispatchError(TI_INCONSISTENT_COMMAND, "ti_call, bad argument")
		end
		knowCmd = true
	end

	if not knowCmd and command["CommandName"] == "ti_file" then 
		if command["arg0"] ~= nil then
			local fileName = command["arg0"]
			local runFile = assert(loadfile(fileName))
			log_debug("loadfile fileName: '"..fileName.."' => res = "..tostring(runFile))
			if runFile ~= nil then
				runFile()
			else
				dispatchError(TI_INCONSISTENT_COMMAND, "ti_file, file " .. tostring(fileName).. " not found")
			end
		else
			dispatchError(TI_INCONSISTENT_COMMAND, "ti_file, bad argument")
		end
		knowCmd = true
	end

	----------------------------------------------------------------------------------------------------
	-- Data Record 
	----------------------------------------------------------------------------------------------------
	if not knowCmd and command["CommandName"]=="ti_sendLog" then
		command["CommandName"] = nil
		args = {}
		i = 1
		for k, value in pairsByKeys(command) do
			args[i] = value
			i = i + 1
		end
		if (args[1] ~= nil) then
			tiDataRecorder.log(unpack(args))
		end
		knowCmd = true
	end
			
	return knowCmd
end

function tiCommandManager.parseCommand(command)
	-- First check the common command and then check the specific command, if the common command is not found.
	local knowCmd = false
	tiCommandManager.logCommand(command)
	local commandName = command.CommandName
	
	knowCmd = tiCommandManager.checkCommand (command)
	
	if not knowCmd and (tiCommandManager.mCommonCommand ~= nil) then
		knowCmd = (tiCommandManager.mCommonCommand.checkcommand(command) or knowCmd)
	end
	
	if not knowCmd and (tiCommandManager.mSpecificCommand ~= nil) then
		knowCmd = (tiCommandManager.mSpecificCommand.checkcommand(command) or knowCmd)
	end
	
	if not knowCmd then
		log_error("tiCommandManager Command unknown: " .. tostring(commandName) )
	else
		log_debug("tiCommandManager Command processed: " .. tostring(commandName))
	end
end

-- Petite sequence d'init squateuse ...

local e1, systemName = tiSystemInfo.get("system-name")
local e2, systemVersion = tiSystemInfo.get("system-version")
local e3, deviceModel = tiSystemInfo.get("device-model")
tiDataRecorder.log("trylive_platform", systemName, systemVersion, deviceModel)
tiDataRecorder.log("trylive_version_lua", getVersion())


function tiCommandManager.loopStep()
	-- This loop pull the command and execute it.
	local command = tiCommandManager.getCommand()
	if command ~= nil then
		tiCommandManager.parseCommand(command)
	end
end

-- Separate loop just for the commands.
repeat
	if tiCommandManager.isInit() then
		tiCommandManager.loopStep()
	end
until coroutine.yield()
