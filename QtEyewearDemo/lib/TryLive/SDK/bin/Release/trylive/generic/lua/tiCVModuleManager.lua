log_loaded_info("tiCVModuleManager.lua")

tiCVModuleManager = {
	mTrackerBaseClass = nil,
	mModules = {},
	debugLevel = 5
}

function tiCVModuleManager.createModule (name, moduleClass, listener, configCV)
	if mTrackerBaseClass == nil then
		mTrackerBaseClass = tiFaceCV:create()			
	end
	local properties = {}
	if configCV ~= nil then
		properties.configCV = configCV
	end
	local CVModule = moduleClass:create (properties)
	if CVModule ~= nil then
		CVModule:init()
		CVModule:registerListener(listener)
	end
	tiCVModuleManager.mModules[name] = CVModule
	return CVModule
end

function tiCVModuleManager.getModule (name)
	return tiCVModuleManager.mModules[name]
end

function tiCVModuleManager.getActiveModules()
	local activeModules = {}
	for i, mod in pairs (tiCVModuleManager.mModules) do
		if mod:isActive() then
			activeModules[i] = mod
		end
	end
	return activeModules
end

function tiCVModuleManager.attachCamera (cameraName, moduleName)
	local CVModule = tiCVModuleManager.mModules[moduleName]
	if CVModule ~= nil then 
		CVModule:attachCamera (tiCameraManager.getCurrentCamera(cameraName))
	end
end

function tiCVModuleManager.detachCamera (cameraName, moduleName)
	local CVModule = tiCVModuleManager.mModules[moduleName]
	if CVModule ~= nil then 
		CVModule:detachCamera (tiCameraManager.getCurrentCamera(cameraName))
	end
end

function tiCVModuleManager.getModuleFromCamera (cameraName)
log_debug("tiCVModuleManager.getModuleFromCamera cameraName: "..tostring(cameraName), tiCVModuleManager.debugLevel)
	local foundModule = nil
	for moduleName, mod in pairs(tiCVModuleManager.mModules) do
		local cam = mod:getCamera()
		if cam ~= nil then 
			if cam:getFriendlyName() == cameraName then
				foundModule = mod
			end
		end
	end
log_debug("tiCVModuleManager.getModuleFromCamera foundModule: "..tostring(foundModule), tiCVModuleManager.debugLevel)
	return foundModule
end

function tiCVModuleManager.getPupilDistance()
	return tiFaceCV.currentPD 
end

function tiCVModuleManager.setPupilDistance(val)
	if val == nil then
		val = 62
	end
	tiFaceCV.setPupilDistance(val)
	for moduleName, mod in pairs(tiCVModuleManager.mModules) do
		if moduleName.setPupilDistance ~= nil then
			moduleName:setPupilDistance (val)
		end
	end
end

function tiCVModuleManager.isTracking (cameraName)
	local val = false
	local tracker = tiCVModuleManager.getModuleFromCamera (cameraName)
	if tracker ~= nil then
		val = tracker:isTracking()
	end
	return val
end

function tiCVModuleManager.pauseTracking (cameraName)
	log_debug ("tiCVModuleManager.pauseTracking mode: "..tostring(cameraName))
	local tracker = tiCVModuleManager.getModuleFromCamera (cameraName)
	if tracker ~= nil then
		tracker:pauseAutoTracking()
	end
end
			
function tiCVModuleManager.startTracking (cameraName)
	log_debug ("tiCVModuleManager.startTracking mode: "..tostring(cameraName))
	local tracker = tiCVModuleManager.getModuleFromCamera (cameraName)
	if tracker ~= nil then
		tracker:runAutoTracking()
	end
end
			
function tiCVModuleManager.stopTracking()
	log_debug ("tiCVModuleManager.stopTrackings")
	local i, tracker
	for i, tracker in pairs(tiCVModuleManager.mModules) do
		if tracker ~= nil then
			tracker:stopAutoTracking()
		end
	end
end

function tiCVModuleManager.setRecordInputImagePath (path)
	log_debug ("tiCVModuleManager.setRecordInputImagePath "..tostring(path))
	tiFaceCV.recordInputImagePath = path
end

function tiCVModuleManager.setRecordInputImage (state)
	log_debug ("tiCVModuleManager.setRecordInputImage "..tostring(state))
	tiFaceCV.recordInputImage = state
	local i, tracker
	for i, tracker in pairs(tiCVModuleManager.mModules) do
		if tracker ~= nil then
			tracker:setRecordInputImage(state)
		end
	end
end

function tiCVModuleManager.setRecordInputImageBufferSize (size)
	log_debug ("tiCVModuleManager.setRecordInputImageBufferSize "..tostring(size))
	tiFaceCV.recordInputImageBufferSize = size
end

function tiCVModuleManager.getProcessingStats()
	local res = {}
	for i, tracker in pairs(tiCVModuleManager.mModules) do
		if tracker ~= nil and tracker:isActive() then
			local preprocessing, processing, postprocessing = tracker:getProcessingStats()
			res[i] = {preprocessing = preprocessing, processing = processing, postprocessing = postprocessing}
		end
	end
	return res
end

