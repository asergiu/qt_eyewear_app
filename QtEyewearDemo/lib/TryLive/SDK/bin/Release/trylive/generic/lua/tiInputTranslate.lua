log_loaded_info("tiInputTranslate.lua")

tiInputTranslate = inheritsFrom (tiInputProcessor)

function tiInputTranslate:init (properties)
	log_debug("tiInputTranslate:init "..tostring(self).." properties: "..tostring(properties))
	
	-- Il y a un probleme sur l'heritage en cascade, ça ne marche qu'au premier niveau
	-- Vu Que tiInputTranslate peut etre derivé, les derivé ne profiteront pas de l'init de la classe mere de tiInputTarget d'ou cette sequence:
	tiInputProcessor.init (self, properties)
	
	-- Séquence spécifique
	
	self.mScaleMove = 1.0		-- Scaling of the move, The defualt value 1 corresponds to the same movement than on the screen (touch or mouse)	
	self.mTranslation = Vector3 (0,0,0)
	
	if properties ~= nil then
		self.mRefAxis = properties.refAxis
		self.mRefObject = properties.refObject
	else
		log_warning ("tiInputTranslate:init missing properties")
	end
end		

function tiInputTranslate:printStatus()
	self.baseClass.printStatus (self)
	log_debug ("tiInputTranslate.printStatus mRefAxis: "..tostring(self.mRefAxis), tiInputProcessor.logLevel)
end

function tiInputTranslate:drag (touches)
	local inputData = nil
	local touch = Vector2 (touches[1].position.x, touches[1].position.y)
	log_debug ("tiInputTranslate.drag nb touches: "..#touches.." pos: "..touch:getX()..","..touch:getY(), tiInputProcessor.logLevel)
	if self.mPreviousNumberTouch ~= 1 then
		self:resetFirstTouch()
	end
	self:printStatus()
	if self:isFirstTouchSet() then
		if self.mFirstTouch:distance (touch) > self.mGapEachMoveInPixels then -- if the distance is too high, that means that we're no longer dragging
			self:resetFirstTouch()
		else
			inputData = {}
			inputData.point = Vector2 (self.mFirstTouch)
			inputData.move = Vector2 (touch:getX()-self.mFirstTouch:getX(), touch:getY()-self.mFirstTouch:getY())
			log_debug("tiInputTranslate.drag point: "..inputData.point:getX()..","..inputData.point:getY().." move: "..inputData.move:getX()..","..inputData.move:getY(), tiInputProcessor.logLevel)
			-- log_debug ("tiInputTranslate.drag refAxis: "..self.mRefAxis)
			-- log_debug ("tiInputTranslate.drag move 3D ".. string.format("%.2f ",pos1:getX())..","..string.format("%.2f ",pos1:getY())..","..string.format("%.2f ",pos1:getZ())..
													-- " ->"..string.format("%.2f ",pos2:getX())..","..string.format("%.2f ",pos2:getY())..","..string.format("%.2f ",pos2:getZ())..
													-- " = "..string.format("%.2f ",pos2:getX()-pos1:getX())..","..string.format("%.2f ",pos2:getY()-pos1:getY())..","..string.format("%.2f ",pos2:getZ()-pos1:getZ())..
													-- " scaled: "..string.format("%.2f ",self.mTranslation:getX())..","..string.format("%.2f ",self.mTranslation:getY())..","..string.format("%.2f ",self.mTranslation:getZ()))
			self:setFirstTouch (touch)
		end
	else
		self:setFirstTouch (touch)
	end
	self.mPreviousNumberTouch = #touches
	return inputData
end

function tiInputTranslate:getInputData (touches, viewport)
	log_debug("tiInputTranslate:getInputData touches: "..#touches, tiInputProcessor.logLevel)
	local inputData = nil
	if #touches == 1 then
		
		local wasFirstTouchSaved = self.mIsFirstTouchSaved
		
		inputData = self:drag (touches)	
		
		if not wasFirstTouchSaved and self:isFirstTouchSet() then 
			sendCallback("manualTransformationChanged","started")
		end
	else
		self:resetFirstTouch()
	end
	return inputData
end

function tiInputTranslate:applyInputData (inputData, viewport)
	log_debug("tiInputTranslate:applyInputData", tiInputProcessor.logLevel)
	local point2 = Vector2 (inputData.point:getX()+inputData.move:getX(), inputData.point:getY()+inputData.move:getY())
	local pos1 = Vector3(viewport:projectOn3Dplane (inputData.point, self.mRefObject, self.mRefAxis))
	local pos2 = Vector3(viewport:projectOn3Dplane (point2, self.mRefObject, self.mRefAxis))
	local translation = Vector3((pos2:getX()-pos1:getX())*self.mScaleMove,(pos2:getY()-pos1:getY())*self.mScaleMove,(pos2:getZ()-pos1:getZ())*self.mScaleMove)
	tiAssetManager.setAssetsManualOffsetTranslation (translation)
end

