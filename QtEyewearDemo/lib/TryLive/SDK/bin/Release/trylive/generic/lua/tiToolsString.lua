log_loaded_info("tiToolsString.lua")
-- This file provides somes utils methods for manage the strings

function string.findLast(str, delim)
	last = 0;
	if str ~= nil then
		for i = 1, string.len(str), 1 do
			if str:sub(i, i) == delim then
				last = i
			end
		end
	end
	return last
end

function string.findFirst(str, delim)
	for i = 1, string.len(str), 1 do
		if str:sub(i, i) == delim then
			return i
		end
	end
end

function string.starts(String,Start)
   return string.sub(String,1,string.len(Start))==Start
end

function string.ends(String,End)
   return End=='' or string.sub(String,-string.len(End))==End
end

function string.split(str,delimiter)
  local result = { }
  local from  = 1
  local delim_from, delim_to = string.find( str, delimiter, from  )
  while delim_from do
    table.insert( result, string.sub( str, from , delim_from-1 ) )
    from  = delim_to + 1
    delim_from, delim_to = string.find( str, delimiter, from  )
  end
  table.insert( result, string.sub( str, from  ) )
  return result
end

function string.escape(str)
   return (str:gsub('%%', '%%%%')
            :gsub('^%^', '%%^')
            :gsub('%$$', '%%$')
            :gsub('%(', '%%(')
            :gsub('%)', '%%)')
            :gsub('%.', '%%.')
            :gsub('%[', '%%[')
            :gsub('%]', '%%]')
            :gsub('%*', '%%*')
            :gsub('%+', '%%+')
            :gsub('%-', '%%-')
            :gsub('%?', '%%?'))
end

function round(num, numDecimalPlaces)
	if numDecimalPlaces == nil then
		numDecimalPlaces = 0
	end
	return tonumber(string.format("%." .. (numDecimalPlaces or 0) .. "f", (num+0.5)))
end


