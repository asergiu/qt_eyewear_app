log_loaded_info("tiCamera.lua")

tiCamera = inheritsFrom (tiListenable)

tiCamera.defaultSizeX = 1
tiCamera.defaultSizeY = 1
	-- Currently video live pause doesn't work on Windows (but it works on iOS)
	-- So there is a Lua implementation, but currently device/video orientation is not correctly handled on iOS
	-- So on Windows, use false, and on iOS use true ...@
tiCamera.useVideoCapturePause = true
tiCamera.debugWithVideoFile = false
tiCamera.logLevel = 5

function tiCamera:init(properties)
	log_debug("tiCamera:init "..tostring(self))
	self.mVideoCapture = nil
	self.mVideoTexture = nil
	self.mVideoConfig = nil
	self.mConfig = nil
	self.mInverted = MIRROR_NONE
	self.mPixelFormat = "NONE"
	self.mIsOpened = false
	self.mCameraType = "unknown"
	self.mCamera = Camera(getCurrentScene():createObject(CID_CAMERA))
	self.mCamera:setName("camera")
	if properties ~= nil then
		self.mConfig = properties
		if properties.name ~= nil then
			self.mCamera:setName("Camera "..properties.name)
		end
	end
	self.mCenter = tiSceneGraph.getCameraCenterNode (self.mCamera:getName())
	-- L'heritage des membres ne fonctionne que sur un niveau, donc comme tiCamera va �tre d�riv�e il faut red�finir les membres des sa classe m�re
	self.mListener = {}
end

function tiCamera:isOpened()
	return self.mIsOpened
end

function tiCamera:getName()
	log_debug ("tiCamera:getName", tiCamera.logLevel)
	return self.mCamera:getName()
end

function tiCamera:getFriendlyName()
	log_debug ("tiCamera:getFriendlyName", tiCamera.logLevel)
	return self.mConfig.friendlyName
end

function tiCamera:getCenterNode()
	return self.mCenter
end

function tiCamera:getCamera()
	log_debug ("tiCamera:getCamera "..self:getName(), tiCamera.logLevel)
	return self.mCamera
end

function tiCamera:getVideoTexture()
	log_debug ("tiCamera:getVideoTexture "..self:getName().." "..tostring(self.mVideoTexture), tiCamera.logLevel)
	return self.mVideoTexture
end

function tiCamera:getInverted()
	self:getVideoConfig()
	return self.mInverted
end

function tiCamera:getVideoOrientation()
	log_debug ("tiCamera:getVideoOrientation "..self:getName(), tiCamera.logLevel)
	local bufferRotation = DEVICE_ROTATE_NONE
	local rotationToApply = DEVICE_ROTATE_NONE
	local inverted = MIRROR_NONE
	local ratio = 0
	if self.mVideoCapture ~= nil then
		rotationToApply = self.mVideoCapture:getRotationToApply()
		bufferRotation = self.mVideoCapture:getBufferRotation()				
		inverted = self:getInverted()
		ratio = self:getWidth() / self:getHeight()	
		if ( (rotationToApply == DEVICE_ROTATE_90) or (rotationToApply == DEVICE_ROTATE_270)) then 
		-- if the video buffer is rotated
			ratio = 1.0 / ratio
		end
	end
	log_debug("tiCamera:getVideoOrientation inverted: ".. inverted .. " rotationToApply: " .. rotationToApply .. " bufferRotation: "..bufferRotation.." ratio: "..ratio, tiCamera.logLevel)
	return bufferRotation, rotationToApply, inverted, ratio
end

function tiCamera:getPosition(pos)
	log_debug ("tiCamera:getPosition "..self:getName(), tiCamera.logLevel)
	if self.mCamera ~= nil then
		self.mCamera:getPosition(pos)
	else
		log_warning ("tiCamera:getPosition, camera nulle")
	end
end

function tiCamera:getWidth()
	log_debug ("tiCamera:getWidth", tiCamera.logLevel)
	local val = tiCamera.defaultSizeX
	if self.mVideoTexture ~= nil then
		val = self.mVideoTexture:getWidth()
	end
	return val
end

function tiCamera:getHeight()
	log_debug ("tiCamera:getHeight", tiCamera.logLevel)
	local val = tiCamera.defaultSizeY
	if self.mVideoTexture ~= nil then
		val = self.mVideoTexture:getHeight()
	end
	return val
end

function tiCamera:getVideoSize()
	log_debug ("tiCamera:getVideoSize", tiCamera.logLevel)
	local width = tiCamera.defaultSizeX
	local height = tiCamera.defaultSizeY
	if self.mVideoTexture ~= nil then
		width = self.mVideoTexture:getWidth()
		height = self.mVideoTexture:getHeight()
	end
	return width, height
end

function tiCamera:getVideoRatio()
	local x, y
	if self.mConfig ~= nil then
		if self.mConfig.use ~= nil then
			x = self.mConfig.use.x
			y = self.mConfig.use.y
		else
			if self.mConfig.capture ~= nil then
				x = self.mConfig.capture.x
				y = self.mConfig.capture.y
			else
				log_warning ("tiCamera:getVideoRatio, no information so return the default one")
				x = 640
				y = 480			
			end
		end
	elseif self.mVideoConfig ~= nil then
		x = self.mVideoConfig:getWidth()
		y = self.mVideoConfig:getHeight()
	else
		log_warning ("tiCamera:getVideoRatio, no information so return the default one")
		x = 640
		y = 480
	end
	
	local ratio = x/y
--	local maxx, maxy, fx, fy = self:getCameraModel()
--	log_debug("tiCamera:getVideoRatio "..maxx.."x"..maxy.." : "..maxx/maxy)
	return ratio
end

function tiCamera:getNearClip()
	log_debug ("tiCamera:getNearClip", tiCamera.logLevel)
	return self.mCamera:getNearClip()
end

function tiCamera:getFarClip()
	log_debug ("tiCamera:getFarClip", tiCamera.logLevel)
	return self.mCamera:getFarClip()
end

function tiCamera:getCameraModel()
	log_debug ("tiCamera:getCameraModel", tiCamera.logLevel)
	local cameraModel, 
			cameraSizeX, cameraSizeY, 
			cameraCenterX, cameraCenterY, 
			cameraFocalX, cameraFocalY, 
			cameraA1, cameraA2, cameraP1, cameraP2, 
			cameraDistortion = self.mCamera:getCameraModel()

--A voir si on rajoute la sequence suivante:
					-- if rotationToApply == DEVICE_ROTATE_270 or rotationToApply == DEVICE_ROTATE_90 then
						-- local tmp = cameraSizeX
						-- cameraSizeX = cameraSizeY
						-- cameraSizeY = tmp
					-- end
			
	return cameraSizeX, cameraSizeY, cameraFocalX, cameraFocalY
end

function tiCamera:close()
	log_call ("tiCamera:close "..self:getName())	
--printStack()
	self:fireEvent ("EventVideoClose")
	if self.mVideoCapture ~= nil then
--		self.mVideoCapture:pause()
-- 		[GSE] coroutine.yield necessary, force frame for the feature SwitchCamera
--		coroutine.yield()
		self.mVideoCapture:close()
		getCurrentScene():deleteObject(self.mVideoCapture)
-- Faut il avant detacher la videocapture de la videotexture ?
		self.mVideoCapture = nil
	end		
	if self.mVideoTexture ~= nil then
		gScene:deleteObject (self.mVideoTexture, true)
		self.mVideoTexture = nil
	end
	self.mIsOpened = false
--	self:fireEvent ("EventBackgroundChanged")
	
	log_call_end()	
end

function tiCamera:closeTemporary()
	log_call ("tiCamera:closeTemporary "..self:getName())	
--printStack()
	self:fireEvent ("EventVideoCloseTemporary")
	if self.mVideoCapture ~= nil then
		self.mVideoCapture:close()
		getCurrentScene():deleteObject(self.mVideoCapture)
-- Faut il avant detacher la videocapture de la videotexture ?
		self.mVideoCapture = nil
	end		
	if self.mVideoTexture ~= nil then
		gScene:deleteObject (self.mVideoTexture, true)
		self.mVideoTexture = nil
	end
	self.mIsOpened = false
	
	log_call_end()	
end

function tiCamera:play()
	log_debug ("tiCamera:play "..self:getName().." useVideoCapturePause: "..tostring(tiCamera.useVideoCapturePause))
	if self.mVideoCapture ~= nil then
		if self:isPaused() then
			log_debug ("tiCamera:play video was paused, useVideoCapturePause: "..tostring(tiCamera.useVideoCapturePause))
			if tiCamera.useVideoCapturePause then
				log_debug ("tiCamera:play just self.mVideoCapture:play()")
				self.mVideoCapture:play()
			else
				if self.mVideoTexture ~= nil then
					log_debug ("tiCamera:play delete video texture")
					gScene:deleteObject (self.mVideoTexture, true)
				end
				log_debug ("tiCamera:play create video texture")
				self.mVideoTexture = VideoTexture (getCurrentScene():createObject(CID_VIDEOTEXTURE))
				self.mVideoTexture:setName("videotexture "..self:getName())
				self.mVideoTexture:setVideoCapture(self.mVideoCapture)			
				self.mVideoCapture:play()
				self:fireEvent ("EventBackgroundChanged")
			end
			self.mPaused = false
			self:fireEvent ("EventVideoPlay")
		else
			log_debug ("tiCamera:play nothing to do, camera is not paused")
		end
	end
end

function tiCamera:pause()
	log_debug ("tiCamera:pause "..self:getName())
--printStack()	
	if self.mVideoCapture ~= nil then
		log_debug ("tiCamera:pause "..self:getName().." frame: "..self.mVideoCapture:getFrameIndex().." useVideoCapturePause: "..tostring(tiCamera.useVideoCapturePause))
		self:fireEvent ("EventVideoPause")
		if tiCamera.useVideoCapturePause then
			self.mVideoCapture:pause()
		else
			local sizeX, sizeY = self:getVideoSize()
			local image = Image (sizeX, sizeY, 4)
if not tiCamera.debugWithVideoFile then			
-- Mise en commentaire temporaire durant les tests sur des video car �a crash sur le format RVB ...			
			image:getFromCapture(self.mVideoCapture)
			image:flip(0)
end			
			if self.mVideoTexture ~= nil then
				gScene:deleteObject (self.mVideoTexture, true)
			end
			self.mVideoTexture = Texture(gScene:createObject(CID_TEXTURE))
			self.mVideoTexture:setWidth (sizeX)
			self.mVideoTexture:setHeight (sizeY)
			self.mVideoTexture:load()
		
if not tiCamera.debugWithVideoFile then				
-- Mise en commentaire temporaire durant les tests sur des video car �a crash sur le format RVB ...
log_debug("tiCamera:pause image:copyToTexture")
			image:copyToTexture(self.mVideoTexture)
			self.mVideoTexture:load()
end
			self:fireEvent ("EventBackgroundChanged")
		end
		self.mPaused = true
		log_debug("tiCamera:pause paused: "..tostring(self:isPaused()))
	end
end

function tiCamera:isPaused()
--	log_debug ("tiCamera:isPaused "..self:getName())
	res = true
	if tiCamera.useVideoCapturePause then
		if self.mVideoCapture ~= nil then
			self.mPaused = self.mVideoCapture:isPaused()
			res = self.mPaused
		end
	else
		res = self.mPaused
	end
	return res
end

function getCheckedPoint (x0, y0, rotationToApply, inverted)
	local x = x0
	local y = y0
	if rotationToApply == DEVICE_ROTATE_270 then
		x = y0
		y = 1-x0
	end
--				if (inverted == MIRROR_VERTICAL) or (inverted == MIRROR_HV) then
		x = 1-x
--				end
--[[ Pour l'instant n'arrive que sur des video sous windowsmac et dans ce cas les points sont deja bons ... va comprendre ...
	if (inverted == MIRROR_HORIZONTAL) or (inverted == MIRROR_HV) then
		y = 1-y
	end
]]--
--		log_debug ("checkPoint ["..i.."] = " .. x0 .. " " .. y0 .. " -> " .. points[i*2+1] .. " " ..points[i*2+2])
	return x, y
end

function tiCamera:projectOnFrontPlane (x2, y2, z3)
	log_debug ("tiCamera:projectOnFrontPlane", tiCamera.logLevel)
	local x3, y3
	local bufferRotation, rotationToApply, inverted, ratio = self:getVideoOrientation()
	local cameraSizeX, cameraSizeY, cameraFocalX, cameraFocalY = self:getCameraModel()
	if rotationToApply == DEVICE_ROTATE_270 or rotationToApply == DEVICE_ROTATE_90 then
		local tmp = cameraSizeX
		cameraSizeX = cameraSizeY
		cameraSizeY = tmp
	end
	local xn, yn = getCheckedPoint(x2, y2, rotationToApply, inverted)
	local x3 = (2*xn-1)*cameraSizeX/2/cameraFocalX*z3
	local y3 = (2*yn-1)*cameraSizeY/2/cameraFocalY*z3
	return x3, y3
end

function tiCamera:getType()
	return self.mCameraType
end


