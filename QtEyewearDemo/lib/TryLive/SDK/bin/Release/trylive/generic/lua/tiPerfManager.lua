log_loaded_info("tiPerfManager.lua")

tiPerfManager = {
	lastDisplayStatsTime = 0,
	displayStatsPeriod = 1,
	formatFPS = "%5.1f",
	formatTime = "%5d",
	displayOn = false,
	textPerfRendering = nil,
	textPerfVideo = nil,
	textPerfCV = nil,
	initialized = false,
	debugLevel = 5
}

local function addText (yPos)
	local size = 30
	local resX, resY = getScreenResolution()
	local text2D = Text2D (getCurrentScene():createObject(CID_TEXT2D))
	text2D:setCharHeight (size/resY) 
--	text2D:setSpaceWidth (10/resX) 
	text2D:setFontName ("TI/Quality/Font")
	text2D:setAlignment (TI_TA_LEFT)
	text2D:setColour(1,1,0)
	text2D:setMetricsMode (TI_TPM_RELATIVE)
	text2D:setLeft (0)
	text2D:setTop (1-(size*(yPos+1))/resY)
--	text2D:setVisibilityMask(tiViewportManager.getVisibilityMask (viewportId))
	return text2D
end

function tiPerfManager.init()
	if not tiPerfManager.initialized then
		tiPerfManager.textPerfRendering = addText(2)
		tiPerfManager.textPerfVideo = addText(1)
		tiPerfManager.textPerfCV = addText(0)
		tiPerfManager.initialized = true
	end
end

function tiPerfManager.displayStats()
	tiPerfManager.init()
	if tiPerfManager.displayOn then
		local currentTime = os.clock()
		if currentTime-tiPerfManager.lastDisplayStatsTime > tiPerfManager.displayStatsPeriod then
			-- Rendering
			local renderingFPS, batches, triangles = tiWindowManager.getRenderTarget():getStatistics()
			local renderingString = "Render: "..string.format(tiPerfManager.formatFPS, renderingFPS).." FPS "..string.format(tiPerfManager.formatTime, 1/renderingFPS*1000).." ms (meshs: "..string.format("%d", batches)..", tris: "..string.format("%d", triangles)..")"
			-- Video capture
			local videoFPS = tiCameraManager.getVideoFrameRate()
			local videoString = "Video: "
			for i, c in pairs (videoFPS) do
				videoString = videoString..i..string.format(tiPerfManager.formatFPS, c).." FPS "
			end
			-- CV
			local CVstats = tiCVModuleManager.getProcessingStats()
			local CVstring = "CV: "
			for i, c in pairs (CVstats) do
				CVstring = CVstring..i.." "..string.format(tiPerfManager.formatTime, c.preprocessing*1000).." + "..string.format(tiPerfManager.formatTime, c.processing*1000).." + "..string.format(tiPerfManager.formatTime, c.postprocessing*1000).." = "..string.format(tiPerfManager.formatTime, (c.preprocessing+c.processing+c.postprocessing)*1000).." ms"
			end
			-- Display
			log_debug ("Perfs: "..renderingString..", "..videoString..", "..CVstring)
			tiPerfManager.textPerfRendering:setText (renderingString)
			tiPerfManager.textPerfVideo:setText (videoString)
			tiPerfManager.textPerfCV:setText (CVstring)
			
			tiPerfManager.lastDisplayStatsTime = currentTime
		end
	end
	tiPerfManager.textPerfRendering:setVisible (tiPerfManager.displayOn)
	tiPerfManager.textPerfVideo:setVisible (tiPerfManager.displayOn)
	tiPerfManager.textPerfCV:setVisible (tiPerfManager.displayOn)
end 