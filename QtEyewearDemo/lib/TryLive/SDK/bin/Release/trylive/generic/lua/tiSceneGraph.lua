log_loaded_info("tiSceneGraph.lua")

gScene = getCurrentScene()

tiSceneGraph = 
{
	NbViewport = 5,
	printLevel = 0,
	printOn = false
}

function tiSceneGraph.initialize (nbViewport)
	local base = Object3D (gScene:getObjectByName("object3d_visibility"))
	tiSceneGraph.NbViewport = nbViewport
	for v = 1, nbViewport do
--log_debug("vp node: ".."object3d_viewport_"..tostring(v))
		local node = Object3D(gScene:createObject(CID_OBJECT3D))
		node:setName("object3d_viewport_"..tostring(v))
		base:addChild(node)
	end
end


function tiSceneGraph.getNodeList (func, numViewport)
	local nodeList = {}
	if numViewport == 0 or numViewport == nil then
		for i = 1, tiSceneGraph.NbViewport do
			nodeList[i] = func (i)
		end
	else
		nodeList = {func (numViewport)}
	end
	return nodeList
end

-- Function temporaire tant que le noeud n'a pas été éclaté en n viewports

function tiSceneGraph.getNodeListTemp (func, numViewport)
	return {func (numViewport)}
end

function tiSceneGraph.getGhost(numViewport)
	return Object3D(gScene:getObjectByName("ghost"))
end

------------------------------------------------
-- Basic functions to access to a specific node

function tiSceneGraph.getViewportNodeName (numViewport)
	return "object3d_viewport_"..numViewport
end

function tiSceneGraph.getCameraCenterNodeName (cameraName)
	return "camera_center"
end

function tiSceneGraph.getCameraCenterNode (cameraName)
	log_debug ("tiSceneGraph.getCameraCenterNode " ..tostring(cameraName))
	return Object3D (gScene:getObjectByName(tiSceneGraph.getCameraCenterNodeName (cameraName)))
end

function tiSceneGraph.getViewportNode (numViewport)
	log_debug ("tiSceneGraph.getViewportNode " ..tostring(numViewport))
	return Object3D (gScene:getObjectByName(tiSceneGraph.getViewportNodeName (numViewport)))
end

function tiSceneGraph.getFaceMainPivotNode (numViewport)
	return Object3D (gScene:getObjectByName("object3d_face_main_pivot"))
end

function tiSceneGraph.getPupilPivotNode (numViewport)
	return Object3D (gScene:getObjectByName("target_label_pupil_pivot"))
end

function tiSceneGraph.getPupilLeftNode (numViewport)
	return Object3D (gScene:getObjectByName("targetPupilLeft"))
end

function tiSceneGraph.getPupilRightNode (numViewport)
	return Object3D (gScene:getObjectByName("targetPupilRight"))
end

function tiSceneGraph.getTopLevelNode()
	return Object3D (gScene:getObjectByName("translationCamera"))
end

------------------------------------------------
-- Functions returning a list of nodes
-- If no viewport specified, returns an array of nodes
-- else, returns an array of one node

function tiSceneGraph.getViewportNodeList (numViewport)
	return tiSceneGraph.getNodeList (tiSceneGraph.getViewportNode, numViewport)
end

function tiSceneGraph.getFaceMainPivotNodeList (numViewport)
	return tiSceneGraph.getNodeList (tiSceneGraph.getFaceMainPivotNode, numViewport)
end

------------------------------------------------
-- Debug functions
		
function tiSceneGraph.printNode(node)
	local displayTransform = true
	local tab = string.rep ("    ", tiSceneGraph.printLevel+1)
	local name = ""
	if (pcall (function()
					node:getName()
				end
				)) then
		name = tiFile.baseName(node:getName())
	else
		name = "NULL"
	end
	local childrenCount = ""
	local isVisible = ""
	local isInheritedVisible = ""
	if (pcall (function()
					node:getChildrenCount()
				end
				)) then
		childrenCount = tostring(node:getChildrenCount())
	else
		childrenCount = "?"
	end
	if (pcall (function()
					node:getVisible()
				end
				)) then
		isVisible = tostring(node:getVisible())
	else
		isVisible = "?"
	end
	if (pcall (function()
					node:getVisibilityMask()
				end
				)) then
		visibilityMask = tostring(node:getVisibilityMask())
	else
		visibilityMask = "?"
	end
	if (pcall (function()
					node:getInheritedVisible ()
				end
				)) then
		isInheritedVisible = tostring(node:getInheritedVisible ())
	else
		isInheritedVisible = "?"
	end
		if(node ~= nil) then
		log_debug_simple (tab .. " " .. name .. " " ..  " children: " .. childrenCount .. " visible: " .. isVisible .. " " .. " visibility mask: " .. visibilityMask .. " inherited visible: " .. isInheritedVisible)
		if displayTransform then
			local form = "%5.1f"
			local vp = Vector3()
			local vt = Vector3()
			node:getPosition (vp, node:getParent())
			node:getPosition (vt, tiSceneGraph.getFaceMainPivotNode(1))
			log_debug_simple (tab .. "     position /pere: " .. string.format(form,vp:getX()) .. " " .. string.format(form,vp:getY()) .. " " .. string.format(form,vp:getZ()) .. " /tracking: " .. string.format(form,vt:getX()) .. " " .. string.format(form,vt:getY()) .. " " .. string.format(form,vt:getZ()) .. " ")					
--						form = "%5.1f"
			local oxp, oyp, ozp = node:getOrientationEuler(node:getParent()) 
			local oxt, oyt, ozt = node:getOrientationEuler(tiSceneGraph.getFaceMainPivotNode(1))
			log_debug_simple (tab .. "     orientat /pere: " .. string.format(form,oxp) .. " " .. string.format(form,oyp) .. " " .. string.format(form,ozp) .. " /tracking: " .. string.format(form,oxt) .. " " .. string.format(form,oyt) .. " " .. string.format(form,ozt) .. " ")					
		end
	else
		log_debug_simple (tab .. " " .. "NULL")
	end
	tiSceneGraph.printLevel = tiSceneGraph.printLevel+1
	if(node ~= nil) then
		if (childrenCount ~="?") then 
			for i=0, node:getChildrenCount()-1 do
				tiSceneGraph.printNode(node:getChild(i))
			end
		end
	end
	tiSceneGraph.printLevel = tiSceneGraph.printLevel-1
	
end

function tiSceneGraph.print(title)
	if tiSceneGraph.printOn then
		if title == nil then
			title = ""
		end
log_debug("tiSceneGraph.print ".. title)
		tiSceneGraph.printLevel = 0
		tiSceneGraph.printNode (gScene)
	end
end

-- Quelques tools Object3D a foutre ailleurs plus tart

function createFleche3D (size, mater)
	local longueurTige = size*0.8
	local diametreTige = 2
	local longeurCone = size*0.2
	local diametreCone = 4
	local tige = ManualEntity(Object3D(gScene:createObject(CID_MANUALENTITY)))
	tige:setupCylinder  (longueurTige, diametreTige/2, diametreTige/2, 5, mater, TI_ME_CENTER_MIN_Y)
	local pointe = ManualEntity(Object3D(gScene:createObject(CID_MANUALENTITY)))
	pointe:setupCone (longeurCone, diametreCone, diametreCone, 10, mater, TI_ME_CENTER_MIN_Y) 
	tige:addChild(pointe)
	pointe:translate (0, longueurTige, 0)
	return tige
end

function createObjectRepere3D(size)
	local objRepere = Object3D(gScene:createObject(CID_OBJECT3D))
	local c = createFleche3D (size, "RepereY")
	c:setName("Repere")
	objRepere:addChild(c)
	c = createFleche3D (size, "RepereZ")
	objRepere:addChild(c)
	c:rotate(90,0,0)
	c = createFleche3D (size, "RepereX")
	objRepere:addChild(c)
	c:rotate(0,0,-90)
	return objRepere
end

function deleteObject (object)
	if object ~= nil then
		local children = {}
		for i = 0, object:getChildrenCount()-1 do
			table.insert (children, object:getChild(i))
		end
		for i, o in pairs(children) do
			deleteObject (o)
		end
		gScene:deleteObject(object)
	end
end

function computeBoundingBoxRec (node, minVec, maxVec)
	local minp = Vector3()
	local maxp = Vector3()
--	log_debug("computeBoundingBoxRec node: "..node:getName().." type: "..tostring(node:getClassID()))
	if node:getClassID() == CID_ENTITY then
--	log_debug("computeBoundingBoxRec mesh")
		node = Entity(node)
		node:getWorldBoundingBox (minp, maxp)
		if minp:getX() < minVec:getX() then minVec:setX(minp:getX()) end
		if minp:getY() < minVec:getY() then minVec:setY(minp:getY()) end
		if minp:getZ() < minVec:getZ() then minVec:setZ(minp:getZ()) end
		if maxp:getX() > maxVec:getX() then maxVec:setX(maxp:getX()) end
		if maxp:getY() > maxVec:getY() then maxVec:setY(maxp:getY()) end
		if maxp:getZ() > maxVec:getZ() then maxVec:setZ(maxp:getZ()) end
	else
		minp:set(minVec)
		maxp:set(maxVec)
		for i = 0, node:getChildrenCount()-1 do
			local child = node:getChild(i)	
			computeBoundingBoxRec (child, minp, maxp)
			if minp:getX() < minVec:getX() then minVec:setX(minp:getX()) end
			if minp:getY() < minVec:getY() then minVec:setY(minp:getY()) end
			if minp:getZ() < minVec:getZ() then minVec:setZ(minp:getZ()) end
			if maxp:getX() > maxVec:getX() then maxVec:setX(maxp:getX()) end
			if maxp:getY() > maxVec:getY() then maxVec:setY(maxp:getY()) end
			if maxp:getZ() > maxVec:getZ() then maxVec:setZ(maxp:getZ()) end
		end
	end
--	log_debug("computeBoundingBoxRec node: "..node:getName().." min: "..string.format(form,minVec:getX())..","..string.format(form,minVec:getY())..","..string.format(form,minVec:getZ()).." max: "..string.format(form,maxVec:getX())..","..string.format(form,maxVec:getY())..","..string.format(form,maxVec:getZ()))
end

function computeBoundingBox (node, minVec, maxVec)
--log_debug("computeBoundingBox node: "..node:getName())
	local MaxVal = 100000
	local MinVal = -MaxVal
	minVec:set(Vector3(MaxVal,MaxVal,MaxVal))
	maxVec:set(Vector3(MinVal,MinVal,MinVal))
	computeBoundingBoxRec (node, minVec, maxVec)
end

-- WARNING: Works only on one mesh, the last one
function findMeshNode (startNode)
	local i
--log_debug ("findMeshNode")	
	-- Check that the asset is a lense
	-- Find the mesh node
	local mesh = nil
	for i = 0, startNode:getChildrenCount()-1 do
		node = startNode:getChild(i)
		name = node:getName()
--log_debug("findMeshNode fils " .. tostring (i) .. " name:" .. name .. " searching for a mesh")
		-- Criteria: the name ends with ".mesh"
		-- not very robust, we will have to improve that but i don't know how to identify an entity in the children
		if string.ends (name, ".mesh") then
			mesh = Entity(node)
		end
	end
	return mesh
end



