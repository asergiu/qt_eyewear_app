log_loaded_info("tiTarget.lua")

tiTarget = {
}

tiTarget.logLevel = 6
tiTarget.debug = false
tiTarget.size = 0.2

function tiTarget.getSizeReference()
--	local sx, sy = RenderWindow(getCurrentScene():getMainView()):getMetrics()
	local sx, sy = getScreenResolution()
	return sx, sy
end

function tiTarget:create (properties)
	log_debug("tiTarget:create")
    local new_inst = {}    -- the new instance
    setmetatable( new_inst, { __index = tiTarget } ) -- all instances share the same metatable
--	self:init (properties)
	new_inst:init (properties)
    return new_inst
end

function tiTarget:init (properties)
	log_debug("tiTarget:init "..tostring(self))
	log_object(properties, "tiTarget:init properties")
--printStack()
	self.mNodeName = properties.nodeName -- Object3D in scenegraph defined in dpd 
	self.mOverlayName = properties.overlay

	self.mParentNode = properties.parentNode
	if properties.size ~= nil then
		self.mSize = properties.size
	else
		self.mSize = tiTarget.size
	end
	-- L'offset est le centre de la zone sensible de l'image (par rapport à son centre)
	-- Si on a une target parfaitement symétrique et centrée, l'offset sera de 0
	
	self.mOffsetX = properties.offsetX 
	self.mOffsetY = properties.offsetY
	
	local windowWidth, windowHeight = tiTarget.getSizeReference()
	self.mSizePix = windowWidth*self.mSize
	-- Visiblement le repere window est -1 1
	-- C'est le rayon de la zone sensible de l'image autour de son centre de sensibilité défini par l'offset
	-- Devrait être défini dans les properties car depend de l'image
	self.mRadius = self.mSizePix / 2 / 2 
log_debug ("tiTarget:init mSize: "..self.mSize.." Window: "..windowWidth..","..windowHeight.." mOverlayTargetOffset: "..self:getOverlayTargetOffsetX()..","..self:getOverlayTargetOffsetY().." mRadius: "..self.mRadius) 
	self.mTranslation = Vector2()
	
	self.mEnable = false
end

function tiTarget:getName()
  return self.mNodeName
end

function tiTarget:getOverlayName()
  return self.mOverlayName
end

function tiTarget:isEnable()
	return self.mEnable
end

function tiTarget:enable (state)
	self.mEnable = state
	self:setVisible (state)
end

function tiTarget:getOverlayTargetOffsetX()
  return self.mOffsetX*self.mSizePix
end

function tiTarget:getOverlayTargetOffsetY()
  return self.mOffsetY*self.mSizePix
end

function tiTarget:getParentNode()
  return Object3D(getCurrentScene():getObjectByName(self.mParentNode)) 
end

function tiTarget:getOverlay()
	local overlayNode = Overlay(getCurrentScene():getObjectByName(self.mOverlayName))
	if overlayNode:isNull() then
		log_warning ("Overlay "..tostring(self.mOverlayName).." not found")
	end
	return overlayNode
end

function tiTarget:getNode()
	log_debug("tiTarget:getNode " .. tostring(self.mNodeName), tiTarget.logLevel)
	return Object3D(getCurrentScene():getObjectByName(self.mNodeName))
end

-- Get position of overlay in screen coordinates on target point of overlay
function tiTarget:getPosition2D(viewport)
	
	local overlay = self:getOverlay()
	local err, x, y = overlay:getScroll() 
--printStack()
--log_object("tiTarget:getPosition2D vp: ",viewport)
	local width, height = viewport:getActualSize()

	x = ((x+1)/2)*width 
	y = height-(((y+1)/2)*height) 

	return Vector2(x, y)	
end

-- Get position of overlay in screen coordinates on drag center of overlay
function tiTarget:getPositionDragPoint (viewport)
--log_object("tiTarget:getPositionDragPoint vp: ",viewport)
--printStack()
	local vec =  self:getPosition2D (viewport)
	return Vector2 (vec:getX()+self:getOverlayTargetOffsetX(), vec:getY()+self:getOverlayTargetOffsetY())
end
	
function tiTarget:move2D (move)
log_debug("tiTarget:move2D "..move:getX()..","..move:getY(), tiTarget.logLevel)
	self:getOverlay():scrollPixel (move:getX(), move:getY())
end

-- Get position in 3D world coordinates
function tiTarget:getPosition3D()
	local eye = self:getNode()
	local eyeVec = Vector3()
	eye:getPosition(eyeVec)
	return eyeVec
end
		
-- Set position of overlay using 3D target position
function tiTarget:updateOverlayFrom3DTarget(viewport)
	if tiTarget.debug then
		local o = self:getNode()
		if o:getChildrenCount() == 0 then
			local c = ManualEntity(Object3D(gScene:createObject(CID_MANUALENTITY)))
			local size = 10
--			c:setupBox(size,size,size,"FacePoint")
			c:setupSphere (size/2, 10, 5, "FacePoint")  
			o:addChild(c)
		end
	end
			
	local x, y = viewport:getScreenPosition(self:getPosition3D()) -- Values already from from  [-1,1] 
--	log_debug("tiTarget:updateOverlayFrom3DTarget "..self.mOverlayName.." 3D: "..self.getPosition3D():getX()..","..self.getPosition3D():getY()..","..self.getPosition3D():getZ().." 2D: "..x..","..y)	
	local overlay = self:getOverlay()
	local err, xPrev, yPrev = overlay:getScroll() 
	overlay:scroll(-1.0*xPrev,-1.0*yPrev) -- reset previous scroll
	overlay:scroll(x, y) -- move overlay to x, y in this viewport
end
		
-- Scale target label
function tiTarget:updateTargetScale(scaleX, scaleY)
	local overlay = self:getOverlay()			
	overlay:setScale(scaleX, scaleY)
end

function tiTarget:setVisible (state)
log_debug("tiTarget:setVisible "..tostring(state))
	local overlay = self:getOverlay()
	overlay:setVisible (state)
end
		
function tiTarget:update(viewport)
log_debug("tiTarget:update "..tostring(viewport), tiTarget.logLevel)
--printStack()
	if viewport ~= nil then
		local viewportWidth, viewportHeight = viewport:getActualSize()
log_debug("tiTarget:update vpsize: "..viewportWidth.." "..viewportHeight, tiTarget.logLevel)
	local windowWidth, windowHeight = tiTarget.getSizeReference()
log_debug("tiTarget:update wsize: "..windowWidth.." "..windowHeight, tiTarget.logLevel)
	
		local scaleX = self.mSize*windowWidth/viewportWidth
		local scaleY = scaleX*(viewportWidth/viewportHeight)
log_debug("tiTarget:update scaleXY: "..scaleX.." "..scaleY, tiTarget.logLevel)

		self:updateTargetScale(scaleX, scaleY) 
		self:updateOverlayFrom3DTarget(viewport)
	end
end

function tiTarget:getRadius()
--	log_debug("tiTarget:getRadius "..self.mRadius)
	return self.mRadius
end
		
function tiTarget:isInside (viewport, position)
	local vec =  self:getPosition2D (viewport)
--	local inside = (position:distance (Vector2 (vec:getX()+self:getOverlayTargetOffsetX(), vec:getY()+self:getOverlayTargetOffsetY())) < self.mRadius)
	local inside = math.max (math.abs (vec:getX()+self:getOverlayTargetOffsetX()-position:getX()), math.abs (vec:getY()+self:getOverlayTargetOffsetY()-position:getY())) < self.mRadius
--	log_debug("tiTarget:isInside "..self:getName().." pos: "..x..","..y.." target: "..string.format("%d",vec:getX())..","..string.format("%d",vec:getY()).." o: "..self:getOverlayTargetOffsetX()..","..self:getOverlayTargetOffsetY().." r: "..self.mRadius.." res: "..tostring(inside))
	return inside
end

function tiTarget:setScale (scale)
	self.mSize = scale
	local windowWidth, windowHeight = tiTarget.getSizeReference()
	self.mSizePix = windowWidth*self.mSize
	self.mRadius = self.mSizePix / 2 / 2
end
