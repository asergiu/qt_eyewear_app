log_loaded_info("tiToolsFile.lua")

local mLocalDataPath = getUserAppDataDirectory()-- "Acep/TryLive/Data/"

function setAppDataPath (path)
	log_call("setAppDataPath " .. tostring(path))			
	if path ~= nil then
		local size = string.len(path)
		local lastCar = string.sub(path, size, size)
						
		-- For avoid problem, add a / at the end of the local path if it's not exist.
		if(size ~= 0 and lastCar ~= "/" and lastCar ~= "\\") then
			path = path .. "/"
		end
		
		mLocalDataPath = tiFile.cleanPath (path)
	end
	log_info("App data path = " .. mLocalDataPath)
	log_call_end()						
end

function getAppDataPath (fileName)
	local res = ""
--log_debug("getAppDataPath " .. tostring(fileName))

	if fileName ~= nil then
		-- Si le nom de fichier contient deja l'app data path, on le renvoie telquel
		if not string.starts (fileName, mLocalDataPath) then
			res = mLocalDataPath .. fileName
		else
			res = fileName
		end
	else
		res = mLocalDataPath
	end
--log_debug("getAppDataPath res2: "..res)		

	return res
end

function getAppConfigPath()
	local path = getAppDataPath().."config/"
	if not File.check (path) then
		File.mkdir (path)
	end
	return path
end
 
function getUserPhotoPath (path)
	return getUserImageDirectory().."/"..path
end


-- version = "undefined"
-- local versionFileName = "./VersionTryLive.txt"
-- local versionFile = assert(loadfile(versionFileName))
-- if versionFile ~= nil then
	-- versionFile()
-- end
-- if tiFile.exists (versionFileName) then
	-- local f = assert(io.open(versionFileName, "r"))
    -- version = f:read("*all")
    -- f:close()
-- else
	-- log_warning ("Version file "..versionFileName.." does not exist")
-- end
-- printStack()
-- log_info ("TryLive SDK Lua files version: "..version)

function setXMLtag1 (fileNameIn, fileNameOut, tagsToUpdate)
	log_debug ("setXMLtag fileNameIn: "..tostring(fileNameIn).." fileNameOut: "..tostring(fileNameOut))
	if tiFile.exists (fileNameIn) then
		local fileOut = assert(io.open(fileNameOut, "w"))
		if fileOut ~= nil then
			for line in io.lines(fileNameIn) do
				for tag, val in pairs (tagsToUpdate) do
					if (line:find ("<"..tag..">") ~= nil) and (line:find ("</"..tag..">") ~= nil)then
						line = "<"..tag..">"..tostring(val).."</"..tag..">"
					end
				end
				fileOut:write (line.."\n")
			end
			fileOut:close()
		else
			log_error ("setXMLtag impossible to write file "..tostring(fileNameOut))
		end
	else
		log_error ("setXMLtag file "..tostring(fileNameIn).." not found")
	end
end
local charNewLine = '\n'
local charCarriageReturn = '\r'	
local endLine = charNewLine

function setXMLtag (fileNameIn, fileNameOut, tagsToUpdate)
	log_debug ("setXMLtag fileNameIn: "..tostring(fileNameIn).." fileNameOut: "..tostring(fileNameOut))
	if tiFile.exists (fileNameIn) then
		local bufferOut = ""
		local bufferIn = File.read(fileNameIn)
		for l, line in pairs (string.split (bufferIn, endLine)) do
--log_debug("setXMLtag line: "..tostring(line))
			for tag, val in pairs (tagsToUpdate) do
				if (line:find ("<"..tag..">") ~= nil) and (line:find ("</"..tag..">") ~= nil)then
					line = "<"..tag..">"..tostring(val).."</"..tag..">"
				end
			end
			bufferOut =  bufferOut .. line .. endLine
		end
		if File.write (fileNameOut, bufferOut) ~= eOk then
--			log_error ("setXMLtag impossible to write file "..tostring(fileNameOut))
		end
	else
		log_error ("setXMLtag file "..tostring(fileNameIn).." not found")
	end
end

function getXMLtag1 (fileNameIn, tagsToUpdate)
	local res = eFailed
	log_debug ("getXMLtag fileNameIn: "..tostring(fileNameIn))
	if tiFile.exists (fileNameIn) then
		for line in io.lines(fileNameIn) do
			for tag, val in pairs (tagsToUpdate) do
				if (line:find ("<"..tag..">") ~= nil) and (line:find ("</"..tag..">") ~= nil) then
					vol = line:gsub("<"..tag..">", ""):gsub("</"..tag..">", ""):gsub("^%s+", ""):gsub("%s+$", "")
					tagsToUpdate[tag] = vol
--					log_debug("getXMLtag Tag: "..tag.." val: "..vol..".")
				end
			end
		end
		res = eOK
	else
		log_error ("getXMLtag file "..tostring(fileNameIn).." not found")
	end
	return res
end

function getXMLtag (fileNameIn, tagsToUpdate)
	local res = eFailed
	log_debug ("getXMLtag fileNameIn: "..tostring(fileNameIn))
	if tiFile.exists (fileNameIn) then
		local bufferIn = File.read (fileNameIn)
		for l, line in pairs (string.split (bufferIn, endLine)) do
--log_debug("getXMLtag line: "..tostring(line))
			for tag, val in pairs (tagsToUpdate) do
				if (line:find ("<"..tag..">") ~= nil) and (line:find ("</"..tag..">") ~= nil) then
					vol = line:gsub("<"..tag..">", ""):gsub("</"..tag..">", ""):gsub("^%s+", ""):gsub("%s+$", "")
					tagsToUpdate[tag] = vol
--log_debug("getXMLtag Tag: "..tag.." val: "..vol..".")
				end
			end
		end
		res = eOK
	else
		log_error ("getXMLtag file "..tostring(fileNameIn).." not found")
	end
	return res
end


tiFile = {}

function tiFile.copy2 (srcName, dstName)
	log_debug ("tiFile.copy2 "..srcName.." "..dstName)
    local inp = assert(io.open(srcName, "rb"))
	if inp ~= nil then
		local out = assert(io.open(dstName, "wb"))
		if out ~= nil then
			local data = inp:read("*all")
			out:write(data)
			assert(out:close())
		else
			log_error ("tiFile.copy impossible to write file "..tostring(dstName))
		end		
		assert(inp:close())
	else
		log_error ("tiFile.copy file "..tostring(srcName).." not found")
	end
 end

function tiFile.copy (srcName, dstName)
	log_debug ("tiFile.copy "..srcName.." "..dstName)
	File.copy (srcName, dstName)
end
  
-------------------------------------------------------------------------------
-- Check if the file in parameter exists. 
-- Warning! This test (io.open) is really instable, sometimes it works very well
-- but sometimes not! Don't really know why, it seems to be a difference between
-- targets (android, ios, studio...)
-------------------------------------------------------------------------------
function tiFile.exists1(name)
	local res = false
	
	if name ~= nil then
		local f, message, errno = io.open (name,"r")
--log_debug("io.open : "..tostring(f).." "..tostring(message).." "..tostring(errno))
		if f ~= nil then 
			io.close (f)
			res = true
		end
	end
	
	return res
end

function tiFile.exists(name)
	return File.check(name)
end

function tiFile.makeDirectory (path)
	File.mkdir (path)
end

function tiFile.makeTree (path)
--log_debug ("tiFile.makeTree "..tostring(path))
	local parent = tiFile.dirName(path)
--log_debug ("tiFile.makeTree check "..parent)
	if not File.check (parent) then
		tiFile.makeTree (parent)
	end
	File.mkdir (path)
end

function tiFile.getCurrentDirectory()
	if getOSType() >= TI_OS_WINSEVEN then
		cmd = "cd"
     else
		cmd = "pwd"
	end
	local res = io.popen(cmd):read'*all'
	local currentDir = res:sub(1, res:len()-1)

	return currentDir
end

function tiFile.isAbsolutePath (path)
	local res = true
	if not string.starts (path, "/") then
		if not string.starts (path, "\\") then
			res = false
		end
	end
end

function tiFile.dirName(str)
	local dirname = ""
	local lastDelim = math.max(string.findLast( str, "/"), string.findLast( str, "\\"))
	if lastDelim >= 0 then
		if lastDelim < string.len(str) then
			dirname = string.sub(str, 1, lastDelim)
		else
			dirname = tiFile.dirName(string.sub(str, 1, lastDelim-1))
		end
	end
	return dirname
end

function tiFile.baseNameWithoutExtension(str)
	return string.sub(str, math.max(string.findLast( str, "/"), string.findLast( str, "\\"))+1 , string.findLast( str, ".") - 1)
end

function tiFile.baseName(str)
	local basename = nil
	if str ~= nil then
		local lastDelim = math.max(string.findLast (str, "/"), string.findLast (str, "\\"))
		if lastDelim == string.len(str) then
			basename = tiFile.baseName(string.sub(str, 1 , lastDelim-1))
		else
			basename = string.sub(str, lastDelim+1 , string.len(str))
		end
	end
	return basename
end

function tiFile.getExtension(str)
	local point = string.findLast( str, ".")
	if point == 0 then return "" end
	return string.sub(str, point + 1, str:len())
end

function tiFile.removeHttp(str)
	local res = str
	if string.find(str, "https://") then
		res = string.sub(str, 9, string.len(str))		
	elseif string.find(str, "http://") then
		res = string.sub(str, 8, string.len(str))	
	end
	return res
end

function tiFile.cleanPath (path)
	return string.gsub (path, "\\", "/"):gsub("//","/"):gsub("/%./","/")

end

function tiFile.cleanPath2 (path)
	return string.gsub (path, "\\", "/"):gsub("/%./","/")

end

function tiFile.writeText (fileNameOut, text)
	log_debug ("tiFile.writeText fileNameOut: "..tostring(fileNameOut))
	local fileOut = assert(io.open(fileNameOut, "w"))
	if fileOut ~= nil then
		fileOut:write (text)
		fileOut:close()
	else
		log_error ("tiFile.writeText impossible to write file "..tostring(fileNameOut))
	end
end

-- Renvoie:
-- nil si le répertoire n'existe pas
-- Sinon une table des noms de fichiers
function tiFile.dir (path)
	local files = nil
	local filesTmp = nil
	if tiFile.exists (path) then
		local cmd = 'dir /b "'..path..'"'
-- log_debug(cmd)
		local str = io.popen(cmd):read'*all'
		filesTmp = str:split("\n")
	end
	-- On vire les lignes vides ou ne contenant que des spaces
	files = {}
	-- log_debug("tiFile.dir "..tostring(path))
	-- log_object(filesTmp)
	if filesTmp ~= nil then
		for i, file in pairs(filesTmp) do
			local s = string.gsub (file, " ", "")
			if string.len(s) > 0 then
				table.insert (files, file)
			end
		end	
	end
	return files
end

local tableLevelJSON = 0

function getIndentTableJSON()
	indent = ""
	for i=0,tableLevelJSON do
			indent = indent .. "    "
	end
	return indent
end

function json.encode (JSONtable)
	local JSONString = ""
	if JSONtable == nil then
--		log_debug (getIndentTableJSON() .. "Table " .. title .. " nil")
	else
		if type(JSONtable) == "table" then
			JSONString = JSONString .. "{\n"
			local size = 0
			for key, value in pairs(JSONtable) do
				size = size+1
			end
			local i = 0
			for key, value in pairs(JSONtable) do
				i = i+1
				JSONString = JSONString .. getIndentTableJSON() .. "\"" .. key .. "\" : "
				if type(value) == "table" then
					tableLevelJSON = tableLevelJSON+1
					JSONString = JSONString .. json.encode (value)
					tableLevelJSON = tableLevelJSON-1
				else
log_debug ("json.encode  key: "..key.." value: "..tostring (value).. " type: "..type(value))					
					if type(value) == 'string' then
						JSONString = JSONString .. "\"" .. tostring(value) .. "\""
					else
						JSONString = JSONString .. tostring(value)
					end
				end
				if i < size then
					JSONString = JSONString  .. ","
				end
				JSONString = JSONString  .. "\n"
			end
			JSONString = JSONString .. getIndentTableJSON() .. "}"
		else
--			JSONString = JSONString .. tostring(JSONtable) .. " : "
			JSONString = JSONString .. json.encode (getAllData (getmetatable(JSONtable)))
		end
	end
	return JSONString
end

