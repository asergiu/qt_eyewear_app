log_loaded_info("tiInputTarget.lua")

tiInputTarget = inheritsFrom (tiInputProcessor)

function tiInputTarget:init (properties)
	log_debug("tiInputTarget:init "..tostring(self).." properties: "..tostring(properties))

	-- Il y a un probleme sur l'heritage en cascade, ça ne marche qu'au premier niveau
	-- Vu Que tiInputTarget va etre derivé, les derivé ne profiteront pas de l'init de la classe mere de tiInputTarget d'ou cette sequence:
	tiInputProcessor.init (self, properties)
	
	self.mTargets = {} -- array of targets	
end

function tiInputTarget:setScale (scale)
	for i, target in pairs (self.mTargets) do
		target:setScale (scale)
	end
end

function tiInputTarget:enable (state)
	log_debug("tiInputTarget:enable "..tostring(state))
	if state == nil then
		state = not self:isEnable()
	end
--			log_debug ("tiInputProcessorManager.enable mode: "..tostring(mode).." state: "..tostring(state))			
	self.mEnable = state
	log_debug ("tiInputTarget.enable: "..tostring(self.mEnable))	
	for i, target in pairs (self.mTargets) do
		target:enable (self.mEnable)
	end
end

function tiInputTarget:updateTargets (viewport)
log_debug("tiInputTarget:updateTargets vp: "..tostring(viewport), tiInputProcessor.logLevel)
	tiSceneGraph.getTopLevelNode():forceUpdate(true)
	for i, target in pairs (self.mTargets) do 
		target:update(viewport)
	end
	
end

function tiInputTarget:findTargetInside (viewport, position)
	local target = nil
	log_debug("tiInputTarget:findTargetInside "..tostring(position:getX()).." "..tostring(position:getY()), tiInputProcessor.logLevel)
	for i, t in pairs(self.mTargets) do
--		log_debug("tiInputTarget.findTargetInside "..tostring(t).." "..tostring(i), tiInputProcessor.logLevel)
		if t:isEnable() and t:isInside (viewport, position) then
			target = t
--			log_debug("tiInputTarget.findTargetInside found "..tostring(t), tiInputProcessor.logLevel)
		end
	end
	return target
end

function tiInputTarget:dragTarget (touches, viewport)
	local inputData = nil
	local left, top = viewport:getPosition()
	local touch = Vector2 (touches[1].position.x-left, touches[1].position.y-top)
	log_debug ("tiInputTarget:dragTarget nb touches: "..#touches.." pos: "..touch:getX()..","..touch:getY(), tiInputProcessor.logLevel)
	
	local clickOnTargetZone = false

	local target = self:findTargetInside (viewport, touch)
	if target ~= nil then
		clickOnTargetZone = true
	else
		self:resetFirstTouch() -- Abort gesture
	end
--log_debug("tiInputTarget.dragTarget mIsFirstTouchSaved: "..tostring(self:isFirstTouchSet()), tiInputProcessor.logLevel)				
	if self.mPreviousNumberTouch ~= 1 then
		self:resetFirstTouch()
	end
self:printStatus()	
	-- If drag has started in target zone, measure current position and add delta to previous
	if self:isFirstTouchSet() then

		-- if the distance is too high, that means that we're no longer dragging
		if self.mFirstTouch:distance (touch) > self.mGapEachMoveInPixels then 
			self:resetFirstTouch()
		else
			inputData = {}
			inputData.target = target
			inputData.move = Vector2 (touch:getX()-self.mFirstTouch:getX(), -(touch:getY()-self.mFirstTouch:getY()))
			self:setFirstTouch (touch)
		end
	else
		-- Start drag if inside target zone
		if clickOnTargetZone then 
			self:setFirstTouch(touch)
		end
	end
	self.mPreviousNumberTouch = #touches
	return inputData
end

function tiInputTarget:getInputData (touches, viewport)
-- Ce traitement pourrait peut être être généralisé à tous les input processor et être déplacé dans la classe mère
	log_debug ("tiInputTarget:getInputData nb touches: "..#touches, tiInputProcessor.logLevel)
	local inputData = nil
	local moveOccured = false
	if #touches == 1 then
		
		local wasFirstTouchSaved = self:isFirstTouchSet();
		
		inputData = self:dragTarget (touches, viewport)
		
		if not wasFirstTouchSaved and self:isFirstTouchSet() then 
			sendCallback("manualTransformationChanged","started")
		end
	else
		self:resetFirstTouch()
	end
	return inputData
end

function tiInputTarget:applyInputData (inputData, viewport)
	log_debug("tiInputTarget:applyInputData", tiInputProcessor.logLevel)
end


