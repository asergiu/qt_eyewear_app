log_loaded_info("tiViewportManager.lua")
-- This class manages viewports for AR view without the top view for TryLive Home.

tiViewportManager = {
	mListeners = {},
	mViewport = {},			-- Tableau de tous les viewports existants 
	mViewportById = {},		-- Tableau des viewports de la vue courante indéxé par leur Id
	mCurrentView = -1,
	mSelectedViewportId = 1,
	logLevel = 6,
	mView = {}
}

function tiViewportManager.setupViewports (keepOpened)
	log_call("tiViewportManager.setupViewports keepOpened: "..tostring(keepOpened))
	if keepOpened == nil then
		keepOpened = true
	end
	local viewportModeConfiguration = tiViewportManager.mView[tiViewportManager.getViewMode()]
--log_object(viewportModeConfiguration,"tiViewportManager.setupViewports")
	-- This function move, scale and place all the viewports.
	-- Get the number of viewport and place it at the right place.
	
	-- remember all the camera already openned
	local cameraToClose = {}
	local i, cam
	for i, cam in pairs (tiCameraManager.getCurrentCameras()) do
		if cam:isOpened() then
			cameraToClose[tostring(cam)] = cam
			log_debug("tiViewportManager.setupViewports cam to close begin: "..cam:getName().." ad: "..tostring(cam))
		end
	end
	
-- log_debug("tiViewportManager.setupViewports cameraToClose DEBUT <<<<<") 
-- for k, c in pairs (cameraToClose) do log_debug("    "..k.." "..tostring(c).." "..c:getName()) end
-- log_debug("tiViewportManager.setupViewports cameraToClose >>>>>")

	-- Check all the camera that will be still used in this configuration to avoid to close them
	for i=1, tiViewportManager.getViewportCount (viewportModeConfiguration) do
		vp = tiViewportManager.mViewport[i]
		if vp ~= nil then
			local cam = tiCameraManager.getCurrentCamera (viewportModeConfiguration[i].camera)
			if cameraToClose[tostring(cam)] ~= nil then
				cameraToClose[tostring(cam)] = nil
			end
		end
	end
	
-- log_debug("tiViewportManager.setupViewports cameraToClose apres fermerture DEBUT <<<<<") 
-- for k, c in pairs (cameraToClose) do log_debug("    "..k.." "..tostring(c).." "..c:getName()) end
-- log_debug("tiViewportManager.setupViewports cameraToClose apres fermerture >>>>>")

	-- Close camera no more needed
	for i, cam in pairs (cameraToClose) do
		log_debug("tiViewportManager.setupViewports cam to close end: "..cam:getName().." ad: "..tostring(cam))
		if keepOpened then
			cam:pause()
		else
			cam:close()
		end
	end

	-- Disable all the viewports.
	for i, vp in pairs(tiViewportManager.mViewport) do
		vp:enable(false)
		tiCameraManager.removeListener(vp)
	end
	tiViewportManager.mViewportById = {}
		 
	for i=1, tiViewportManager.getViewportCount (viewportModeConfiguration) do
		vp = tiViewportManager.mViewport[i]
		if vp ~= nil then
log_object(configViewport,"tiViewportManager.setupViewports viewport "..i.. " id: "..tostring(viewportModeConfiguration[i].id))
			vp:setup (viewportModeConfiguration[i], keepOpened)
			tiViewportManager.mViewportById[vp.mId] = vp
		end
	end

	if tiViewportManager.mViewportById[tiViewportManager.mSelectedViewportId] == nil then
		tiViewportManager.mSelectedViewportId = tiViewportManager.mViewport[1].mId
	end
	tiViewportManager.selectViewportById (tiViewportManager.mSelectedViewportId)
	
tiCameraManager.printCameras()				
	log_call_end()			
end
					
function tiViewportManager.setScale (cameraFriendlyName, val, center)
	log_call("tiViewportManager.setScale cameraFriendlyName: "..tostring(cameraFriendlyName).." scale:"..tostring(val).." centered: "..tostring(center))

	for i, vp in pairs(tiViewportManager.mViewport) do
		local cam = vp:getCamera()
		if cam ~= nil then
			if cam:getFriendlyName() == cameraFriendlyName then
				vp:setScale (val, center)
			end
		end
	end
	tiViewportManager.setupViewports(true)
--	tiViewportManager.initContent()
	log_call_end()			
end
			
function tiViewportManager.setViewportScale (viewportId, scale)
	log_call("tiViewportManager.setViewportScale viewportId: "..tostring(viewportId).." scale:"..scale)
	if viewportId == nil then
		viewportId = tiViewportManager.mSelectedViewportId
	end
	local vp = tiViewportManager.mViewportById[viewportId]
	if vp ~= nil then
		vp:setScale (scale)
	else
		log_warning ("tiViewportManager.setViewportScale viewportId: "..tostring(viewportId).." not found")
	end
	tiViewportManager.setupViewports(true)
	log_call_end()			
end

function tiViewportManager.setViewportCenterLocked (viewportId, status)
	log_call("tiViewportManager.setViewportCenterLocked viewportId: "..tostring(viewportId).." status: "..tostring(status))
	if viewportId == nil then
		viewportId = tiViewportManager.mSelectedViewportId
	end
	local vp = tiViewportManager.mViewportById[viewportId]
	if vp ~= nil then
		vp:setCenterLocked (status)
		tiViewportManager.setupViewports(true)
	else
		log_warning ("tiViewportManager.setViewportCenterLocked viewportId: "..tostring(viewportId).." not found")
	end
	log_call_end()			
end

function tiViewportManager.refresh()
	local refreshNeeded = false
	for id, vp in pairs (tiViewportManager.mViewportById) do
		if vp:getCenterLocked() then
--			refreshNeeded = true
			vp:reshape()
		end
	end	
--	if refreshNeeded then
	-- pourrait être largement simplifié car seulement utile pour un viewport et simplememnt pour son positionnement ...
--		tiViewportManager.setupViewports(true)
--	end
end

function tiViewportManager.getViewportToProcess (viewportId)
	local listViewport = {}
log_debug("tiViewportManager.getViewportToProcess viewportId: "..tostring(viewportId))	
	if viewportId == nil then
		listViewport = tiViewportManager.mViewportById
	elseif tiViewportManager.mViewportById[viewportId] ~= nil then
		listViewport = {tiViewportManager.mViewportById[viewportId]}
	end
--log_object(listViewport, " tiViewportManager.getViewportToProcess")
	return listViewport
end
			
function tiViewportManager.setBackgroundColor (r, g, b, a, viewportId)
	log_debug ("tiViewportManager.setBackgroundColor "..tostring(r).." "..tostring(g).." "..tostring(b).." "..tostring(a).." vp: "..tostring(viewportId)) 
	for i, vp in pairs(tiViewportManager.getViewportToProcess(viewportId)) do
		log_debug ("tiViewportManager.setBackgroundColor vp: "..vp:getName())
		vp:setBackgroundColor (r, g, b, a)
		log_debug ("tiViewportManager.setBackgroundColor res: "..vp:toString())
	end				
end

-- Listeners
function tiViewportManager.registerListener (listener)
	table.insert (tiViewportManager.mListeners, listener)
end
			
-- x, y en coordonnées normalisées dans la fenetre
function tiViewportManager.selectViewportByPos (x, y)		
--	log_debug ("tiViewportManager.selectViewportByPos " .. x .. "," .. y)
	-- Manage the selection of the viewports by a click (or touch)
	for id, vp in pairs (tiViewportManager.mViewportById) do
		if vp:isIn (x, y) then
			tiViewportManager.selectViewportById(id)
			return		
		end
	end
end
			
-- Select an object in the scene graph
function tiViewportManager.selectObject (x, y)
	return tiViewportManager.mViewportById[tiViewportManager.mSelectedViewportId]:pick(x, y)
end
			
function tiViewportManager.getVisibilityMask (viewportId)
	local mask = -1
	local vp = tiViewportManager.mViewportById[viewportId]
	if vp ~= nil then
		mask = vp:getVisibilityMask()
	end
	return mask
end

function tiViewportManager.getViewportIdByVisibilityMask (mask)
	local viewportId = nil
	for id, vp in pairs (tiViewportManager.mViewportById) do
		if vp:getVisibilityMask() == mask then
			viewportId = id
		end
	end
	return viewportId
end
			
function tiViewportManager.getViewportMaxCount()
	return #(tiViewportManager.mViewport)
end

function tiViewportManager.getSelectedViewportId()
	return tiViewportManager.mSelectedViewportId
end
					
function tiViewportManager.getCurrentViewport()
	log_debug ("tiViewportManager.getCurrentViewport SelectedViewportId: "..tostring(tiViewportManager.getSelectedViewportId()).." "..tostring(tiViewportManager.mViewportById[tiViewportManager.getSelectedViewportId()]), tiViewportManager.logLevel)

	return tiViewportManager.mViewportById[tiViewportManager.getSelectedViewportId()]
end
			
function tiViewportManager.getViewport (viewportId)  
	return tiViewportManager.mViewportById[viewportId]
end

function tiViewportManager.getCurrentViewportPosition()
	local viewport = tiViewportManager.mViewportById[tiViewportManager.getSelectedViewportId()]
	return viewport:getPosition()
end
			
function tiViewportManager.setSelectedViewportId (viewportId)
	log_debug ("tiViewportManager.setSelectedViewportId " .. viewportId, tiViewportManager.logLevel)
	local res = false
	if tiViewportManager.mViewportById[viewportId] ~= nil then
		tiViewportManager.mSelectedViewportId = viewportId
		res = true
	else
		log_warning("tiViewportManager.getSelectedViewportId id not found: "..viewportId)
	end
	return res
	--tiViewportManager.printViewports()
end

function tiViewportManager.selectViewportById (viewportId)
--	log_debug ("tiViewportManager.selectViewportById " .. viewportId)
	if tiViewportManager.setSelectedViewportId (viewportId) then	
		local vp = tiViewportManager.mViewportById[viewportId]

		for key, listener in pairs(tiViewportManager.mListeners) do
			listener.onViewportSelection (viewportId, vp:getDimensionNative())
		end
	end
end

function tiViewportManager.getViewMode()
	return tiViewportManager.mCurrentView
end

function tiViewportManager.show()
	tiViewportManager.setupViewports(true)
end
			
function tiViewportManager.initContent()
	log_call("tiViewportManager.initContent")
	for i, vp in pairs(tiViewportManager.mViewport) do
		vp:initContent()
	end
	tiViewportManager.setupViewports(true)
	log_call_end()			
end

function tiViewportManager.hide()
	for i, vp in pairs(tiViewportManager.mViewport) do
		vp:enable(false)
	end
end
			
-- utilisé que par tryon photo ...
function tiViewportManager.setCamera(camera)
	log_call ("tiViewportManager.setCamera " .. tostring(camera))
	log_debug ("################# ATENTION peut foutre la merde #################")
	for i, vp in pairs(tiViewportManager.mViewport) do
		vp:setCamera(camera)
	end
	log_call_end()			
end
			
function tiViewportManager.getViewportCount (ViewConfig)
	local res = 0
	if ViewConfig ~= nil then
		res = #ViewConfig
	end
	return res
end

function tiViewportManager.getViewportCountInMode (mode)
	local res = 0
	if mode ~= nil then
		local ViewConfig = tiViewportManager.mView[mode]
		res = tiViewportManager.getViewportCount (ViewConfig)
	end
	return res
end

function tiViewportManager.getViewportCountInCurrentMode()
	return tiViewportManager.getViewportCountInMode (tiViewportManager.getViewMode())
end
			
function tiViewportManager.getViewportListInCurrentMode()
	local viewportList = {}
	local viewportModeConfiguration = tiViewportManager.mView[tiViewportManager.getViewMode()]
	for i=1, tiViewportManager.getViewportCount (viewportModeConfiguration) do
		table.insert (viewportList, tiViewportManager.mViewport[i])
	end
	return viewportList
end

function tiViewportManager.printViewports()
	log_debug("Viewport mode: "..tostring(tiViewportManager.mCurrentView).." selected: "..tiViewportManager.mSelectedViewportId)
	log_debug("Viewport per index")
	for i, vp in pairs(tiViewportManager.mViewport) do
		log_viewport (vp:getViewport(), "mode "..tiViewportManager.mCurrentView.." vp["..i.."] mId: ".. vp.mId)
	end
	log_debug("Viewport per Id")
	for id, vp in pairs (tiViewportManager.mViewportById) do
		log_viewport (vp:getViewport(), " vpid["..id.."]")
	end

end
			
function tiViewportManager.getViewports()
	local viewports = {}
	for i, vp in pairs(tiViewportManager.mViewport) do
		table.insert (viewports, vp:getViewport())
	end
	return viewports
end

function tiViewportManager.getViewportIdList()
	local viewportId = {}
	for i, vp in pairs(tiViewportManager.mViewportById) do
		table.insert (viewportId, i)
	end
	return viewportId
end

function tiViewportManager.setViewMode (mode, selectedViewport, keepOpened)
	local res = eOk
	log_call ("tiViewportManager.setViewMode " .. mode)

	keepOpened = false	-- Patch pour le pb de relacement du tracking sur ios ...
	
--	printStack()
--log_debug("type of mode: "..type(mode))
--log_object (tiViewportManager.mView,"tiViewportManager.setViewMode mView")	
	local view = tiViewportManager.mView[mode]
	if view == nil then
		log_warning ("tiViewportManager.setViewMode, bad viewport mode requested: "..mode)
		res = eFailed
	else
		tiViewportManager.mCurrentView = mode
		if tiViewportManager.mViewportById[selectedViewport] == nil then
			selectedViewport = tiViewportManager.mViewport[1].mId
		end
		tiViewportManager.mSelectedViewportId = selectedViewport
		tiViewportManager.setupViewports(keepOpened)
		tiViewportManager.printViewports()
	end
	log_call_end()	
	return res
end

function tiViewportManager.getViewportNode (viewportId)
--log_debug("tiViewportManager.getViewportNode '"..viewportId.."'"..type(viewportId))
--tiViewportManager.printViewports()
	local node = nil
	local vp = tiViewportManager.mViewportById[viewportId]
	if vp ~= nil then
--log_viewport(vp:getViewport(), " VP trouve")
		node = tiSceneGraph.getViewportNode(vp.mIndex)
	end
	return node
end

function tiViewportManager.addView (ViewName, ViewConfig)
	log_debug("tiViewportManager.addView "..tostring(ViewName))	
	log_object(ViewConfig, "ViewName")
	tiViewportManager.mView[tostring(ViewName)] = ViewConfig
end

function tiViewportManager.getModes()
	local modes = {}
	for mode, config in pairs(tiViewportManager.mView) do
		table.insert (modes, mode)
	end
	return modes
end

function tiViewportManager.initialize (nbViewport)
	log_call("tiViewportManager.initialize nbViewport: "..tostring(nbViewport))
	if nbViewport == nil then
		nbViewport = 5
	end
	local maxViewportCount = 0
	for i, config in pairs(tiViewportManager.mView) do
		if #config > maxViewportCount then
			maxViewportCount = #config
		end
	end
	maxViewportCount = nbViewport
	for i = 1, maxViewportCount do
		local vp = tiViewport:create()
		vp:initialize (i)
		tiViewportManager.mViewport[i] = vp
	end
	tiViewportManager.printViewports()
	
	log_call_end()			
end




			