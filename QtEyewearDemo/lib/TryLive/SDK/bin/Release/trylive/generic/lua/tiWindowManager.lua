log_loaded_info("tiWindowManager.lua")
-- This file manage the Screenshot.
-- The take part is still used and functionnal.

tiWindowManager = {
	ScreenshotState = {inactive = 0, screenshotRequested = 2, screenshotReady = 4},
	mScreenshotPath = nil,
	mOffscreen = false
}

tiWindowManager.mScreenshotState = tiWindowManager.ScreenshotState.inactive

----------------------------------------------------------------------------------
--	Private functions
----------------------------------------------------------------------------------

function tiWindowManager._updateFromScreenshotRequested()
	local renderWindow = tiWindowManager.getRenderTarget()
	log_debug("tiWindowManager._updateFromScreenshotRequested before dumpNextFrame renderWindow: "..tostring(renderWindow).." path: "..tostring(tiWindowManager.mScreenshotPath))
	renderWindow:dumpNextFrame (tiWindowManager.mScreenshotPath)
	tiWindowManager.mScreenshotState = tiWindowManager.ScreenshotState.screenshotReady
end

function tiWindowManager.takeScreenshotSynchrone (filename)
startChrono("tiWindowManager.takeScreenshotSynchrone")
--	tiWindowManager.mScreenshotState = tiWindowManager.ScreenshotState.screenshotRequested
	local renderWindow = tiWindowManager.getRenderTarget()
	log_debug("tiWindowManager.takeScreenshotSynchrone before dumpNextFrame renderWindow: "..tostring(renderWindow).." path: "..filename)
	coroutine.yield()
startChrono("tiWindowManager.takeScreenshotSynchrone dumpNextFrame")
	renderWindow:dumpNextFrame (filename)
stopChrono("tiWindowManager.takeScreenshotSynchrone dumpNextFrame")
--	tiWindowManager.mScreenshotState = tiWindowManager.ScreenshotState.inactive
stopChrono("tiWindowManager.takeScreenshotSynchrone")
	sendCallback ("takeScreenshotEnded", filename)
end

function tiWindowManager._updateFromScreenshotReady()
	tiDataRecorder.log ("trylive_feature_begin", "snapshot_AR")
	sendCallback ("takeScreenshotEnded", tiWindowManager.mScreenshotPath)
	tiDataRecorder.log ("trylive_feature_end", "snapshot_AR")
	tiWindowManager.mScreenshotState = tiWindowManager.ScreenshotState.inactive
end
			
function tiWindowManager.takeScreenshot (screenshotPath)
	if screenshotPath ~= nil then
		tiWindowManager.mScreenshotPath = screenshotPath
	else
		tiWindowManager.mScreenshotPath = getUserPhotoPath ("screenshot.jpg")
	end
	-- Don't allow a take photo during a other take photo.
	if (not tiWindowManager.isScreenshotRequested()) then
		tiWindowManager.mScreenshotState = tiWindowManager.ScreenshotState.screenshotRequested
	else
		dispatchError (TI_INTERNAL_ERROR, "screenshot already requested.")
	end
end
			
function tiWindowManager.isScreenshotRequested()
	return tiWindowManager.mScreenshotRequested
end
		
function tiWindowManager.loop()
	if (tiWindowManager.mScreenshotState == tiWindowManager.ScreenshotState.screenshotRequested) then
		tiWindowManager._updateFromScreenshotRequested()
	elseif (tiWindowManager.mScreenshotState == tiWindowManager.ScreenshotState.screenshotReady) then
		tiWindowManager._updateFromScreenshotReady()
	end
end
		

function saveScreenshot(filename)
	local name = getUserPhotoPath (filename)
	local renderWindow = RenderWindow(getCurrentScene():getMainView())
	local res = renderWindow:dumpNextFrame(name)
log_debug("saveScreenshot "..name.." res="..res)
end


local currentWindowWidth = -1
local currentWindowHeight = -1

function tiWindowManager.getRenderTargetSize()
	local windowWidth, windowHeight = tiWindowManager._getRenderTargetSize()
	if windowWidth == 0 or  windowHeight == 0 then
		log_warning ("Bad window size: "..tostring(windowWidth).."x"..tostring(windowHeight)..", use previous one: "..tostring(currentWindowWidth).."x"..tostring(currentWindowHeight))
		windowWidth = currentWindowWidth
		windowHeight = currentWindowHeight
	else
		currentWindowWidth = windowWidth
		currentWindowHeight = windowHeight		
	end
	return windowWidth, windowHeight
end

function tiWindowManager._getRenderTargetSize()
	local width  = 0
	local height = 0
	local target = tiWindowManager.getRenderTarget()
	if target then width, height = target:getMetrics() end
	return width, height
end



local window = {}
window.target   = gScene:getMainView()
window.viewport = gScene:getMainViewport()

local texture = {}
texture.target   = nil
texture.viewport = nil

function tiWindowManager.setOffscreen(name, width, height)
	log_debug ("tiWindowManager.setOffscreen : " .. name .. ", " .. width .. "x" .. height)
	if tiWindowManager.mOffscreen then error("mOffscreen already set") end
	tiWindowManager.mOffscreen = true
	window.viewport:enable(false)
	window.target:setAutoUpdate(false)

	texture.target = RenderTexture(gScene:createObject(CID_RENDERTEXTURE))
	texture.target:setName(name)
	texture.target:setFSAA(4)
	texture.target:setWidth(width)
	texture.target:setHeight(height)
	texture.target:setFormat(PF_R8G8B8A8)
	texture.target:load()

	tiViewportManager.initialize()
f = texture.target:getFormat()
log_debug ("tiWindowManager.setOffscreen format: "..tostring(f))
	-- sendCallback("mOffscreen mode", name, width, height);
end

function tiWindowManager.getRenderTarget()
	if tiWindowManager.mOffscreen then return texture.target
	else              return window.target
	end
end




