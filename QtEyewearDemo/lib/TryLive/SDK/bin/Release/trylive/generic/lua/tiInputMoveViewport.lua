log_loaded_info("tiInputMoveViewport.lua")

tiInputMoveViewport = inheritsFrom (tiInputProcessor)
--tiInputProcessor.logLevel =0
function tiInputMoveViewport:init (properties)
	log_debug("tiInputMoveViewport:init "..tostring(self).." properties: "..tostring(properties))
	
	self.mScaleMove = 1.0		-- Scaling of the move, The defualt value 1 corresponds to the same movement than on the screen (touch or mouse)	
	self.mTranslation = Vector3 (0,0,0)
	
	if properties ~= nil then
		self.mRefAxis = properties.refAxis
		self.mRefObject = properties.refObject
	else
		log_warning ("tiInputMoveViewport:init missing properties")
	end
end		

function tiInputMoveViewport:printStatus()
	self.baseClass.printStatus (self)
end

function tiInputMoveViewport:drag (touches)
	local inputData = nil
	local touch = Vector2 (touches[1].position.x, touches[1].position.y)
	log_debug ("tiInputMoveViewport.drag nb touches: "..#touches.." pos: "..touch:getX()..","..touch:getY(), tiInputProcessor.logLevel)
	if self.mPreviousNumberTouch ~= 1 then
		self:resetFirstTouch()
	end
	self:printStatus()
	if self:isFirstTouchSet() then
		if self.mFirstTouch:distance (touch) > self.mGapEachMoveInPixels then -- if the distance is too high, that means that we're no longer dragging
			self:resetFirstTouch()
		else
			inputData = {}
			inputData.mScale = 1
			inputData.mMove = Vector2 (touch:getX()-self.mFirstTouch:getX(), touch:getY()-self.mFirstTouch:getY())
			log_debug("tiInputMoveViewport.drag mMove: "..inputData.mMove:getX()..","..inputData.mMove:getY(), tiInputProcessor.logLevel)
			self:setFirstTouch (touch)
		end
	else
		self:setFirstTouch (touch)
	end
	self.mPreviousNumberTouch = #touches
	return inputData
end

function tiInputMoveViewport:scale (touches)
	local inputData = nil
	local touch1 = Vector2 (touches[1].position.x, touches[1].position.y)
	local touch2 = Vector2 (touches[2].position.x, touches[2].position.y)
	log_debug ("tiInputMoveViewport:scale nb touches: "..#touches.." pos: "..touch1:getX()..","..touch1:getY(), tiInputProcessor.logLevel)
	self:printStatus()
	if self:isFirstTouchSet() and self:isSecondTouchSet() then
		if (self.mFirstTouch:distance (touch1) > self.mGapEachMoveInPixels) or (self.mSecondTouch:distance (touch2) > self.mGapEachMoveInPixels) then -- if the distance is too high, that means that we're no longer dragging
			self:resetFirstTouch()
			self:resetSecondTouch()
		else
			inputData = {}
			local previousDist = self.mSecondTouch:distance (self.mFirstTouch)
			local dist = touch2:distance (touch1)
			log_debug("tiInputMoveViewport:scale previousDist: "..previousDist.." dist: "..dist, tiInputProcessor.logLevel)
			if previousDist ~= 0 and dist ~= 0 then
				inputData.mScale = dist/previousDist	
			else
				inputData.mScale = 1
			end
			inputData.mMove = Vector2 (touch1:getX()-touch2:getX()-(self.mFirstTouch:getX()-self.mSecondTouch:getX()), touch1:getY()-touch2:getY()-(self.mFirstTouch:getY()-self.mSecondTouch:getY()))
			log_debug("tiInputMoveViewport:scale previousTouch: "..self.mFirstTouch:getX()..","..self.mFirstTouch:getY().." "..self.mSecondTouch:getX()..","..self.mSecondTouch:getY(), tiInputProcessor.logLevel)
			log_debug("tiInputMoveViewport:scale currentTouch:  "..touch1:getX()..","..touch1:getY().." "..touch2:getX()..","..touch2:getY(), tiInputProcessor.logLevel)
			log_debug("tiInputMoveViewport:scale mScale: "..inputData.mScale.." mMove: "..inputData.mMove:getX()..","..inputData.mMove:getY(), tiInputProcessor.logLevel)
			self:setFirstTouch (touch1)
			self:setSecondTouch (touch2)
		end
	else
		self:setFirstTouch (touch1)
		self:setSecondTouch (touch2)
	end
	self.mPreviousNumberTouch = #touches
	return inputData
end

function tiInputMoveViewport:getInputData (touches, viewport)
	log_debug("tiInputMoveViewport:getInputData touches: "..#touches, tiInputProcessor.logLevel)
	local inputData = nil

	if #touches ~= 2 then
		self:resetSecondTouch()
	end
	if #touches == 1 then
		
		local wasFirstTouchSaved = self.mIsFirstTouchSaved
		
		inputData = self:drag (touches)	
		
		if not wasFirstTouchSaved and self:isFirstTouchSet() then 
			sendCallback("manualTransformationChanged","started")
		end
	elseif #touches == 2 then
		local wasSecondTouchSaved = self.mIsSecondTouchSaved
		
		inputData = self:scale (touches)
		
		if not wasSecondTouchSaved and self:isSecondTouchSet() then 
			sendCallback("manualTransformationChanged","started")
		end
	else
		self:resetFirstTouch()
	end
	return inputData
end

function tiInputMoveViewport:applyInputData (inputData, viewport)
	log_debug("tiInputMoveViewport:applyInputData vp: "..tostring(viewport), tiInputProcessor.logLevel)
	log_debug("tiInputMoveViewport:applyInputData mScale: "..inputData.mScale.." move: "..inputData.mMove:getX()..","..inputData.mMove:getY(), tiInputProcessor.logLevel)
	if viewport ~= nil then
		local sx, sy = viewport:getActualSize()
--		log_debug("tiInputMoveViewport:applyInputData vp actualsize: "..sx..","..sy)
		sx, sy = viewport:getSize()
--		log_debug("tiInputMoveViewport:applyInputData vp size: "..sx..","..sy)
		viewport:moveCenter (Vector2 (-inputData.mMove:getX()/sx*2, inputData.mMove:getY()/sy*2))
		if inputData.mScale ~= 1 then
			local currentScale = viewport:getScale()
			log_debug ("tiInputMoveViewport:applyInputData currentScale: "..currentScale.." mScale: "..inputData.mScale, tiInputProcessor.logLevel)
			viewport:setScale (currentScale*(1-(1-inputData.mScale)/2))
		end
		viewport:reshape()
	end
end

