-- tiCommonInputManager.lua script
--[[
This class is an abstracion layer of input devices which can be touchscreen or mouse
]] --
log_loaded_info("tiInputDevice.lua")

tiInputDevice = {
	mInit = false,
	mTouchscreen = nil,
	mMouse = nil,
	mKeyboard = nil,
	mRenderWindow = nil,
	mWidthWindow = 0,
	mHeightWindow = 0,
	mTouches = {},
	mSignalScreenClicked = false
}
			
function tiInputDevice.init()
	if not tiInputDevice.mInit then
		log_debug ("tiInputDevice.init")
		local inputManager = getInputManager()
		if getOSType() >= TI_OS_WINSEVEN then 
			tiInputDevice.mMouse =  Mouse(inputManager:getDevice(TIINPUT_MOUSEDEVICE))
			tiInputDevice.mKeyboard = Keyboard(inputManager:getDevice(TIINPUT_KEYBOARDDEVICE))
		else
			tiInputDevice.mTouchscreen = TouchScreen(inputManager:getDevice(TIINPUT_TOUCHSCREENDEVICE))
			tiInputDevice.mTouchscreen:acquire()
		end
		tiInputDevice.mRenderWindow = RenderWindow(getCurrentScene():getMainView())
		tiInputDevice.mWidthWindow, tiInputDevice.mHeightWindow = tiInputDevice.mRenderWindow:getMetrics()  
		tiInputDevice.mInit = true
	end
end

function tiInputDevice.getTouches()
	tiInputDevice.init()
	tiInputDevice.mTouches = {}
	if tiInputDevice.mMouse ~= nil then
--		log_debug("tiCommonInputManager.getTouches Mouse button: "..tostring(mMouse:isButtonDown (0))..","..tostring(mMouse:isButtonDown (1))..","..tostring(mMouse:isButtonDown (2))..","..tostring(mMouse:isButtonDown (3)))
		if tiInputDevice.mMouse:isButtonDown (TIMOUSE_LEFTBUTTON) then
			if tiInputDevice.mRenderWindow ~= nil then
				local ret, x, y = tiInputDevice.mRenderWindow:getLocalCursorCoordinates() -- get render view coordinates

				if ret == eOk then 
--log_debug("tiInputDevice.getTouches win: "..tiInputDevice.mWidthWindow.."x"..tiInputDevice.mHeightWindow.." pick: "..x.."x"..y) 		
					if x >= 0 and x <= tiInputDevice.mWidthWindow and y >= 0 and y <= tiInputDevice.mHeightWindow then
						tiInputDevice.mTouches[1] = {position = {x = x, y = y}} 
					end
					

					if tiInputDevice.mSignalScreenClicked then 
						sendCallback("screenClicked")
					end
				else 
					log_error("tiInputDevice.getTouches " .. tostring(ret))
				end
			end
		end
	end
	if tiInputDevice.mTouchscreen ~= nil then
		tiInputDevice.mTouches = tiInputDevice.mTouchscreen:getTouches()
	end
	if tiInputDevice.mTouches ~= nil and #(tiInputDevice.mTouches) > 0 then
--		log_debug("tiInputDevice.getTouches touch: "..tiInputDevice.mTouches[1].position.x..","..tiInputDevice.mTouches[1].position.y)
	end
	return tiInputDevice.mTouches
end

function tiInputDevice.wasKeyPressed (key)
	tiInputDevice.init()
	return tiInputDevice.mKeyboard ~= nil and tiInputDevice.mKeyboard:wasKeyPressed (key, true)
end
		
function tiInputDevice.isKeyDown (key)
	tiInputDevice.init()
	return tiInputDevice.mKeyboard ~= nil and tiInputDevice.mKeyboard:isKeyDown (key)
end

function tiInputDevice.isClick()
	tiInputDevice.init()
	return (#tiInputDevice.mTouches ~= 0) 
end
			
function tiInputDevice.getPositionClick()
	tiInputDevice.init()
	if tiInputDevice.mTouches[1] ~= nil then
		return tiInputDevice.mTouches[1].position.x, tiInputDevice.mTouches[1].position.y
	else
		return -10, -10
	end
end

-- Ne fonctionne pas, car mMouse:getWheelDelta() renvoie toujours 0
function tiInputDevice.getWheelDelta()
	tiInputDevice.init()
	return tiInputDevice.mMouse:getWheelDelta()
end
			
			
