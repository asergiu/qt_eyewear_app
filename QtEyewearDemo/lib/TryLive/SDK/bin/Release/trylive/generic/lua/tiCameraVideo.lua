log_loaded_info("tiCameraVideo.lua")

tiCameraVideo = inheritsFrom (tiCamera)

function tiCameraVideo:init (properties)
	log_debug("tiCameraVideo:init "..tostring(self).." properties: "..tostring(properties), tiCamera.logLevel)
	self.mCameraType = "video"
	if properties ~= nil then
		if self.mConfig.videoConfig == nil then
			self:makeVideoConfig()
		end
		if self.mConfig.cameraCalib == nil then
			self:makeCameraCalib()
		end
	else
		log_warning ("tiCameraVideo:init missing properties")
	end
	if getOSType() >= TI_OS_WINSEVEN then
		-- Currently video live pause doesn't work on Windows (but it works on iOS)
		-- So there is a Lua implementation, but currently device/video orientation is not correctly handled on iOS
		-- So on Windows, use false, and on iOS use true ...

		tiCamera.useVideoCapturePause = false
	end
end

function tiCameraVideo:log(title)
	if title ~= nil then 
		log_debug (title..", Camera:")
	else
		log_debug ("Camera:")
	end
	log_debug ("Name: "..tostring(self:getName())..", FriendlyName: "..tostring(self.mConfig.friendlyName)..", Device: "..tostring(self.mConfig.device))
	log_debug("Capture: " .. tostring(self.mConfig.capture.x) .. " x " .. tostring(self.mConfig.capture.y) .. ", Used: " .. tostring(self.mConfig.use.x) .. " x " .. tostring(self.mConfig.use.y)..", FocalY: "..tostring(self.mConfig.focalY))
	log_debug("Driver: " .. tostring(self.mConfig.driver) .. ", Inverted: " .. tostring(self.mConfig.inverted) .. ", Format: " .. tostring(self.mConfig.pixel))
end

function tiCameraVideo:makeVideoConfig()
	local fileName = getAppConfigPath() .. "video " .. self:getName() .. ".xml"
	local f = assert (io.open (fileName, "w"), "Impossible to open "..fileName.." for writing")
	--If the open fails, the error message goes as the second argument to assert, which then shows the message.

	if f ~= nil then
		log_debug("tiCameraVideo:makeVideoConfig "..fileName)
		if self.mConfig.use == nil or self.mConfig.use.x == nil or self.mConfig.use.y == nil then
			self.mConfig.use = {}
			self.mConfig.use.x = self.mConfig.capture.x
			self.mConfig.use.y = self.mConfig.capture.y
		end
		if self.mConfig.capture == nil then
			self.mConfig.capture = {}
		end
		local videoType = 0
		if self.mConfig.type ~= nil then
			videoType = self.mConfig.type
		end
		local rate = 30
		if self.mConfig.rate ~= nil then
			rate = self.mConfig.rate
		end
		log_debug("tiCameraVideo:makeVideoConfig capture: " .. tostring(self.mConfig.capture.x) .. " x " .. tostring(self.mConfig.capture.y) .. ", used: " .. tostring(self.mConfig.use.x) .. " x " .. tostring(self.mConfig.use.y))
		log_debug("tiCameraVideo:makeVideoConfig driver: " .. tostring(self.mConfig.driver) .. ", inverted: " .. tostring(self.mConfig.inverted) .. ", format: " .. tostring(self.mConfig.pixel))
		local ratio = self.mConfig.use.x/self.mConfig.use.y
		f:write('<?xml version="1.0" standalone="yes" ?>\n')
		f:write('<VIDEOCAP>\n')
			f:write('<VIDEOCAP>\n')
				f:write('<COMMENTS>'..self:getName()..'</COMMENTS>\n')
				f:write('<VIDEO_CAPTURE_FILE></VIDEO_CAPTURE_FILE>\n')
				f:write('<VIDEO_CAPTURE_NUM_DRIVER>'..tostring(self.mConfig.driver)..'</VIDEO_CAPTURE_NUM_DRIVER>\n')
				f:write('<VIDEO_CAPTURE_PIXEL_FORMAT>'..tostring(self.mConfig.pixel)..'</VIDEO_CAPTURE_PIXEL_FORMAT>\n')
				if self.mConfig.capture ~= nil and self.mConfig.capture.x ~= nil and self.mConfig.capture.y ~= nil then
					f:write('<VIDEO_CAPTURE_CAMERA_WIDTH>'..tostring(self.mConfig.capture.x)..'</VIDEO_CAPTURE_CAMERA_WIDTH>\n')
					f:write('<VIDEO_CAPTURE_CAMERA_HEIGHT>'..tostring(self.mConfig.capture.y)..'</VIDEO_CAPTURE_CAMERA_HEIGHT>\n')
				end
				if self.mConfig.use.xOffset ~= nil then
					f:write('<VIDEO_CAPTURE_X>'..tostring(self.mConfig.use.xOffset)..'</VIDEO_CAPTURE_X>\n')
				end
				if self.mConfig.use.yOffset ~= nil then
					f:write('<VIDEO_CAPTURE_Y>'..tostring(self.mConfig.use.yOffset)..'</VIDEO_CAPTURE_Y>\n')
				end
				f:write('<VIDEO_CAPTURE_WIDTH>'..tostring(self.mConfig.use.x)..'</VIDEO_CAPTURE_WIDTH>\n')
				f:write('<VIDEO_CAPTURE_HEIGHT>'..tostring(self.mConfig.use.y)..'</VIDEO_CAPTURE_HEIGHT>\n')
				f:write('<VIDEO_CAPTURE_DELAY>3</VIDEO_CAPTURE_DELAY>\n')
				f:write('<VIDEO_CAPTURE_RATE>'..tostring(rate)..'</VIDEO_CAPTURE_RATE>\n')
				f:write('<VIDEO_CAPTURE_NB_FRAME_BUFFERS>20</VIDEO_CAPTURE_NB_FRAME_BUFFERS>\n')
				f:write('<VIDEO_CAPTURE_INTERLACED>0</VIDEO_CAPTURE_INTERLACED>\n')
				if self.mConfig.inverted ~= nil then
					f:write('<VIDEO_CAPTURE_INVERTED>'..tostring(self.mConfig.inverted)..'</VIDEO_CAPTURE_INVERTED>\n')
				end
				f:write('<VIDEO_CAPTURE_PRIORITY>3</VIDEO_CAPTURE_PRIORITY>\n')
				f:write('<VIDEO_CAPTURE_TYPE>'..tostring(videoType)..'</VIDEO_CAPTURE_TYPE>\n')
				f:write('<VIDEO_CAPTURE_NAME>'..self.mConfig.name..'</VIDEO_CAPTURE_NAME>\n')
				f:write('<VIDEO_CAPTURE_CPU>-1</VIDEO_CAPTURE_CPU>\n')
				f:write('<VIDEO_CAPTURE_PAUSE>1</VIDEO_CAPTURE_PAUSE>\n')
				f:write('<VIDEO_CAPTURE_DEINTERLACE>1</VIDEO_CAPTURE_DEINTERLACE>\n')
				f:write('<VIDEO_CAPTURE_SHADER>0</VIDEO_CAPTURE_SHADER>\n')
				f:write('<VIDEO_CAPTURE_FRIENDLY_NAME>'..self.mConfig.friendlyName..'</VIDEO_CAPTURE_FRIENDLY_NAME>\n')
				f:write('<VIDEO_CAPTURE_DEBUG>0</VIDEO_CAPTURE_DEBUG>\n')
				f:write('<VIDEO_CAPTURE_RATE_IS_DYNAMIC>false</VIDEO_CAPTURE_RATE_IS_DYNAMIC>\n')
				f:write('<VIDEO_CAPTURE_RESYNCHRO_PERIOD>1200.000000</VIDEO_CAPTURE_RESYNCHRO_PERIOD>\n')
				f:write('<VIDEO_CAPTURE_STEADY_STATE_MARGIN>0.330000</VIDEO_CAPTURE_STEADY_STATE_MARGIN>\n')
				f:write('<VIDEO_CAPTURE_TIMECODE_REINIT_PERIOD>0.500000</VIDEO_CAPTURE_TIMECODE_REINIT_PERIOD>\n')
			f:write('</VIDEOCAP>\n')
		f:write('</VIDEOCAP>\n')
		f:close()
		self.mConfig.videoConfig = fileName
	else
		log_warning ("tiCameraVideo:makeVideoConfig, impossible to create: "..fileName) 
	end
end

function tiCameraVideo:makeCameraCalib()
	local fileName = getAppConfigPath() .. "calib " .. self:getName() .. ".xml"
	local f = assert (io.open (fileName, "w"), "Impossible to open "..fileName.." for writing")
	--If the open fails, the error message goes as the second argument to assert, which then shows the message.

	if f ~= nil then
		local capture = {}
		if self.mConfig.capture == nil or self.mConfig.capture.x == nil or self.mConfig.capture.y == nil then
			capture.x = self.mConfig.use.x
			capture.y = self.mConfig.use.y
		else
			capture = self.mConfig.capture
		end

		local focal = self.mConfig.focalY * capture.y
		log_debug("tiCameraVideo:makeCameraCalib "..fileName)
		log_debug("tiCameraVideo:makeCameraCalib Calib = " .. tostring(capture.x) .. " x " .. tostring(capture.y) .. " focal = " .. focal) 
		f:write('<?xml version="1.0" standalone="yes" ?>\n')
		f:write('<CAMERABASE>\n')
    		f:write('<CAMERABASE>\n')
        		f:write('<COMMENTS></COMMENTS>\n')
        		f:write('<MODELE>OPENCV_0</MODELE>\n')
        		f:write('<MAXX>'..tostring(self.mConfig.use.x)..'</MAXX>\n')
        		f:write('<MAXY>'..tostring(self.mConfig.use.y)..'</MAXY>\n')
        		f:write('<PX>'..tostring(self.mConfig.use.x/2)..'</PX>\n')
        		f:write('<PY>'..tostring(self.mConfig.use.y/2)..'</PY>\n')
        		f:write('<FX>'..tostring(focal)..'</FX>\n')
        		f:write('<FY>'..tostring(focal)..'</FY>\n')
        		f:write('<A1>0.00000000</A1>\n')
        		f:write('<A2>0.00000000</A2>\n')
        		f:write('<P1>0.00000000</P1>\n')
        		f:write('<P2>0.00000000</P2>\n')
        		f:write('<DEDISTORTION>false</DEDISTORTION>\n')
    		f:write('</CAMERABASE>\n')
		f:write('</CAMERABASE>\n')
		f:close()
		self.mConfig.cameraCalib = fileName
	else
		log_warning ("tiCameraVideo:makeCameraCalib, impossible to create: "..fileName) 
	end
end

function tiCameraVideo:getVideoCapture()
	log_debug ("tiCameraVideo:getVideoCapture "..self:getName(), tiCamera.logLevel)
	return self.mVideoCapture
end

function tiCameraVideo:getVideoCaptureId()
	local id = nil
	if self.mVideoCapture ~= nil then
		id = self.mVideoCapture:getVidCapID()
	end
	log_debug ("tiCameraVideo:getVideoCaptureId "..self:getName()..": "..tostring(id), tiCamera.logLevel)
	return id
end

function tiCameraVideo:getVideoConfig()
	if self.mVideoConfig == nil then
		if self.mVideoCapture ~= nil then
			log_debug ("tiCameraVideo:getVideoConfig update "..self:getName())
			self.mVideoConfig = VideoCaptureConfig (self.mVideoCapture:getVideoConfig())	
			self.mInverted = self.mVideoConfig:getInverted()
			self.mPixelFormat = self.mVideoConfig:getPixelFormat()
			
			-- Init de la taille d'image par defaultSize
			-- ce n'est utilie que pour les camera photo, quand on initialize les viewports avant d'avoir initialis� la photo de background
	
			tiCamera.defaultSizeX = self.mVideoConfig:getWidth()
			tiCamera.defaultSizeY = self.mVideoConfig:getHeight()
		end
	end
	return self.mVideoConfig
end

function tiCameraVideo:getPixelFormat()
	self:getVideoConfig()
	return self.mPixelFormat
end

function tiCameraVideo:applyCamera()
	self.mCamera:setNearClip(10.0)
	self.mCamera:setFarClip(5000.0)
	local calibFile = self.mConfig["cameraCalib"]
	log_debug ("tiCameraVideo:applyCamera using camera calib: " .. calibFile, tiCamera.logLevel)
	if self.mCamera:setResource (calibFile) ~= eOk then
		dispatchError(TI_FILE_NOT_FOUND, "Cannot load camera calib file " .. calibFile)
	end
	log_camera(self.mCamera, "tiCameraVideo:applyCamera camera de base")
	local videoX, videoY = self:getVideoSize()
	if videoX ~= 0 and videoY ~= 0 then
		local cameraModel, 
			cameraSizeX, cameraSizeY, 
			cameraCenterX, cameraCenterY, 
			cameraFocalX, cameraFocalY, 
			cameraA1, cameraA2, cameraP1, cameraP2, 
			cameraDistortion = self.mCamera:getCameraModel()
		cameraSizeX = videoX
		cameraSizeY = videoY
		cameraCenterX = videoX/2
		cameraCenterY = videoY/2
		self.mCamera:setCameraModel (cameraModel, 
			cameraSizeX, cameraSizeY, 
			cameraCenterX, cameraCenterY, 
			cameraFocalX, cameraFocalY, 
			cameraA1, cameraA2, cameraP1, cameraP2, 
			cameraDistortion)
		self.mCamera:applyCameraModel(true)
		log_camera(self.mCamera, "tiCameraVideo:applyCamera camera adapt�e � la video "..videoX.."x"..videoY)
	end
end

function tiCameraVideo:open(notify)
	log_call ("tiCameraVideo:open notify: "..tostring(notify))
		if notify == nil then
			notify = true
		end
	-- In case video capture is already open, close it before in order to open a new one
	if self.mVideoCapture ~= nil then
		log_call_end()
		dispatchError(TI_INTERNAL_ERROR, "Video capture is already opened")
		return false
	end
	
	if self.mConfig == nil then
		log_call_end()
		dispatchError(TI_INTERNAL_ERROR, "Do not open video capture, camera device config was not initialized yet")
		return false
	end
	log_debug ("tiCameraVideo:open, video config: " .. self.mConfig["videoConfig"]..", Calib: "..self.mConfig["cameraCalib"])

	self.mVideoCapture = VideoCapture(getCurrentScene():createObject (CID_VIDEOCAPTURE))
	self.mVideoCapture:setName("videocapture "..self:getName())
	log_debug ("tiCameraVideo:open, video capture id: " .. self.mVideoCapture:getVidCapID())
	
	if self.mVideoCapture:setResource (self.mConfig["videoConfig"]) ~= eOk then
		dispatchError(TI_FILE_NOT_FOUND, "Cannot load config video capture file " .. self.mConfig["videoConfig"])
		log_call_end()
		return false
	end

	self:log ("tiCameraVideo:open")
	
	-- Lancement de la video capure
	
	self.mVideoCapture:open()
	self.mVideoCapture:play()
	if self.mConfig.rotation ~= nil then
		self.mVideoCapture:setBufferRotation(self.mConfig.rotation)
	end
	
	-- [GSE] We try to wait one frame of the video Capture but it seems that the behaviour of the method getFrameIndex
	-- is not what we expected. So we decide to wait two frames. there was a problem especially with Android for some devices
	-- when we switch the camera back/front
	-- Quand on ouvre la video capture il faut attendre quelques frames avant d'en avoir une potable
	local minFrame = 1
	local begin = os.clock()
	local duree = 0
	local dureeMax = 2 -- secondes
	while (self.mVideoCapture:getFrameIndex() < minFrame) and (duree < dureeMax) do
--		log_debug("frame index: "..self.mVideoCapture:getFrameIndex())
		coroutine.yield()
		duree = os.clock() - begin
	end
	log_debug("tiCameraVideo:open : wait for frame #"..minFrame..": "..tostring(duree).." secondes")
	if duree >= dureeMax then
		log_warning ("tiCameraVideo:open camera "..self:getName().." seems to have some trouble")
	end
		
	self.mVideoCapture:enableAutoFocus()
	self.mVideoConfig = nil
	self:getVideoConfig()
	
	if self.mVideoTexture == nil then
		log_debug ("tiCameraVideo:open : set video texture")
		self.mVideoTexture = VideoTexture (getCurrentScene():createObject(CID_VIDEOTEXTURE))
		self.mVideoTexture:setName ("videotexture "..self:getName())
	end
	
	self.mVideoTexture:setVideoCapture(self.mVideoCapture)
	self:applyCamera()
	
--	tiViewportManager.initContent()
	
	self.mIsOpened = true
	if notify then
		self:fireEvent ("EventBackgroundChanged")
	end
	self:fireEvent ("EventVideoStart")
	log_debug ("tiCameraVideo:open success: "..self:getName())

	log_call_end()
	return true
end

function tiCameraVideo:captureImage (minFrameIndex)
	log_call ("tiCameraVideo:captureImage minFrameIndex: "..tostring(minFrameIndex))
	local image = nil
	local cameraFOVy, cameraRatio, timeCode, frameIndex
	if minFrameIndex == nil then
		minFrameIndex = -1
	end
	
	if self.mVideoCapture ~= nil then
		frameIndex = self.mVideoCapture:getFrameIndex()
		if frameIndex >= minFrameIndex then
			timeCode = self.mVideoCapture:getTimeCode()
			log_debug("tiCameraVideo:captureImage video resolution " .. self:getWidth().."x"..self:getHeight().." px format: " .. self:getPixelFormat().." inverted: " .. self:getInverted().." buf rot: "..self.mVideoCapture:getBufferRotation(), tiCamera.logLevel)			

			-- Store the camera information associated to this video frame
			
			local	cameraModel, 
					cameraSizeX, cameraSizeY, 
					cameraCenterX, cameraCenterY, 
					cameraFocalX, cameraFocalY, 
					cameraA1, cameraA2, cameraP1, cameraP2, 
					cameraDistortion = self.mCamera:getCameraModel()			

			local width, height
			local rotationToApply = self.mVideoCapture:getRotationToApply()
			local inverted = self:getInverted()
			
			log_debug("tiCameraVideo:captureImage inverted: ".. inverted .. " rotationToApply: " .. rotationToApply)--, tiCamera.logLevel)
	
			if rotationToApply == DEVICE_ROTATE_90 or rotationToApply == DEVICE_ROTATE_270 then
				width = self:getHeight()
				height = self:getWidth()
			else
				width = self:getWidth()
				height = self:getHeight()
			end
--log_camera (self.mCamera, "tiCameraVideo:captureImage camera ")
log_debug ("tiCameraVideo:captureImage focaly " .. cameraFocalY .. " dx " .. width .. " dy " .. height, tiCamera.logLevel)			
			cameraFOVy = math.atan2(height/2, cameraFocalY)*2
			cameraRatio = width / height
log_debug ("tiCameraVideo:captureImage cameraFOVy " .. cameraFOVy .. " cameraRatio " .. cameraRatio, tiCamera.logLevel)			
	
			local pixelFormat = self:getPixelFormat()
			if  pixelFormat == "BGR32_1" or  pixelFormat == "RGB32" or  pixelFormat == "YUY2" or pixelFormat == "NV12" or pixelFormat == "NV21" then
startChrono("tiCameraVideo:captureImage 1 create image")
				image = Image (self:getWidth(), self:getHeight(),4)
stopChrono("tiCameraVideo:captureImage 1 create image")
startChrono("tiCameraVideo:captureImage 2 capture image")
				image:getFromCapture(self.mVideoCapture)
stopChrono("tiCameraVideo:captureImage 2 capture image")
log_debug ("tiCameraVideo:captureImage timeCode: "..timeCode.." frameIndex: "..frameIndex, tiCamera.logLevel)

			-- To render video according various conditions (device orientation, buffer orientation etc.) Tiare already handle it internally except front landscape at least on iOS with current configs
			-- But to be able to use the cpatured video frame without any knowledge of the capture conditions, we do the job here
startChrono("tiCameraVideo:captureImage 3 convert image")
				tiCameraVideo.invertImage (image, inverted)
				image = tiCameraVideo.rotateImage (image, rotationToApply)
			
				-- For an unknown reason, for the case landscape back we have also to add a 180 rotation, at least on iOS
				local renderingRotation = getRenderingRotation()
				if renderingRotation == DEVICE_ROTATE_90 or renderingRotation == DEVICE_ROTATE_270 then
					if self.mConfig.friendlyName:lower() == "back" then
						image:flip(-1)
					end
				end
			
stopChrono("tiCameraVideo:captureImage 3 convert image")
			else
				log_warning("tiCameraVideo:captureImage pixel format not implemented: " .. pixelFormat)
			end
		end
--local bufferRotation, rotationToApply, inverted, ratio = self:getVideoOrientation()
--log_debug ("tiCameraVideo:captureImage getVideoOrientation: bufferRotation: "..bufferRotation..", rotationToApply: "..rotationToApply..", inverted: "..inverted..", ratio: "..ratio)
	else
		log_warning ("tiCameraVideo:captureImage videoCapture = nil")
	end
	log_call_end()

	return image, cameraFOVy, cameraRatio, timeCode, frameIndex
end

function tiCameraVideo.invertImage (image, inverted)
	if inverted ~= 0 and image ~= nil then
		if inverted == MIRROR_HORIZONTAL then	-- 1
			image:flip(1)					-- flipMode = 0  means flipping around x-axis
		elseif inverted == MIRROR_VERTICAL then	-- 2
			image:flip(0)					-- flipMode > 0  (e.g. 1) means flipping around y-axis
		elseif inverted == MIRROR_HV then		-- 3
			image:flip(-1)				-- flipMode < 0  (e.g. -1) means flipping around both axises
		end
	end
end

function tiCameraVideo.rotateImage (imageIn, rotation)
	local imageOut
	if rotation == DEVICE_ROTATE_90 or rotation == DEVICE_ROTATE_270 then
		imageOut = Image (imageIn:getHeight(), imageIn:getWidth(), imageIn:getChannelNb())
		imageIn:transpose (imageOut)
		if rotation == DEVICE_ROTATE_270 then
			-- Rotate 270 = transpose + flip/x
			imageOut:flip(0)
		else
			-- Rotate 90 = transpose + flip/y
			imageOut:flip(1)
		end
	elseif rotation == DEVICE_ROTATE_180 then
		imageOut = imageIn
		imageOut:flip(-1)
	else
		imageOut = imageIn
	end	
	
	return imageOut
end

--[[
local i = Image (self.mImage:getHeight(), self.mImage:getWidth(), self.mImage:getChannelNb())
self.mImage:transpose (i)
local f, n = string.gsub(self.mFileName, ".jpg", "_rotate.jpg")
log_debug ("tiPhoto:saveImage rotate "..tostring(f).. " "..tostring(n))
i:save(f)			
]]--
			--				if rotationToApply == DEVICE_ROTATE_90 or rotationToApply == DEVICE_ROTATE_270 then
--[[
				if inverted ~= 0 then
					if inverted == 1 then
						image:flip(1)					-- flipMode = 0  means flipping around x-axis
					elseif inverted == 2 then
						image:flip(0)					-- flipMode > 0  (e.g. 1) means flipping around y-axis
					elseif inverted == 3 then
						image:flip(-1)				-- flipMode < 0  (e.g. -1) means flipping around both axises
					end
				end
	enum {
		DEVICE_ROTATE_NONE		= -1,	
		DEVICE_ROTATE_0			= 0, 
		DEVICE_ROTATE_90		= 1, 		// 90 counter clock wise rotation
		DEVICE_ROTATE_180		= 2,		// ...
		DEVICE_ROTATE_270		= 3
	};	
	enum {
		MIRROR_NONE			= 0, 
		MIRROR_HORIZONTAL	= 1, // flip top/bottom
		MIRROR_VERTICAL		= 2, // flip left/right ("mirror" used for try live eyewear)
		MIRROR_HV 			= MIRROR_HORIZONTAL | MIRROR_VERTICAL 				
]]--

function tiCameraVideo:saveImage (fileName)
	log_call ("tiCameraVideo:saveImage " .. fileName)
	local minFrameIndex = -1
	local image, cameraFOVy, cameraRatio, timeCode, frameIndex = self:captureImage (minFrameIndex)
	image:save(fileName)
	log_call_end()
	return cameraFOVy, cameraRatio, timeCode, frameIndex
end

function tiCameraVideo:getFrameRate()
--	return self.mVideoCapture:dumpDebugData()
--	return self.mVideoCapture:getRate()
-- log_object(self.mVideoCapture:dumpDebugData())
	return self.mVideoCapture:getDynamicRate()
end
	




