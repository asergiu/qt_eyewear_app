log_loaded_info("tiInputRotateScale.lua")

tiInputRotateScale = inheritsFrom (tiInputProcessor)

tiInputRotateScale.TI_GAP_SCALE_MIN = 0.8
tiInputRotateScale.TI_GAP_SCALE_MAX = 1.20
tiInputRotateScale.TI_TOUCH_SPEED_ROTATING = 1.5
tiInputRotateScale.TI_ACTIVATE_ROTATE_PITCH = false
tiInputRotateScale.TI_ABSOLUTE_MAX_ROTATE_VALUE = 15

function tiInputRotateScale:init (properties)
	log_debug("tiInputRotateScale:init "..tostring(self).." properties: "..tostring(properties))
	
	self.mCurrentScale = 1
	self.mCurrentOrientation = Vector3(0, 0, 0)
	self.mScaleOffset = 1
	self.mOrientationOffset = Vector3(0, 0, 0)	
end

function tiInputRotateScale:getScale()
	return (self.mScaleOffset * self.mCurrentScale)
end
		
function tiInputRotateScale:getOrientation()
	return Vector3(self.mOrientationOffset:getX() + self.mCurrentOrientation:getX(), self.mOrientationOffset:getY() + self.mCurrentOrientation:getY(), self.mOrientationOffset:getZ() + self.mCurrentOrientation:getZ())
end			
		
function tiInputRotateScale.reset()
	log_debug("tiInputRotateScale.reset")
	self.mOrientationOffset = Vector3(0, 0, 0)
	self.mCurrentOrientation = Vector3(0, 0, 0)
	self.mScaleOffset = 1
	self.mCurrentScale = 1
end
		
function tiInputRotateScale:scale(touches)
	log_debug ("tiInputRotateScale.scale")
	-- Compute the scale value
	local distNorm = math.pow(touches[1].position.x - mSecondTouch:getY(), 2) +  math.pow(touches[1].position.y - mSecondTouch:getY(), 2)
	local distCurrent = math.pow(touches[1].position.x - touches[2].position.x, 2) +  math.pow(touches[1].position.y - touches[2].position.y, 2)
	local tempScale = (distCurrent / distNorm)
	tempScale = tiInputRotateScale.TI_GAP_SCALE_MIN + (tempScale * (1 - tiInputRotateScale.TI_GAP_SCALE_MIN))
	-- Apply scale
	if tempScale * self.mScaleOffset < tiInputRotateScale.TI_GAP_SCALE_MIN then
		self.mCurrentScale = tiInputRotateScale.TI_GAP_SCALE_MIN / self.mScaleOffset
	elseif tempScale * mScaleOffset > tiInputRotateScale.TI_GAP_SCALE_MAX then
		self.mCurrentScale = tiInputRotateScale.TI_GAP_SCALE_MAX / self.mScaleOffset
	else
		self.mCurrentScale = tempScale
	end
end
		
function tiInputRotateScale:rotate(touches)
	log_debug ("tiInputRotateScale.rotate")
	-- Compute the rotate value
	-- Compute the angle between vector (FirstTouch, SecondTouch) and vector (fistPress, CurrentPress)
	-- This angle is divide by 2, angle on x and angle on y
	local vectorNorm = Vector3(self.mSecondTouch:getY() - touches[1].position.x, self.mSecondTouch:getY() - touches[1].position.y, 0)
	local vectorCurrent = Vector3(touches[2].position.x - touches[1].position.x, touches[2].position.y - touches[1].position.y, 0)
	local angleZ = 0
	local angleX = 0
	if tiInputRotateScale.TI_ACTIVATE_ROTATE_PITCH then
		-- WRANING : Angle in degree
		local vectorCurrentTemp = Vector3(vectorCurrent)
		vectorCurrentTemp:setY(vectorNorm:getY())
		angleZ = math.deg (vectorNorm:getRadianAngle(vectorCurrentTemp)) * tiInputRotateScale.TI_TOUCH_SPEED_ROTATING
		if touches[2].position.x > self.mSecondTouch:getY() then
			angleZ = -angleZ
		end
		vectorCurrentTemp = Vector3(vectorCurrent)
		vectorCurrentTemp:setX(vectorNorm:getX())
		angleX = math.deg (vectorNorm:getRadianAngle(vectorCurrentTemp)) * tiInputRotateScale.TI_TOUCH_SPEED_ROTATING							
		if touches[2].position.y > self.mSecondTouch:getY() then
		--if ((vectorNorm:getX() * vectorCurrentTemp:getY()) - (vectorNorm:getY() * vectorCurrentTemp:getX())) < 0 then
			angleX = -angleX
		end
	else
		angleZ = math.deg (vectorNorm:getRadianAngle(vectorCurrent)) * tiInputRotateScale.TI_TOUCH_SPEED_ROTATING				
		if ((vectorNorm:getX() * vectorCurrent:getY()) - (vectorNorm:getY() * vectorCurrent:getX())) > 0 then												
				angleZ = -angleZ
		end
	end
	if angleZ + self.mOrientationOffset:getZ() > tiInputRotateScale.TI_ABSOLUTE_MAX_ROTATE_VALUE then
		angleZ = tiInputRotateScale.TI_ABSOLUTE_MAX_ROTATE_VALUE - self.mOrientationOffset:getZ()
	end
			
	if angleZ + self.mOrientationOffset:getZ() < -tiInputRotateScale.TI_ABSOLUTE_MAX_ROTATE_VALUE then
		angleZ = -tiInputRotateScale.TI_ABSOLUTE_MAX_ROTATE_VALUE - self.mOrientationOffset:getZ()
	end				
	if angleX + self.mOrientationOffset:getX() > tiInputRotateScale.TI_ABSOLUTE_MAX_ROTATE_VALUE then
		angleX = tiInputRotateScale.TI_ABSOLUTE_MAX_ROTATE_VALUE - self.mOrientationOffset:getX()
	end		
	if angleX + self.mOrientationOffset:getX() < -tiInputRotateScale.TI_ABSOLUTE_MAX_ROTATE_VALUE then
		angleX = -tiInputRotateScale.TI_ABSOLUTE_MAX_ROTATE_VALUE - self.mOrientationOffset:getX()
	end				
	-- Apply rotation
	self.mCurrentOrientation = Vector3(angleX, 0, angleZ)
end
		
function tiInputRotateScale:rotateAndScale(touches)
	-- If it's not the first time we detect the second finger
	if self:isSecondTouchSet() then
		self:scale(touches)
		self:rotate(touches)
	else
		-- Save the second press position
		self:setSecondTouch (Vector2(touches[2].position.x, touches[2].position.y))
	end
end
		
function tiInputRotateScale:getInputData (touches, viewport)
	local moveOccured = false
	if #touches == 2 then
		self:rotateAndScale(touches)
		moveOccured = true
	end
	if #touches ~= 2 then
		self:resetSecondTouch()
		self.mScaleOffset = self:getScale()
		self.mOrientationOffset = self:getOrientation()
		self.mCurrentOrientation = Vector3(0, 0, 0)
		self.mCurrentScale = 1
	end
	return moveOccured
end
