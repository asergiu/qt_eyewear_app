@echo off

rem "All the relative paths are relative to the project's root!"
SET windeployqt=C:\Qt\5.5\msvc2010\bin\windeployqt.exe
if EXIST "C:\Qt\5.12.1\msvc2017_64\bin\windeployqt.exe" (
	SET windeployqt=C:\Qt\5.12.1\msvc2017_64\bin\windeployqt.exe
)
SET ISCC="C:\Program Files (x86)\Inno Setup 5\ISCC.exe"
SET GITEXE="c:\Program Files (x86)\Git\bin\git.exe"
IF NOT EXIST %GITEXE% (
	SET GITEXE="c:\Program Files\Git\bin\git.exe"
)
SET BVERSION=%GITEXE% describe --long
SET qmldir=qml
SET exefile=release\%1.exe
SET issfile=deployment_scripts\createUpdate.iss

SET BCMD=%GITEXE% rev-parse --abbrev-ref HEAD
SET BNAME=""
FOR /F "tokens=*" %%i IN ('%BCMD%') DO SET BNAME=%%i

SET TIMESTAMP=%DATE:/=%_%TIME::=%
SET TIMESTAMP=%TIMESTAMP: =%
SET TIMESTAMP=%TIMESTAMP:-=%
SET TIMESTAMP=%TIMESTAMP:.=%

IF NOT EXIST %windeployqt% (
	ECHO ERROR! Cannot find: [%windeployqt%] ! Set the correct path to windeployqt tool.
	goto :eof
)

IF NOT EXIST %exefile% (
	ECHO ERROR! Cannot find: [%exefile%] ! Did you build the project?
	goto :eof
)

IF NOT EXIST %ISCC% (
	ECHO ERROR! Cannot find: %ISCC% ! Do you have Inno Setup 5 installed?
	goto :eof
)

IF NOT EXIST %issfile% (
	ECHO ERROR! Cannot find: %issfile% ! It should be next to this .bat file
	goto :eof
)

rem RUN windeployqt tool
echo windeployqt START
start /WAIT %windeployqt% --qmldir %qmldir% %exefile%
echo windeployqt END

rem RUN Inno Setup tool
echo Inno Setup START : %issfile%

FOR /F "tokens=*" %%i IN ('%BVERSION%') DO %ISCC% %issfile% /DAPPNAME=%1 /DGVERSION=%%i /DBUILDDATE=%TIMESTAMP% /DBRANCH=%BNAME%

echo Inno Setup END
