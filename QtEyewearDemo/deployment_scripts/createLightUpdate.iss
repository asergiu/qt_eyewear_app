#define MyAppPublisher "Acep France"

#define IconPath "..\"
#define IconName "AppIcon.ico"
#define Lib "..\lib"
#define ExtrasLibs "..\extras\libs"
#define ExtrasInstallers "..\extras\installers"
#define Extras "..\extras"
#define DeployDir "..\release"

[Setup]
AppId={{EyewearKiosk_{#BRANCH}}}
OutputBaseFilename=EyewearKioskLightUpdate_{#BRANCH}_{#GVERSION}__{#BUILDDATE}

AppName={#APPNAME}
AppVersion={#GVERSION}
AppPublisher={#MyAppPublisher}
DefaultDirName={pf}\{#APPNAME}
DisableDirPage=yes
DefaultGroupName={#APPNAME}
DisableProgramGroupPage=yes
OutputDir=.
SetupIconFile={#IconPath}\{#IconName}
Compression=lzma
SolidCompression=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; 
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}";

[Files]
Source: "{#DeployDir}\sort-files\*";                DestDir: "{app}/EyewearKiosk/sort-files/";        Flags: ignoreversion recursesubdirs createallsubdirs skipifsourcedoesntexist;
Source: "{#DeployDir}\*.exe";                       DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion;
Source: "{#DeployDir}\*.new";                       DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion skipifsourcedoesntexist;
Source: "{#DeployDir}\*.ini";                       DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion skipifsourcedoesntexist onlyifdoesntexist;
Source: "{#IconPath}\{#IconName}";                  DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion skipifsourcedoesntexist;
Source: "{#Extras}\UpdateAssistant.exe.new";        DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion skipifsourcedoesntexist;
Source: "{#Extras}\updater.ini";                    DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion skipifsourcedoesntexist onlyifdoesntexist;

[Icons]
Name: "{group}\{#APPNAME}"; Filename: "{app}\EyewearKiosk\{#APPNAME}.exe"; IconFilename: {app}\EyewearKiosk\{#IconName}
Name: "{group}\{cm:UninstallProgram,{#APPNAME}}"; Filename: "{uninstallexe}"; IconFilename: {app}\EyewearKiosk\{#IconName}
Name: "{commondesktop}\{#APPNAME}"; Filename: "{app}\EyewearKiosk\{#APPNAME}.exe"; Tasks: desktopicon; IconFilename: {app}\EyewearKiosk\{#IconName}
Name: "{commondesktop}\PAUSE-{#APPNAME}"; Filename: "{app}\EyewearKiosk\{#APPNAME}.exe"; Parameters:"--pause-state"; Tasks: desktopicon; IconFilename: {app}\EyewearKiosk\{#IconName}
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#APPNAME}"; Filename: "{app}\EyewearKiosk\{#APPNAME}.exe"; Tasks: quicklaunchicon; IconFilename: {app}\EyewearKiosk\{#IconName}

[Registry]
Root: HKCU; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"; ValueType: string; ValueName: "{#AppName}"; ValueData: """{app}\EyewearKiosk\sys_restart_launcher.bat"""; Flags: uninsdeletevalue

[Run]
Filename: "{app}\EyewearKiosk\{#APPNAME}.exe"; Parameters: "--pause-state"; Description: "{cm:LaunchProgram,{#StringChange(APPNAME, '&', '&&')}}"; Flags: nowait postinstall skipifsilent