#define MyAppPublisher "Acep France"

#define IconPath "..\"
#define IconName "AppIcon.ico"
#define Lib "..\lib"
#define ExtrasLibs "..\extras\libs"
#define ExtrasInstallers "..\extras\installers"
#define Extras "..\extras"
#define DeployDir "..\release"

[Setup]
AppId={{EyewearKiosk_{#BRANCH}}}
OutputBaseFilename=EyewearKiosk_{#BRANCH}_{#GVERSION}__{#BUILDDATE}

AppName={#APPNAME}
AppVersion={#GVERSION}
AppPublisher={#MyAppPublisher}
DefaultDirName={pf}\{#APPNAME}
DisableDirPage=yes
DefaultGroupName={#APPNAME}
DisableProgramGroupPage=yes
OutputDir=.
SetupIconFile={#IconPath}\{#IconName}
Compression=lzma
SolidCompression=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; 
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}";

[Files]
Source: "{#DeployDir}\*";  Excludes: "custom";      DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion recursesubdirs createallsubdirs;
Source: "{#IconPath}\{#IconName}";                  DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion;
Source: "{#Lib}\TryLive\SDK\bin\Release\*";         DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion;
Source: "{#Lib}\TryLive\config.json";               DestDir: "{app}/lib/TryLive";        Flags: ignoreversion;

Source: "{#Extras}\sys_restart_launcher.bat";       DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion
Source: "{#Extras}\UpdateAssistant.exe.new";        DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion
Source: "{#Extras}\updater.ini";       				      DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion
Source: "{#Extras}\brands.json";       				      DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion
Source: "{#ExtrasLibs}\*";                          DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion recursesubdirs createallsubdirs;
Source: "{#ExtrasInstallers}\vcredist10_x86.exe";   DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion;                                 AfterInstall: RunVC10Redist;
Source: "{#ExtrasInstallers}\vcredist13_x86.exe";   DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion;                                 AfterInstall: RunVC13Redist;
Source: "{#ExtrasInstallers}\oalinst.exe";          DestDir: "{app}/EyewearKiosk";       Flags: ignoreversion;                                 AfterInstall: RunOALInst;
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

Source: "{#DeployDir}\custom\{#BRANCH}\*";           DestDir: "{app}/EyewearKiosk";       Flags: skipifsourcedoesntexist ignoreversion recursesubdirs createallsubdirs;

[Icons]
Name: "{group}\{#APPNAME}"; Filename: "{app}\EyewearKiosk\{#APPNAME}.exe"; IconFilename: {app}\EyewearKiosk\{#IconName}
Name: "{group}\{cm:UninstallProgram,{#APPNAME}}"; Filename: "{uninstallexe}"; IconFilename: {app}\EyewearKiosk\{#IconName}
Name: "{commondesktop}\{#APPNAME}"; Filename: "{app}\EyewearKiosk\{#APPNAME}.exe"; Tasks: desktopicon; IconFilename: {app}\EyewearKiosk\{#IconName}
Name: "{commondesktop}\PAUSE-{#APPNAME}"; Filename: "{app}\EyewearKiosk\{#APPNAME}.exe"; Parameters:"--pause-state"; Tasks: desktopicon; IconFilename: {app}\EyewearKiosk\{#IconName}
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#APPNAME}"; Filename: "{app}\EyewearKiosk\{#APPNAME}.exe"; Tasks: quicklaunchicon; IconFilename: {app}\EyewearKiosk\{#IconName}

[Code]
procedure RunVC10Redist;
var 
	ResultCode: Integer;
begin
  if not Exec(ExpandConstant('{app}\EyewearKiosk\vcredist10_x86.exe'), '/passive /norestart','',SW_SHOWNORMAL, ewWaitUntilTerminated, ResultCode) then
    MsgBox('vcredist10_x86.exe installer failed to run!' + #13#10 + SysErrorMessage(ResultCode), mbError, MB_OK)

end;

procedure RunVC13Redist;
var 
	ResultCode: Integer;
begin
  if not Exec(ExpandConstant('{app}\EyewearKiosk\vcredist13_x86.exe'), '/passive /norestart','',SW_SHOWNORMAL, ewWaitUntilTerminated, ResultCode) then
    MsgBox('vcredist13_x86.exe installer failed to run!' + #13#10 + SysErrorMessage(ResultCode), mbError, MB_OK)

end;

procedure RunOALInst;
var 
	ResultCode: Integer;
begin
  if not Exec(ExpandConstant('{app}\EyewearKiosk\oalinst.exe'), '/silent','',SW_SHOWNORMAL, ewWaitUntilTerminated, ResultCode) then
    MsgBox('oalinst.exe installer failed to run!' + #13#10 + SysErrorMessage(ResultCode), mbError, MB_OK)

end;

[Registry]
Root: HKCU; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"; ValueType: string; ValueName: "{#AppName}"; ValueData: """{app}\EyewearKiosk\sys_restart_launcher.bat"""; Flags: uninsdeletevalue

[Run]
Filename: "{app}\EyewearKiosk\{#APPNAME}.exe"; Parameters: "--pause-state"; Description: "{cm:LaunchProgram,{#StringChange(APPNAME, '&', '&&')}}"; Flags: nowait postinstall skipifsilent