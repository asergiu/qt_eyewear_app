#include "offscreenwidget.h"
#include <TryLive.h>

#include <QPainter>
#include <QLabel>
#include <QHBoxLayout>
#include "qstackedlayout.h"
#include <cstring>
#include "qpushbutton.h"
#include <QDebug>
#include <QQuickWidget>
#include <QQmlContext>

OffscreenWidget::OffscreenWidget(QWidget  * parent) : QWidget (parent)
{
    mBuffer      = nullptr;
    mImage       = nullptr;
    mSize.width  = 640;
    mSize.height = 480;

    m_renderQuickWidget = new QQuickWidget(this);

    m_renderQuickWidget->setResizeMode(QQuickWidget::SizeRootObjectToView);
    m_renderQuickWidget->setAttribute(Qt::WA_AlwaysStackOnTop);
    m_renderQuickWidget->setClearColor(Qt::transparent);

    m_renderQuickWidget->resize(mSize.width, mSize.height);
    m_renderQuickWidget->setGeometry(0,0,mSize.width, mSize.height); // TODO does it replace resize?
    m_renderQuickWidget->raise();
}

OffscreenWidget::~OffscreenWidget()
{
    clear();
}

void OffscreenWidget::setContextProperty(const QString &name, QObject* value)
{
    m_renderQuickWidget->rootContext()->setContextProperty(name, value);
}

void OffscreenWidget::setSource(const QString &name)
{
    m_renderQuickWidget->setSource(QUrl(name));
}

void OffscreenWidget::setQuickwidgetVisible(bool visible)
{
    if(!m_renderQuickWidget)return;
    m_renderQuickWidget->setVisible(visible);
}

void OffscreenWidget::clear()
{
    delete mImage;
    delete [] mBuffer;
    mImage  = nullptr;
    mBuffer = nullptr;
    mSize.width  = 0;
    mSize.height = 0;
}

void OffscreenWidget::render(TryLive & trylive, unsigned int width, unsigned int height)
{
    if ((mSize.width != width) || (mSize.height != height))
    {
        clear();
        if ((width > 0) && (height > 0))
        {
            mSize.width  = width;
            mSize.height = height;
            auto ms = width * height * 3;
            mBuffer = new unsigned char[ms];
            std::memset(mBuffer, 0, ms);
            mImage  = new QImage(mBuffer, width, height, QImage::Format_RGB888);
        }
    }
    if ((mBuffer != nullptr) && trylive.getPixels(mBuffer))
    {
        // We suggest only using repaint() if you need an immediate repaint,
        // for example during animation. In almost all circumstances update() is better,
        // as it permits Qt to optimize for speed and minimize flicker.
        repaint();
       // qDebug() << "OffscreenWidget call repaint";
    }
}

void OffscreenWidget::paintEvent(QPaintEvent *event)
{

    QPainter painter(this);
    QSize s = this->size();

    if (mImage != nullptr)
    {
        if ((s.width() != mSize.width) || (s.height() != mSize.height))
        {

            painter.drawImage(0, 0, mImage->scaled(s, Qt::KeepAspectRatio, Qt::SmoothTransformation));
        }
        else
        {
            painter.drawImage(0, 0, *mImage);
        }
    }


    //m_renderQuickWidget->resize(s.width(), s.height());
    m_renderQuickWidget->setGeometry(0,0,mSize.width, mSize.height); // TODO does it replace resize?


    QWidget::paintEvent(event); // TODO needed ?
}

