#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "smipc.h"
#include "settings/email/emailsettings.h"
#include "settings/email/cconfigxml.h"
#include "settings/settings.h"
#include "settings/ddebug.h"
#include "src/model/product.h"
#include "src/model/brand.h"
#include "src/inference/pythoninference.h"
#include "eyewear.h"
#include "settings/email/utils.h"
#include "settings/cconfigdialog.h"

#include <QQuickView>
#include <QQmlContext>
#include <QtQml>
#include <QDesktopWidget>
#include <QPainter>
#include <QPicture>
#include <QPixmap>
#include <QImage>
#include <QVariant>
#include <QDialog>
#include <QLabel>
#include <QPixmap>
#include <QHBoxLayout>
#include <QFont>
#include <QFontDatabase>
#include <QMenu>

#include "licensing/cregistration.h"
#include "regdialog.h"
#include "src/amazon/amazonqr.h"
#include "clickablelabel.h"
#include "cutils.h"

#include "settings/globals.h"
extern TryLive trylive;
#include "src/trylivecontroller.h"

#ifdef Q_OS_WIN
#include "windows_hacks.h"
#include <QDir>
#include <Psapi.h>
#include <process.h>
#include <TlHelp32.h>
#include <WinBase.h>
#include <winternl.h>
#endif
#include "TryLive.h"
#include <QtPlatformHeaders/QWindowsWindowFunctions>

const QString REG_DATA_FILE = "EyewearKiosk.bin";
const QString BUNDLE_ID = "com.acep.desktop.eyewearkiosk";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent, Qt::FramelessWindowHint | Qt::WindowSystemMenuHint),
    m_ui(new Ui::MainWindow)
{
    m_licenseCode = -1;
    m_tlCatalogLoaded = false;
    m_registration = NULL;
    m_regDialog = NULL;
    m_trayIcon = NULL;
    m_leftWidget = NULL;
    m_rightWidget = NULL;
    m_botLeftWidget = NULL;
    m_botRightWidget = NULL;
    m_botWidget = NULL;
    m_captureWidget = NULL;
    m_countdownWidget = NULL;
    m_fitMeBetter = NULL;
    m_captureAssetReference = "";

    m_ui->setupUi(this);
    appState = INITIALIZING;
}

void MainWindow::init(const bool& startInPauseState)
{
    this->setStyleSheet("background-color: black;");
    this->setMenuBar(0);
    QMainWindow::removeToolBar(m_ui->mainToolBar);
    setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint);

    QSettings* settings = Settings::getInstance();

    /* *** register custom objects *** */
    qmlRegisterType<Filter>("Filter", 1, 0, "Filter");
    qmlRegisterType<Product>("Product", 1, 0, "Product");
    qmlRegisterType<Brand>("Brand", 1, 0, "Brand");
    qmlRegisterType<CUtils>("CUtils", 1, 0, "CUtils");

    createActions();
    createTrayIcon();

    /* *** INFERENCE *** */
    m_attrDetector = new PythonInference(this);
    connect(m_attrDetector, SIGNAL(faceAttributesDetected(QJsonDocument)), this, SLOT(onAttributeDetectorFinished(QJsonDocument)));

    /* *** SMIPC *** */
    int id = settings->value("smipc/device_id", QVariant(4321)).toInt();
    int send_port = settings->value("smipc/sender_port", QVariant(45454)).toInt();
    int recv_port = settings->value("smipc/receiver_port", QVariant(45455)).toInt();
    m_smipc = new SMIPC(id, send_port, recv_port);
    connect(m_smipc, SIGNAL(resumeApplication()), this, SLOT(resumeApplication()));
    connect(m_smipc, SIGNAL(pauseApplication()), this, SLOT(pauseApplication()));
    connect(m_smipc, SIGNAL(hideWindow()), this, SLOT(hideWindow()));
    connect(m_smipc, SIGNAL(brandName(QString)),this, SLOT(onBrandNameReceived(QString)));

    /* *** TL CONTROLLER *** */
    m_tlController = new TryLiveController(&trylive, getRenderWinId());
    QObject::connect(m_tlController, SIGNAL(trackingStatusChanged(bool)), this, SLOT(onTrackingStatusChanged(bool)));
    QObject::connect(m_tlController, SIGNAL(tooFar()), this, SIGNAL(trackingTooFar()));
    QObject::connect(m_tlController, SIGNAL(tooClose()), this, SIGNAL(trackingTooClose()));
    QObject::connect(m_tlController, SIGNAL(tryLivePaused()), this, SLOT(onTryLivePaused()));
    QObject::connect(m_tlController, SIGNAL(catalogChanged()), this, SIGNAL(catalogChanged()));
    QObject::connect(m_tlController, SIGNAL(catalogChanged()), this, SLOT(onTryLiveRendering()));
    QObject::connect(m_tlController, SIGNAL(loadingAssetsProgressChanged(int, int)), this, SIGNAL(catalogProgressChanged(int, int)));
    QObject::connect(m_tlController, SIGNAL(screenshotTaken(QString, QString)), this, SLOT(onCaptureTaken(QString, QString)));
    QObject::connect(m_tlController, SIGNAL(frameCaptured(QString)), this, SLOT(onCaptureTakenForInference(QString)));
    QObject::connect(m_tlController, SIGNAL(assetLoaded(Product*)), this, SIGNAL(selectedAssetChanged(Product*)));

    /* *** TOUCHLESS *** */
    bool isTouchLess = settings->value("touchless",QVariant(false)).toBool();
    m_touchlessController = new TouchlessController(isTouchLess);
    QObject::connect(m_tlController, SIGNAL(screenshotTaken(QString, QString)), m_touchlessController, SLOT(onCaptureTaken(QString, QString)));
    QObject::connect(m_touchlessController,&TouchlessController::takeSnapshot, [this](){
        this->startCaptureClick();
    });
    QObject::connect(m_touchlessController,&TouchlessController::socketDisconnected, [this](){
        if(m_tlController->isTracking())
            m_touchlessController->connectToWebSocket();
    });
    QObject::connect(m_touchlessController,&TouchlessController::fitBetter, [this](){
        this->fitBetter();
    });

    /* *** FILTER *** */
    m_filter = new Filter();
    connect(m_filter, SIGNAL(genderTypeChanged()), m_tlController, SLOT(applyFilter()));
    connect(m_filter, SIGNAL(frameTypeChanged() ), m_tlController, SLOT(applyFilter()));
    connect(m_filter, SIGNAL(priceLowChanged()  ), m_tlController, SLOT(applyFilter()));
    connect(m_filter, SIGNAL(priceHighChanged() ), m_tlController, SLOT(applyFilter()));
    connect(m_filter, SIGNAL(priceMinChanged()  ), m_tlController, SLOT(applyFilter()));
    connect(m_filter, SIGNAL(priceMaxChanged()  ), m_tlController, SLOT(applyFilter()));
    connect(m_filter, SIGNAL(brandChanged()     ), m_tlController, SLOT(applyFilter()));

    QString orientation = settings->value("screenOrientation", QVariant("horizontal")).toString();
    if(!orientation.compare("vertical",Qt::CaseInsensitive) || !orientation.compare("portrait",Qt::CaseInsensitive))
        orientation="_vertical";
    else
        orientation = "";

    QQuickView *leftQml = new QQuickView();
    leftQml->rootContext()->setContextProperty("mainWindow", this);
    m_leftWidget = QWidget::createWindowContainer(leftQml, this);
    leftQml->setSource(QUrl("qrc:/layout/qml/leftLayout"+orientation+".qml"));

    QQuickView *rightMostQml = new QQuickView();
    rightMostQml->rootContext()->setContextProperty("mainWindow", this);
    bool showPrices = Settings::getInstance()->value("show_prices", "true").toBool();
    rightMostQml->rootContext()->setContextProperty("show_prices", showPrices);
    bool showBrand = Settings::getInstance()->value("show_brand", "true").toBool();
    rightMostQml->rootContext()->setContextProperty("show_brand", showBrand);
    bool showAssetInfo = Settings::getInstance()->value("show_asset_info", "true").toBool();
    rightMostQml->rootContext()->setContextProperty("show_asset_info", showAssetInfo);
    m_rightWidget = QWidget::createWindowContainer(rightMostQml, this);
    rightMostQml->setSource(QUrl("qrc:/layout/qml/rightLayout"+orientation+".qml"));

    QQuickView *botLeftQml = new QQuickView();
    botLeftQml->rootContext()->setContextProperty("mainWindow", this);
    m_botLeftWidget = QWidget::createWindowContainer(botLeftQml, this);
    botLeftQml->setSource(QUrl("qrc:/layout/qml/botLeftLayout"+orientation+".qml"));

    QQuickView *botRightQml = new QQuickView();
    botRightQml->rootContext()->setContextProperty("mainWindow", this);
    botRightQml->rootContext()->setContextProperty("touchlessController", m_touchlessController);
    m_botRightWidget = QWidget::createWindowContainer(botRightQml, this);
    botRightQml->setSource(QUrl("qrc:/layout/qml/botRightLayout"+orientation+".qml"));

    QQuickView *carouselQml = new QQuickView();
    carouselQml->rootContext()->setContextProperty("mainWindow", this);
    carouselQml->rootContext()->setContextProperty("touchlessController", m_touchlessController);
    m_botWidget = QWidget::createWindowContainer(carouselQml, this);
    carouselQml->setSource(QUrl("qrc:/layout/qml/botLayout"+orientation+".qml"));

    QQuickView *captureQml = new QQuickView();
    captureQml->rootContext()->setContextProperty("mainWindow", this);
    captureQml->rootContext()->setContextProperty("touchlessController", m_touchlessController);
    m_captureWidget = QWidget::createWindowContainer(captureQml, this);
    captureQml->setSource(QUrl("qrc:/layout/qml/captureLayout"+orientation+".qml"));
    m_captureWidget->setGeometry(m_captureWidget->geometry().adjusted(-1,-1,1,1));
    m_captureWidget->setVisible(false);

    QQuickView *countdownQml = new QQuickView();
    countdownQml->rootContext()->setContextProperty("mainWindow", this);
    m_countdownWidget = QWidget::createWindowContainer(countdownQml, this);
    countdownQml->setSource(QUrl("qrc:/layout/qml/countdownLayout"+orientation+".qml"));
    m_countdownWidget->setGeometry(m_countdownWidget->geometry().adjusted(-1,-1,1,1));
    m_countdownWidget->setVisible(false);

    //    QQuickView *fitMeBetterQml = new QQuickView();
    //    fitMeBetterQml->rootContext()->setContextProperty("mainWindow", this);
    //    m_fitMeBetter = QWidget::createWindowContainer(fitMeBetterQml, this);
    //    fitMeBetterQml->setSource(QUrl("qrc:/layout/qml/FitBetter.qml"));
    //    m_fitMeBetter->setGeometry(m_fitMeBetter->geometry().adjusted(-1,-1,1,1));
    //    m_fitMeBetter->setVisible(false);

    if (isFitBetterEnabled()){
        m_fitMeBetter = new QWidget(this, Qt::Window | Qt::FramelessWindowHint);
        m_fitMeBetter->setAttribute(Qt::WA_TranslucentBackground);
        m_fitMeBetter->setStyleSheet("background-color:#01FFFFFF");
        QHBoxLayout* fitLayout = new QHBoxLayout();
        QLabel* lb = new QLabel(m_fitMeBetter);
        lb->setMinimumSize(191,126);
        lb->setPixmap(QPixmap(":/resources/blank_btn"));
        QHBoxLayout* insideLayout = new QHBoxLayout();
        ClickableLabel* lbText = new ClickableLabel(m_fitMeBetter);
        lbText->setStyleSheet("color: #696969; font-weight: bold; font: 22px");
        int id = QFontDatabase::addApplicationFont(":/resources/Quicksand-Medium");
        QString family = QFontDatabase::applicationFontFamilies(id).at(0);
        QFont f(family);
        f.setPointSize(22);
        lbText->setFont(f);
        lbText->setText(tr("Fit me\nbetter"));
        lbText->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
        insideLayout->addWidget(lbText);
        lb->setLayout(insideLayout);
        fitLayout->addWidget(lb);
        m_fitMeBetter->setLayout(fitLayout);
        m_fitMeBetter->setGeometry(m_fitMeBetter->geometry().adjusted(-1,-1,1,1));
        m_fitMeBetter->setVisible(false);
        connect(lbText, SIGNAL(clicked()), this, SLOT(fitBetter()));
    }
    else m_fitMeBetter = nullptr;

    /* *** Screen Saver TIMER *** */
    m_screenSaverTimer = new QTimer(this);
    connect(m_screenSaverTimer, SIGNAL(timeout()), this, SLOT(onScreenSaverTimerTriggered()));
    connect(m_screenSaverTimer, SIGNAL(timeout()), m_filter, SLOT(resetFilter()));
    int screenSaverTimeout = settings->value("timeout_millis", QVariant(DEFAULT_TIMER_TIMEOUT)).toInt();
    m_screenSaverTimer->setInterval(screenSaverTimeout);
    m_screenSaverTimer->setSingleShot(false);

    /* *** SHARE TIMER *** */
    m_shareTimer = new QTimer(this);
    connect(m_shareTimer, SIGNAL(timeout()), this, SLOT(onShareTimerTriggered()));
    connect(m_shareTimer, SIGNAL(timeout()), m_filter, SLOT(resetFilter()));
    int shareTimeout = settings->value("share_timeout", QVariant(DEFAULT_TIMER_TIMEOUT)).toInt();
    m_shareTimer->setInterval(shareTimeout);
    m_shareTimer->setSingleShot(true);

    /* *** EMAIL TIMER *** */
    m_emailTimer = new QTimer(this);
    connect(m_emailTimer, SIGNAL(timeout()),this, SLOT(onEmailTimerTriggered()));
    int emailTimeout = settings->value("email_timeout", QVariant(DEFAULT_TIMER_TIMEOUT)).toInt();
    m_emailTimer->setInterval(emailTimeout);
    m_emailTimer->setSingleShot(true);

    /* *** Web Socket Inactivity Timer *** */
    m_webSocketInactivityTimer = new QTimer(this);
    int webSocketTimeout = settings->value("websocket_inactivity_timeout", QVariant(DEFAULT_TIMER_TIMEOUT)).toInt();
    connect(m_webSocketInactivityTimer, &QTimer::timeout, [this](){
        m_touchlessController->disconnectWebSocket();
    });
    m_webSocketInactivityTimer->setInterval(webSocketTimeout);
    m_webSocketInactivityTimer->setSingleShot(true);

    /* *** EMAIL (code order is important) *** */
    QString alternateDir = QCoreApplication::applicationDirPath();
    CConfigXML* xml = CConfigXML::getInstance();
    xml->loadConfigFile("email_config.xml", "", alternateDir);
    xml->save();
    m_emailSettings = new EmailSettings(this);

    /* *** resume application *** */
    QString configState = settings->value("state", QVariant("normal")).toString();
    if(startInPauseState || configState.compare("paused") == 0)
    {
        /* *** just init trylive if app started in pause state. NO CAMERA PLAY (2nd param: false)*** */
        m_tlController->initTryLive(getRenderWinId(), false);
        appState = PAUSED;
    }
    else
    {
        resumeApplication();
        appState = RUNNING;
    }

    m_registration = new CRegistration(REG_DATA_FILE);
    m_registration->setSoftwareBundle(BUNDLE_ID);
    QString sdir = m_registration->getWinWritableDir() + "/EyewearKiosk/";
    QDir ddir;
    if (!ddir.exists(sdir)) ddir.mkpath(sdir);

    m_regDialog = new RegDialog(this);
    m_regDialog->setModal(true);
    m_regDialog->setRegistration(m_registration);
    m_regDialog->resize(1920,1080);
    m_regDialog->move(600, 200);
    connect(m_regDialog, &RegDialog::licenseFinished, this, &MainWindow::onRegFinished);
    m_licenseCode = m_registration->checkLicense();
    if (m_licenseCode != 0 ) {
        qDebug() << "checking .. " << m_licenseCode;
        m_regDialog->showFullScreen();
    }

    qApp->installEventFilter(this);

    /* *** hide cursor timer *** */
    m_hideCursorTimer = new QTimer(this);
    m_hideCursorTimer->setInterval(2000);
    m_hideCursorTimer->setSingleShot(true);
    connect(m_hideCursorTimer, SIGNAL(timeout()), this, SLOT(onHideCursorTriggered()));
    m_hideCursorTimer->start();
    m_trayIcon->show();
}

void MainWindow::onBrandNameReceived(QString brand)
{
    m_filter->setBrand(brand);
}

bool MainWindow::isCarouselVisible()
{
    return Settings::getInstance()->value("show_carousel",0).toBool();
}

int MainWindow::autoScrollInterval()
{
    return Settings::getInstance()->value("auto_scroll_interval",0).toInt();
}

bool MainWindow::resetTrackingByAutoScroll(){
    return Settings::getInstance()->value("reset_tracking_by_auto_scroll", false).toBool();
}

bool MainWindow::isFitBetterEnabled()
{
    return Settings::getInstance()->value("fit_better_enabled",0).toBool();
}

bool MainWindow::isTLTracking()
{
    return m_tlController->isTracking();
}

void MainWindow::fitBetter()
{
    qDebug() << "fitBetter";
    m_tlController->restartTracking();
}

void MainWindow::onRegFinished(int code){
    m_licenseCode = code;
    //    qDebug() << "!!! TL catalog: " << m_tlCatalogLoaded << " licCode:" << m_licenseCode;
    if (code == 0){
        if (m_tlCatalogLoaded){
            m_tlController->pause(false);
            /* *** SHOW WINDOW *** */
            QWindowsWindowFunctions::setWindowActivationBehavior(QWindowsWindowFunctions::AlwaysActivateWindow);
            this->showMaximized();
            this->showFullScreen();
            QWindowsWindowFunctions::setHasBorderInFullScreen(this->windowHandle(), true);

#ifdef Q_OS_WIN
            this->raise();
            raiseWindow(HWND(this->winId()));
#else
            this->raise();// bring on top
#endif
            this->activateWindow();
        }
    }
    else {
        m_tlController->pause(true);
    }
}

void MainWindow::onTryLiveRendering() {
    if (!m_tlCatalogLoaded) m_tlCatalogLoaded = true;
    //    qDebug() << "!! onTryLiveRendering cat:" << m_tlCatalogLoaded << " licCode:" << m_licenseCode;
    if (m_licenseCode!=0) m_tlController->pause(true);
}

void MainWindow::onCaptureTaken(const QString &imagePath, QString origPath)
{
    qDebug() << QString(" MainWindow picture taken and saved at: " + imagePath).toStdString().c_str();

    QSettings* settings = Settings::getInstance();
    int shareTimeout = settings->value("share_timeout", QVariant(30000)).toInt();
    m_shareTimer->start(shareTimeout);

    m_captureWidget->setVisible(true);
    m_countdownWidget->setVisible(false);
    forceResize();

    /* *** generate e-mail image *** */
    createEmailImage(imagePath);

    emit captureTaken(imagePath);
}

void MainWindow::onCaptureTakenForInference(const QString &imagePath)
{
    qDebug() << QString(" MainWindow capture taken and saved at: " + imagePath).toStdString().c_str();

    QImage img(imagePath);
    img = img.convertToFormat(QImage::Format_RGB888);
    QByteArray ba;
    QBuffer buffer(&ba);
    buffer.open(QIODevice::WriteOnly);
    img.save(&buffer, "JPG");
    uchar* buff = (uchar*)malloc(ba.size());
    memcpy(buff,ba.data(),ba.size());
    QSettings* settings = Settings::getInstance();
    int cameraFocal = settings->value("camera/focal", QVariant(200)).toInt();

    QList<QRect> faceRois;
    m_attrDetector->detectFaceAttributes(buff, img.width(), img.height(), 3, faceRois, cameraFocal, ba.size());
}

void MainWindow::onAttributeDetectorFinished(QJsonDocument json)
{
    qDebug() << "audience analysis results: " << json.toJson(QJsonDocument::Compact);
    QJsonArray array = json.object().value("results").toArray();
    if(array.size() > 0){
        QString gender = array.at(0).toObject().value("gender").toString("u");
        m_filter->setGenderType(!gender.compare("f") ? Filter::FEMALE : !gender.compare("m") ? Filter::MALE : Filter::UNISEX );
    }
}

void MainWindow::onHideCursorTriggered()
{
    QApplication::setOverrideCursor(Qt::BlankCursor);
}

void MainWindow::resumeApplication()
{
    qDebug() << QString(" ------------------------------------------- RESUME APPLICATION: " + QString::number(appState == RUNNING)).toStdString().c_str();

    if(appState == RUNNING) return;
    appState = RUNNING;

    /* *** reset status message *** */
    setInfoMessage("");

    /* *** resume TryLive *** */
    m_tlController->pause(false);

    /* *** SHOW WINDOW *** */
    this->showMaximized();
    this->showFullScreen();

    QWindowsWindowFunctions::setHasBorderInFullScreen(this->windowHandle(), true);

#ifdef Q_OS_WIN
    this->raise();
    raiseWindow(HWND(this->winId()));
#else
    this->raise();// bring on top
#endif
    this->activateWindow();
    this->setVisible(true);
    if(m_fitMeBetter)m_fitMeBetter->setVisible(false);
    /* *** restart screen saver timer *** */
    QSettings* settings = Settings::getInstance();
    int screenSaverTimeout = settings->value("timeout_millis", QVariant(DEFAULT_TIMER_TIMEOUT)).toInt();
    m_screenSaverTimer->start(screenSaverTimeout);
    if(m_touchlessController->isTouchless())
        m_webSocketInactivityTimer->start();


    /* *** send SMIPC Resumed -> the recipient's window should hide *** */
    m_smipc->sendMsg(SMIPC::Resumed);
}

void MainWindow::pauseApplication()
{
    qDebug() <<" ------------------ PAUSE APPLICATION: " << (appState == PAUSED);

    if(appState == PAUSED) return;
    appState = PAUSED;

    /* *** pause TryLive *** */
    m_tlController->pause(true);

    /* *** stop screen saver timer *** */
    m_screenSaverTimer->stop();
    m_webSocketInactivityTimer->stop();
    m_touchlessController->disconnectWebSocket();

    /* *** MINIMIZE main window *** */
    /* HIDE WINODW IN hideWindow slot (smoother transition effect between smipc applications) */

    m_tlController->refresh();

    setInfoMessage(tr("Application is in PAUSED state"));

    /* *** smipc send PAUSED must be called after TryLive has stopped ( see onTryLivePaused ) *** */
}

void MainWindow::hideWindow() //called after the other smipc application resumed completly (window on top & visible on the screen)
{
    QThread::msleep(500);

    qDebug() << " ----------------- HIDE WINDOW ";

    /* *** MINIMIZE main window *** */
    /* DO NOT MINIMIZE WINODW (poor ui efficiency)*/
    this->setVisible(false);
    if(m_fitMeBetter)m_fitMeBetter->setVisible(false);
}

void MainWindow::onTrackingStatusChanged(const bool &tracking)
{
    if(tracking)
    {
        m_screenSaverTimer->stop();
        m_webSocketInactivityTimer->stop();
        if(m_touchlessController->isTouchless() && !m_touchlessController->isConnected()){
            m_touchlessController->connectToWebSocket();
        }
        if (m_fitMeBetter) m_fitMeBetter->setVisible(true);
        forceResize();
    }
    else
    {
        QSettings* settings = Settings::getInstance();
        int screenSaverTimeout = settings->value("timeout_millis", QVariant(DEFAULT_TIMER_TIMEOUT)).toInt();
        m_screenSaverTimer->start(screenSaverTimeout);

        if(m_touchlessController->isTouchless())
            m_webSocketInactivityTimer->start();
    }
    emit trackingChanged(tracking);
}

void MainWindow::onTryLivePaused()
{
    qDebug() << " MainWindow TryLive paused";

    if(!m_captureWidget->isVisible())
    {
        /* *** after trylive successfuly paused notify other SMIPC application that this is Paused *** */
        m_smipc->sendMsg(SMIPC::Paused);
    }
}

void MainWindow::onScreenSaverTimerTriggered()
{
    qDebug() << " MainWindow screen saver timer triggered";

    ::AllowSetForegroundWindow(ASFW_ANY);

    m_smipc->sendMsg(SMIPC::WillPause);
}

void MainWindow::onShareTimerTriggered()
{
    qDebug() << " MainWindow share timer triggered";

    /* *** simulate cancel capture *** */
    cancelCaptureClick();
}

void MainWindow::onEmailTimerTriggered()
{
    qDebug()<<" MainWindow email timer triggered";

    cancelCaptureClick();
}

void* MainWindow::getRenderWinId()
{
    return (void*)m_ui->videoWidget->winId();
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    QSize size = event->size();
    size.setHeight(this->geometry().height());
    int leftWidth = 0.22 * size.width();
    int videoWidth = 0.56 * size.width();
    int rightWidth = 0.22 * size.width();

    int videoHeight = 0.80 * size.height();
    int carouselHeight = 0.2 * size.height();

    float countdownYRatio = 0.77;
    QSettings* settings = Settings::getInstance();
    QString orientation = settings->value("screenOrientation", QVariant("horizontal")).toString();
    if(orientation == "vertical" || orientation == "Vertical" ||
            orientation == "portrait" || orientation == "Portrait"){
        countdownYRatio = 0.83;
        leftWidth = 0;
        videoWidth = 0.77 * size.width();
        rightWidth = 0.23 * size.width();
    }

    m_rightWidget->setGeometry(leftWidth+videoWidth, 0, rightWidth, videoHeight);
    m_ui->videoWidget->setGeometry(leftWidth, 0, videoWidth, videoHeight);

    m_leftWidget->setGeometry(0, 0, leftWidth, videoHeight);

    // Bottom widgets : left + carousel + right
    m_botWidget->setGeometry(leftWidth,
                             videoHeight,
                             videoWidth,
                             carouselHeight);

    m_botLeftWidget->setGeometry(0,
                                 videoHeight,
                                 leftWidth,
                                 m_botWidget->height());

    m_botRightWidget->setGeometry(leftWidth + videoWidth,
                                  m_botWidget->y(),
                                  rightWidth,
                                  m_botWidget->height() );

    m_captureWidget->setGeometry(leftWidth,
                                 0,
                                 videoWidth,
                                 size.height());

    m_countdownWidget->setGeometry(m_ui->videoWidget->x(),
                                   m_ui->videoWidget->height()*countdownYRatio,
                                   m_ui->videoWidget->width(),
                                   m_ui->videoWidget->height()*0.17);

    if (m_fitMeBetter) m_fitMeBetter->setGeometry(m_ui->videoWidget->x() + m_ui->videoWidget->width()/2.0 - 100,
                               m_ui->videoWidget->height() - 150,
                               191,
                               126);

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    m_tlController->stop();
    event->accept();
    //::PostQuitMessage(0);
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (    event->type() == QEvent::Wheel  ||
            event->type() == QEvent::MouseMove ||
            event->type() == QEvent::TouchUpdate ||
            event->type() == QEvent::TabletPress ||
            event->type() == QEvent::MouseButtonPress ||
            event->type() == QEvent::InputMethod
            )
    {
        QSettings* settings = Settings::getInstance();
        if(m_screenSaverTimer->isActive())
        {
            int screenSaverTimeout = settings->value("timeout_millis", QVariant(DEFAULT_TIMER_TIMEOUT)).toInt();
            m_screenSaverTimer->start(screenSaverTimeout);
        }

        if(m_shareTimer->isActive())
        {
            int shareTimeout = settings->value("share_timeout", QVariant(20000)).toInt();
            m_shareTimer->start(shareTimeout);
        }

        if (m_emailTimer->isActive()){
            int emailTimeout = settings->value("email_timeout", QVariant(60000)).toInt();
            m_emailTimer->start(emailTimeout);
        }

        /* *** show cursor *** */
        QApplication::setOverrideCursor(Qt::ArrowCursor);
        m_hideCursorTimer->start();
    }
    return QObject::eventFilter(obj, event);
}

QQmlListProperty<Product> MainWindow::catalog()
{
    return QQmlListProperty<Product>(this, *m_tlController->catalog());
}

void MainWindow::skipCatalogClick()
{
    m_tlController->cancelFullAssetLoading();
    emit skipCatalogClicked();
}

void MainWindow::captureClick()
{
    m_tlController->snapshot();
    m_captureAssetReference = m_tlController->currentAsset()->reference();
    if (m_fitMeBetter) m_fitMeBetter->setVisible(false);
    emit captureClicked();
}

void MainWindow::forceResize()
{
    QResizeEvent *ev = new QResizeEvent(QSize(this->geometry().width(),this->geometry().height()),QSize(1,1));
    resizeEvent(ev);
}

void MainWindow::startCaptureClick()
{
    if(m_touchlessController->isTouchless())
        emit hideTouchlessQr();
    QSettings* settings = Settings::getInstance();
    int shareTimeout = settings->value("share_timeout", QVariant(30000)).toInt();
    m_shareTimer->start(shareTimeout);

    m_countdownWidget->setVisible(true);
    if (m_fitMeBetter) m_fitMeBetter->setVisible(false);
    forceResize();

    m_screenSaverTimer->stop();
    m_webSocketInactivityTimer->stop();

    if (m_fitMeBetter) m_fitMeBetter->setVisible(false);
    emit startCaptureClicked();
}

void MainWindow::restartTrackingClick()
{
    m_captureAssetReference = "";
    m_tlController->restartTracking();
    emit restartTrackingClicked();
}

void MainWindow::cancelCaptureClick()
{
    m_captureAssetReference = "";
    if(m_touchlessController->isTouchless())
        emit showTouchlessQr();
    m_shareTimer->stop();
    m_emailTimer->stop();

    //m_tlController->pause(false);
    m_tlController->setToVideoMode();
    if (m_fitMeBetter) m_fitMeBetter->setVisible(true);
    m_captureWidget->setVisible(false);
    m_countdownWidget->setVisible(false);
    m_screenSaverTimer->start();
    if(m_touchlessController->isTouchless())
        m_webSocketInactivityTimer->start();

    if (m_fitMeBetter) m_fitMeBetter->setVisible(true);
    emit cancelCaptureClicked();
}

void MainWindow::shareClick()
{
    m_shareTimer->stop();

    generateQR();

    emit shareClicked();

    m_emailTimer->start();
}

void MainWindow::resetEmailTimer()
{
    qDebug()<<"MainWindow: reset email timer";
    m_emailTimer->stop();
    m_emailTimer->start();
}

void MainWindow::sendEmail(const QString& destAddr)
{
    m_shareTimer->start();

    m_emailSettings->reload();
    m_emailSettings->setDestAddr(destAddr);
    //    m_emailSettings->setAttachmentFiles(QStringList() << m_tlController->getScreenshotPathFramed());
    m_emailSettings->setAttachmentFiles(QStringList() << m_tlController->getScreenshotPath());
    m_emailSettings->save(true);
    bool ret = m_emailSettings->sendEmail();

    if(!ret)
    {
        QSettings* settings = Settings::getInstance();
        int shareTimeout = settings->value("share_timeout", QVariant(30000)).toInt();
        m_shareTimer->start(shareTimeout);
    }

    emit emailSent(destAddr, ret, m_emailSettings->mailErrorMessage());
    m_emailTimer->stop();
}

Product* MainWindow::selectedAsset()
{
    return m_tlController->currentAsset();
}

void MainWindow::setSelectedAsset(Product *p)
{
    m_tlController->changeAsset(p);
    //    emit selectedAssetChanged(p); // !! do not emit selectedAssetChanged here!! (see connect(m_tlController, SIGNAL(assetLoaded(Product*)), this, SIGNAL(selectedAssetChanged(Product*)));)
    //                                     [qml]--->(main)controller.changeAsset->(controller)emit assetChanged->(main)emit selectedAssetChanged()->[qml - onSelectedAssetChanged]
    //                                  This workaround is done such that the programatically calls of changeAsset() should reflect also in qml thus connect
    //                                  assetLoaded from controller to selectedAssetChanged from main
}

int MainWindow::getVisibleCatalogLength()
{
    return m_tlController->getVisibleCatalogLength();
}

Filter* MainWindow::filter()
{
    return m_filter;
}

QString MainWindow::infoMessage()
{
    return m_infoMessage;
}

void MainWindow::setInfoMessage(QString im)
{
    m_infoMessage = im;
    emit infoMessageChanged();
}

MainWindow::~MainWindow()
{
    delete m_touchlessController;
    delete m_ui;
}

const QString MainWindow::createEmailImage(const QString& imagePath)
{
    QSettings* settings = Settings::getInstance();

    QImage frameImage;
    if(QFile::exists("email_topper.png"))
        frameImage.load("email_topper.png");
    else
        frameImage.load(":/resources/resources/email_topper.png");

    QImage capturedImage(imagePath);
    QPixmap frameImagePixmap(frameImage.width(),frameImage.height());

    QRect draw_image_rect;
    int frame_x = settings->value("email_share_frame_dimensions/frame_x", 0).toInt();
    int frame_y = settings->value("email_share_frame_dimensions/frame_y", 0).toInt();
    int frame_height = settings->value("email_share_frame_dimensions/frame_height", 0).toInt();
    int frame_width = settings->value("email_share_frame_dimensions/frame_width", 0).toInt();
    float frame_ratio = frame_width/(float)frame_height;
    float img_ratio = capturedImage.width()/(float)capturedImage.height();

    if(frame_ratio > 1)
    {
        int draw_img_h = frame_width / img_ratio;

        int diff = abs( (frame_y/2) - (capturedImage.height()/2) );

        draw_image_rect = QRect(frame_x, frame_y/*- (diff/2)*/, frame_width, draw_img_h);
    }
    else
    {
        int draw_img_w = frame_width / img_ratio;

        int diff = abs( (frame_x/2) - (capturedImage.width()/2) );

        draw_image_rect = QRect(frame_x/*- (diff/2)*/, frame_y, draw_img_w, frame_height);
    }

    QPainter frameImagePainter(&frameImagePixmap);
    frameImagePainter.setRenderHint(QPainter::Antialiasing, true);
    frameImagePainter.setRenderHint(QPainter::TextAntialiasing, true);
    frameImagePainter.setRenderHint(QPainter::SmoothPixmapTransform, true);
    frameImagePainter.fillRect(frameImage.rect(), QColor(255,0,255));
    frameImagePainter.drawImage(draw_image_rect, capturedImage);
    frameImagePainter.drawImage(frameImage.rect(), frameImage);
    frameImagePainter.end();

    QString imagePathFramed = imagePath.split('.')[0] + "-framed." + imagePath.split('.')[1];
    frameImagePixmap.save(imagePathFramed);
    qDebug() << QString(" MainWindow framed image saved at: " + imagePathFramed).toStdString().c_str();

    return imagePathFramed;
}

void MainWindow::generateQR(){
    QStringList standardLocations = QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation);
    QString imagePath = Eyewear::getDataDir() + "/" +
            (m_captureAssetReference.isEmpty() ? m_tlController->currentAsset()->reference() : m_captureAssetReference )
            + ".jpg";
    QString qrPath = Eyewear::getDataDir() + "/qr.png";

    // Creat directory in AppData if it doesn't exist
    QString dirPathLogo = standardLocations.at(0) + "/TL/";
    QDir dir(dirPathLogo);
    if (!dir.exists()) dir.mkdir(".");

    QString imagePathLogo = dirPathLogo + "screenshotLogo.jpg";
    QImage base(imagePath);
    QFileInfo baseInfo(imagePath);
    if (!base.save(imagePathLogo)){
        qDebug() << "!!~~ Error saving image as" << imagePathLogo << "~~"
                 << imagePath << QFile::exists(imagePath) << baseInfo.size() << base.size() << m_captureAssetReference;
        return;
    }

    Utils::addLogoOnImage(imagePathLogo);

    if(QFile::exists(imagePathLogo) && !qrPath.isEmpty()){
        QString uuid = QUuid::createUuid().toString();
        QString photoKey = uuid + ".jpg";
        QString photoOrigKey = uuid+ "_orig.jpg";
        AmazonQR aQr;
        QString url = aQr.uploadImage(imagePathLogo,false,photoKey);
        if(!url.isEmpty()){
            if(aQr.generateQR(url,qrPath)){
                qDebug() << QString(" MainWindow upload/generate QR process finished. SUCCESS! qr image saved to specified path. ").toStdString().c_str();
                emit qrImageReady(qrPath);

                std::string origPhoto = Eyewear::getTempDir().toStdString() + "/photo_shot.jpg";
                QString origPhotoPath = QString(origPhoto.c_str());
                if(QFile::exists(origPhotoPath)){
                    QString url_orig = aQr.uploadImage(origPhotoPath,false,photoOrigKey);
                    qDebug()<<" MainWindow: uploaded original photo:"<<url_orig;
                }
            }else{
                qDebug() << QString(" MainWindow upload/generate QR process finished. FAILED! can't generate QR CODE. ").toStdString().c_str();
                emit qrImageReady("");
            }
        }else{
            qDebug() << QString(" MainWindow upload/generate QR process finished. FAILED! can't upload photo. ").toStdString().c_str();
            emit qrImageReady("");
        }
    }else{
        qDebug() << QString(" MainWindow upload/generate QR process finished. FAILED! inexistent photo or qrPath is empty. ").toStdString().c_str();
        emit qrImageReady("");
    }
}

bool MainWindow::isSmartMirrorAvailable()
{
    return QFile::exists(QString(SMART_MIRROR_PATH) + "ACEP_Launcher.exe");
}

QStringList MainWindow::getProcessesList()
{
    QStringList runningProcesses;
#ifdef Q_OS_WIN
    DWORD aProcesses[1024], cbNeeded, cProcesses;
    if(!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded)){
        return runningProcesses;
    }

    cProcesses = cbNeeded/sizeof(DWORD);

    for(unsigned int i=0; i<cProcesses;i++){
        if(aProcesses[i]!=0){
            TCHAR processName[MAX_PATH] = TEXT("<unknown>");
            HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, aProcesses[i]);
            if(NULL != hProcess){
                HMODULE hMod;
                DWORD cbneeded;
                if(EnumProcessModules(hProcess,&hMod,sizeof(hMod),&cbneeded)){
                    GetModuleBaseName(hProcess,hMod,processName, sizeof(processName)/sizeof(TCHAR));
                }
            }
            CloseHandle(hProcess);
            runningProcesses.append(QString::fromWCharArray(processName));
        }
    }
#endif
    return runningProcesses;
}

#include <process.h>
void MainWindow::startSimulations(){
    qDebug() << "!!!startSimulations: " << isSmartMirrorAvailable();
    QString cmd = QString(SMART_MIRROR_PATH) + "ACEP_Launcher.exe";

    if(getProcessesList().contains("ACEP_Launcher.exe")){
        HWND windowHandle = FindWindowA(NULL,"ACEP Simulations");
        qDebug()<<"RaiseWindow:"<<SetForegroundWindow(windowHandle);
    }else{
        if (QFile::exists(cmd)){
            QStringList arg;
            arg << "-sim";
            connect(&m_pr, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onSimulationsFinished(int, QProcess::ExitStatus)));
            m_pr.setWorkingDirectory(SMART_MIRROR_PATH);
            m_screenSaverTimer->stop();
            m_emailTimer->stop();
            m_pr.start(cmd, arg);
        }
    }
}

void MainWindow::onSimulationsFinished(int exitCode, QProcess::ExitStatus exitStatus){
    qDebug() << "!!!!!! sims exitCode:" << exitCode << exitStatus;
    m_screenSaverTimer->start();
}

void MainWindow::createActions(){
    m_minimizeAction = new QAction(tr("&Pause"), this);
    connect(m_minimizeAction, &QAction::triggered, this, &MainWindow::pauseApplication);
    connect(m_minimizeAction, &QAction::triggered, this, &MainWindow::hideWindow);

    m_restoreAction = new QAction(tr("&Restore"), this);
    connect(m_restoreAction, &QAction::triggered, this, &MainWindow::resumeApplication);

    m_logsAction = new QAction(tr("&Logs"), this);
    connect(m_logsAction, &QAction::triggered, this, &MainWindow::showLogs);

    m_settingsAction = new QAction(tr("&Settings"), this);
    connect(m_settingsAction, &QAction::triggered, this, &MainWindow::showSettings);

    m_quitAction = new QAction(tr("&Quit"), this);
    connect(m_quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);
}

void MainWindow::createTrayIcon(){
    m_trayIconMenu = new QMenu(this);
    m_trayIconMenu->addAction(m_minimizeAction);
    m_trayIconMenu->addAction(m_restoreAction);
    m_trayIconMenu->addSeparator();
    m_trayIconMenu->addAction(m_logsAction);
    m_trayIconMenu->addAction(m_settingsAction);
    m_trayIconMenu->addSeparator();
    m_trayIconMenu->addAction(m_quitAction);
    m_trayIconMenu->setStyleSheet("background-color: lightgray;");

    m_trayIcon = new QSystemTrayIcon(this);
    m_trayIcon->setContextMenu(m_trayIconMenu);
    m_trayIcon->setIcon(QIcon(":/resources/app_icon"));
    m_trayIcon->setVisible(true);
    m_trayIcon->show();
}

void MainWindow::showLogs(){
    qDebug() << "showLogs";
}

#include "settings/cconfigdialog.h"
void MainWindow::showSettings(){
    qDebug() << "showSettings";
    m_configDialog = new CConfigDialog(APP_CONFIG_FILEPATH);
    m_configDialog->show();
}

void MainWindow::showTrayMessage(QString msg, QString title){
    if (title.isEmpty()) title = "Eyewear Kiosk";
    if (m_trayIcon) m_trayIcon->showMessage(title, msg, QIcon(":/resources/app_icon"), 10000);
}
