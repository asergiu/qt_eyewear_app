#include "capplication.h"
#include <QQuickItem>
#include <QDateTime>
#include <QWidget>

#ifdef QT_DEBUG
#define DEBUGS 4
#else
#define DEBUGS 2
#endif
#include "settings.h"
#include "settings/ddebug.h"
#include "buildversion.h"

#include <QLocalServer>
#include <QLocalSocket>
#include <QProcess>
#include <QTimer>

CApplication::CApplication(int &argc, char **argv) : QApplication(argc, argv)
{
    m_serverSingleApp = NULL;
    m_socketSingleApp = NULL;
    m_windowSingleApp = NULL;
    m_updateTimer = NULL;
#ifdef Q_OS_WIN
        QString serverName = "EyewearKiosk";
        m_socketSingleApp = new QLocalSocket();
        m_socketSingleApp->connectToServer(serverName);
        if(m_socketSingleApp->waitForConnected(1000)){
            m_socketSingleApp->close();
            qDebug() << "!!! !!! already started !!! !!!";
            ::exit(EXIT_SUCCESS); // Terminate the program using STDLib's exit function
        }
        else {
            // If the connection is insuccessful, this is the main process - So we create a Local Server
            m_serverSingleApp = new QLocalServer();
            m_serverSingleApp->removeServer(serverName);
            m_serverSingleApp->setSocketOptions(QLocalServer::WorldAccessOption);
            m_serverSingleApp->listen(serverName);
            QObject::connect(m_serverSingleApp, SIGNAL(newConnection()), this, SLOT(slotConnectionEstablished()));
        }
#endif
}

CApplication::~CApplication(){
    if (m_serverSingleApp) { delete m_serverSingleApp; m_serverSingleApp = NULL; }
    if (m_socketSingleApp) { delete m_socketSingleApp; m_socketSingleApp = NULL; }
}

void CApplication::slotConnectionEstablished()
{
    m_serverSingleApp->nextPendingConnection();
    if (m_windowSingleApp){
        m_windowSingleApp->show();
        m_windowSingleApp->activateWindow();
//        m_windowSingleApp->requestActivate();
    }
}

void CApplication::connectQWindow(QWidget* win){
    m_windowSingleApp = win;
}

void CApplication::checkForUpdate(QString app_name, QString app_bundle, QString channel, QString deviceid){
    if (QFile::exists("../../.git")) return;

    if (!m_updateTimer){
        m_updateTimer = new QTimer(this);
        m_updateTimer->setInterval(3 * 3600 * 1000); // 3 hours
        connect(m_updateTimer, SIGNAL(timeout()), this, SLOT(checkForUpdate()));
        m_updateTimer->start();
    }

    if (!app_name.isEmpty()) m_app_name = app_name;
    if (!app_bundle.isEmpty()) m_app_bundle = app_bundle;
    if (!channel.isEmpty()) m_channel = channel;
    if (!deviceid.isEmpty()) m_deviceid = deviceid;

    QString updaterName = "UpdateAssistant.exe";
    QFile newUpdater(updaterName + ".new");
    QFile oldUpdater(updaterName);
    if(newUpdater.exists()){
        if(oldUpdater.exists()) oldUpdater.remove();
        newUpdater.rename(updaterName);
    }
    QFile file(updaterName);
    if(file.exists()){
        QStringList arguments;
        QString buildVersion = BUILD_VERSION;
        QString branch = BUILD_BRANCH;
        if(buildVersion.startsWith("v")) buildVersion.replace(0,1,"");
        QStringList versionSplit = buildVersion.split("-");
        if(versionSplit.length()>2) buildVersion.replace("-"+versionSplit[versionSplit.length()-1],"");
        arguments <<"--br"<<branch<<"-b"<<m_app_bundle<<"-v"<<buildVersion<<"-n"<<m_app_name<<"-s 2";

        if(!m_channel.isEmpty())  arguments<<"-c"<<m_channel;
        if(!m_deviceid.isEmpty()) arguments<<"-d"<<m_deviceid;

        Settings* m_st = Settings::instance();
        arguments << "-l" << FL_V("language","en_US");

        QProcess p;
        qDebug()<<"[Updater]Starting updater:"<<file.fileName()<<" with args:"<<arguments;
        p.startDetached(file.fileName(),arguments);

    }else {
        qDebug()<<"[Updater]Update Assistant does not exist!";
    }
}
