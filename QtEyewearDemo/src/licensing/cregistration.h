#ifndef CREGISTRATION_H
#define CREGISTRATION_H

#include <QString>
#include <QObject>
#include <QNetworkAccessManager>

class QNetworkReply;
class QNetworkAccessManager;
class SystemUtils;
class NetworkUtils;

class CRegistration : public QObject
{
    Q_OBJECT
public:
    CRegistration(QString dataFileName);
    int checkLicense();
    Q_INVOKABLE void checkCredentials(QString login, QString password);
    Q_INVOKABLE void cancelActiveRequest();
    void setSoftwareBundle(QString bundleId);

    QString getWinWritableDir();

private slots:
    void networkRequestFinished(QNetworkReply *reply, QString id);

signals:
    void licenseValidationResult(bool valid);
    void credentialsCheckFinished(int code);

private:
    enum EncryptDecrypt { ENCRYPT, DECRYPT };
    QString m_login;
    QString m_password;
    QString m_uuid;
    int m_crtRuns;
    uint m_expiryTimestamp;
    qint64 m_lastRunTimestamp;
    QString m_dataFile;
    QString m_bundleId;
    SystemUtils *m_sysUtils;
    NetworkUtils *m_netUtils;
    bool m_credentialsCheckInProgress;
    QNetworkReply *m_currentNetReply;

    static const QString HOST_NAME;
    static const QString ACCOUNT_URL;
    static const QString EXPIRATION_DATE_URL;
    static const QString AUTHORIZATION_URL;
    static const QString SOFT_REQUEST_URL;
    static const QString LICENSE_TYPE_URL;
    static const QString RESET_SESSION_URL;
    static const int MAX_RUNS;

    QVariant getJsonValue(QByteArray json, QString key);


    void setAppDataFile(QString dataFileName);
    bool readStoredValues();
    bool writeStoredValues();
    int checkExpirationDateSync();
    int loginUserSync(QString login, QString password);
    int checkAuthorizationSync(QString uuid);
    int checkSoftwareBundleSync();
    void loginUserAsync();
    void checkExpirationDateAsync();
    void checkAuthorizationAsync();
    void checkSoftwareBundleAsync();
    void checkLicenseTypeAsync();
    void logoutUserAsync();
    QByteArray endecrypt(const QByteArray data, EncryptDecrypt encDec, QByteArray keyStr);
    void signalFinishedChecking(int status, bool valid);
};

#endif // CREGISTRATION_H
