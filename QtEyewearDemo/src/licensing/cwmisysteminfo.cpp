#include "cwmisysteminfo.h"
#include <QByteArray>
#include <QCoreApplication>
#include <qdebug.h>

#define _WIN32_DCOM
#include <iostream>
#include <WbemIdl.h>
using namespace std;


#pragma comment(lib, "wbemuuid.lib")

CWMISystemInfo *CWMISystemInfo::instance = NULL;

CWMISystemInfo::CWMISystemInfo()
{
    m_locator = NULL;
    m_services = NULL;
    QCoreApplication *app = QCoreApplication::instance();
    string className = string(app->metaObject()->className());
    initCom = className != "QApplication" && className != "CApplication";
    initWMISystem();
}

QString CWMISystemInfo::getMacAddress()
{
//    string queryMacAddr = "SELECT * FROM Win32_NetworkAdapter where PhysicalAdapter=true";
    string queryMacAddr = "select * from Win32_NetworkAdapter where PhysicalAdapter=true and MACAddress!='' and not(serviceName like '%VMnet%') and not(Name like '%Bluetooth%') and not(ServiceName like 'tap%')";
    string macAddr = getWmiField(queryMacAddr, L"MACAddress");
    return QString(macAddr.c_str());
}

QString CWMISystemInfo::getDiskSerialNumber()
{
    string queryDiskSN = "SELECT * FROM Win32_DiskDrive WHERE InterfaceType!='USB' AND InterfaceType!='1394'";
    string diskSN = getWmiField(queryDiskSN, L"SerialNumber");
    return QString(diskSN.c_str());
}

QString CWMISystemInfo::getWindowsSerialNumber()
{
    string queryWindowsSN = "SELECT * FROM Win32_OperatingSystem";
    string windowsSN = getWmiField(queryWindowsSN, L"SerialNumber");
    return QString(windowsSN.c_str());
}

QString CWMISystemInfo::getWindowsVersion()
{
    string queryWindowsSN = "SELECT * FROM Win32_OperatingSystem";
    string windowsSN = getWmiField(queryWindowsSN, L"Version", false);
    return QString(windowsSN.c_str());
}

QString CWMISystemInfo::getComputerSystemModel(){
#ifdef Q_OS_WIN
    string q = "select * from Win32_ComputerSystem";
    string r = getWmiField(q, L"MODEL", false);
    return QString(r.c_str());
#endif
}

QString CWMISystemInfo::getComputerSystemManufacturer(){
    string q = "select * from Win32_ComputerSystem";
    string r = getWmiField(q, L"MANUFACTURER", false);
    return QString(r.c_str());
}

QString CWMISystemInfo::getComputerName(){
    string q = "select * from Win32_ComputerSystem";
    string r = getWmiField(q, L"Name", false);
    return QString(r.c_str());
}

CWMISystemInfo *CWMISystemInfo::getInstance()
{
    if (!instance)
        instance = new CWMISystemInfo();
    return instance;
}

CWMISystemInfo::~CWMISystemInfo()
{
    releaseWMISystem();
}

bool CWMISystemInfo::initWMISystem()
{
    HRESULT hres;

    // Step 1: --------------------------------------------------
    // Initialize COM. ------------------------------------------

    if (initCom) {
        hres =  CoInitializeEx(0, COINIT_MULTITHREADED);
        if (FAILED(hres))
        {
            qDebug() << "Failed to initialize COM library. Error code = 0x"
                << hex << hres << endl;
            return 1;                  // Program has failed.
        }

        // Step 2: --------------------------------------------------
        // Set general COM security levels --------------------------
        // Note: If you are using Windows 2000, you need to specify -
        // the default authentication credentials for a user by using
        // a SOLE_AUTHENTICATION_LIST structure in the pAuthList ----
        // parameter of CoInitializeSecurity ------------------------

        hres =  CoInitializeSecurity(
                    NULL,
                    -1,                          // COM authentication
                    NULL,                        // Authentication services
                    NULL,                        // Reserved
                    RPC_C_AUTHN_LEVEL_DEFAULT,   // Default authentication
                    RPC_C_IMP_LEVEL_IMPERSONATE, // Default Impersonation
                    NULL,                        // Authentication info
                    EOAC_NONE,                   // Additional capabilities
                    NULL                         // Reserved
                    );


        if (FAILED(hres))
        {
            qDebug() << "Failed to initialize security. Error code = 0x"
                 << hex << hres << endl;
            CoUninitialize();
            return false;                    // Program has failed.
        }
    }

    // Step 3: ---------------------------------------------------
    // Obtain the initial locator to WMI -------------------------

    hres = CoCreateInstance(
                CLSID_WbemLocator,
                0,
                CLSCTX_INPROC_SERVER,
                IID_IWbemLocator, (LPVOID *) &m_locator);

    if (FAILED(hres))
    {
        qDebug() << "Failed to create IWbemLocator object."
             << " Err code = 0x"
             << hex << hres << endl;
        return false;                 // Program has failed.
    }

    // Step 4: -----------------------------------------------------
    // Connect to WMI through the IWbemLocator::ConnectServer method

    // Connect to the root\cimv2 namespace with
    // the current user and obtain pointer pSvc
    // to make IWbemServices calls.
    hres = m_locator->ConnectServer(
                _bstr_t(L"ROOT\\CIMV2"), // Object path of WMI namespace
                NULL,                    // User name. NULL = current user
                NULL,                    // User password. NULL = current
                0,                       // Locale. NULL indicates current
                NULL,                    // Security flags.
                0,                       // Authority (for example, Kerberos)
                0,                       // Context object
                &m_services                    // pointer to IWbemServices proxy
                );

    if (FAILED(hres))
    {
        qDebug() << "Could not connect. Error code = 0x"
             << hex << hres << endl;
        m_locator->Release();
        return false;                // Program has failed.
    }

    // Step 5: --------------------------------------------------
    // Set security levels on the proxy -------------------------

    hres = CoSetProxyBlanket(
                m_services,                        // Indicates the proxy to set
                RPC_C_AUTHN_WINNT,           // RPC_C_AUTHN_xxx
                RPC_C_AUTHZ_NONE,            // RPC_C_AUTHZ_xxx
                NULL,                        // Server principal name
                RPC_C_AUTHN_LEVEL_CALL,      // RPC_C_AUTHN_LEVEL_xxx
                RPC_C_IMP_LEVEL_IMPERSONATE, // RPC_C_IMP_LEVEL_xxx
                NULL,                        // client identity
                EOAC_NONE                    // proxy capabilities
                );

    if (FAILED(hres))
    {
        qDebug() << "Could not set proxy blanket. Error code = 0x"
             << hex << hres << endl;
        m_services->Release();
        m_locator->Release();
        return false;               // Program has failed.
    }
    return true;
}

string CWMISystemInfo::getWmiField(string query, LPCWSTR field, bool checkFieldContent)
{
    // Step 6: --------------------------------------------------
    // Use the IWbemServices pointer to make requests of WMI ----

    if (!m_services) {
        qDebug() << "WMI System is not initialized (initWMISystem() not called before?)";
        return "";
    }

    HRESULT hres;
    IEnumWbemClassObject* pEnumerator = NULL;
    hres = m_services->ExecQuery(
                bstr_t("WQL"),
                bstr_t(query.c_str()),
                WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
                NULL,
                &pEnumerator);

    if (FAILED(hres))
    {
        qDebug() << "Query failed." << " Error code = 0x" << hex << hres;
        m_services->Release();
        m_locator->Release();
        CoUninitialize();
        return "";               // Program has failed.
    }

    if(pEnumerator == NULL){
        qDebug()<<"Warning in ::getWmiField! pEnumerator is NULL. return empty string.";
        return "";
    }

    // Step 7: -------------------------------------------------
    // Get the data from the query in step 6 -------------------

    IWbemClassObject *pclsObj;
    ULONG uReturn = 0;
    string requestedValue;

    hres = pEnumerator->Next(WBEM_INFINITE, 1,
                                   &pclsObj, &uReturn);

    if (FAILED(hres)){
        qDebug() << "Warning! pEnumerator->Next error." << " Error code = 0x" << hex << hres << ". return empty string.";
        return "";
    }

    if(pclsObj == NULL){
        qDebug() << "Warning in ::getWmiField! pclsObj NULL value. return empty string";
        return "";
    }

    VARIANT vtProp;

    // Get the value of the requested property
    hres = pclsObj->Get(field, 0, &vtProp, 0, 0);

    if (FAILED(hres)){
        qDebug() << "Warning! pclsObj->Get error." << " Error code = 0x" << hex << hres << ". return empty string.";
        return "";
    }

    _bstr_t bs(vtProp.bstrVal);
    if(vtProp.bstrVal == NULL){
        qDebug() << "Warning in ::getWmiField! vtProp.bstrlVal NULL value. return empty string";
        return "";
    }
    requestedValue = string(static_cast<const char*>(bs));
    if(checkFieldContent){
        QString s = QString::fromStdString(requestedValue);
        s = s.trimmed();
        requestedValue = s.toStdString();
        if (requestedValue.find_first_not_of("-:0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz") != std::string::npos) {
            cout << "WMI: invalid value: " << requestedValue << "; will set it to empty" << endl;
            requestedValue = "";
        }
    }

    VariantClear(&vtProp);

    pclsObj->Release();
    pEnumerator->Release();

    return requestedValue;
}

void CWMISystemInfo::releaseWMISystem() {
    // Cleanup
    // ========
    m_services->Release();
    m_locator->Release();
    if (initCom)
        CoUninitialize();
}
