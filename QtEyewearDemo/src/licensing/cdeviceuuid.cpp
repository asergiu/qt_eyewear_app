#include "cdeviceuuid.h"
#include <QCryptographicHash>

#ifdef Q_OS_WIN
#include "cwmisysteminfo.h"
#endif

QString CDeviceUUID::getDeviceUUID(){
#ifdef Q_OS_WIN
    CWMISystemInfo *sysInfo = CWMISystemInfo::getInstance();
    QCryptographicHash cryptoHash(QCryptographicHash::Md5);
    cryptoHash.addData(sysInfo->getWindowsSerialNumber().toUtf8());
    cryptoHash.addData(sysInfo->getMacAddress().toUtf8());
    cryptoHash.addData(sysInfo->getDiskSerialNumber().toUtf8());
    return QString(cryptoHash.result().toHex());
#else
    return "";
#endif
}

QString CDeviceUUID::getProductName(){
#ifdef Q_OS_WIN
    CWMISystemInfo *sysInfo = CWMISystemInfo::getInstance();
    return "W"+sysInfo->getWindowsVersion()+"-"+sysInfo->getWindowsSerialNumber()+"--"+sysInfo->getMacAddress();
#else
    return "";
#endif
}
