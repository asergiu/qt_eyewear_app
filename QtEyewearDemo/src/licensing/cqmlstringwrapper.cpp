#include "cqmlstringwrapper.h"

CQmlStringWrapper::CQmlStringWrapper() : m_content("")
{
}

CQmlStringWrapper::CQmlStringWrapper(QString content) : m_content(content)
{
}

QString CQmlStringWrapper::getContent()
{
    return m_content;
}

void CQmlStringWrapper::setContent(QString content)
{
    m_content = content;
    emit contentChanged(content);
}
