#ifndef CDEVICEUUID_H
#define CDEVICEUUID_H

#include <QString>

class CDeviceUUID
{
public:
    static QString getDeviceUUID();
    static QString getProductName();
};

#endif // CDEVICEUUID_H
