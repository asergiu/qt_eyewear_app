#include "networkutils.h"
#include <QNetworkProxyFactory>
#include <QEventLoop>
#include "cqmlstringwrapper.h"

NetworkUtils *NetworkUtils::m_instance = NULL;

NetworkUtils *NetworkUtils::getInstance()
{
    if (!m_instance) {
        m_instance = new NetworkUtils();
    }
    return m_instance;
}

NetworkUtils::NetworkUtils(QObject *parent) : QObject(parent)
{
    QList<QNetworkProxy> listOfProxies = QNetworkProxyFactory::systemProxyForQuery();
    if (listOfProxies.length())
        m_networkManager.setProxy(listOfProxies[0]);
    connect(&m_networkManager, SIGNAL(finished(QNetworkReply *)), this, SLOT(onNetworkRequestFinished(QNetworkReply*)));
}

NetworkUtils::~NetworkUtils()
{
}

QNetworkReply *NetworkUtils::makeSyncRequest(QUrl url)
{
    QNetworkReply *reply = m_networkManager.get(QNetworkRequest(url));
    // make a synchronous request
    QEventLoop eventLoop;
    disconnect(&m_networkManager, SIGNAL(finished(QNetworkReply *)), 0, 0);
    connect(&m_networkManager, SIGNAL(finished(QNetworkReply *)), &eventLoop, SLOT(quit()));
    eventLoop.exec();
    while (reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 302) { // there was a redirect
        QUrl newUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
        delete reply;
        reply = m_networkManager.get(QNetworkRequest(newUrl));
        eventLoop.exec();
    }
    connect(&m_networkManager, SIGNAL(finished(QNetworkReply *)), this, SLOT(onNetworkRequestFinished(QNetworkReply*)));
    return reply;
}

QNetworkReply *NetworkUtils::makeAsyncRequest(QUrl url, QString id)
{
//    qDebug() << "make async request to " << url << " with id: " << id;
    QNetworkRequest request = QNetworkRequest(url);
    CQmlStringWrapper *requestId = new CQmlStringWrapper(id);
    request.setOriginatingObject(requestId);
    return m_networkManager.get(request);
}

void NetworkUtils::onNetworkRequestFinished(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError) {
        qDebug()<<"network request finished with error: "<<reply->errorString();
        goto end;
    }

    if (reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 302) { // there was a redirection
        QUrl newUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
        QString requestId = ((CQmlStringWrapper*)reply->request().originatingObject())->getContent();
        delete reply->request().originatingObject();
        delete reply;
        makeAsyncRequest(newUrl, requestId);
        return;
    }

end:
    QString id = QString(((CQmlStringWrapper*)reply->request().originatingObject())->getContent());
    delete reply->request().originatingObject();
//    qDebug() << "netutils: network request finished " << id;
    emit networkRequestFinished(reply, id);
}
