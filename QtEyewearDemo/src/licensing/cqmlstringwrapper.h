#ifndef CQMLSTRINGWRAPPER_H
#define CQMLSTRINGWRAPPER_H

#include <QObject>

// QString wrapper used to modify the value of a context property from QML
class CQmlStringWrapper : public QObject
{
    Q_OBJECT

public:
    Q_PROPERTY(QString Content READ getContent WRITE setContent NOTIFY contentChanged)

    CQmlStringWrapper();
    CQmlStringWrapper(QString content);
    QString getContent();
    void setContent(QString content);

signals:
    void contentChanged(QString content);

private:
    QString m_content;
};

#endif // CQMLSTRINGWRAPPER_H
