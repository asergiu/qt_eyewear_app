#ifndef CWMISYSTEMINFO_H
#define CWMISYSTEMINFO_H

#include <comdef.h>
#include <QString>

class IWbemLocator;
class IWbemServices;

class CWMISystemInfo
{
public:
    QString getMacAddress();
    QString getDiskSerialNumber();
    QString getWindowsSerialNumber();
    QString getWindowsVersion();
    QString getComputerSystemModel();
    QString getComputerSystemManufacturer();
    QString getComputerName();

    static CWMISystemInfo *getInstance();
    ~CWMISystemInfo();
private:
    IWbemLocator *m_locator;
    IWbemServices *m_services;
    std::string getWmiField(std::string, LPCWSTR field, bool checkFieldContent=true);
    CWMISystemInfo();
    bool initWMISystem();
    void releaseWMISystem();
    bool initCom;
    static CWMISystemInfo *instance;
};

#endif // WMISYSTEMINFO_H
