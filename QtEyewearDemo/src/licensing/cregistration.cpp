#include "cregistration.h"
#include "cdeviceuuid.h"
#include <QDataStream>
#include <QFile>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkProxy>
#include <QNetworkProxyFactory>
#include <QStringList>
#include <QDir>
#include <QSettings>
#include <QApplication>
#include <QCryptographicHash>
#include "networkutils.h"

#include "cryptlib.h"
using CryptoPP::Exception;
#include "hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;
#include "filters.h"
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;
#include "aes.h"
using CryptoPP::AES;
#include "ccm.h"
using CryptoPP::CBC_Mode;
#include "osrng.h"
using namespace CryptoPP;


const QString CRegistration::HOST_NAME = "license.opticvideo.com";
const QString CRegistration::ACCOUNT_URL = "https://%1/account?login=%2&password=%3";
const QString CRegistration::EXPIRATION_DATE_URL = "https://%1/checkExpirationDate";
const QString CRegistration::AUTHORIZATION_URL = "https://%1/checkAuthorization?uuid=%2";
const QString CRegistration::SOFT_REQUEST_URL = "https://%1/softRequest";
const QString CRegistration::LICENSE_TYPE_URL = "https://%1/getLicenseType";
const QString CRegistration::RESET_SESSION_URL = "https://%1/resetSession";
const int CRegistration::MAX_RUNS = 1; // maximal number of runs after last license check with server; if negative, the run counting mechanism is disabled
const QByteArray sigKeyStr("j|tG%4tHg^QHGCGP|6hVlS^H-Z-T#%GWAH&nGj?T8PIaWfYu4Bj%Myh5oH6Wbv4xxaS2|4B0NY2TJ6#gLyzKb12$zy3Kke67iX+UD6nQqy73Nd@2cSInJmLmY4oS!d1sHt_vp3M2UWhQzXywSo6u0v&MUL|$o+GaBO1OmyOz5VySLppxGKsA#2yJ$9fPvm@d!INfmNi09F6?%$Y|Xooo4tJvw6nWm9PJ@I=7cJB7A7ywjllO8W@KMY!#H1#|?140");
const QByteArray dataKeyStr("4=xN--D^eW7AxB-A1c6AtMMqB5o?y5+&SVhg%#8c=XZ&sDYJiM#?eFSqEK=mUi_Zz@LQ=p!WGj+_ypZ+%=W$nk^6PX%tePN&xPEqtKW&tQp|zFIZtLL1gK-eO=&AZZ?A$hpUgWfA5IrzDfV9jKyu9XZfIJf@#+cG&CI@eiikAh|w5OqJ3P*!ELUFtEEPc2AC42eu=zxtXnFS0|8Oby@qIO#%4f%h3L6NqB6h3SuEv1R=wyDdo&zGoa3s4Ztra=+|");
const QByteArray ivStr("G#A5rjuaLw9x-+#L+33JRPoYikt#mJ64-r9he23V&bAHhUIGQgT^E$2r|Z8$UXgops^W8-gKkw@&ewihqOa7XbMNnIZHK!U!E8Zh_mRKC%5i?XlX==V0X#Q@yq!slunT");

CRegistration::CRegistration(QString dataFileName)
{
    setAppDataFile(dataFileName);
    m_bundleId = "";
    m_expiryTimestamp = 0;
    m_lastRunTimestamp = 0;
    m_netUtils = NetworkUtils::getInstance();
    connect(m_netUtils, SIGNAL(networkRequestFinished(QNetworkReply*, QString)), this, SLOT(networkRequestFinished(QNetworkReply*, QString)));
    m_credentialsCheckInProgress = false;
    m_currentNetReply = NULL;
}

QVariant CRegistration::getJsonValue(QByteArray json, QString key)
{
    QJsonObject jsonObj = QJsonDocument::fromJson(json).object();
    QVariantMap responseMap = jsonObj.toVariantMap();
    return responseMap[key];
}

QString CRegistration::getWinWritableDir(){
    QString shellFoldersStr = "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders";
    QSettings shellFoldersReg(shellFoldersStr, QSettings::NativeFormat);
    QString appDataStr = "Common AppData";
    QString appDataPath = shellFoldersReg.value(appDataStr).toString();
    return appDataPath.replace("\\", "/");
}

int CRegistration::checkLicense()
{
    if (!QFile::exists(m_dataFile)) {
        return 30;
    }

    bool validValues = readStoredValues();
    if (!validValues) {
        return 30;
    }

    QString actualUUID = CDeviceUUID::getDeviceUUID();
    qint64 crtTimestamp = QDateTime::currentDateTimeUtc().toSecsSinceEpoch();
    int maxTimestampDiff = 3 * 60 * 60; // 3h

    if (m_expiryTimestamp > 0 && crtTimestamp > m_expiryTimestamp) {
        //qDebug() << "!!times:" << m_expiryTimestamp << crtTimestamp;
        return 5;
    }

    // consider time fluctuations only if the license is not unlimited (when unlimited m_expiryTimestamp == 0)
    if ((m_expiryTimestamp > 0) && (m_lastRunTimestamp - crtTimestamp > maxTimestampDiff)) {
        //qDebug() << "!~times:" << m_expiryTimestamp << crtTimestamp << m_lastRunTimestamp << ((int)m_lastRunTimestamp - (int)crtTimestamp > (int)maxTimestampDiff);
        return 5;
    }

    if (actualUUID != m_uuid) {
        return 30;
    }

    m_lastRunTimestamp = crtTimestamp;
    if (MAX_RUNS > 0) {
        m_crtRuns = m_crtRuns < 1 ? 0 : m_crtRuns - 1;
    }
    loginUserAsync();
    writeStoredValues();

    return MAX_RUNS > 0 && m_crtRuns < 1 ? 15 : 0;
}

void CRegistration::checkCredentials(QString login, QString password)
{
    m_credentialsCheckInProgress = true;
    m_login = login;
    m_password = password;
    m_uuid = CDeviceUUID::getDeviceUUID();
    loginUserAsync();
}

void CRegistration::cancelActiveRequest()
{
    if (m_currentNetReply && m_credentialsCheckInProgress && m_currentNetReply->isRunning()) {
        m_currentNetReply->abort();
    }
}

void CRegistration::setSoftwareBundle(QString bundleId)
{
    m_bundleId = bundleId;
}

void CRegistration::networkRequestFinished(QNetworkReply *reply, QString id)
{
    m_currentNetReply = NULL;
    if (reply->error() != QNetworkReply::NoError) {
        reply->deleteLater();
        if (m_credentialsCheckInProgress) {
            m_credentialsCheckInProgress = false;
            emit credentialsCheckFinished(-1);
        } else if (MAX_RUNS > 0 && m_crtRuns < 1) {
            emit licenseValidationResult(false);
        }
        return;
    }
    QByteArray responseData = reply->readAll();
    reply->deleteLater();

    if (id == "login")
    {
        int responseStatus = getJsonValue(responseData, "status").toInt();
        if (responseStatus == 0) { // auth successful
            checkSoftwareBundleAsync();
        } else {
            signalFinishedChecking(responseStatus, false);
        }
    }
    else if (id == "bundle")
    {
        int responseStatus = getJsonValue(responseData, "status").toInt();
        bool bundleValid = false;
        if (responseStatus == 2) {
            QString bundleId = getJsonValue(responseData, "bundle").toString();
            if (bundleId == m_bundleId) {
                checkLicenseTypeAsync();
                bundleValid = true;
            }
        }
        if (!bundleValid) {
            signalFinishedChecking(responseStatus, false);
        }
    }
    else if (id == "license_type")
    {
        int licenseType = getJsonValue(responseData, "type").toInt();
        if (licenseType == 1 || licenseType == 2) {
            checkAuthorizationAsync();
        } else {
            signalFinishedChecking(31, false);
        }
    }
    else if (id == "authorization")
    {
        int responseStatus = getJsonValue(responseData, "status").toInt();
        if (responseStatus == 7 || responseStatus == 9) {
            checkExpirationDateAsync();
        } else {
            signalFinishedChecking(responseStatus, false);
        }
    }
    else if (id == "expiry_date")
    {
        int responseStatus = getJsonValue(responseData, "status").toInt();
        if (responseStatus == 4) {
            QString serverDateStr = reply->rawHeader("Date");
            serverDateStr.chop(4); // remove Time Zone
            QDateTime serverDate = QDateTime::fromString(serverDateStr, "ddd, dd MMM yyyy HH:mm:ss");
            if (serverDate.isValid()) {
                m_lastRunTimestamp = serverDate.toSecsSinceEpoch();
            }
            m_expiryTimestamp = getJsonValue(responseData, "date").toUInt();
            m_crtRuns = MAX_RUNS;
            writeStoredValues();
            signalFinishedChecking(0, true);
        } else {
            signalFinishedChecking(responseStatus, false);
        }
    }
}

void CRegistration::setAppDataFile(QString dataFileName)
{
    m_dataFile = getWinWritableDir() + "/EyewearKiosk/" + dataFileName;
}

bool CRegistration::readStoredValues()
{
    QFile inFile(m_dataFile);
    if (!inFile.exists())
        return false;
    inFile.open((QIODevice::ReadOnly));
    QDataStream inDataStream(&inFile);
    QByteArray encData, encSignature;
    inDataStream >> encData >> encSignature;
    QByteArray storedSignature = endecrypt(encSignature, CRegistration::DECRYPT, sigKeyStr);
    QByteArray inData = endecrypt(encData, CRegistration::DECRYPT, dataKeyStr);

    QByteArray computedSignature = QCryptographicHash::hash(inData, QCryptographicHash::Sha256);
    //qDebug()<<">>>>>>REG:"<<(storedSignature != computedSignature);
    if (storedSignature != computedSignature) {
        return false;
    }

    QDataStream baDataStream(&inData, QIODevice::ReadOnly);
    baDataStream >> m_login >> m_password >> m_uuid >> m_crtRuns >> m_expiryTimestamp >> m_lastRunTimestamp;
    //qDebug() << "read values: " << m_login << m_password << m_uuid << m_crtRuns << m_expiryTimestamp << m_lastRunTimestamp;
    inFile.close();

    return true;
}

bool CRegistration::writeStoredValues()
{
    QFile outFile(m_dataFile);
    if (!outFile.open(QIODevice::WriteOnly)) {
        qDebug()<<"unable to open file: "<<m_dataFile;
        return false;
    }

    QDataStream outDataStream(&outFile);
    QByteArray outData;
    QDataStream baDataStream(&outData, QIODevice::WriteOnly);
    baDataStream << m_login << m_password << m_uuid << m_crtRuns << m_expiryTimestamp << m_lastRunTimestamp;
    //qDebug() << "written values: " << m_login << m_password << m_uuid << m_crtRuns << m_expiryTimestamp << m_lastRunTimestamp;

    // generate hash
    QByteArray signature = QCryptographicHash::hash(outData, QCryptographicHash::Sha256);
    QByteArray encSignature = endecrypt(signature, CRegistration::ENCRYPT, sigKeyStr);
    QByteArray encData = endecrypt(outData, CRegistration::ENCRYPT, dataKeyStr);
    //qDebug()<<">>>>>>"<<outData;
    //qDebug()<<">>>>>>"<<encData;
    outDataStream << encData << encSignature;

    outFile.close();
    return true;
}

// -1 -> network error
// 4 -> license is active
// 5 -> license has expired
int CRegistration::checkExpirationDateSync()
{
    QString expDateUrl = EXPIRATION_DATE_URL.arg(HOST_NAME);
    QNetworkReply *reply = m_netUtils->makeSyncRequest(QUrl(expDateUrl));
    if (reply->error() != QNetworkReply::NoError) {
        delete reply;
        return -1;
    }
    QByteArray response = reply->readAll();
    delete reply;
    int responseStatus = getJsonValue(response, "status").toInt();
    if (responseStatus == 4) {
        m_expiryTimestamp = getJsonValue(response, "date").toUInt();
    }
    return responseStatus;
}

// -1 -> network error
// 0 -> Access Granted
// 1 -> Access Denied
int CRegistration::loginUserSync(QString login, QString password)
{
    QString urlStr = ACCOUNT_URL.arg(HOST_NAME, login, password);
    QNetworkReply *reply = m_netUtils->makeSyncRequest(QUrl(urlStr));
    if (reply->error() != QNetworkReply::NoError) {
        delete reply;
        return -1;
    }

    QByteArray response = reply->readAll();
    delete reply;

    return getJsonValue(response, "status").toInt();
}

// -1 -> network error
// 7 -> Device Already Exists;
// 8 -> To Many Devices For License;
// 9 -> New Device Database Record Was Added
int CRegistration::checkAuthorizationSync(QString uuid)
{
    QString authCheckUrl = AUTHORIZATION_URL.arg(HOST_NAME, uuid);
    QNetworkReply *reply = m_netUtils->makeSyncRequest(QUrl(authCheckUrl));
    if (reply->error() != QNetworkReply::NoError) {
        delete reply;
        return -1;
    }
    QByteArray response = reply->readAll();
    delete reply;
    return getJsonValue(response, "status").toInt();
}

// -1 -> network error
// 2 -> software bundle ok
// 3 -> software bundle mismatch
int CRegistration::checkSoftwareBundleSync()
{
    QString softCheckUrl = SOFT_REQUEST_URL.arg(HOST_NAME);
    QNetworkReply *reply = m_netUtils->makeSyncRequest(QUrl(softCheckUrl));
    if (reply->error() != QNetworkReply::NoError) {
        delete reply;
        return -1;
    }
    QByteArray response = reply->readAll();
    delete reply;
    if (getJsonValue(response, "status").toInt() == 2) {
        QString bundleId = getJsonValue(response, "bundle").toString();
        if (bundleId == m_bundleId)
            return 2;
        else
            return 3;
    }
    return -1;
}

void CRegistration::loginUserAsync()
{
    QString loginUrl = ACCOUNT_URL.arg(HOST_NAME, m_login, m_password);
    m_currentNetReply = m_netUtils->makeAsyncRequest(QUrl(loginUrl), "login");
}

void CRegistration::checkExpirationDateAsync()
{
    QString dateCheckUrl = EXPIRATION_DATE_URL.arg(HOST_NAME);
    m_currentNetReply = m_netUtils->makeAsyncRequest(QUrl(dateCheckUrl), "expiry_date");
}

void CRegistration::checkAuthorizationAsync()
{
    QString authCheckUrl = AUTHORIZATION_URL.arg(HOST_NAME, m_uuid);
    m_currentNetReply = m_netUtils->makeAsyncRequest(QUrl(authCheckUrl), "authorization");
}

void CRegistration::checkSoftwareBundleAsync()
{
    QString bundleCheckUrl = SOFT_REQUEST_URL.arg(HOST_NAME);
    m_currentNetReply = m_netUtils->makeAsyncRequest(QUrl(bundleCheckUrl), "bundle");
}

void CRegistration::checkLicenseTypeAsync()
{
    QString licenseTypeCheckUrl = LICENSE_TYPE_URL.arg(HOST_NAME);
    m_currentNetReply = m_netUtils->makeAsyncRequest(QUrl(licenseTypeCheckUrl), "license_type");
}

void CRegistration::logoutUserAsync()
{
    QString resetSessionUrl = RESET_SESSION_URL.arg(HOST_NAME);
    m_currentNetReply = m_netUtils->makeAsyncRequest(QUrl(resetSessionUrl), "logout");
}

QByteArray CRegistration::endecrypt(const QByteArray data, EncryptDecrypt encDec, QByteArray keyStr)
{
    const size_t messageLen = data.size();

        QByteArray ed; ed.fill(0,messageLen);
        SecByteBlock key(reinterpret_cast<const unsigned char*>(keyStr.constData()),16);
        SecByteBlock iv(reinterpret_cast<const unsigned char*>(ivStr.constData()),16);
        if(encDec == CRegistration::ENCRYPT){
            CFB_Mode<AES>::Encryption cfbEncryption(key, key.size(), iv);
            cfbEncryption.ProcessData((unsigned char *)ed.data(),(unsigned char *)data.data(), messageLen);

        }else{
            CFB_Mode<AES>::Decryption cfbDecryption(key, key.size(), iv);
            cfbDecryption.ProcessData((unsigned char *)ed.data(),(unsigned char *)data.data(), messageLen);
        }
        return ed;
}

void CRegistration::signalFinishedChecking(int status, bool valid)
{
    logoutUserAsync();
    if (m_credentialsCheckInProgress) {
        m_credentialsCheckInProgress = false;
        emit credentialsCheckFinished(status);
    } else {
        if (!valid) {
            m_uuid = "Gs?tZtv&6tiisXR26_v|twEkKh=TQ!=X";
            writeStoredValues();
        }
        emit licenseValidationResult(valid);
    }
}
