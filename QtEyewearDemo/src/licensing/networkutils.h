#ifndef NETWORKUTILS_H
#define NETWORKUTILS_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class NetworkUtils : public QObject
{
    Q_OBJECT

public:
    static NetworkUtils* getInstance();
    QNetworkReply* makeSyncRequest(QUrl url);
    QNetworkReply* makeAsyncRequest(QUrl url, QString id);

signals:
    void networkRequestFinished(QNetworkReply *reply, QString id);

private slots:
    void onNetworkRequestFinished(QNetworkReply *reply);

private:
    static NetworkUtils *m_instance;
    QNetworkAccessManager m_networkManager;

    explicit NetworkUtils(QObject *parent = 0);
    ~NetworkUtils();
};

#endif // NETWORKUTILS_H
