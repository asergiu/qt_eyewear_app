#ifndef REGDIALOG_H
#define REGDIALOG_H

#include <QDialog>

namespace Ui {
class RegDialog;
}

class CRegistration;

class RegDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RegDialog(QWidget *parent = nullptr);
    ~RegDialog();
    void setRegistration(CRegistration* reg);

private slots:
    void on_btn_ok_clicked();
    void on_btn_cancel_clicked();

public slots:
    void licenseValidation(bool valid);
    void credentialsCheckFinished(int code);

signals:
    void licenseFinished(int code);


private:
    Ui::RegDialog *ui;
    CRegistration* m_reg;
    int m_licenseCode;
};

#endif // REGDIALOG_H
