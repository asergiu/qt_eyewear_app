#ifndef WINDOWS_HACKS_H
#define WINDOWS_HACKS_H

#include <Windows.h>
// Helper hackish function which brings the windowHandle passed as parameter
// in front of the other windows, since QWidget::raise() no longer works on Windows
void raiseWindow(HWND windowHandle);

#endif // WINDOWS_HACKS_H
