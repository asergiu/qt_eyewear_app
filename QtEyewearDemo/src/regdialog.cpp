#include "regdialog.h"
#include "ui_regdialog.h"
#include "licensing/cregistration.h"

#include <QApplication>

RegDialog::RegDialog(QWidget *parent) : QDialog(parent), ui(new Ui::RegDialog)
{
    this->setWindowFlags(Qt::Dialog | Qt::Tool | Qt::FramelessWindowHint);
    ui->setupUi(this);
    this->setStyleSheet("background-color:lightgray;");
    //this->ui->frame->setStyleSheet("background-color:gray;");
}

RegDialog::~RegDialog()
{
    delete ui;
}

void RegDialog::on_btn_ok_clicked()
{
    this->ui->label_status->setText(tr("Connecting .."));
    QString l = ui->line_login->text().trimmed();
    QString p = ui->line_password->text().trimmed();
    if (m_reg) m_reg->checkCredentials(l, p);
}

void RegDialog::on_btn_cancel_clicked()
{
    qApp->quit();
}

void RegDialog::setRegistration(CRegistration* reg){
    m_reg = reg;
    connect(m_reg, &CRegistration::licenseValidationResult, this, &RegDialog::licenseValidation);
    connect(m_reg, &CRegistration::credentialsCheckFinished, this, &RegDialog::credentialsCheckFinished);
}

void RegDialog::licenseValidation(bool valid){
    this->ui->label_status->setText(tr("Connected"));
    if (valid) hide();
    else this->ui->label_status->setText(tr("Invalid license"));
    emit licenseFinished(valid ? 0 : 128);
}

void RegDialog::credentialsCheckFinished(int code){
    this->ui->label_status->setText(tr("Connected"));
    if (code==0) hide();
    else this->ui->label_status->setText(tr("Invalid credentials [%1]").arg(code));
    emit licenseFinished(code);
}
