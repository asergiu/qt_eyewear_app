#include "mainwindow.h"

#include "capplication.h"
#include "settings.h"
#include "TryLive.h"

#include <QApplication>
#include <QCoreApplication>
#include <QFileInfo>
#include <QDir>
#include <QDebug>
#include <QDateTime>
#include <qstandardpaths.h>
#include <iostream>

#include "settings/ddebug.h"
#include <QThread>
#include <QTranslator>

#include "eyewear.h"

TryLive trylive;

static const QtMessageHandler QT_DEFAULT_MESSAGE_HANDLER = qInstallMessageHandler(0);

#define BUNDLE "com.acep.desktop.eyewearkiosk"
#define APP_NAME "Eyewear Kiosk"

void setLanguage(){
    Settings* m_st = Settings::instance();
    QString langCode = FL_V("language", "en_US");
    QTranslator* m_translator = new QTranslator();
    QLatin1String prefix = QLatin1String(":/translations/");
    QLocale language = QLocale(langCode);

    if (!m_translator->load(language.name(), prefix)){
        qWarning() << "translator " << prefix+language.name() << ".qm not loaded !";
        if (!m_translator->load(QLocale(), QLatin1String(""), QLatin1String(""), prefix)){
            qWarning() << QLocale().name() << "translator did not install! Trying to install en_US language...";
            if (!m_translator->load(QLocale("en_US"), QLatin1String(""), QLatin1String(""), prefix)) {
                qWarning() << "en_US translator did not install!";
            }
            else qDebug() << "Using en_US translator";
        }
        else qDebug() << "Using" << QLocale().name() << "translator";
    }
    else qDebug() << "Using" << /*prefix+*/language.name() << "translator";
    qApp->installTranslator(m_translator);
}

int main(int argc, char *argv[]){
    //qputenv("QT_DEBUG_PLUGINS", QByteArray("1"));
    qputenv("QT_IM_MODULE", QByteArray("freevirtualkeyboard"));
    qSetMessagePattern("[%{time yy/MM/dd hh:mm:ss.zzz} %{if-debug}D%{endif}%{if-info}I%{endif}%{if-warning}W%{endif}%{if-critical}C%{endif}%{if-fatal}F%{endif}] %{threadid}:%{file}:%{line}:%{function}: %{message}");
    qInstallMessageHandler(DDebug::customMessageHandler);
    CApplication app(argc, argv);
    Settings* m_st = Settings::instance();

    DDebug* dd = DDebug::instance();
    QString logPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation)+"/EyewearDemo";
    QDir d; d.mkpath(logPath);
    QString displayLogs = FL_V("logs/display","both");
    dd->setWriteLogsToFile(!displayLogs.compare("both", Qt::CaseInsensitive) || !displayLogs.compare("file", Qt::CaseInsensitive));
    dd->setWriteLogsToConsole(!displayLogs.compare("both", Qt::CaseInsensitive) || !displayLogs.compare("console", Qt::CaseInsensitive));
    dd->setMaxKeepDays(FL_I("logs/max_days_old",15));
    dd->setLogPath(logPath);
    setLanguage();

    QStringList args = app.arguments();
    qDebug() << "!!! SSL:" << QSslSocket::supportsSsl() << QSslSocket::sslLibraryBuildVersionString() << QSslSocket::sslLibraryVersionString();

    HRESULT hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);

    Eyewear e;
    e.init(args.contains("--pause-state", Qt::CaseInsensitive));

    app.connectQWindow(e.kioskMainWindow());
    app.checkForUpdate(APP_NAME, BUNDLE, FL_V("channel","stable"), FL_V("deviceid",""));

    //QTimer::singleShot(3000, &e, SLOT(showKiosk()));
    app.exec();
    return 0;
}
