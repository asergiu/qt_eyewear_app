#include "windows_hacks.h"


void raiseWindow(HWND windowHandle)
{
    if (!::IsWindow(windowHandle)) {
        return;
    }

//    // Simulate the press of ALT key
//    const BYTE pressedState = 0x80;
//    BYTE keyState[256] = {0};
//    if (::GetKeyboardState((LPBYTE)&keyState)) {
//        if (!(keyState[VK_MENU] & pressedState)) {
//            ::keybd_event(VK_MENU, 0, KEYEVENTF_EXTENDEDKEY | 0, 0);
//        }
//    }

    // Set the window we need to raise as foreground window
    ::SetForegroundWindow(windowHandle);

//    // Simulate the release of ALT key
//    if (::GetKeyboardState((LPBYTE)&keyState)) {
//        if (!(keyState[VK_MENU] & pressedState)) {
//            ::keybd_event(VK_MENU, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
//        }
//    }
}
