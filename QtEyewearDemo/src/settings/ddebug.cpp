#include "ddebug.h"

#include <QDebug>
#include <QLoggingCategory>
#include <QDirIterator>
#include <QDir>
#include <QStringList>
#include <QDate>
#include <QFile>

#include <string.h>
#include <iostream>
#include <fstream>
#include <QTime>
#include <QString>
DDebug* DDebug::m_instance = nullptr;

static const QtMessageHandler QT_DEFAULT_MESSAGE_HANDLER = qInstallMessageHandler(0); // default Qt message handler

DDebug* DDebug::instance() {
    if (m_instance == nullptr) m_instance = new DDebug();
    return m_instance;
}

DDebug::DDebug(){
    connect(&m_checkTimer,SIGNAL(timeout()),this,SLOT(checkLogsDates()));
    m_checkTimer.start(1000*60*60*3); // check old logs every 3 hours
}

void DDebug::mutexLock(){
#ifndef NDEBUG
    m_mtx.lock();
#endif
}

void DDebug::mutexUnlock(){
#ifndef NDEBUG
    m_mtx.unlock();
#endif
}

void DDebug::setLogPath(QString dir){
    m_log_path = dir;
    qDebug() << "===";
    qDebug() << "===~~~";
    qDebug() << "===~~~ START APP - setLogPath to" << m_log_path;
    checkLogsDates();
}

void DDebug::checkLogsDates(){
    qDebug() << "Checking for old logs ...";
    QDirIterator iterator(m_log_path,QStringList()<<"*.log.txt",QDir::Files);
    while(iterator.hasNext()){
        QFile log(iterator.next());
        QFileInfo logInfo(log.fileName());
        QString dateString = logInfo.fileName().split(".").at(0);
        QDate creationDate = QDate::fromString(dateString,"yyyyMMdd");
        QDate currentDate = QDate::currentDate();
        if(m_max_keep_days > 0 && currentDate.toJulianDay() - creationDate.toJulianDay() > m_max_keep_days){
            log.remove();
            qDebug() << "Deleting older log file:" << log.fileName();
        }
    }
}

void DDebug::setMaxKeepDays(int days){
    m_max_keep_days = days;
}

QString DDebug::getCurrentDate() {
    return QDate::currentDate().toString("yy-MM-dd");
}

QString DDebug::getCurrentTime() {
    return QTime::currentTime().toString("hh:mm:ss:zzz");
}

QString DDebug::getCurrentDateTime() {
    return QDateTime::currentDateTime().toString("yy-MM-dd hh:mm:ss:zzz");
}

void DDebug::saveCustomMessageToDisk(QString msg){
    if(m_writeLogsToFile){
        QString path = m_log_path + QDate::currentDate().toString("yyyyMMdd") + ".log.txt";
        if (path.length() > 0) {
            std::ofstream out;
            out.open(path.toStdString().c_str(), std::ios::out | std::ios::app);
            out << msg.toStdString().c_str() << std::endl;
            out.close();
        }
    }
}

void DDebug::customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg){
    DDebug::instance()->mutexLock();
    QString mm = qFormatLogMessage(type, context, msg);
    if (!mm.contains("Finished unloading unused resources in resource group TI/EFUSION") &&
            !mm.contains("VirtualKeyboardInputContext::setFocusObject")){ // ignoring certain libvlc/dxva2 lines
        (*QT_DEFAULT_MESSAGE_HANDLER)(type, context, msg);
        DDebug::instance()->saveCustomMessageToDisk(mm);
    }
    DDebug::instance()->mutexUnlock();
}
