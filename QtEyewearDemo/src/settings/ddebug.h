#ifndef DDEBUG_H
#define DDEBUG_H

#include <mutex>
#include <QTimer>
#include <QObject>


// sample use:
//   #define __TIMED__
//   #include "ddebug.h"
//   ... ...
//   TIMED_START
//   ... A ...
//   TIMED_TICK
//   ... B ...
//   TIMED_TICK
//   ... ...
//   TIMED_DISPLAY("Delta ticks display: ")
#ifdef __TIMED__
  #define TIMED_START std::vector<double> timed_delta;auto t0 = std::chrono::system_clock::now();
  #define TIMED_TICK { std::chrono::duration<double> d(std::chrono::system_clock::now() - t0); timed_delta.push_back( d.count() ); }
  #define TIMED_DISPLAY(MSG) std::cout<<MSG;for(int i=1; i<timed_delta.size(); i++) std::cout<<(timed_delta[i]-timed_delta[i-1])<<' ';std::cout<<std::endl;
#else
  #define TIMED_START
  #define TIMED_TICK
  #define TIMED_DISPLAY(MSG)
#endif


class DDebug : public QObject {
    Q_OBJECT
protected:
    DDebug();

public:
    static DDebug* instance();
    static void customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

    void saveCustomMessageToDisk(QString msg);
    void mutexLock();
    void mutexUnlock();

    QString getCurrentTime();
    QString getCurrentDate();
    QString getCurrentDateTime();

    void setLogPath(QString dir);
    void setWriteLogsToFile(bool writeLogsToFile){ m_writeLogsToFile = writeLogsToFile; }
    void setWriteLogsToConsole(bool writeLogsToConsole){ m_writeLogsToConsole = writeLogsToConsole; }
    void setMaxKeepDays(int days);

    bool writeLogsToConsole(){return m_writeLogsToConsole;}
public slots:
    void checkLogsDates();

private:
    static DDebug* m_instance;

    std::mutex m_mtx;
    bool m_writeLogsToFile;
    bool m_writeLogsToConsole;
    int m_max_keep_days;
    QString m_log_path;
    QTimer m_checkTimer;
};

#endif // DDEBUG_H
