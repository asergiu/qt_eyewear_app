#ifndef CConfigDialog_H
#define CConfigDialog_H

#include <QWidget>
#include <QSettings>
#include <QMap>

class QMainWindow;
class CConfigDialog : public QWidget
{
    Q_OBJECT

public:
    explicit CConfigDialog(QWidget *parent = nullptr);
    CConfigDialog(QString configPath);

    Q_INVOKABLE QMap<QString,QMap<QString,QString>> getSettingsMap(){return m_settingsMap;}
    Q_INVOKABLE QStringList getSettingsGroups(){return m_settingsMap.keys();}
    Q_INVOKABLE QStringList getSettingsKeys(QString group){return m_settingsMap.value(group).keys();}
    Q_INVOKABLE QString getSettingsValue(QString group, QString key){return m_settingsMap.value(group).value(key);}
    Q_INVOKABLE void setSettingsValue(QString group, QString key, QString value);
    Q_INVOKABLE void save();
    Q_INVOKABLE void cancel();

private:
    QSettings *m_settings;
    QMap<QString,QMap<QString,QString>> m_settingsMap;
    QWidget *m_widget;
};


#endif // CConfigDialog_H
