#include "cconfigdialog.h"

#include <QQuickView>
#include <QQmlContext>
#include <QtQuickWidgets/QQuickWidget>
#include <QVBoxLayout>
#include <QMainWindow>

CConfigDialog::CConfigDialog(QWidget *parent)
{

}

CConfigDialog::CConfigDialog(QString configPath)
{
    this->setGeometry(0,0,900,900);
    this->setStyleSheet("background-color:#ffffff;");
    m_settings = new QSettings(configPath, QSettings::IniFormat);

    QStringList keys = m_settings->allKeys();
    keys.sort();

    for(QString groupKey : keys){
        QString group = "general";
        QString key = groupKey;
        QString value = m_settings->value(groupKey).toString();
        if(groupKey.contains("/")){
            QStringList splitted = groupKey.split("/");
            group = splitted[0];
            key = splitted[1];
        }
        if(m_settingsMap.contains(group)){
            m_settingsMap[group].insert(key,value);
        }else{
            QMap<QString, QString> map;
            map.insert(key,value);
            m_settingsMap.insert(group,map);
        }
        qDebug()<<group<<key<<value;
    }

    QQuickWidget *quickWidget = new QQuickWidget();
    quickWidget->rootContext()->setContextProperty("configDialog", this);

    quickWidget->setResizeMode(QQuickWidget::SizeRootObjectToView);
    quickWidget->setSource(QUrl(QStringLiteral("qrc:/layout/qml/cconfig.qml")));
    QVBoxLayout *l = new QVBoxLayout;
    l->addWidget(quickWidget);
    setLayout(l);
}


void CConfigDialog::cancel()
{
    this->close();
}

void CConfigDialog::setSettingsValue(QString group, QString key, QString value)
{
    if(m_settingsMap.keys().contains(group)){
        if(m_settingsMap.value(group).keys().contains(key)){
            m_settingsMap[group][key]=value;
        }else{
            m_settingsMap[group].insert(key,value);
        }
    }else{
        QMap<QString, QString> map;
        map.insert(key,value);
        m_settingsMap.insert(group,map);
    }
}

void CConfigDialog::save()
{
    for(QString group:m_settingsMap.keys())
    {
        for(QString key:m_settingsMap.value(group).keys())
        {
            QString value = m_settingsMap.value(group).value(key);
            if(group=="general"){
                m_settings->setValue(key,value);
            }else{
                m_settings->setValue(group+"/"+key,value);
            }
        }
    }
}
