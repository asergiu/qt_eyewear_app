#ifndef CAPPLICATION_H
#define CAPPLICATION_H

#include <QApplication>
#include <QObject>

class QLocalSocket;
class QLocalServer;
class QWidget;
class QTimer;

class CApplication : public QApplication
{
    Q_OBJECT

public:
    CApplication(int &argc, char **argv);
    ~CApplication();
    void connectQWindow(QWidget *win);

public slots:
    void checkForUpdate(QString app_name="", QString app_bundle="", QString channel="", QString deviceid="");

private slots:
    void slotConnectionEstablished();

private:
  QLocalSocket* m_socketSingleApp;
  QLocalServer* m_serverSingleApp;
  QWidget*      m_windowSingleApp;
  QTimer*       m_updateTimer;
  QString       m_app_name, m_app_bundle, m_channel, m_deviceid;
};

#endif // CAPPLICATION_H
