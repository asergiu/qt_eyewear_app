#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QQmlListProperty>
#include <QJsonObject>
#include <QJsonDocument>
#include <QProcess>
#include <QAction>
#include <QSystemTrayIcon>
#include "src/model/product.h"
#include "src/model/filter.h"
#include "src/touchlesscontroller.h"

#define SMART_MIRROR_PATH "C:/Program Files (x86)/ACEP_Launcher/"

using namespace std;

namespace Ui {
class MainWindow;
}

class TryLiveController;
class PythonInference;
class SMIPC;
class QTimer;
class EmailSettings;
class QProcess;
class CRegistration;
class RegDialog;
class CConfigDialog;

class MainWindow : public QMainWindow
{
    Q_OBJECT

    enum AppState
    {
        INITIALIZING,
        RUNNING,
        PAUSED
    };

    /* *** Q PROPERTIES *** */
    Q_PROPERTY(QQmlListProperty<Product> catalog READ catalog  NOTIFY catalogChanged)
    Q_PROPERTY(Product* selectedAsset READ selectedAsset WRITE setSelectedAsset NOTIFY selectedAssetChanged)
    Q_PROPERTY(Filter* filter READ filter CONSTANT)
    Q_PROPERTY(QString infoMessage READ infoMessage WRITE setInfoMessage NOTIFY infoMessageChanged)


public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void init(const bool& startInPauseState = false);
    /* Get id of rendering window for trylive view */
    void* getRenderWinId();

    void initRegDialog();

    /* *** Q INVOKABLE *** */
    Q_INVOKABLE void skipCatalogClick();
    Q_INVOKABLE void captureClick();     /* startCaptureClick() -> QML onStartCaptureClicked() -> captureClick() */
    Q_INVOKABLE void startCaptureClick();
    Q_INVOKABLE void restartTrackingClick();
    Q_INVOKABLE void cancelCaptureClick();
    Q_INVOKABLE void shareClick();
    Q_INVOKABLE void sendEmail(const QString& destAddr);
    Q_INVOKABLE int getVisibleCatalogLength();
    Q_INVOKABLE QString currencySymbol(){return m_currencySymbol;}
    Q_INVOKABLE void generateQR();
    Q_INVOKABLE int autoScrollInterval();
    Q_INVOKABLE bool resetTrackingByAutoScroll();
    Q_INVOKABLE bool isCarouselVisible();
    Q_INVOKABLE bool isSmartMirrorAvailable();
    Q_INVOKABLE bool isTLTracking();

    /* *** Q PROPERTIES setters & getters *** */
    QQmlListProperty<Product> catalog();
    Product* selectedAsset();
    void setSelectedAsset(Product* p);
    Filter* filter();
    QString infoMessage();
    void setInfoMessage(QString im);
    void setCurrencySymbol(QString symbol){m_currencySymbol = symbol;}

    /* *** EVENTS *** */
    void resizeEvent(QResizeEvent * event);
    void closeEvent(QCloseEvent* event);
    bool eventFilter(QObject *obj, QEvent *event);

    QStringList getProcessesList();

    void createActions();
    void createTrayIcon();

public slots:
    void onRegFinished(int code);
    void onTryLiveRendering();

    void resumeApplication();
    void pauseApplication();
    void onBrandNameReceived(QString brand);
    void hideWindow();//called after the other smipc application resumed completly (window on top & visible on the screen)
    void onTrackingStatusChanged(const bool& tracking);
    void onTryLivePaused();
    void onScreenSaverTimerTriggered();
    void onShareTimerTriggered();
    void onEmailTimerTriggered();
    void resetEmailTimer();
    void onCaptureTaken(const QString& imagePath, QString origPath);
    void fitBetter();
    bool isFitBetterEnabled();
    ///inference
    /*!
     * \brief onCaptureTakenForInference: image taken without assets within TryLive (trylive.takePhoto)
     * \param imagePath
     */
    void onCaptureTakenForInference(const QString& imagePath);
    void onAttributeDetectorFinished(QJsonDocument json);
    void onHideCursorTriggered();

    void startSimulations();
    void onSimulationsFinished(int exitCode, QProcess::ExitStatus exitStatus);

    void showLogs();
    void showSettings();
    void showTrayMessage(QString msg, QString title = "");

signals:
    void catalogChanged();
    void catalogProgressChanged(int progress, int total);
    void captureClicked();
    void startCaptureClicked();
    void restartTrackingClicked();
    void cancelCaptureClicked();
    void shareClicked();
    void emailSent(const QString& destAddr, bool success, QString errMsg);
    void selectedAssetChanged(Product* p);
    void skipCatalogClicked();
    void captureTaken(const QString& imagePath);
    void infoMessageChanged();
    void assetChanged();
    void currencySymbolChanged();
    void qrImageReady(const QString& qrPath);
    void hideTouchlessQr();
    void showTouchlessQr();
    void trackingTooFar();
    void trackingTooClose();
    void trackingChanged(bool tracked);

private:
    Ui::MainWindow *m_ui;
    AppState appState;
    TryLiveController* m_tlController;
    bool m_tlCatalogLoaded;
    PythonInference *m_attrDetector;
    SMIPC* m_smipc;
    QTimer* m_screenSaverTimer;
    QTimer* m_webSocketInactivityTimer;
    QTimer* m_shareTimer;
    QTimer* m_emailTimer;
    QTimer* m_hideCursorTimer;
    Filter* m_filter;
    EmailSettings* m_emailSettings;
    QString m_infoMessage;
    QString m_currencySymbol;
    CRegistration *m_registration;
    TouchlessController *m_touchlessController;

    QWidget* m_leftWidget;
    QWidget* m_rightWidget;
    QWidget* m_botLeftWidget;
    QWidget* m_botRightWidget;
    QWidget* m_botWidget;
    QWidget* m_captureWidget;
    QWidget* m_countdownWidget;
    QWidget* m_fitMeBetter;

    RegDialog* m_regDialog;

    QProcess m_pr;
    int m_licenseCode;

    const QString createEmailImage(const QString& imagePath);

    void forceResize();
    bool m_isSimulationRunning;

    QAction* m_minimizeAction;
    QAction* m_restoreAction;
    QAction* m_logsAction;
    QAction* m_settingsAction;
    QAction* m_quitAction;

    QSystemTrayIcon* m_trayIcon;
    QMenu* m_trayIconMenu;
    CConfigDialog* m_configDialog;
    QString m_captureAssetReference;
};

#endif // MAINWINDOW_H
