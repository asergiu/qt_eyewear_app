SEP="\\"

PROJ_DIR = $$PWD
PROJ_DIR = $$replace(PROJ_DIR, "/", $${SEP})
CP = "XCOPY /E /Y "
CPSINGLE = "XCOPY /Y "

#COPY MODEL
DEST_DIR = $$DESTDIR
DEST_DIR = $$replace(DEST_DIR, "/", $${SEP})
CMD_CPY_SMIPC = "$${CP} $$SMIPC_HOME$${SEP}"vc"$$MSVC_VER$${SEP}$$TARCH$${SEP}"smipc"$$QT_VERSION".dll" $$DEST_DIR$${SEP}"

message($$CMD_CPY_SMIPC)

copySMIPC.commands = $$CMD_CPY_SMIPC
copySMIPC-check.commands = $$CMD_CPY_SMIPC
copySMIPC-make_first.commands = $$CMD_CPY_SMIPC
copySMIPC-make_default.commands = $$CMD_CPY_SMIPC
copySMIPC-make_all.commands = $$CMD_CPY_SMIPC
copySMIPC-qmake_all.commands = $$CMD_CPY_SMIPC

QMAKE_EXTRA_TARGETS *= copySMIPC copySMIPC-check copySMIPC-make_default copySMIPC-make_first copySMIPC-make_all copySMIPC-qmake_all copySMIPC-clean copySMIPC-distclean
PRE_TARGETDEPS *= copySMIPC
