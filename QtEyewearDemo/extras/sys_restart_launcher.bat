@ECHO OFF

rem The script is called at system reboot

timeout /t 20

cd %~dp0

start "EyewearKiosk" "EyewearKiosk.exe" --pause-state