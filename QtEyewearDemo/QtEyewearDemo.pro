TEMPLATE = app

Release: DESTDIR = $$PWD/release
Debug: DESTDIR = $$PWD/debug

#Uncomment the line below to create installation kit / update kit ( + trylive included ) / lightUpdate ( only exe and .new files )
#CONFIG += createKit
#CONFIG += createUpdate
#CONFIG += createLightUpdate
CONFIG += console
CONFIG += x86_64
CONFIG += C++11

TARGET=EyewearKiosk
OBJECTS_DIR = gen
MOC_DIR = gen
RCC_DIR = gen

QT += core gui network websockets widgets qml quick xml quickwidgets
DEFINES += QT_MESSAGELOGCONTEXT

RC_ICONS += AppIcon.ico

RESOURCES += qml.qrc

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

#COMPILER-version
MSVC_VER = $$(VisualStudioVersion)
MSVC_VER ~= s/\.[0-9]//

#ARCHITECTURE
TARCH="x86"
contains(QMAKE_TARGET.arch, x86_64) {
    TARCH="x64"
}

#EyeWear LIB
EYEWEARLIB_HOME = "D:/work/qt_eyewear_lib"
INCLUDEPATH += "$$EYEWEARLIB_HOME"
CONFIG(release, debug|release): LIBS += -L$$EYEWEARLIB_HOME/"release" -lqt_eyewear_lib
CONFIG(debug, debug|release): LIBS += -L$$EYEWEARLIB_HOME/"debug" -lqt_eyewear_lib

#*** TRYLIVE
TRYLIVEPATH = $$PWD/lib/TryLive
INCLUDEPATH += $$TRYLIVEPATH/SDK/include
LIBS += -L$$TRYLIVEPATH/SDK/lib/Release -lTryLiveSDK
LIBS += -luser32

#QRCODE - Windows (OURS)
QRCODE_HOME = "D:\\soft\\libqr"
INCLUDEPATH += $$QRCODE_HOME/cpp
CONFIG(release, debug|release): LIBS += -L$$QRCODE_HOME/"vc"$$MSVC_VER/$$TARCH/ -lqr$$QT_VERSION
CONFIG(debug, debug|release): LIBS += -L$$QRCODE_HOME/"vc"$$MSVC_VER/$$TARCH/ -lqrd$$QT_VERSION

#AWS Amazon - Windows (OURS)
AWS_HOME = "D:\\soft\\aws"
INCLUDEPATH += $$AWS_HOME/include
CONFIG(release, debug|release): LIBS += -L$$AWS_HOME/lib/"vc"$$MSVC_VER/$$TARCH/release -laws-cpp-sdk-core -laws-cpp-sdk-s3
CONFIG(debug, debug|release): LIBS += -L$$AWS_HOME/lib/"vc"$$MSVC_VER/$$TARCH/debug -laws-cpp-sdk-core -laws-cpp-sdk-s3

#QCA
CRYPTOPP_HOME = "D:/soft/cryptlib"
INCLUDEPATH += "$$CRYPTOPP_HOME/include"
CONFIG(release, debug|release): LIBS += -L$$CRYPTOPP_HOME/"vc"$$MSVC_VER/$$TARCH/"Release" -lcryptopp
CONFIG(debug, debug|release): LIBS += -L$$CRYPTOPP_HOME/"vc"$$MSVC_VER/$$TARCH/"Debug" -lcryptopp

#SMIPC - Windows (OURS)
SMIPC_HOME = "D:\\soft\\smipc"
INCLUDEPATH += $$SMIPC_HOME
CONFIG(release, debug|release): LIBS += -L$$SMIPC_HOME/"vc"$$MSVC_VER/$$TARCH/ -lsmipc$$QT_VERSION
CONFIG(debug, debug|release): LIBS += -L$$SMIPC_HOME/"vc"$$MSVC_VER/$$TARCH/ -lsmipcd$$QT_VERSION

ISW_DIR="D:/IntelSWTools/openvino/"
ISW_LIBS_D=$$ISW_DIR"inference_engine/samples/"
ISW_OPENCV=$$ISW_DIR"opencv/lib/"
ISW_OPENCV_BIN=$$ISW_DIR"opencv/bin/"
ISW_INFERENCE=$$ISW_DIR"deployment_tools/inference_engine/"
OPENCV_VERSION=412

CONFIG(release, debug|release): ISW_LIBS_DIR=$$ISW_LIBS_D"intel64/Release/"
else: ISW_LIBS_DIR=$$ISW_LIBS_D"intel64/Debug/"

CONFIG(release, debug|release): ISW_TOOLS_LIBS_DIR=$$ISW_LIBS_D"deployment_tools/inference_engine/lib/intel64/Release/"
else: ISW_TOOLS_LIBS_DIR=$$ISW_LIBS_D"deployment_tools/inference_engine/lib/intel64/Debug/"

INCLUDEPATH += $$ISW_INFERENCE"samples/common" \
               $$ISW_INFERENCE"samples/common/format_reader" \
               $$ISW_INFERENCE"external/tbb/include" \
               $$ISW_INFERENCE"src/extension" \
               $$ISW_INFERENCE"include" \
               $$ISW_DIR"opencv/include"

LIBS += -lole32 -loleaut32 -lshlwapi -L$$ISW_LIBS_DIR -lcpu_extension -L$$ISW_TOOLS_LIBS_DIR -L$$ISW_OPENCV
CONFIG(release, debug|release): LIBS += -L$$ISW_INFERENCE"lib/intel64/Release" -linference_engine
else: LIBS += -L$$ISW_INFERENCE"lib/intel64/Debug" -linference_engined
CONFIG(release, debug|release): LIBS += -lopencv_world$$OPENCV_VERSION -ltbb
else: LIBS += -lopencv_world$${OPENCV_VERSION}d -ltbb_debug

# openVInference
INCLUDEPATH += $$PWD/../../openVInference/src/
LIBS += -L$$PWD/../../openVInference/deploy/lib -lopenVInference

SOURCES += \
    src/main.cpp \
    src/capplication.cpp \
    src/settings/ddebug.cpp \
    src/licensing/cdeviceuuid.cpp \
    src/licensing/cregistration.cpp \
    src/licensing/cwmisysteminfo.cpp \
    src/licensing/networkutils.cpp \
    src/capplication.cpp \
    src/main.cpp \
    src/windows_hacks.cpp \
    src/licensing/cqmlstringwrapper.cpp \
    src/regdialog.cpp \
    src/settings/cconfigdialog.cpp

win32 {
SOURCES += \
    src/windows_hacks.cpp
}

HEADERS += \
    src/windows_hacks.h \
    src/capplication.h \
    src/settings/ddebug.h \
    src/licensing/cdeviceuuid.h \
    src/licensing/cregistration.h \
    src/licensing/cwmisysteminfo.h \
    src/licensing/networkutils.h \
    src/licensing/cqmlstringwrapper.h \
    src/regdialog.h \
    src/settings/cconfigdialog.h

FORMS += \
    src/mainwindow.ui \
    src/regdialog.ui

SEP="\\"

PROJ_DIR = $$PWD
PROJ_DIR = $$replace(PROJ_DIR, "/", $${SEP})

CP = "XCOPY /E /Y "
CPSINGLE = "XCOPY /Y "

#********************** TryLive libs: copy the content of TryLive/SDK/bin inside deploy folder
# !! Attention !! TryLive/SDK/bin is necessary also for development runtime, this is the reason the copy of it is NOT done in .iss setup file
CONFIG(release, debug|release): CMD_CPY = "$${CP} $$PROJ_DIR$${SEP}lib$${SEP}TryLive$${SEP}SDK$${SEP}bin$${SEP}Release$${SEP}* $$PROJ_DIR$${SEP}release$${SEP} "
CONFIG(debug, debug|release): CMD_CPY = "$${CP} $$PROJ_DIR$${SEP}lib$${SEP}TryLive$${SEP}SDK$${SEP}bin$${SEP}Debug$${SEP}* $$PROJ_DIR$${SEP}debug$${SEP} "

copyTL.commands = $$CMD_CPY
copyTL-check.commands = $$CMD_CPY
copyTL-make_first.commands = $$CMD_CPY
copyTL-make_default.commands = $$CMD_CPY
copyTL-make_all.commands = $$CMD_CPY
copyTL-qmake_all.commands = $$CMD_CPY

QMAKE_EXTRA_TARGETS *= copyTL copyTL-check copyTL-make_default copyTL-make_first copyTL-make_all copyTL-qmake_all copyTL-clean copyTL-distclean
PRE_TARGETDEPS *= copyTL

#********************** Config: copy the content of Config/ inside deploy folder
# !! Attention !! Config is necessary also for development runtime, this is the reason the copy of it is NOT done in .iss setup file
CONFIG(release, debug|release): CFG_CPY = "$${CP} $$PROJ_DIR$${SEP}Config$${SEP}* $$PROJ_DIR$${SEP}release$${SEP} "
CONFIG(debug, debug|release): CFG_CPY = "$${CP} $$PROJ_DIR$${SEP}Config$${SEP}* $$PROJ_DIR$${SEP}debug$${SEP} "

copyCfg.commands = $$CFG_CPY
copyCfg-check.commands = $$CFG_CPY
copyCfg-make_first.commands = $$CFG_CPY
copyCfg-make_default.commands = $$CFG_CPY
copyCfg-make_all.commands = $$CFG_CPY
copyCfg-qmake_all.commands = $$CFG_CPY

QMAKE_EXTRA_TARGETS *= copyCfg copyCfg-check copyCfg-make_default copyCfg-make_first copyCfg-make_all copyCfg-qmake_all copyCfg-clean copyCfg-distclean
PRE_TARGETDEPS *= copyCfg

#********************** Config: copy the content of FreeVirtualKeyboard/ inside deploy folder
# !! Attention !! FreeVirtualKeyboard is necessary also for development runtime, this is the reason the copy of it is NOT done in .iss setup file
CONFIG(release, debug|release): FVK_CPY = "$${CP} $$PROJ_DIR$${SEP}lib$${SEP}FreeVirtualKeyboard$${SEP}* $$PROJ_DIR$${SEP}release$${SEP} "
CONFIG(debug, debug|release): FVK_CPY = "$${CP} $$PROJ_DIR$${SEP}lib$${SEP}FreeVirtualKeyboard$${SEP}* $$PROJ_DIR$${SEP}debug$${SEP} "

copyFVK.commands = $$FVK_CPY
copyFVK-check.commands = $$FVK_CPY
copyFVK-make_first.commands = $$FVK_CPY
copyFVK-make_default.commands = $$FVK_CPY
copyFVK-make_all.commands = $$FVK_CPY
copyFVK-qmake_all.commands = $$FVK_CPY

QMAKE_EXTRA_TARGETS *= copyFVK copyFVK-check copyFVK-make_default copyFVK-make_first copyFVK-make_all copyFVK-qmake_all copyFVK-clean copyFVK-distclean
PRE_TARGETDEPS *= copyFVK

#********************** GIT VERSION
# If there is no version tag in git this one will be used
VERSION = 0.0.1

# Need to discard STDERR so get path to NULL device
win32 {
    NULL_DEVICE = NUL # Windows doesn't have /dev/null but has NUL
} else {
    NULL_DEVICE = /dev/null
}

# Need to call git with manually specified paths to repository
BASE_GIT_COMMAND = git
#BASE_GIT_COMMAND = git --git-dir $$PWD/.git --work-tree $$PWD

# Trying to get version from git tag / revision
GIT_VERSION = $$system($$BASE_GIT_COMMAND describe --always --tags 2> $$NULL_DEVICE)
GIT_COMMIT_COUNT = $$system($$BASE_GIT_COMMAND rev-list HEAD --count 2> $$NULL_DEVICE)
GIT_BRANCH = $$system($$BASE_GIT_COMMAND rev-parse --abbrev-ref HEAD 2> $$NULL_DEVICE)
win32 {
    BDATE = $$system("date /T")
    BTIME = $$system("time /T")
    BUILD_DATE = $$BDATE-$$BTIME
} else {
    BUILD_DATE = $$system("date -u +%Y-%m-%d-%H-%M")
}
BUILD_DATE ~= s/:/"-"
CLEAN_VERSION = $$GIT_VERSION
CLEAN_VERSION ~= s/-/"."
CLEAN_VERSION ~= s/g/""
CLEAN_VERSION ~= s/\.\d+\.[a-f0-9]{6,}//
FILE_VERSION = $$GIT_VERSION
FILE_VERSION ~= s/-/"."
FILE_VERSION ~= s/\.g.+//
FILE_VERSION ~= s/\./,/
FILE_VERSION ~= s/v//

VERSION = $$GIT_VERSION
VERSION ~= s/\-g[a-f0-9]{6,}//
VERSION ~= s/v//
VERSION ~= s/\-/./

#********************** BUILD VERSION
win32 {
    BUILD_FILE = buildversion.h
    CMD_RM = del
}

L1 = $$LITERAL_HASH'define BUILD_DATE "'$$BUILD_DATE'"'
L2 = $$LITERAL_HASH'define BUILD_VERSION "'$$GIT_VERSION'"'
L3 = $$LITERAL_HASH'define BUILD_VERSION_CLEAN "'$$CLEAN_VERSION'"'
L4 = $$LITERAL_HASH'define BUILD_REVISION_COUNT "'$$GIT_COMMIT_COUNT'"'
L5 = $$LITERAL_HASH'define BUILD_BRANCH "'$$GIT_BRANCH'"'
L6 = $$LITERAL_HASH'define STR_VERSION "'$$GIT_VERSION'\\0"'
L7 = $$LITERAL_HASH'define FILE_VERSION '$$FILE_VERSION
L8 = $$LITERAL_HASH'define STR_FILE_VERSION "'$$FILE_VERSION'\\0"'
win32: BUILD_CMD = echo $${L1} > $${BUILD_FILE} && echo $${L2} >> $${BUILD_FILE} && echo $${L3} >> $${BUILD_FILE} && echo $${L4} >> $${BUILD_FILE} && echo $${L5} >> $${BUILD_FILE} && echo $${L6} >> $${BUILD_FILE} && echo $${L7} >> $${BUILD_FILE} && echo $${L8} >> $${BUILD_FILE}
unix: BUILD_CMD = echo \'$${L1}\' > $${BUILD_FILE} && echo \'$${L2}\' >> $${BUILD_FILE} && echo \'$${L3}\' >> $${BUILD_FILE} && echo \'$${L4}\' >> $${BUILD_FILE} && echo \'$${L5}\' >> $${BUILD_FILE} && echo \'$${L6}\' >> $${BUILD_FILE} && echo \'$${L7}\' >> $${BUILD_FILE} && echo \'$${L8}\' >> $${BUILD_FILE}

    buildversion.commands = $$BUILD_CMD
    buildversion-check.commands = $$BUILD_CMD
    buildversion-make_default.commands = $$BUILD_CMD
    buildversion-make_all.commands = $$BUILD_CMD
    buildversion-make_first.commands = $$BUILD_CMD

    buildversion-clean.commands = $${CMD_RM} $${BUILD_FILE}
    buildversion-distclean.commands = $${CMD_RM} $${BUILD_FILE}

QMAKE_EXTRA_TARGETS += buildversion buildversion-check buildversion-make_default buildversion-make_first buildversion-make_all buildversion-clean buildversion-distclean
PRE_TARGETDEPS += buildversion

#********************** CREATE KIT
win32 {
    CONFIG(release, debug|release){
        createKit{#defined in CONFIG variable (check .pro file)
            include($$PWD/CpySMIPC.pri)
            QMAKE_POST_LINK += $$PWD\\deployment_scripts\\createKit.bat $$TARGET
        }
        createUpdate {
            include($$PWD/CpySMIPC.pri)
            QMAKE_POST_LINK += $$PWD\\deployment_scripts\\createUpdateKit.bat $$TARGET
        }
        createLightUpdate {
            include($$PWD/CpySMIPC.pri)
            QMAKE_POST_LINK += $$PWD\\deployment_scripts\\createLightUpdate.bat $$TARGET
        }
    }
}

TRANSLATIONS = translations/en_US.ts \
    translations/fr_FR.ts \
    translations/es_ES.ts

#********************** TRANSLATIONS lupdate
EXTRA_QML = $$files(qml/*.qml, true)
#LIST_THEMES = $$files(themes/*, false)
EXTRA_XML = $$files(Config/*.xml, true)
lupdate_only{
    SOURCES += $$EXTRA_QML $$EXTRA_XML
}
